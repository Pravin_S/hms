﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Collections.Generic;
using Kendo.Mvc.UI;
using System.Data;
using System;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;
using DocumentFormat.OpenXml.EMMA;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using System.Collections;

namespace HMSMemri.Controllers
{
    public class CustomerController : IMCPBase
    {
        /// <summary>
        /// Summary for delared the Global Variables
        /// </summary>
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        protected DataRow mr = null;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;

        [Obsolete]
        public CustomerController(IOptions<BaseSettings.ServerVar> serverVar, IHostingEnvironment hostingEnvironment) : base(serverVar)
        {
            _serverVar = serverVar;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Summary for Index page(UI),check logged user
        /// </summary>
        /// <returns></returns>

        #region Index UI
        public IActionResult Index()
        {
            LoadState loadState = new LoadState();
            DataSet ds = new DataSet();
            GetCustomerResponse response = new GetCustomerResponse();
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }

            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.customer_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.customer_show_closed;
            return View();
        }
        #endregion

        #region Grid data load
        [HttpPost]
        public JsonResult LoadCustomerData(bool isactive, bool toggleActive)
        {
            LoadState loadState = new LoadState();
            GetCustomerResponse response = new GetCustomerResponse();
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();

            try
            {
                // Get Save State in Customer Grid based the user
                mr = TableDefs.User.GetUserGrid(ds, userID.CurrentUser.UserID, 10);
                if (mr != null)
                {
                    if (mr.Table.Rows.Count > 0)
                    {
                        loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                        loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                        loadState.json_col_ord = mr["json_col_ord"].ToString();
                        response.loadState = loadState;
                        dsResult.Errors = "true";

                    }
                }

                // get the value for sp, API Call
                DataView dvCustomer = TableDefs.Customer.GetCustomers(ds, isactive);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listCustomer = JsonConvert.DeserializeObject<lst_tbl_cust>(jsonData);


                DataView dvFacility = TableDefs.Facility.Getfacilities(ds2, true);
                string jsonData2 = JsonConvert.SerializeObject(ds2, Formatting.Indented);
                var listFacility = JsonConvert.DeserializeObject<lst_tbl_facility>(jsonData2);

                foreach (var cus in listCustomer.tbl_cust)
                {
                    cus.lst_tbl_facility = new List<tbl_facility>();
                    cus.lst_tbl_facility = listFacility.tbl_facility.Where(c => c.cust_id == cus.cust_id).OrderBy(x => x.facility_name).ToList();
                }

                dsResult.Data = listCustomer.tbl_cust.ToList().OrderBy(x => x.cust_name);
                dsResult.Total = listCustomer.tbl_cust.Count;
            }
            catch (Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "Grid Load/Custometer", error.errormsg);
                HttpContext.Session.SetString("error_log", error.errormsg);
                result.success = false;
                result.message = $"An error occurred retrieving Customer List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }

        #endregion

        #region Displaying the edited customer records

        public ActionResult EditCustomer(string Id)
        {
            //Declaring the variables
            GetCustomerResponse response = new GetCustomerResponse();
            Customer customer = new Customer();
            List<CustomerStatus> customerStatus = new List<CustomerStatus>();
            List<ServiceType> serviceType = new List<ServiceType>();
            List<States> States = new List<States>();
            response.Statuscode = 0;

            try
            {
                string IDcheck = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            }
            catch (Exception)
            {

                return View("~/Views/Shared/Error_EditUrl.cshtml");
            }
            string ID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            GlobalData Global = new GlobalData();
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }
            try
            {
                DataSet ds = new DataSet();
                DataSet ds2 = new DataSet();
                //Customer grid API call
                mr = TableDefs.Customer.GetCustomer(ds, Utility.ToInt(ID.ToString()));
                if (mr != null)
                {
                    Global.cust_id = Utility.ToInt(mr["cust_id"]);
                    GlobalData.SetGlobalSessione(HttpContext.Session, "CustomerSession", Global);
                    GlobalData cust_id = GlobalData.GetGlobalSessione(HttpContext.Session, "CustomerSession");

                    customer.CustomerId = Utility.ToInt(mr["cust_id"]);
                    customer.CustomerName = mr["cust_name"].ToString();
                    customer.Address1 = mr["address1"].ToString();
                    customer.Address2 = mr["address2"].ToString();
                    customer.City = mr["city"].ToString();
                    customer.State = mr["st"].ToString();
                    customer.ZipCode = mr["zip_code"].ToString();
                    customer.Country = mr["country"].ToString();
                    customer.PhoneNumber = Utility.FormatPhone(mr["phone"].ToString(), true);
                    customer.Fax = Utility.FormatPhone(mr["fax"].ToString(), true);
                    customer.VisitAllocation = Utility.ToInt(mr["visits_alloc"]);
                    customer.DefaultLabor = Utility.ToFloat(mr["labor_rate"]);
                    customer.CustomerStatus = Utility.ToInt(mr["cust_stat_id"]);
                    customer.Notes = mr["notes"].ToString();
                    customer.FacilityCount = Utility.ToInt(mr["facility_cnt"]);
                    customer.ContactCount = Utility.ToInt(mr["contact_cnt"]);
                    customer.TicketCount = Utility.ToInt(mr["ticket_cnt"]);
                    customer.IsActive = Convert.ToBoolean(mr["is_active"]);
                    DataView servicetype = TableDefs.Customer.GetCustomerService(ds2, customer.CustomerId);
                    if (servicetype != null)
                    {
                        customer.cust_service_type = new List<tbl_cust_service>();

                        DataTable dtservicetype = servicetype.ToTable();
                        for (int i = 0; i < dtservicetype.Rows.Count; i++)
                        {
                            tbl_cust_service cust_service = new tbl_cust_service();
                            cust_service.service_tp_id = Utility.ToInt(dtservicetype.Rows[i]["service_tp_id"]);
                            customer.cust_service_type.Add(cust_service);
                        }
                        customer.service_typeIds = customer.cust_service_type.Select(x => x.service_tp_id).ToArray();
                    }                    
                }

                //Customer Status API call
                DataView dvCustomerStatus = TableDefs.Customer.GetCustomerStatusForDropDown(ds, true, "* Select One *");
                if (dvCustomerStatus != null)
                {
                    DataTable dtCustomerStatus = dvCustomerStatus.ToTable();
                    for (int i = 0; i < dtCustomerStatus.Rows.Count; i++)
                    {
                        CustomerStatus status = new CustomerStatus();
                        status.cust_stat_id = Utility.ToInt(dtCustomerStatus.Rows[i]["cust_stat_id"]);
                        status.cust_stat_desc = dtCustomerStatus.Rows[i]["cust_stat_desc"].ToString();
                        customerStatus.Add(status);
                    }
                }

                //State API call
                DataView dvStates = TableDefs.General.GetStatesForDropDown(ds, true, "* Select One *");
                if (dvStates != null)
                {
                    DataTable dtStates = dvStates.ToTable();
                    for (int i = 0; i < dtStates.Rows.Count; i++)
                    {
                        States state = new States();
                        state.abbr = dtStates.Rows[i]["abbr"].ToString();
                        state.st = dtStates.Rows[i]["st"].ToString();
                        States.Add(state);
                    }
                }
                //show selected list item in Customer Status
                List<SelectListItem> customerselectionlist = new List<SelectListItem>();
                foreach (var item in customerStatus)
                {
                    customerselectionlist.Add(new SelectListItem
                    {
                        Text = item.cust_stat_desc.ToString(),
                        Value = item.cust_stat_id.ToString()
                    });
                }
                response.CustomStatusModule = customerselectionlist;

                //show selected list item in State
                List<SelectListItem> stateselectionlist = new List<SelectListItem>();
                foreach (var stateItem in States)
                {
                    stateselectionlist.Add(new SelectListItem
                    {
                        Text = stateItem.st.ToString(),
                        Value = stateItem.abbr.ToString()
                    });
                }
                response.StateModule = stateselectionlist;

                #region Service Type
                DataView dvServiceTypes = TableDefs.General.GetServiceTypesForDropDown(ds, true,false,"All");
                if (dvServiceTypes != null)
                {
                    DataTable dtServiceTypes = dvServiceTypes.ToTable();
                    for (int i = 0; i < dtServiceTypes.Rows.Count; i++)
                    {
                        ServiceType service = new ServiceType();
                        service.service_tp_id = Utility.ToInt(dtServiceTypes.Rows[i]["service_tp_id"]);
                        service.service_tp_desc = dtServiceTypes.Rows[i]["service_tp_desc"].ToString();
                        serviceType.Add(service);
                    }                 
                }
                //show selected list item in Customer Status
                List<SelectListItem> serviceTypelist = new List<SelectListItem>();
                foreach (var item in serviceType)
                {
                    serviceTypelist.Add(new SelectListItem
                    {
                        Text = item.service_tp_desc.ToString(),
                        Value = item.service_tp_id.ToString()
                    });
                }

                response.serviceTypeModule = serviceTypelist.ToList();
                #endregion
                if (customer.CustomerId > 0)
                {
                    response.customerdetail = customer;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.customerdetail = customer;
                    response.Status = "New";
                    response.ErrorMessage = "Customer not found";
                    response.Statuscode = 0;
                }

            }
            catch (System.Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "Get Customer", error.errormsg);
                HttpContext.Session.SetString("error_log", error.errormsg);
                response.Statuscode = 0;
                response.Status = "Failure";
                return RedirectToAction("Error_Log", "Home");
            }

            return View(response);
        }
        #endregion

        #region Add,Edit the Customer records 
        [HttpPost]
        public GetCustomerResponse EditCustomers(Customer customer)
        {
            int userID = 0;
            List<ServiceType> serviceType = new List<ServiceType>();
            List<string> serviceType1 = new List<string>();
            List<ServiceType> serviceType2 = new List<ServiceType>();
            DateTime Contract_end_dt = new DateTime(2025, 1, 1);
            GetCustomerResponse response = new GetCustomerResponse();
            DataSet ds2 = new DataSet();
            DataSet ds = new DataSet();
            try
            {
                //Split Comma values  List in Multiple Service Type
                serviceType1 = customer.ServiceTypesList.Split(", ").ToList();
                DataView dvServiceTypes = TableDefs.General.GetServiceTypesForDropDown(ds, true, false, "All");
                if (dvServiceTypes != null)
                {
                    DataTable dtServiceTypes = dvServiceTypes.ToTable();
                    for (int i = 0; i < dtServiceTypes.Rows.Count; i++)
                    {
                        ServiceType service = new ServiceType();
                        service.service_tp_id = Utility.ToInt(dtServiceTypes.Rows[i]["service_tp_id"]);
                        service.service_tp_desc = dtServiceTypes.Rows[i]["service_tp_desc"].ToString();

                        var test = serviceType1.Where(c => c == service.service_tp_desc).FirstOrDefault();
                        if (test != null)
                        {
                            serviceType.Add(service);
                        }                                           
                    }
                }
                if (ModelState.IsValid)
                {
                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    if (data != null)
                    {
                        userID = data.CurrentUser.UserID;
                        string phoneNumber = Utility.FormatPhone(customer.PhoneNumber, false);
                        string faxNumber = Utility.FormatPhone(customer.Fax, false);

                        if (customer.CustomerId == 0)
                        {                           
                            int customer_id = Utility.ToInt(DBAccess.executeScalar("usp_cust_new", userID, customer.CustomerName, customer.Address1, customer.Address2, customer.City, customer.State, "US", customer.ZipCode, phoneNumber, faxNumber, customer.CustomerStatus, customer.Notes, customer.DefaultLabor, customer.VisitAllocation));
                            foreach (var service in serviceType)
                            {
                                DBAccess.executeSQL("usp_cust_service_new", customer_id, service.service_tp_id);
                            }
                        }
                        else
                        {
                            DBAccess.executeSQL("usp_cust_update", userID, customer.CustomerId, customer.CustomerName, customer.Address1, customer.Address2, customer.City, customer.State, "US", customer.ZipCode, phoneNumber, faxNumber, customer.CustomerStatus, customer.Notes, customer.DefaultLabor, customer.VisitAllocation);

                            #region Service update
                            DataView servicetype = TableDefs.Customer.GetCustomerService(ds2, customer.CustomerId);
                            if (servicetype != null)
                            {
                                customer.cust_service_type = new List<tbl_cust_service>();

                                DataTable dtservicetype = servicetype.ToTable();
                                for (int i = 0; i < dtservicetype.Rows.Count; i++)
                                {
                                    tbl_cust_service cust_service = new tbl_cust_service();
                                    cust_service.service_tp_id = Utility.ToInt(dtservicetype.Rows[i]["service_tp_id"]);
                                    customer.cust_service_type.Add(cust_service);
                                }
                             
                                foreach (var service in serviceType)
                                {
                                    var check = customer.cust_service_type.Where(c => c.service_tp_id == service.service_tp_id).FirstOrDefault();
                                    if(check == null)
                                    {
                                        DBAccess.executeSQL("usp_cust_service_new", customer.CustomerId, service.service_tp_id);
                                    }
                                }
                                foreach (var service in customer.cust_service_type)
                                {
                                    var check = serviceType.Where(c => c.service_tp_id== service.service_tp_id).FirstOrDefault();
                                    if (check == null)
                                    {
                                        DBAccess.executeSQL("usp_cust_service_remove", customer.CustomerId, service.service_tp_id);
                                    }
                                }

                            }
                            #endregion
                        }
                        response.Status = "Success";
                    }
                }
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Add-Edit/Customer", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region SaveStateData
        [HttpPost]
        public IActionResult SaveStateData(int usr_id, int grid_id, string json_col_ord)
        {
            GetCustomerResponse response = new GetCustomerResponse();

            DBAccess.executeSQL("usp_usr_grid_col_update", usr_id, grid_id, json_col_ord);
            response.Status = "Success";
            return Json(response);
        }
        #endregion

        #region Load the Customer State
        public IActionResult LoadCustomerState(int usr_id, int grid_id)
        {
            LoadState loadState = new LoadState();
            DataSet ds = new DataSet();
            GetCustomerResponse response = new GetCustomerResponse();

            mr = TableDefs.User.GetUserGrid(ds, usr_id, grid_id);
            if (mr != null)
            {
                loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                loadState.json_col_ord = mr["json_col_ord"].ToString();
            }

            if (loadState.usr_id > 0)
            {
                response.loadState = loadState;
                response.Statuscode = 1;
                response.Status = "Success";
            }
            else
            {
                response.loadState = loadState;
                response.Status = "New";
                response.ErrorMessage = "Customer not found";
                response.Statuscode = 0;
            }
            return Json(response);
        }
        #endregion

        #region Customer Export List
        [HttpPost]
        [Obsolete]
        public GetCustomerResponse ExportCustomers(List<string> CustomerList)
        {
            DataSet ds = new DataSet();
            GetCustomerResponse response = new GetCustomerResponse();

            try
            {
                if (CustomerList != null)
                {
                    if (CustomerList != null && CustomerList.Count > 0)
                    {
                        //Set File Name and Path
                        string FilePath = Path.Combine(_hostingEnvironment.ContentRootPath, "wwwroot/ExportFile/");
                        string docPath = Path.Combine(FilePath, "customers/");
                        if (Directory.Exists(docPath))
                        {
                            DirectoryInfo di = new DirectoryInfo(docPath);
                            FileInfo[] files = di.GetFiles();
                            foreach (FileInfo file in files)
                            {
                                file.Delete();
                            }
                        }

                        string file_name = $"customers_{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";
                        string pathFileName = docPath + file_name;

                        if (!Directory.Exists(FilePath))
                        {
                            Directory.CreateDirectory(FilePath);
                        }

                        // Path Not exists then create new file path
                        if (!Directory.Exists(docPath))
                        {
                            Directory.CreateDirectory(docPath);
                        }
                        //File name  exists then Delete the  file
                        if (System.IO.File.Exists(pathFileName))
                        {
                            System.IO.File.Delete(docPath);
                        }

                        //convert list to string
                        string customerList = String.Join<string>(",", CustomerList);
                        DataTable dtCustomers = TableDefs.Customer.GetCustomersForExport(ds, customerList).ToTable();

                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dtCustomers);
                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(pathFileName);
                                response.Status = "Success";
                                response.ExcelLink = "/ExportFile/customers/" + file_name;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Export/Customer", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
            }
            return response;
        }
        #endregion

        #region Customer Dropdown for Contracts
        
        [HttpPost]
        public JsonResult LoadCustomerDropdown(bool isactive, bool toggleActive)
        {
            List<tbl_cust> customerList = new List<tbl_cust>();
            List<tbl_cust> customerListDrop = new List<tbl_cust>();
            LoadState loadState = new LoadState();
            GetCustomerResponse response = new GetCustomerResponse();
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();

            try
            {
                // get the value for sp, API Call
                DataView dvCustomer = TableDefs.Customer.GetCustomersForDropDown(ds, true, "All", isactive);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listCustomerDropdown=JsonConvert.DeserializeObject<lst_tbl_cust>(jsonData);
                customerListDrop = listCustomerDropdown.tbl_cust.Where(x => x.cust_id == 0).ToList();
                listCustomerDropdown.tbl_cust.RemoveAt(0);
                customerList = listCustomerDropdown.tbl_cust.ToList().OrderBy(x => x.cust_name).ToList();
                customerList = customerListDrop.Concat(customerList).ToList();
                dsResult.Data = customerList;
                dsResult.Total = customerList.Count;
            }
            catch (Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "CustomerDropDown/Customer", error.errormsg);
                HttpContext.Session.SetString("error_log", error.errormsg);
                result.success = false;
                result.message = $"An error occurred retrieving Customer List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion
    }
}
