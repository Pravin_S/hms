﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Collections.Generic;
using Kendo.Mvc.UI;
using System.Data;
using System;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace HMSMemri.Controllers
{
    public class CustomerContactController : IMCPBase
    {
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        public CustomerContactController(IOptions<BaseSettings.ServerVar> serverVar) : base(serverVar)
        {
            _serverVar = serverVar;
        }
        #region Index UI
        public IActionResult Index()
        {
            return View();

        }
        #endregion

        #region  Displaying the edited Customer Contact records
        public IActionResult AddEditContact(string id)
        {
            GetCustomerContactResponse response = new GetCustomerContactResponse();
            CustomerContact contact = new CustomerContact();
            DataSet ds = new DataSet();
            List<CustomerContactDrop> CustomerContactdrop = new List<CustomerContactDrop>();
            GlobalData Global = new GlobalData();
            GlobalData cust_id = GlobalData.GetGlobalSessione(HttpContext.Session, "cust_id");
            GlobalData facility_id = GlobalData.GetGlobalSessione(HttpContext.Session, "facility_id");
            GlobalData Cust_ContactId = GlobalData.GetGlobalSessione(HttpContext.Session, "cust_Contactid");
            try
            {
                string IDcheck = id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(id)) : "0";
            }
            catch (Exception)
            {

                return View("~/Views/Shared/Error_EditUrl.cshtml");
            }

            string ContactId = id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(id)) : "0";
            int ID = Convert.ToInt32(ContactId);
            try
            {
                if (cust_id != null || Cust_ContactId.cust_id != null)
                {
                    DataRow GetCustomerContact = TableDefs.Customer.GetCustomerContact(ds, ID);
                    if (GetCustomerContact != null)
                    {
                        contact.cust_contact_id = Utility.ToInt(GetCustomerContact["cust_contact_id"]);
                        contact.cust_id = Utility.ToInt(GetCustomerContact["cust_id"]);
                        contact.FirstName = GetCustomerContact["first_name"].ToString();
                        contact.LastName = GetCustomerContact["last_name"].ToString();
                        contact.Title = GetCustomerContact["title"].ToString();
                        contact.PhoneNumber = Utility.FormatPhone(GetCustomerContact["phone"].ToString(), true);
                        contact.CellPhone = Utility.FormatPhone(GetCustomerContact["cell"].ToString(), true);
                        contact.FaxNumber = Utility.FormatPhone(GetCustomerContact["fax"].ToString(), true);
                        contact.EmailAddress = GetCustomerContact["email"].ToString();
                        contact.AltNumber1 = GetCustomerContact["alternate_num1"].ToString();
                        contact.AltNumber2 = GetCustomerContact["alternate_num2"].ToString();
                        contact.ContactType = Utility.ToInt(GetCustomerContact["contact_tp_id"]);
                        contact.Notes = GetCustomerContact["notes"].ToString();
                        contact.ClientPassword = GetCustomerContact["pswd"].ToString();
                        contact.IsActive = Convert.ToBoolean(GetCustomerContact["is_active"]);
                        //contact.ClientLogin = GetCustomerContact["login"].ToString();
                        
                        
                    }
                    else
                    {
                        contact.cust_id = Cust_ContactId.cust_id;
                        contact.IsActive = true;
                    }
                }


                //contact type Drop Down
                DataView dvContact = TableDefs.General.GetContactTypesForDropDown(ds, true, true,"* Select One *");
                if (dvContact != null)
                {
                    DataTable dtContact = dvContact.ToTable();
                    for (int i = 0; i < dtContact.Rows.Count; i++)
                    {
                        CustomerContactDrop status = new CustomerContactDrop();
                        status.contact_tp_id = Utility.ToInt(dtContact.Rows[i]["contact_tp_id"]);
                        status.contact_tp_desc = dtContact.Rows[i]["contact_tp_desc"].ToString();
                        CustomerContactdrop.Add(status);
                    }
                }
                List<SelectListItem> ContactModule = new List<SelectListItem>();
                foreach (var item in CustomerContactdrop)
                {
                    ContactModule.Add(new SelectListItem
                    {
                        Text = item.contact_tp_desc.ToString(),
                        Value = item.contact_tp_id.ToString()
                    });
                }
                response.ContactModule = ContactModule;


                if (contact.cust_contact_id > 0)
                {
                    response.CustomerContactDetail = contact;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.CustomerContactDetail = contact;
                    response.Status = "Failure";
                    response.ErrorMessage = " Customer Contact not found";
                    response.Statuscode = 0;
                }
            }

            catch (Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "Get CustomerContact/CustomerContact", error.errormsg);
                HttpContext.Session.SetString("error_log", error.errormsg);
                response.Statuscode = 0;
                response.Status = "Failure";
                return RedirectToAction("Error_Log", "Home");
            }
            return View(response);  
        }
        #endregion
        
        #region AddEdit Customer Contact
        public GetCustomerContactResponse AddEditCustomerContacts(CustomerContact request)
        {
            CustomerContact contact = new CustomerContact();
            DataSet ds = new DataSet();
            int userID = 0;
            GetCustomerContactResponse response = new GetCustomerContactResponse();
            try
            {
                if (ModelState.IsValid)
                {
                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    if (data != null)
                    {
                        userID = data.CurrentUser.UserID;
                        request.PhoneNumber = Utility.FormatPhone(request.PhoneNumber, false);
                        request.CellPhone = Utility.FormatPhone(request.CellPhone, false);
                        request.FaxNumber = Utility.FormatPhone(request.FaxNumber, false);
                        request.AltNumber1 = Utility.FormatPhone(request.AltNumber1, false);
                        request.AltNumber2 = Utility.FormatPhone(request.AltNumber2, false);
                        if (request.cust_contact_id == 0)
                        {
                            string result = Utility.ToString(DBAccess.executeScalar("usp_cust_contact_new", userID, request.cust_id, request.FirstName, request.LastName, request.Title, request.PhoneNumber, request.CellPhone,
                               request.EmailAddress, request.FaxNumber, request.AltNumber1, request.AltNumber2, request.ContactType,
                                request.Notes, Utility.Encrypt(request.ClientPassword), "", true, request.IsActive));

                        }
                        else
                        {
                            DataRow GetCustomerContact = TableDefs.Customer.GetCustomerContact(ds, request.cust_contact_id);
                            contact.ClientPassword = GetCustomerContact["pswd"].ToString();
                            request.ClientPassword = (request.ClientPassword == contact.ClientPassword) ? request.ClientPassword : Utility.Encrypt(request.ClientPassword);

                            string result = Utility.ToString(DBAccess.executeScalar("usp_cust_contact_update", userID, request.cust_contact_id, request.cust_id, request.FirstName, request.LastName, request.Title, request.PhoneNumber, request.CellPhone,
                                request.EmailAddress, request.FaxNumber, request.AltNumber1, request.AltNumber2, request.ContactType,
                                request.Notes, request.ClientPassword, "", true, request.IsActive));
                        }
                            response.Status = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "Add-Edit/CustomerContact", error.errormsg);
                HttpContext.Session.SetString("error_log", error.errormsg);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
               return response;
        }
        #endregion

        #region Load Contact

        [HttpPost]
        public JsonResult LoadContactGrid(bool isactive, bool toggleActive, int Cust_contactID)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                Global.cust_id = Cust_contactID;
                GlobalData.SetGlobalSessione(HttpContext.Session, "cust_Contactid", Global);

                DataView dvContact = TableDefs.Customer.GetCustomerContacts(ds, Cust_contactID, isactive);
                string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listContact = JsonConvert.DeserializeObject<lst_tbl_cust_contact>(jsonData2);

                dsResult.Data = listContact.tbl_cust_contact.ToList().OrderBy(x => x.first_name);
                dsResult.Total = listContact.tbl_cust_contact.Count;
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Grid Load/CustomerContact", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Customer Contact List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion
    }
}
