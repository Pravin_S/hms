﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Collections.Generic;
using Kendo.Mvc.UI;
using System.Data;
using System;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Globalization;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;

namespace HMSMemri.Controllers
{
    public class InventoryPartsController : IMCPBase
    {
        /// <summary>
        /// Summary for declare Global Variables
        /// </summary>
        
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        protected DataRow mr = null;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        public InventoryPartsController(IOptions<BaseSettings.ServerVar> serverVar, IHostingEnvironment hostingEnvironment) : base(serverVar)
        {
            _serverVar = serverVar;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Summary for Index page(UI),check logged user
        /// </summary>
        /// <returns></returns>

        #region Index UI
        public IActionResult Index(string Id)
        {
            DataSet ds = new DataSet();
            GetInventoryPartsResponse response = new GetInventoryPartsResponse();
            //Globally to check the login status
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }

            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.contracts_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;
            string InvId = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            int ID = Convert.ToInt32(InvId);
            try
            {
                string IDcheck = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            }
            catch (System.Exception)
            {
                return View("~/Views/Shared/Error_EditUrl.cshtml");
            }

            Global.inv_id = ID;
            GlobalData.SetGlobalSessione(HttpContext.Session, "inv_id", Global);

            ViewBag.ShowAll = Global.contracts_show_closed;
            return View();

        }
        #endregion

        #region Grid data load
        [HttpPost]
            public JsonResult LoadInventoryPartsGrid(DateTime DatetimePicker)
            {
            LoadState loadState = new LoadState();
            GetInventoryPartsResponse response = new GetInventoryPartsResponse();
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

            //Get Inv ID
            string Date=DatetimePicker.ToString("yyyy-MM-dd");
            GlobalData Inventory = GlobalData.GetGlobalSessione(HttpContext.Session, "inv_id");
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();

            try
            {
                    // get the value for sp, API Call
                    DataView dvCharge = TableDefs.Ticket.GetTicketChargesByInventoryID(ds,Utility.ToInt(Inventory.inv_id), Date);
                    string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                    var listTicketCharge = JsonConvert.DeserializeObject<lst_tbl_ticket_check_tp>(jsonData);
                    dsResult.Data = listTicketCharge.tbl_ticket_check_tp.ToList();
                    dsResult.Total = listTicketCharge.tbl_ticket_check_tp.Count;
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Grid Load/Inventory Parts", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Contracts List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }

        #endregion

        #region Inventory Parts Export List
        [HttpPost]
        [Obsolete]
        public GetInventoryPartsResponse ExportInventoryParts(List<string> InventoryPartsList)
        {
            DataSet ds = new DataSet();
            GetInventoryPartsResponse response = new GetInventoryPartsResponse();
            try
            {
                if (InventoryPartsList != null)
                {
                    if (InventoryPartsList != null && InventoryPartsList.Count > 0)
                    {
                        string FilePath = Path.Combine(_hostingEnvironment.ContentRootPath, "wwwroot/ExportFile/");
                        string docPath = Path.Combine(FilePath, "InventoryParts/");
                        if (Directory.Exists(docPath))
                        {
                            DirectoryInfo di = new DirectoryInfo(docPath);
                            FileInfo[] files = di.GetFiles();
                            foreach (FileInfo file in files)
                            {
                                file.Delete();
                            }
                        }
                        string file_name = $"inventoryparts_{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";
                        string pathFileName = docPath + file_name;

                        if (!Directory.Exists(FilePath))
                        {
                            Directory.CreateDirectory(FilePath);
                        }

                        // Path Not exists then create new file path
                        if (!Directory.Exists(docPath))
                        {
                            Directory.CreateDirectory(docPath);
                        }

                        //File name  exists then Delete the  file
                        if (System.IO.File.Exists(pathFileName))
                        {
                            System.IO.File.Delete(docPath);
                        }

                        //convert list to string
                        string inventoryPartsListlist = String.Join<string>(",", InventoryPartsList);
                        DataTable dtCharge = TableDefs.Ticket.GetTicketChargesExport(ds, inventoryPartsListlist).ToTable();

                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dtCharge);
                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(pathFileName);
                                response.Status = "Success";
                                response.ExcelLink = "/ExportFile/InventoryParts/" + file_name;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Export/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Export/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
            }
            return response;

        }
        #endregion
    }
}
