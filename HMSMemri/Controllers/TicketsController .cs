﻿using hmsBO;
using HMSMemri.BaseSettings;
using HMSMemri.Models;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;

namespace HMSMemri.Controllers
{
    public class TicketsController : IMCPBase
    {
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        protected DataRow mr = null;
        [Obsolete]
        public TicketsController(IOptions<BaseSettings.ServerVar> serverVar, IHostingEnvironment hostingEnvironment) : base(serverVar)
        {
            _serverVar = serverVar;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            //Globally to check the login status
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.tickets_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.tickets_show_closed;
            return View();


        }

        #region Load Tickets

        [HttpPost]
        public JsonResult LoadTicketsGrid(bool isMyTickets, int facilityID, int ID, bool toggleActive)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    if (isMyTickets == true)
                    {
                        DataView dvTickets = TableDefs.Ticket.GetTicketsByUser(ds, facilityID, ID, toggleActive);
                        string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                        var listTickets = JsonConvert.DeserializeObject<lst_tbl_tickets>(jsonData2);

                        dsResult.Data = listTickets.tbl_ticket.ToList();
                        dsResult.Total = listTickets.tbl_ticket.Count;
                    }
                    else
                    {
                        DataView dvTickets = TableDefs.Ticket.GetTicketsByUser(ds, facilityID, 0 /*user id*/, toggleActive);
                        string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                        var listTickets = JsonConvert.DeserializeObject<lst_tbl_tickets>(jsonData2);

                        dsResult.Data = listTickets.tbl_ticket.ToList();
                        dsResult.Total = listTickets.tbl_ticket.Count;

                    }

                }
                else
                {
                    GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                    int CustID = CustomerDetail.CurrentCust.cust_id;
                    DataView dvTickets = TableDefs.Ticket.GetTicketsByCustomerByFacility(ds, CustID, facilityID, toggleActive);
                    string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                    var listTickets = JsonConvert.DeserializeObject<lst_tbl_tickets>(jsonData2);

                    dsResult.Data = listTickets.tbl_ticket.ToList();
                    dsResult.Total = listTickets.tbl_ticket.Count;

                }
            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Grid Load/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Grid Load/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                result.success = false;
                result.message = $"An error occurred retrieving request List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region Displaying the edited  Tickets records

        public ActionResult EditTickets(string Id)
        {
            //Declaring the variables
            GetTicketResponse response = new GetTicketResponse();
            Tickets Tickets = new Tickets();
            List<TicketAddMethod> ticketAddMethod = new List<TicketAddMethod>();
            List<TicketType> ticketType = new List<TicketType>();
            List<Priority> priority = new List<Priority>();
            List<TicketStatus> ticketStat = new List<TicketStatus>();
            List<UserNameDropDown> UserNameDropdown = new List<UserNameDropDown>();
            List<GroupNameDropDown> GroupNameDropdown = new List<GroupNameDropDown>();
            List<dropCustomer> CustomerStatus = new List<dropCustomer>();
            List<dropFacilty> FacilityStatus = new List<dropFacilty>();
            List<tbl_facility> facilityListDrop = new List<tbl_facility>();
            response.Statuscode = 0;
            string ID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            GlobalData Global = new GlobalData();
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }
            try
            {
                DataSet ds = new DataSet();

                // Customer Drop Down
                DataView dvCustomerFacility = TableDefs.Customer.GetCustomersForDropDown(ds, true, "* Select One *", true);
                if (dvCustomerFacility != null)
                {
                    DataTable dtCustomer = dvCustomerFacility.ToTable();
                    for (int i = 0; i < dtCustomer.Rows.Count; i++)
                    {
                        dropCustomer customer = new dropCustomer();
                        customer.cust_id = Utility.ToInt(dtCustomer.Rows[i]["cust_id"]);
                        customer.cust_name = dtCustomer.Rows[i]["cust_name"].ToString();
                        CustomerStatus.Add(customer);
                    }
                }

                List<SelectListItem> Customerselectionlist = new List<SelectListItem>();
                foreach (var item in CustomerStatus)
                {
                    Customerselectionlist.Add(new SelectListItem
                    {
                        Text = item.cust_name,
                        Value = item.cust_id.ToString()
                    });
                }

                response.CustomerDropdownModule = Customerselectionlist;

                //request grid API call
                mr = TableDefs.Ticket.GetTicketsByID(ds, Utility.ToInt(ID.ToString()));
                if (mr != null)
                {
                    Tickets.ticket_id = Utility.ToInt(mr["ticket_id"]);
                    Tickets.CustomerID= Utility.ToInt(mr["cust_id"]);
                    Tickets.FacilityID= Utility.ToInt(mr["facility_id"]);
                    Tickets.CallerName = mr["caller_name"].ToString();
                    Tickets.CallerPhone = Utility.FormatPhone(mr["caller_phone"].ToString(), true);
                    Tickets.CaseNumber= mr["case_num"].ToString();
                    Tickets.TicketAddMethodID = Utility.ToInt(mr["ticket_addmethod_id"]);
                    Tickets.Summary = mr["summary"].ToString();
                    Tickets.TicketTypeID = Utility.ToInt(mr["ticket_tp_id"]);
                    Tickets.AssignedUserID = Utility.ToInt(mr["assigned_to_usr_id"]);
                    Tickets.GroupID = Utility.ToInt(mr["group_id"]);
                    Tickets.PriorityID = Utility.ToInt(mr["ticket_priority_stat_id"].ToString());
                    Tickets.StartDate = Convert.ToDateTime(mr["start_dt"]).ToString("MM/dd/yyyy");
                    Tickets.DeadLineDate = Convert.ToDateTime(mr["deadline_dt"]).ToString("MM/dd/yyyy");
                    Tickets.DeadLineDate1=Convert.ToDateTime(mr["deadline_dt"]).ToString("HH/mm tt");
                    Tickets.TicketStatID = Convert.ToInt32(mr["ticket_stat_id"]);
                    Tickets.TicketDescription = mr["ticket_desc"].ToString();
                    Tickets.CaseNumber = mr["case_num"].ToString();
                    DataRow FacilityName = TableDefs.Facility.GetFacility(ds, Tickets.FacilityID);
                    if (FacilityName != null)
                    {
                        Tickets.facility_name = FacilityName["facility_name"].ToString();
                    }
                    DataRow customername = TableDefs.Customer.GetCustomer(ds, Tickets.CustomerID);
                    if (customername != null)
                    {
                        Tickets.cust_name = customername["cust_name"].ToString();
                    }
                    GlobalData Ticket_inv = GlobalData.GetGlobalSessione(HttpContext.Session, "Ticket_inv");
                    if (Ticket_inv != null)
                    {
                        Tickets.inv_id = Ticket_inv.inv_id;
                    }
                }
                else
                {
                    GlobalData Ticket_inv = GlobalData.GetGlobalSessione(HttpContext.Session, "Ticket_inv");
                    if (Ticket_inv != null)
                    {
                        DataRow FacilityName = TableDefs.Facility.GetFacility(ds, Ticket_inv.facility_id);
                        if (FacilityName != null)
                        {
                            Tickets.facility_name = FacilityName["facility_name"].ToString();
                        }
                        DataRow customername = TableDefs.Customer.GetCustomer(ds, Ticket_inv.cust_id);
                        if (customername != null)
                        {
                            Tickets.cust_name = customername["cust_name"].ToString();
                        }
                        Tickets.CustomerID = Ticket_inv.cust_id;
                        Tickets.FacilityID = Ticket_inv.facility_id;
                        Tickets.inv_id = Ticket_inv.inv_id;
                        Tickets.DeadLineDate = DateTime.UtcNow.ToString("MM/dd/yyyy");
                        Tickets.DeadLineDate1 = DateTime.UtcNow.ToString("HH/mm tt");
                        Tickets.PriorityID = 20;
                    }
                    else
                    {
                        return RedirectToAction("Error_Log", "Home");
                    }
                    
                }

                //Ticket Add Method Module  API call
                DataView dvTicketAddMethodModule = TableDefs.Ticket.GetTicketAddMethod(ds, true, "* Select One *");
                if (dvTicketAddMethodModule != null)
                {
                    DataTable dtAddMethod = dvTicketAddMethodModule.ToTable();
                    for (int i = 0; i < dtAddMethod.Rows.Count; i++)
                    {
                        TicketAddMethod status = new TicketAddMethod();
                        status.ticket_addmethod_id = Utility.ToInt(dtAddMethod.Rows[i]["ticket_addmethod_id"]);
                        status.ticket_addmethod_desc = dtAddMethod.Rows[i]["ticket_addmethod_desc"].ToString();
                        ticketAddMethod.Add(status);
                    }
                }

                //TicketType API call
                //States = requestAPI.GetStateDropdown();
                DataView dvTicketType = TableDefs.Ticket.GetTicketType(ds, true, "* Select One *");
                if (dvTicketType != null)
                {
                    DataTable dttypes = dvTicketType.ToTable();
                    for (int i = 0; i < dttypes.Rows.Count; i++)
                    {
                        TicketType types = new TicketType();
                        types.ticket_tp_id = Utility.ToInt(dttypes.Rows[i]["ticket_tp_id"]);
                        types.ticket_tp_desc = dttypes.Rows[i]["ticket_tp_desc"].ToString();
                        ticketType.Add(types);
                    }
                }
                //Priority API call
                DataView dvPriority = TableDefs.Ticket.GetPriority(ds, false, "* Select One *");
                if (dvPriority != null)
                {
                    DataTable dttypes = dvPriority.ToTable();
                    for (int i = 0; i < dttypes.Rows.Count; i++)
                    {
                        Priority prior = new Priority();
                        prior.ticket_priority_stat_id = Utility.ToInt(dttypes.Rows[i]["ticket_priority_stat_id"]);
                        prior.ticket_priority_stat_desc = dttypes.Rows[i]["ticket_priority_stat_desc"].ToString();
                        priority.Add(prior);
                    }
                }

                //Get Ticket Status API call
                DataView dvStatus = TableDefs.Ticket.GetTicketStatus(ds, true, "* Select One *");
                if (dvStatus != null)
                {
                    DataTable dttypes = dvStatus.ToTable();
                    for (int i = 0; i < dttypes.Rows.Count; i++)
                    {
                        TicketStatus stat = new TicketStatus();
                        stat.ticket_stat_id = Utility.ToInt(dttypes.Rows[i]["ticket_stat_id"]);
                        stat.ticket_stat_desc = dttypes.Rows[i]["ticket_stat_desc"].ToString();
                        ticketStat.Add(stat);
                    }
                }

                //show selected list item in Ticket Status
                List<SelectListItem> ticketStatuslist = new List<SelectListItem>();
                foreach (var item in ticketStat)
                {
                    ticketStatuslist.Add(new SelectListItem
                    {
                        Text = item.ticket_stat_desc.ToString(),
                        Value = item.ticket_stat_id.ToString()
                    });
                }
                response.TicketStatusModule = ticketStatuslist;


                //show selected list item in Add Method
                List<SelectListItem> ticketAddselectionlist = new List<SelectListItem>();
                foreach (var item in ticketAddMethod)
                {
                    ticketAddselectionlist.Add(new SelectListItem
                    {
                        Text = item.ticket_addmethod_desc.ToString(),
                        Value = item.ticket_addmethod_id.ToString()
                    });
                }
                response.TicketAddMethodModule = ticketAddselectionlist;

                //show selected list item in TicketType
                List<SelectListItem> ticketTypeSelectionlist = new List<SelectListItem>();
                foreach (var TicketTItem in ticketType)
                {
                    ticketTypeSelectionlist.Add(new SelectListItem
                    {
                        Text = TicketTItem.ticket_tp_desc.ToString(),
                        Value = TicketTItem.ticket_tp_id.ToString()
                    });
                }
                response.TicketTypeModule = ticketTypeSelectionlist;

                //show selected list item in TicketPriorities
                List<SelectListItem> PrioritySelectionlist = new List<SelectListItem>();
                foreach (var priorities in priority)
                {
                    PrioritySelectionlist.Add(new SelectListItem
                    {
                        Text = priorities.ticket_priority_stat_desc.ToString(),
                        Value = priorities.ticket_priority_stat_id.ToString()
                    });
                }
                response.PriorityModule = PrioritySelectionlist;

                // Show User Name DropDown

                DataView dvUsername = TableDefs.User.GetUsersForDropDown(ds, false,true, "* Select User *");
                if (dvUsername != null)
                {
                    DataTable dtUsername = dvUsername.ToTable();
                    for (int i = 0; i < dtUsername.Rows.Count; i++)
                    {
                        UserNameDropDown status = new UserNameDropDown();
                        status.usr_id = Utility.ToInt(dtUsername.Rows[i]["usr_id"]);
                        status.usr_name = dtUsername.Rows[i]["usr_name"].ToString();
                        UserNameDropdown.Add(status);
                    }
                }

                List<SelectListItem> UserNameSelectionlist = new List<SelectListItem>();
                foreach (var username in UserNameDropdown)
                {
                    UserNameSelectionlist.Add(new SelectListItem
                    {
                        Text = username.usr_name.ToString(),
                        Value = username.usr_id.ToString()
                    });
                }
                response.UserNameDropDownModule = UserNameSelectionlist;

                //Show GroupName DropDown

                DataView dvGroupName = TableDefs.User.GetGroupsForDropDown(ds, false, true, "* Select Group *");
                if (dvGroupName != null)
                {
                    DataTable dtGroupName = dvGroupName.ToTable();
                    for (int i = 0; i < dtGroupName.Rows.Count; i++)
                    {
                        GroupNameDropDown status = new GroupNameDropDown();
                        status.group_id = Utility.ToInt(dtGroupName.Rows[i]["group_id"]);
                        status.group_name = dtGroupName.Rows[i]["group_name"].ToString();
                        GroupNameDropdown.Add(status);
                    }
                }
                List<SelectListItem> GroupNameSelectionlist = new List<SelectListItem>();
                foreach (var groupname in GroupNameDropdown)
                {
                    GroupNameSelectionlist.Add(new SelectListItem
                    {
                        Text = groupname.group_name.ToString(),
                        Value = groupname.group_id.ToString()
                    });
                }
                response.GroupNameDropDownModule = GroupNameSelectionlist;
                // Facility DropDown
                int custId = Tickets.CustomerID;
                if (custId != 0)
                {
                    DataView dvFacility = TableDefs.Facility.GetFacilityByCustomerForDropDown(ds, custId, true, "* Select One *", true);
                    if (dvFacility != null)
                    {
                        DataTable dtFacility = dvFacility.ToTable();
                        for (int i = 0; i < dtFacility.Rows.Count; i++)
                        {
                            dropFacilty facility = new dropFacilty();
                            facility.facility_id = Utility.ToInt(dtFacility.Rows[i]["facility_id"]);
                            facility.facility_name = dtFacility.Rows[i]["facility_name"].ToString();
                            FacilityStatus.Add(facility);
                        }
                    }
                    List<SelectListItem> Facilityselectionlist = new List<SelectListItem>();
                    foreach (var item in FacilityStatus)
                    {
                        Facilityselectionlist.Add(new SelectListItem
                        {
                            Text = item.facility_name,
                            Value = item.facility_id.ToString()
                        });
                    }
                    response.FacilityDropdownModule = Facilityselectionlist;
                }
                else
                {
                    List<SelectListItem> Facilityselectionlist = new List<SelectListItem>();

                    Facilityselectionlist.Add(new SelectListItem
                    {
                        Text = "* Select One *",
                        Value = "0"
                    });

                    response.FacilityDropdownModule = Facilityselectionlist;
                }




                if (Tickets.ticket_id > 0)
                {
                    response.Ticketdetail = Tickets;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.Ticketdetail = Tickets;
                    response.Status = "New";
                    response.ErrorMessage = "Tickets not found";
                    response.Statuscode = 0;
                }

            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Get Ticket/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Get Ticket/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
                return RedirectToAction("Error_Log", "Home");
            }
            return View(response);
        }

        #endregion

        #region Add,Edit the Tickets records 

        [HttpPost]
        public GetTicketResponse AddEditTicket(Tickets request)
        {
            int userID = 0;
            GetTicketResponse response = new GetTicketResponse();
            response.Ticketdetail = new Tickets();
            try
            {
                if (ModelState.IsValid)
                {
                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    if (data != null)
                    {
                        userID = data.CurrentUser.UserID;
                        string phoneNumber = Utility.FormatPhone(request.CallerPhone, false);
                        GlobalData Ticket_inv = GlobalData.GetGlobalSessione(HttpContext.Session, "Ticket_inv");
                        if (request.ticket_id == 0)
                        {
                            int ticket_id = Utility.ToInt(DBAccess.executeScalar("usp_ticket_new", userID, request.CaseNumber, request.CustomerID, request.FacilityID,
                                request.AssignedUserID, userID, request.GroupID, request.TicketStatID, request.TicketAddMethodID,
                                request.Summary, 0, request.TicketTypeID, request.PriorityID, DateTime.UtcNow.ToString("MM/dd/yyyy"),
                               request.DeadLineDate,
                                request.TicketDescription, 0, DateTime.UtcNow.ToString("MM/dd/yyyy"),
                                DateTime.UtcNow.ToString("MM/dd/yyyy"), true, request.CallerName, phoneNumber, true));
                            response.Ticketdetail.ticket_id = ticket_id;
                            if (Ticket_inv != null && ticket_id > 0)
                            {
                                DBAccess.executeSQL("usp_ticket_inv_new", userID, ticket_id, Ticket_inv.inv_id);
                            }
                        }
                        else
                        {
                            DBAccess.executeSQL("usp_ticket_update", userID, request.ticket_id, request.CaseNumber,
                                request.CustomerID, request.FacilityID, request.AssignedUserID, userID, request.GroupID,
                            request.TicketStatID, request.TicketAddMethodID, request.Summary, 0, request.TicketTypeID, request.PriorityID,
                            DateTime.UtcNow.ToString("MM/dd/yyyy"),
                          request.DeadLineDate, request.TicketDescription, 0, DateTime.UtcNow.ToString("MM/dd/yyyy"),
                           DateTime.UtcNow.ToString("MM/dd/yyyy"), true, request.CallerName, phoneNumber, true);

                        }
                        response.Status = "Success";
                    }

                }
            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Add-Edit/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Add-Edit/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }

        #endregion

        #region Edit ticket recordes
        #region Add,Edit the Tickets records 

        [HttpPost]
        public GetTicketResponse AddEditTickets(EditTickets request)
        {
            int userID = 0;
            GetTicketResponse response = new GetTicketResponse();
            try
            {
                if (ModelState.IsValid)
                {
                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    if (data != null)
                    {
                        userID = data.CurrentUser.UserID;
                        string phoneNumber = Utility.FormatPhone(request.CallerPhone, false);

                        if (request.ticket_id != 0)
                        {
                            DBAccess.executeSQL("usp_ticket_update", userID, request.ticket_id, request.CaseNumber,
                               request.CustomerID, request.FacilityID, request.AssignedUserID, userID, request.GroupID,
                           request.TicketStatID, request.TicketAddMethodID, request.Summary, 0, request.TicketTypeID, request.PriorityID,
                           DateTime.UtcNow.ToString("MM/dd/yyyy"),
                         request.DeadLineDate, request.TicketDescription, 0, DateTime.UtcNow.ToString("MM/dd/yyyy"),
                          DateTime.UtcNow.ToString("MM/dd/yyyy"), true, request.CallerName, phoneNumber, true);
                        }
                        
                        response.Status = "Success";
                    }

                }
            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Add-Edit/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Add-Edit/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }

        #endregion
        #endregion

        #region Details page
        public ActionResult Details(string Id)
        {
            GetTicketResponse response = new GetTicketResponse();
            EditTickets Tickets = new EditTickets();
            List<TicketType> ticketType = new List<TicketType>();
            List<Priority> priority = new List<Priority>();
            List<TicketStatus> ticketStat = new List<TicketStatus>();
            response.Statuscode = 0;
            string ID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            GlobalData Global = new GlobalData();
            //Globally to check the login status
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            try
            {
                _TicketSubMenu(Utility.ToInt(ID.ToString()));
                DataSet ds = new DataSet();
                //request grid API call
                //request = requestAPI.GetrequestByID(ID);
                mr = TableDefs.Ticket.GetTicketsByID(ds, Utility.ToInt(ID.ToString()));
                if (mr != null)
                {
                    Tickets.ticket_id = Utility.ToInt(mr["ticket_id"]);
                    Tickets.CustomerID = Utility.ToInt(mr["cust_id"]);
                    Tickets.FacilityID = Utility.ToInt(mr["facility_id"]);

                    GlobalData.SetGlobalSessione(HttpContext.Session, "cust_id", Global);
                    GlobalData.SetGlobalSessione(HttpContext.Session, "facility_id", Global);

                    GlobalData cust_id = GlobalData.GetGlobalSessione(HttpContext.Session, "cust_id");
                    GlobalData facility_id = GlobalData.GetGlobalSessione(HttpContext.Session, "facility_id");


                    Tickets.CallerName = mr["caller_name"].ToString();
                    Tickets.CallerPhone = Utility.FormatPhone(mr["caller_phone"].ToString(), true);
                    Tickets.CaseNumber = mr["case_num"].ToString();
                    Tickets.TicketAddMethodID = Utility.ToInt(mr["ticket_addmethod_id"]);
                    Tickets.Summary = mr["summary"].ToString();
                    Tickets.TicketTypeID = Utility.ToInt(mr["ticket_tp_id"]);
                    Tickets.AssignedUserID = Utility.ToInt(mr["assigned_to_usr_id"]);
                    Tickets.GroupID = Utility.ToInt(mr["group_id"]);
                    Tickets.PriorityID = Utility.ToInt(mr["ticket_priority_stat_id"].ToString());
                    Tickets.StartDate = Convert.ToDateTime(mr["start_dt"]).ToString("MM/dd/yyyy");
                    Tickets.DeadLineDate = string.IsNullOrEmpty(mr["deadline_dt"].ToString()) ? null : Convert.ToDateTime(mr["deadline_dt"]).ToString("MM/dd/yyyy");
                    Tickets.DeadLineDate1 = string.IsNullOrEmpty(mr["deadline_dt"].ToString()) ? null : Convert.ToDateTime(mr["deadline_dt"]).ToString("HH/mm tt");
                    Tickets.TicketStatID = Convert.ToInt32(mr["ticket_stat_id"]);
                    Tickets.TicketDescription = mr["ticket_desc"].ToString();
                    Tickets.CaseNumber = mr["case_num"].ToString();
                    //Tickets.User = Utility.ToInt(mr[""]);
                }
                else
                {
                    //Tickets.StartDate = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    Tickets.DeadLineDate = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    Tickets.DeadLineDate1 = DateTime.UtcNow.ToString("HH/mm tt");
                    Tickets.PriorityID = 20;
                }



                //TicketType API call
                //States = requestAPI.GetStateDropdown();
                DataView dvTicketType = TableDefs.Ticket.GetTicketType(ds, true, "* Select One *");
                if (dvTicketType != null)
                {
                    DataTable dttypes = dvTicketType.ToTable();
                    for (int i = 0; i < dttypes.Rows.Count; i++)
                    {
                        TicketType types = new TicketType();
                        types.ticket_tp_id = Utility.ToInt(dttypes.Rows[i]["ticket_tp_id"]);
                        types.ticket_tp_desc = dttypes.Rows[i]["ticket_tp_desc"].ToString();
                        ticketType.Add(types);
                    }
                }
                //Priority API call
                DataView dvPriority = TableDefs.Ticket.GetPriority(ds, false, "* Select One *");
                if (dvPriority != null)
                {
                    DataTable dttypes = dvPriority.ToTable();
                    for (int i = 0; i < dttypes.Rows.Count; i++)
                    {
                        Priority prior = new Priority();
                        prior.ticket_priority_stat_id = Utility.ToInt(dttypes.Rows[i]["ticket_priority_stat_id"]);
                        prior.ticket_priority_stat_desc = dttypes.Rows[i]["ticket_priority_stat_desc"].ToString();
                        priority.Add(prior);
                    }
                }

                //Get Ticket Status API call
                DataView dvStatus = TableDefs.Ticket.GetTicketStatus(ds, true, "* Select One *");
                if (dvStatus != null)
                {
                    DataTable dttypes = dvStatus.ToTable();
                    for (int i = 0; i < dttypes.Rows.Count; i++)
                    {
                        TicketStatus stat = new TicketStatus();
                        stat.ticket_stat_id = Utility.ToInt(dttypes.Rows[i]["ticket_stat_id"]);
                        stat.ticket_stat_desc = dttypes.Rows[i]["ticket_stat_desc"].ToString();
                        ticketStat.Add(stat);
                    }
                }

                //show selected list item in Ticket Status
                List<SelectListItem> ticketStatuslist = new List<SelectListItem>();
                foreach (var item in ticketStat)
                {
                    ticketStatuslist.Add(new SelectListItem
                    {
                        Text = item.ticket_stat_desc.ToString(),
                        Value = item.ticket_stat_id.ToString()
                    });
                }
                response.TicketStatusModule = ticketStatuslist;




                //show selected list item in TicketType
                List<SelectListItem> ticketTypeSelectionlist = new List<SelectListItem>();
                foreach (var TicketTItem in ticketType)
                {
                    ticketTypeSelectionlist.Add(new SelectListItem
                    {
                        Text = TicketTItem.ticket_tp_desc.ToString(),
                        Value = TicketTItem.ticket_tp_id.ToString()
                    });
                }
                response.TicketTypeModule = ticketTypeSelectionlist;

                //show selected list item in TicketPriorities
                List<SelectListItem> PrioritySelectionlist = new List<SelectListItem>();
                foreach (var priorities in priority)
                {
                    PrioritySelectionlist.Add(new SelectListItem
                    {
                        Text = priorities.ticket_priority_stat_desc.ToString(),
                        Value = priorities.ticket_priority_stat_id.ToString()
                    });
                }
                response.PriorityModule = PrioritySelectionlist;





                if (Tickets.ticket_id > 0)
                {
                    response.EditTicketsdetail = Tickets;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.EditTicketsdetail = Tickets;
                    response.Status = "New";
                    response.ErrorMessage = "Tickets not found";
                    response.Statuscode = 0;
                }

            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Get Ticket/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Get Details/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
                return RedirectToAction("Error_Log", "Home");
            }
            return View(response);
        }
        #endregion

        #region Log page
        public ActionResult Logs(string Id)
        {
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.tickets_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.tickets_show_closed;
            return View();
        }
        [HttpPost]
        public JsonResult gridLogsloadData(int IsTicketId)
        { 
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                _TicketSubMenu(IsTicketId);
                DataView dvLogs = TableDefs.Ticket.GetTicketReportLog(ds, IsTicketId);
                string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var lst_ticket_log = JsonConvert.DeserializeObject<lst_ticket_log>(jsonData2);

                dsResult.Data = lst_ticket_log.tbl_ticket_log.ToList();
                dsResult.Total = lst_ticket_log.tbl_ticket_log.Count;
            }
            catch (System.Exception ex)
            {
                result.success = false;
                result.message = $"An error occurred retrieving request List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }


        public JsonResult EditLogPage(int IsTicketId, int Id)
        {
            //Declaring the variables
            tbl_ticket_log Ticketslogs = new tbl_ticket_log();
            DataSet ds = new DataSet();
            GlobalData Global = new GlobalData();
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                //return RedirectToAction("Index", "Login");

            }
            try
            {
                DataRow mr = TableDefs.Ticket.GetTicketLog(ds, Id);
                if (mr != null)
                {
                    Ticketslogs.ticket_log_id = Id;
                    Ticketslogs.ticket_id = Utility.ToInt(mr["ticket_id"]);
                    Ticketslogs.trk_ent_usr_id= Utility.ToInt(mr["trk_ent_usr_id"]);
                    Ticketslogs.trk_ent_dt = Convert.ToDateTime(mr["trk_ent_dt"]);
                    Ticketslogs.log_desc = mr["log_desc"].ToString();
                    Ticketslogs.log_action = mr["log_action"].ToString();
                    Ticketslogs.log_hours = Utility.ToInt(mr["log_hours"]);
                }
                var data = Ticketslogs;
                return Json(new { data = data });
            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Get Repairs/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Get Repairs/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
            }
            return Json(new { data = Ticketslogs });
        }


        [HttpPost]
        public GetTicketResponse Editlog(tbl_ticket_log request)
        {
            int userID = 0;
            GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            userID = data.CurrentUser.UserID;
            GetTicketResponse response = new GetTicketResponse();
            try
            {
                DBAccess.executeSQL("usp_ticket_log_update", request.ticket_log_id, request.log_hours, request.log_desc);
            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;

        }

            public GetTicketResponse logDelete(int ticket_log_id)
        {
            GetTicketResponse response = new GetTicketResponse();
            DataSet ds = new DataSet();

            try
            {
                DBAccess.executeSQL("usp_ticket_log_remove", ticket_log_id);
                response.Status = "Success";

            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region Actions page
        public ActionResult Actions(string Id)
        {
            GetTicketResponse response = new GetTicketResponse();
            ActionTickets Tickets = new ActionTickets();
            List<UserNameDropDown> UserNameDropdown = new List<UserNameDropDown>();
            List<TicketStatus> ticketStat = new List<TicketStatus>();
            List<GroupNameDropDown> GroupNameDropdown = new List<GroupNameDropDown>();
            List<dropLogType> dropLogType = new List<dropLogType>();
            response.Statuscode = 0;
            string ID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            GlobalData Global = new GlobalData();
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }
            try
            {
                DataSet ds = new DataSet();
                //request grid API call
                //request = requestAPI.GetrequestByID(ID);
                mr = TableDefs.Ticket.GetTicketsByID(ds, Utility.ToInt(ID.ToString()));
                if (mr != null)
                {
                    Tickets.ticket_id = Utility.ToInt(mr["ticket_id"]);
                    Tickets.CustomerID = Utility.ToInt(mr["cust_id"]);
                    Tickets.FacilityID = Utility.ToInt(mr["facility_id"]);

                    Tickets.GroupID = Utility.ToInt(mr["group_id"]);
                    Tickets.AssignedUserID = Utility.ToInt(mr["assigned_to_usr_id"]);
                    GlobalData.SetGlobalSessione(HttpContext.Session, "cust_id", Global);
                    GlobalData.SetGlobalSessione(HttpContext.Session, "facility_id", Global);

                    GlobalData cust_id = GlobalData.GetGlobalSessione(HttpContext.Session, "cust_id");
                    GlobalData facility_id = GlobalData.GetGlobalSessione(HttpContext.Session, "facility_id");

                    Tickets.log_tp_id = "log";
                    Tickets.TicketStatID = 20;
                    Tickets.ServiceDate = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    Tickets.Compentency = 0;
                }

                // Show User Name DropDown

                DataView dvUsername = TableDefs.User.GetUsersForDropDown(ds, false, true, "-- Not Selected --");
                if (dvUsername != null)
                {
                    DataTable dtUsername = dvUsername.ToTable();
                    for (int i = 0; i < dtUsername.Rows.Count; i++)
                    {
                        UserNameDropDown status = new UserNameDropDown();
                        status.usr_id = Utility.ToInt(dtUsername.Rows[i]["usr_id"]);
                        status.usr_name = dtUsername.Rows[i]["usr_name"].ToString();
                        UserNameDropdown.Add(status);
                    }
                }

                List<SelectListItem> UserNameSelectionlist = new List<SelectListItem>();
                foreach (var username in UserNameDropdown)
                {
                    UserNameSelectionlist.Add(new SelectListItem
                    {
                        Text = username.usr_name.ToString(),
                        Value = username.usr_name.ToString()
                    });
                }
                response.UserNameDropDownModule = UserNameSelectionlist;


                //Get Ticket Status API call
                DataView dvStatus = TableDefs.Ticket.GetTicketStatus(ds, true, "-- Select One --");
                if (dvStatus != null)
                {
                    DataTable dttypes = dvStatus.ToTable();
                    for (int i = 0; i < dttypes.Rows.Count; i++)
                    {
                        TicketStatus stat = new TicketStatus();
                        stat.ticket_stat_id = Utility.ToInt(dttypes.Rows[i]["ticket_stat_id"]);
                        stat.ticket_stat_desc = dttypes.Rows[i]["ticket_stat_desc"].ToString();
                        ticketStat.Add(stat);
                    }
                }


                List<SelectListItem> ticketStatuslist = new List<SelectListItem>();
                foreach (var item in ticketStat)
                {
                    ticketStatuslist.Add(new SelectListItem
                    {
                        Text = item.ticket_stat_desc.ToString(),
                        Value = item.ticket_stat_id.ToString()
                    });
                }
                response.TicketStatusModule = ticketStatuslist;

                //Show GroupName DropDown

                DataView dvGroupName = TableDefs.User.GetGroupsForDropDown(ds, false, true, "-- Not Selected --");
                if (dvGroupName != null)
                {
                    DataTable dtGroupName = dvGroupName.ToTable();
                    for (int i = 0; i < dtGroupName.Rows.Count; i++)
                    {
                        GroupNameDropDown status = new GroupNameDropDown();
                        status.group_id = Utility.ToInt(dtGroupName.Rows[i]["group_id"]);
                        status.group_name = dtGroupName.Rows[i]["group_name"].ToString();
                        GroupNameDropdown.Add(status);
                    }
                }
                List<SelectListItem> GroupNameSelectionlist = new List<SelectListItem>();
                foreach (var groupname in GroupNameDropdown)
                {
                    GroupNameSelectionlist.Add(new SelectListItem
                    {
                        Text = groupname.group_name.ToString(),
                        Value = groupname.group_name.ToString(),
                    });
                }
                response.GroupNameDropDownModule = GroupNameSelectionlist;
                // Log type DropDown 
                DataView dvLogType = TableDefs.Ticket.GetLogType(ds, false, "* Select Log Type *");
                if (dvLogType != null)
                {
                    DataTable dtLogType = dvLogType.ToTable();
                    for (int i = 0; i < dtLogType.Rows.Count; i++)
                    {
                        dropLogType status = new dropLogType();
                        status.ticket_log_tp_id = Utility.ToInt(dtLogType.Rows[i]["ticket_log_tp_id"]);
                        status.ticket_log_tp_desc = dtLogType.Rows[i]["ticket_log_tp_desc"].ToString();
                        dropLogType.Add(status);
                    }
                }
                List<SelectListItem> LogtypeSelectionlist = new List<SelectListItem>();
                foreach (var logtype in dropLogType)
                {
                    LogtypeSelectionlist.Add(new SelectListItem
                    {
                        Text = logtype.ticket_log_tp_desc.ToString(),
                        Value = logtype.ticket_log_tp_desc.ToString()
                    });
                }
                response.LogTypeDropdownModule = LogtypeSelectionlist;

                if (Tickets.ticket_id > 0)
                {
                    response.ActionTicketDetail = Tickets;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.ActionTicketDetail = Tickets;
                    response.Status = "New";
                    response.ErrorMessage = "Tickets not found";
                    response.Statuscode = 0;
                }

            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Get Action Page/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Get Action Page/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
                return RedirectToAction("Error_Log", "Home");
            }
            return View(response);
        }


        #region Add-Edit Action Page
        public GetTicketResponse AddEditAction(ActionTickets request)
        {
            int userID = 0;
            string userName;
            DataSet ds = new DataSet();
            GetTicketResponse response = new GetTicketResponse();

            try
            {
                if (ModelState.IsValid)
                {
                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    if (data != null)
                    {
                        userID = data.CurrentUser.UserID;
                        userName = data.CurrentUser.UserName;

                        if (request.ticket_id != 0)
                        {

                            DBAccess.executeSQL("usp_ticket_actions_update", userID, request.ticket_id, request.AssignedUserID, request.GroupID, request.add_log_entry
                              , request.ticket_log_tp_id, request.log_entry, request.time_worked);

                            if (request.Ticket == true)
                            {

                                if (request.GroupID1 != "-- Not Selected --")
                                {
                                    string Group = "ASSIGNED TO: GROUP:" + request.GroupID1 + " by " + userName + " " + DateTime.Now.ToString("ddd MMM  dd  HH:mm:ss yyyy.");
                                    DBAccess.executeSQL("usp_ticket_log_new", userID, request.ticket_id, request.log_tp_id, request.time_worked, Group);
                                }

                                if (request.AssignedUserID1 != "-- Not Selected --")
                                {
                                    string User = "ASSIGNED TO: USER:" + request.AssignedUserID1 + " by " + userName + " " + DateTime.Now.ToString("ddd MMM  dd  HH:mm:ss yyyy.");
                                    DBAccess.executeSQL("usp_ticket_log_new", userID, request.ticket_id, request.log_tp_id, request.time_worked, User);
                                }

                            }
                            if (request.LogEntry == true)
                            {
                                DBAccess.executeSQL("usp_ticket_log_new", userID, request.ticket_id, request.log_tp_id, request.time_worked, request.log_entry);
                            }

                            if (request.YankTicket == true)
                            {
                                string Yaak = "YANKED by " + userName + " " + DateTime.Now.ToString("ddd MMM  dd  HH:mm:ss yyyy") + ". " + request.log_entry;
                                DBAccess.executeSQL("usp_ticket_log_new", userID, request.ticket_id, request.log_tp_id, request.time_worked, Yaak);
                            }
                            if (request.Status == true)
                            {
                                DBAccess.executeSQL("usp_ticket_log_new", userID, request.ticket_id, request.log_tp_id, request.time_worked, request.log_entry);


                                DBAccess.executeSQL("usp_ticket_status_change", userID, request.ticket_id, request.TicketStatID, request.ServiceDate
                                                                 , request.Compentency);
                            }
                            response.Status = "Success";

                        }
                    }

                }

            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Edit Action/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Edit Action/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;


        }

        #endregion

        #endregion

        #region TicketInventory page
        public ActionResult TicketInventory(string Id)
        {
            //List<TicketType> ticketType = new List<TicketType>();
            GetTicketResponse response = new GetTicketResponse();
            InventoryTicket ticket = new InventoryTicket();
            DataSet ds = new DataSet();
            response.Statuscode = 0;
            string ID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            GlobalData Global = new GlobalData();
            //Globally to check the login status
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            try
            {
                mr = TableDefs.Ticket.GetTicketReport(ds, Utility.ToInt(ID.ToString()));
                if (mr != null)
                {

                    //ticket.Status = Utility.ToInt(mr["ticket_stat_id"]);
                    ticket.HospitalNumber = mr["hos_control_num"].ToString();
                    ticket.control = mr["control_num"].ToString();
                    ticket.Department = mr["dept_name"].ToString();
                    ticket.PreviouscontrolNumber = mr["previous_control_num"].ToString();
                    ticket.Rootclass = mr["prod_class_desc"].ToString();
                    ticket.SubClass = mr["prod_sub_class_desc"].ToString();
                    ticket.Model = mr["model"].ToString();
                    ticket.serial = mr["serial_no"].ToString();
                    
                    ticket.Status=  mr["inv_stat_desc"].ToString();
                    ticket.priority = mr["ticket_priority_stat_desc"].ToString();
                    ticket.ArrivedVia= mr["ticket_addmethod_desc"].ToString();
                   
                    ticket.LastPmCompleted = string.IsNullOrEmpty(mr["last_pm_dt"].ToString()) ? null : Convert.ToDateTime(mr["last_pm_dt"]).ToString("MM/dd/yyyy");
                    ticket.NextScheduledPm = string.IsNullOrEmpty(mr["next_pm_dt"].ToString()) ? null : Convert.ToDateTime(mr["next_pm_dt"]).ToString("MM/dd/yyyy");
                    //ticket.PmInterval = Utility.ToInt(mr["inv_pm_interval_tp_desc"]);
                    ticket.servicecovreage = mr["vendor_coverage"].ToString();

                    ticket.InstallDate = string.IsNullOrEmpty(mr["install_dt"].ToString()) ? null : Convert.ToDateTime(mr["install_dt"]).ToString("MM/dd/yyyy");
                    ticket.WarrentyExpires = string.IsNullOrEmpty(mr["warranty_exp_dt"].ToString()) ? null : Convert.ToDateTime(mr["warranty_exp_dt"]).ToString("MM/dd/yyyy");
                    

                    ticket.CallerName = mr["caller_name"].ToString();
                    ticket.CallerPhone = Utility.FormatPhone(mr["caller_phone"].ToString(), true);
                    ticket.ClosedDate = string.IsNullOrEmpty(mr["closed_dt"].ToString()) ? null : Convert.ToDateTime(mr["closed_dt"]).ToString("MM/dd/yyyy");
                    ticket.ServiceDate = string.IsNullOrEmpty(mr["service_dt"].ToString()) ? null : Convert.ToDateTime(mr["service_dt"]).ToString("MM/dd/yyyy");


                    ticket.CreatedBy= mr["created_by"].ToString();
                    ticket.AssignTo = mr["assigned_to"].ToString();
                    ticket.ClosedBy = mr["closed_by"].ToString();
                    ticket.Riskfactor = mr["risk_num"].ToString();
                    ticket.Manufacturer = mr["mfg_name"].ToString();
                    ticket.facility_name= mr["facility_name"].ToString();
                    ticket.cust_name = mr["cust_name"].ToString();
                    ticket.ticket_id = Utility.ToInt(mr["ticket_id"]);

                    ticket.Ownership = mr["inv_ownership_tp_desc"].ToString();
                    
                    ticket.inv_id = Utility.ToInt(mr["inv_id"]);
                    DataRow GetInventory = TableDefs.Inventory.GetInventory(ds, ticket.inv_id);
                    if (GetInventory != null)
                    {
                        ticket.Riskfactor = GetInventory["risk_num"].ToString();
                        ticket.SystemId = GetInventory["system_id"].ToString();
                        ticket.WarrentyStatus = GetInventory["inv_war_stat_desc"].ToString();
                        ticket.InvStatus = GetInventory["is_active"].ToString();
                        ticket.contract_po = GetInventory["contract_po"].ToString();
                        ticket.parts_po = GetInventory["parts_po"].ToString();
                    }
                    DataRow GetTicketType = TableDefs.Ticket.GetTicketsByID(ds, ticket.ticket_id);
                    if(GetTicketType != null)
                    {
                        ticket.ticket_tp_desc = GetTicketType["ticket_tp_desc"].ToString();
                        ticket.PmInterval = GetTicketType["inv_pm_interval_tp_desc"].ToString();
                    }

                }
                if (ticket.ticket_id > 0)
                {
                    response.InventoryTicketDetails = ticket;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.InventoryTicketDetails = ticket;
                    response.Status = "New";
                    response.ErrorMessage = "Tickets not found";
                    response.Statuscode = 0;
                }
            }

            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Inventory Details/Tickets", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
            }
            return View(response);

        }
        #endregion

        #region Repairs page
        public ActionResult Repairs(string Id)
        {
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.tickets_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.tickets_show_closed;
            return View();
        }
        [HttpPost]
        public JsonResult gridRepairloadData(int IsTicketId)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                _TicketSubMenu(IsTicketId);
                DataView dvTickets = TableDefs.Ticket.GetTicketReportCharges(ds, IsTicketId);
                string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var lst_tbl_ticket_check_tp = JsonConvert.DeserializeObject<lst_tbl_ticket_check_tp>(jsonData2);

                dsResult.Data = lst_tbl_ticket_check_tp.tbl_ticket_check_tp.ToList();
                dsResult.Total = lst_tbl_ticket_check_tp.tbl_ticket_check_tp.Count;
            }
            catch (System.Exception ex)
            {
                result.success = false;
                result.message = $"An error occurred retrieving request List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }


        #region Displaying the edited  Tickets records

        public JsonResult EditRepairPage(int IsTicketId, int Id)
        {
            //Declaring the variables
            tbl_ticket_check_tp Tickets = new tbl_ticket_check_tp();
            DataSet ds = new DataSet();
            GlobalData Global = new GlobalData();
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                //return RedirectToAction("Index", "Login");

            }
            try
            {
                DataView dvTickets = TableDefs.Ticket.GetTicketReportCharges(ds, IsTicketId);
                string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var lst_tbl_ticket_check_tp = JsonConvert.DeserializeObject<lst_tbl_ticket_check_tp>(jsonData2);
                Tickets = lst_tbl_ticket_check_tp.tbl_ticket_check_tp.Where(c => c.ticket_charge_id == Convert.ToInt32(Id)).FirstOrDefault();
                var data = Tickets;
                return Json(new { data = data });
            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Get Repairs/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Get Repairs/Tickets", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
            }
            return Json(new { data = Tickets });
        }

        #endregion


        #region Edit Repair
        [HttpPost]
        public GetTicketResponse PostRepair(tbl_ticket_check_tp request)
        {
            int userID = 0;
            GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            userID = data.CurrentUser.UserID;
            GetTicketResponse response = new GetTicketResponse();
            try
            {
                if (request.ticket_charge_id != 0)
                {

                    DBAccess.executeSQL("usp_ticket_charge_update",
                        userID,
                        request.ticket_charge_id,
                        request.ticket_id,
                        request.charge_tp_id,
                        request.qty,
                        request.cost,
                        request.charge,
                        request.part_no,
                        request.charge_desc,
                        request.quoted,
                        request.po);
                }
                else
                {
                    DBAccess.executeSQL("usp_ticket_charge_new", userID, request.ticket_id, request.charge_tp_id, request.qty, request.cost,
                        request.charge, request.part_no, request.charge_desc, request.quoted, request.po);
                }               
                response.Status = "Success";
            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;

        }
        #endregion

       
        [HttpGet]
        //DropDown
        public JsonResult RepairEditDropdown()
        {
            try
            {
                DataSet ds = new DataSet();
                List<ChargeTypeDropDown> chargeTypeDropdown = new List<ChargeTypeDropDown>();
                DataView dvchrg = TableDefs.Ticket.GetTicketChargeTypesForDropDown(ds, true, true, "* Select One *");
                if (dvchrg != null)
                {
                    DataTable dtchrgRepair = dvchrg.ToTable();
                    for (int i = 0; i < dtchrgRepair.Rows.Count; i++)
                    {
                        ChargeTypeDropDown status = new ChargeTypeDropDown();
                        status.charge_tp_id = Utility.ToInt(dtchrgRepair.Rows[i]["charge_tp_id"]);
                        status.charge_tp_desc = dtchrgRepair.Rows[i]["charge_tp_desc"].ToString();
                        chargeTypeDropdown.Add(status);
                    }
                }
                var data = chargeTypeDropdown.ToList();
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {

                throw;
            }

            return Json(new { data = 0 });
        }

        public GetTicketResponse RepairDelete(int ticket_charge_id)
        {
            GetTicketResponse response = new GetTicketResponse();
            DataSet ds = new DataSet();

            try
            {
                DBAccess.executeSQL("usp_ticket_charge_delete", ticket_charge_id);
                response.Status = "Success";

            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region TicketContacts page
        public ActionResult TicketContacts(string Id)
        {
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.tickets_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.tickets_show_closed;
            return View();
        }

        [HttpPost]
        public JsonResult LoadTicketContactsGrid(bool isactive, int IsTicketId)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            int Cust_contactID = 0;
            try
            {
                mr = TableDefs.Ticket.GetTicketsByID(ds, Utility.ToInt(IsTicketId));
                if (mr != null)
                {
                    Cust_contactID = Utility.ToInt(mr["cust_id"]);
                    DataView dvContact = TableDefs.Customer.GetCustomerContacts(ds, Cust_contactID, isactive);
                    string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                    var listContact = JsonConvert.DeserializeObject<lst_tbl_cust_contact>(jsonData2);

                    dsResult.Data = listContact.tbl_cust_contact.ToList().OrderBy(x => x.first_name);
                    dsResult.Total = listContact.tbl_cust_contact.Count;
                }
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "ContactsGrid/TicketContact", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Customer Contact List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region Notify page
        public ActionResult Notify(string Id)
        {
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.tickets_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.tickets_show_closed;
            return View();
        }

        [HttpPost]
        public JsonResult gridNotifyloadData(int IsTicketId)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                DataView dvTickets = TableDefs.Ticket.GetTicketNotify(ds, IsTicketId);
                string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var lst_tbl_ticket_notify = JsonConvert.DeserializeObject<lst_tbl_ticket_notify>(jsonData2);

                dsResult.Data = lst_tbl_ticket_notify.tbl_ticket_notify.ToList();
                dsResult.Total = lst_tbl_ticket_notify.tbl_ticket_notify.Count;
            }
            catch (System.Exception ex)
            {
                result.success = false;
                result.message = $"An error occurred retrieving request List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }

        [HttpGet]
        //DropDown
        public JsonResult NotifyDropdown()
        {
            try
            {
                DataSet ds = new DataSet();
                List<UserNameDropDown> UserNameDropdown = new List<UserNameDropDown>();
                DataView dvUsername = TableDefs.User.GetUsersForDropDown(ds, false, true, "* Select User *");
                if (dvUsername != null)
                {
                    DataTable dtUser = dvUsername.ToTable();
                    for (int i = 0; i < dtUser.Rows.Count; i++)
                    {
                        UserNameDropDown status = new UserNameDropDown();
                        status.usr_id = Utility.ToInt(dtUser.Rows[i]["usr_id"]);
                        status.usr_name = dtUser.Rows[i]["usr_name"].ToString();
                        UserNameDropdown.Add(status);
                    }
                }
                var data = UserNameDropdown.ToList();
                return Json(new { data = data });
            }
            catch (Exception)
            {

                throw;
            }

            return Json(new { data = 0 });
        }

        //Add Notify
        [HttpPost]
        public GetTicketResponse AddNotify(tbl_ticket_notify request)
        {
            int userID = 0;
            GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            userID = data.CurrentUser.UserID;
            GetTicketResponse response = new GetTicketResponse();
            try
            {
                if (userID != 0)
                {
                    string result = Utility.ToString(DBAccess.executeScalar("usp_ticket_notify_new", request.ticket_id, userID, request.notify_id));
                }
                 response.Status = "Success";
            }
            catch (System.Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "Add/Notify/Tickets", error.errormsg);
                HttpContext.Session.SetString("error_log", error.errormsg);
                response.ErrorMessage = ex.Message;
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;

        }

        //Notify Delete
        public GetTicketResponse DeleteNotify(int ticket_id,string notify_email)
        {
            GetTicketResponse response = new GetTicketResponse();
            DataSet ds = new DataSet();

            try
            {
                DBAccess.executeSQL("usp_ticket_notify_remove", ticket_id, notify_email);
                response.Status = "Success";

            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region Testing page
        public ActionResult Testing(string Id)
        {
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.tickets_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.tickets_show_closed;
            return View();
        }

        public JsonResult LoadTestResults(int ticket_id)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                _TicketSubMenu(ticket_id);

                DataView dvFile = TableDefs.Ticket.GetTicketReportTestResults(ds, ticket_id);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listResult = JsonConvert.DeserializeObject<lst_tbl_test_results>(jsonData);

                dsResult.Data = listResult.tbl_test_results.ToList().OrderBy(x => x.test_results_id);
                dsResult.Total = listResult.tbl_test_results.Count;

            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Test Result Grid Load/Tickets", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving TestResults List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }

        public JsonResult LoadHistoryResults(int ticket_id)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                _TicketSubMenu(ticket_id);

                DataView dvFile = TableDefs.Ticket.GetTicketReportTestResultsHistory(ds, ticket_id);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);

                var listResult = JsonConvert.DeserializeObject<lst_tbl_test_results>(jsonData);


                dsResult.Data = listResult.tbl_test_results.ToList().OrderBy(x => x.test_results_id);
                dsResult.Total = listResult.tbl_test_results.Count;


            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "History Test Result Grid Load/Tickets", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving TestResults List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }

        public GetTicketResponse TicketTestResultsDelete(int usr_id, int test_results_id)
        {
            GetTicketResponse response = new GetTicketResponse();
            DataSet ds = new DataSet();

            try
            {
                DBAccess.executeSQL("usp_ticket_test_results_delete", usr_id, test_results_id);
                response.Status = "Success";

            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Test Result Delete/Tickets", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }

        [HttpGet]
        //DropDown
        public JsonResult TestTypeDropdown()
        {
            try
            {
                DataSet ds = new DataSet();
                List<tbl_test> TestTypeDropdown = new List<tbl_test>();
                DataView dvTest = TableDefs.Ticket.GetTicketTestTypesForDropDown(ds, true, true, "* Select Testing *");
                if (dvTest != null)
                {
                    DataTable dtTest = dvTest.ToTable();
                    for (int i = 0; i < dtTest.Rows.Count; i++)
                    {
                        tbl_test status = new tbl_test();
                        status.test_id = Utility.ToInt(dtTest.Rows[i]["test_id"]);
                        status.test_type = dtTest.Rows[i]["test_type"].ToString();
                        TestTypeDropdown.Add(status);
                    }
                }
                var data = TestTypeDropdown.ToList();
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "TestType DropDown/Tickets", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
            }

            return Json(new { data = 0 });
        }

        [HttpPost]
        public GetInventoiesResponse SaveTestResult(tbl_test_results request)
        {
            int userID = 0;
            GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            userID = data.CurrentUser.UserID;
            GetInventoiesResponse response = new GetInventoiesResponse();
            try
            {
                if (request.ticket_id != 0)
                {
                    DBAccess.executeSQL("usp_ticket_test_results_new", userID, request.ticket_id, request.test_id, request.param1, request.param2, request.param3, request.test_dt, request.test_equip);
                    response.Status = "Success";
                }
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Add Test Result/Tickets", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;

        }

        public string testResults(int id)
        {
            DataSet ds = new DataSet();
            string type = "";
            mr = TableDefs.Ticket.GetTest(ds, id);
            if (mr != null)
            {
                type = mr["display_header"].ToString();
            }
            return type;
        }

        #endregion

        #region Attachment page
        public ActionResult Attachment(string Id)
        {
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.tickets_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.tickets_show_closed;
            return View();
        }
        public JsonResult LoadFileAttachment(int ticket_id)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                _TicketSubMenu(ticket_id);

                DataView dvFile = TableDefs.General.GetFileAttachments(ds, ticket_id, (int)FileAttachType.Ticket);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listFile = JsonConvert.DeserializeObject<lst_tbl_file_attach>(jsonData);

                dsResult.Data = listFile.tbl_file_attach.ToList().OrderBy(x => x.file_attach_id);
                dsResult.Total = listFile.tbl_file_attach.Count;

                GlobalData.SetGlobalSessione(HttpContext.Session, "file_attach_id", Global);
            }
            catch (Exception ex)
            {
                result.success = false;
                result.message = $"An error occurred retrieving Inventory List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }

        public GetTicketResponse TicketAttachmentDelete(int file_attach_id)
        {
            GetTicketResponse response = new GetTicketResponse();
            DataSet ds = new DataSet();

            try
            {
                DBAccess.executeSQL("usp_file_attach_remove", file_attach_id);
                response.Status = "Success";

            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }

        [HttpPost]
        private string GetImagePath(IFormFile image)
        {
            string VirtualPath = $@"/attachments/{DateTime.Now:yyyy}/{DateTime.Now:MM}/{DateTime.Now:dd}/";

            string photo1Name = string.Empty;
            IFormFile Photo1 = image;


            var extension = "";
            var ImagePath = "";
            if (string.Equals(image.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) || string.Equals(image.ContentType, "application/pdf", StringComparison.OrdinalIgnoreCase)
                || string.Equals(image.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) || string.Equals(image.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) || string.Equals(image.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                var millsecond = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                var FileName = Path.GetFileName(image.FileName);

                var Filename = ContentDispositionHeaderValue.Parse(image.ContentDisposition).FileName.Trim('"');
                //  Filename = hasSpecialChar(Filename);

                extension = Path.GetExtension(Filename);
                Filename = Path.GetFileNameWithoutExtension(Filename)+ extension;
                var filePath = Filename;
                ImagePath = VirtualPath + filePath;

                if (!Directory.Exists(_hostingEnvironment.WebRootPath + VirtualPath))
                {
                    Directory.CreateDirectory(_hostingEnvironment.WebRootPath + VirtualPath);
                }

                // normal code
                if (System.IO.File.Exists(_hostingEnvironment.WebRootPath + ImagePath))
                    System.IO.File.Delete(_hostingEnvironment.WebRootPath + ImagePath);
                using (FileStream fs = System.IO.File.Create(_hostingEnvironment.WebRootPath + ImagePath))
                {
                    Photo1.CopyTo(fs);
                    fs.Flush();
                }
            }
            return ImagePath;
        }

        [HttpPost]
        public GetTicketResponse uploadFileAttachments(tbl_file_attach request)
        {
            GetTicketResponse response = new GetTicketResponse();
            DataSet ds = new DataSet();
            int userID = 0;
            try
            {
                GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

                double fileSizeKB = request.Image_path.Length / 1024;

                var Logo = GetImagePath(request.Image_path);
                var Files = request.Image_path;
                if (Files != null)
                {
                    userID = data.CurrentUser.UserID;
                    var FileName = Path.GetFileName(Logo);
                    DBAccess.executeSQL("usp_file_attach_new", userID, (int)FileAttachType.Ticket, request.ticket_id,FileName, fileSizeKB > 0 ?(fileSizeKB + " "+"KB") :(request.Image_path.Length + " " + "bytes"), Logo);

                }
                response.Status = "Success";

            }
            catch (Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
 
        #endregion

        #region Audits page
        public ActionResult Audits(string Id)
        {
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.tickets_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.tickets_show_closed;
            return View();
        }
        [HttpPost]
        public JsonResult gridAuditloadData(int IsTicketId)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                _TicketSubMenu(IsTicketId);
                DataView dvAudit = TableDefs.Ticket.GetTicketAudit(ds, IsTicketId);
                string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var lst_tbl_ticket_audit = JsonConvert.DeserializeObject<lst_tbl_ticket_audit>(jsonData2);

                dsResult.Data = lst_tbl_ticket_audit.tbl_ticket_audit.ToList();
                dsResult.Total = lst_tbl_ticket_audit.tbl_ticket_audit.Count;
            }
            catch (System.Exception ex)
            {
                result.success = false;
                result.message = $"An error occurred retrieving request List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion    

        public void _TicketSubMenu(int ticket_id)
        {
            DataSet ds = new DataSet();
            DataRow customerrow = null;
            DataRow facilityrow = null;
            DataRow userrow = null;
            DataRow row = null;
            try
            {
                row = TableDefs.Ticket.GetTicketsByID(ds, Utility.ToInt(ticket_id));
                if (row != null)
                {
                    HttpContext.Session.SetString("IsTicketId", ticket_id.ToString());
                    HttpContext.Session.SetString("IsTicket_ticket_summary", row["summary"].ToString());
                    HttpContext.Session.SetString("IsTicket_vendor_name_1", row["vendor_name_1"].ToString());
                    HttpContext.Session.SetString("IsTicket_vendor_name_2", row["vendor_name_2"].ToString());
                    HttpContext.Session.SetString("IsTicket_ticket_stat_desc", row["ticket_stat_desc"].ToString());
                    HttpContext.Session.SetString("IsTicket_ticket_tp_desc", row["ticket_tp_desc"].ToString());
                    HttpContext.Session.SetString("IsTicket_ticket_priority_stat_desc", row["ticket_priority_stat_desc"].ToString());
                    HttpContext.Session.SetString("IsTicket_control_num", row["control_num"].ToString());
                    HttpContext.Session.SetString("IsTicket_serial_no", row["serial_no"].ToString());
                    HttpContext.Session.SetString("IsTicket_mfg_name", row["mfg_name"].ToString());
                    HttpContext.Session.SetString("IsTicket_model", row["model"].ToString());
                    HttpContext.Session.SetString("IsTicket_prod_class_desc", row["prod_class_desc"].ToString());
                    HttpContext.Session.SetString("IsTicket_prod_sub_class_desc", row["prod_sub_class_desc"].ToString());
                    HttpContext.Session.SetString("IsTicket_inv_pm_interval_tp_desc", row["inv_pm_interval_tp_desc"].ToString());
                    HttpContext.Session.SetString("IsTicket_last_activity_dt", row["last_activity_dt"].ToString());
                    HttpContext.Session.SetString("IsTicket_next_activity", row["next_activity"].ToString());
                    HttpContext.Session.SetString("IsTicket_dept_name", row["dept_name"].ToString());
                    HttpContext.Session.SetString("IsTicket_service_dt", string.IsNullOrEmpty(row["service_dt"].ToString()) ? "" : Convert.ToDateTime(row["service_dt"]).ToString("MM'/'dd'/'yyyy"));

                    DataView dvTickets = TableDefs.Ticket.GetTicketReportCharges(ds, ticket_id);
                    DataView dvLogs = TableDefs.Ticket.GetTicketReportLog(ds, ticket_id);
                    DataView dvFile = TableDefs.General.GetFileAttachments(ds, ticket_id, (int)FileAttachType.Ticket);
                    DataView dvAudit = TableDefs.Ticket.GetTicketAudit(ds, ticket_id);

                    string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                    var lst_ticket_log = JsonConvert.DeserializeObject<lst_ticket_log>(jsonData2);
                    dsResult.Data = lst_ticket_log.tbl_ticket_log.ToList();
                    dsResult.Total = lst_ticket_log.tbl_ticket_log.Count;
                    HttpContext.Session.SetString("LogCount", dsResult.Total.ToString());

                    HttpContext.Session.SetString("RepairCount", dvTickets.Count.ToString());
                    HttpContext.Session.SetString("AttachmentCount", dvFile.Count.ToString());
                    HttpContext.Session.SetString("AuditCount", dvAudit.Count.ToString());

                    customerrow = TableDefs.Customer.GetCustomer(ds, Utility.ToInt(row["cust_id"]));
                    facilityrow = TableDefs.Facility.GetFacility(ds, Utility.ToInt(row["facility_id"]));
                    userrow = TableDefs.User.GetUser(ds, Utility.ToInt(row["assigned_to_usr_id"]));
                    if (customerrow != null)
                        HttpContext.Session.SetString("IsTicket_Customer", customerrow["cust_name"].ToString());
                    if (facilityrow != null)
                        HttpContext.Session.SetString("IsTicket_Facility", facilityrow["facility_name"].ToString());
                    if (userrow != null)
                        HttpContext.Session.SetString("IsTicket_User", userrow["usr_name"].ToString());

                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        
    }
}
