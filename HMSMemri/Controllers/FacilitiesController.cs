﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.Text;
using ClosedXML.Excel;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace HMSMemri.Controllers
{
    public class FacilitiesController : IMCPBase
    {
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        protected DataRow mr = null;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;

        [Obsolete]
        public FacilitiesController(IOptions<BaseSettings.ServerVar> serverVar, IHostingEnvironment hostingEnvironment) : base(serverVar)
        {
            _serverVar = serverVar;
            _hostingEnvironment = hostingEnvironment;
        }

        #region Facility Index page
        /// <summary>
        /// Summary for Facility Index page
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        public IActionResult Index()
        {
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }

            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.facilities_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.facilities_show_closed;

            // application's base path
            string contentRootPath = _hostingEnvironment.ContentRootPath;

            // application's publishing path
            string webRootPath = _hostingEnvironment.WebRootPath;

            return View();
        }
        #endregion

        #region AddEditFacility
        public ActionResult AddEditFacility(string Id)
        {
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }

            GetFacilityResponse response = new GetFacilityResponse();
            Facility facility = new Facility();
            List<dropFacilityStatus> facilityStatus = new List<dropFacilityStatus>();
            List<dropCustomer> CustomerStatus = new List<dropCustomer>();
            List<States> States = new List<States>();
            response.Statuscode = 0;

            try
            {
                string IDcheck = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            }
            catch (Exception)
            {

                return View("~/Views/Shared/Error_EditUrl.cshtml");
            }

            try
            {
                string FacilityID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
                int ID = Convert.ToInt32(FacilityID);
                GlobalData Global = new GlobalData();
                GlobalData cust_id = GlobalData.GetGlobalSessione(HttpContext.Session, "CustomerSession");
                DataSet ds = new DataSet();
                //Facility
                DataRow mr = TableDefs.Facility.GetFacility(ds, ID);
                if (mr != null)
                {
                    Global.facility_id = Utility.ToInt(mr["facility_id"]);
                    Global.cust_id = Utility.ToInt(mr["cust_id"]);

                    GlobalData.SetGlobalSessione(HttpContext.Session, "facility_id", Global);
                    GlobalData.SetGlobalSessione(HttpContext.Session, "cust_id", Global);

                    GlobalData facility_id = GlobalData.GetGlobalSessione(HttpContext.Session, "facility_id");

                    facility.FacilityId = Utility.ToInt(mr["facility_id"]);
                    facility.cust_id = Utility.ToInt(mr["cust_id"]);
                    facility.FacilityName = mr["facility_name"].ToString();
                    facility.FacilityStatus = Utility.ToInt(mr["facility_stat_id"]);
                    facility.Address1 = mr["address1"].ToString();
                    facility.Address2 = mr["address2"].ToString();
                    facility.City = mr["city"].ToString();
                    facility.State = mr["st"].ToString();
                    facility.ZipCode = mr["zip"].ToString();
                    facility.Country = mr["country"].ToString();
                    facility.PhoneNumber = Utility.FormatPhone(mr["phone"].ToString(), true);
                    facility.Fax = Utility.FormatPhone(mr["fax"].ToString(), true);
                    facility.Notes = mr["notes"].ToString();
                    facility.Tech_id = Utility.ToInt(mr["tech_id"]);
                    facility.Sales_rep_id = Utility.ToInt(mr["sales_rep_id"]);
                    facility.IsActive = Utility.ToBool(mr["is_active"], false);
                    facility.Facility_stat_desc = mr["facility_stat_desc"].ToString();
                    facility.Inventory_cnt = Utility.ToInt(mr["inventory_cnt"]);
                    facility.TicketCount = Utility.ToInt(mr["ticket_cnt"]);
                    facility.OpenTicketCount = Utility.ToInt(mr["open_ticket_cnt"]);
                    facility.ContractCount = Utility.ToInt(mr["contract_cnt"]);
                }
                else
                {
                    if (cust_id == null)
                    {
                        facility.cust_id =0;
                    }
                    else
                    {
                        facility.cust_id = cust_id.cust_id;
                    }

                }

                //Facility Status
                DataView dvFacilityStatus = TableDefs.Facility.GetFacilityStatusForDropDown(ds, true, "* Select One *");
                if (dvFacilityStatus != null)
                {
                    DataTable dtFacilityStatus = dvFacilityStatus.ToTable();
                    for (int i = 0; i < dtFacilityStatus.Rows.Count; i++)
                    {
                        dropFacilityStatus status = new dropFacilityStatus();
                        status.facility_stat_id = Utility.ToInt(dtFacilityStatus.Rows[i]["facility_stat_id"]);
                        status.facility_status_desc = dtFacilityStatus.Rows[i]["facility_stat_desc"].ToString();
                        facilityStatus.Add(status);
                    }
                }

                List<SelectListItem> Facilityselectionlist = new List<SelectListItem>();
                foreach (var item in facilityStatus)
                {
                    Facilityselectionlist.Add(new SelectListItem
                    {
                        Text = item.facility_status_desc,
                        Value = item.facility_stat_id.ToString()
                    });
                }

                response.FacilityStatusModule = Facilityselectionlist;

                //State API call
                DataView dvStates = TableDefs.General.GetStatesForDropDown(ds, true, "* Select One *");
                if (dvStates != null)
                {
                    DataTable dtStates = dvStates.ToTable();
                    for (int i = 0; i < dtStates.Rows.Count; i++)
                    {
                        States state = new States();
                        state.abbr = Convert.ToString(dtStates.Rows[i]["abbr"]);
                        state.st = dtStates.Rows[i]["st"].ToString();
                        States.Add(state);
                    }
                }
                //show selected list item in State
                List<SelectListItem> stateselectionlist = new List<SelectListItem>();
                foreach (var stateItem in States)
                {
                    stateselectionlist.Add(new SelectListItem
                    {
                        Text = stateItem.st.ToString(),
                        Value = stateItem.abbr.ToString()
                    });
                }
                response.StateModule = stateselectionlist;

                // Customer Dropdown

                //Facility Status
                DataView dvCustomerFacility = TableDefs.Customer.GetCustomersForDropDowns(ds);
                if (dvCustomerFacility != null)
                {
                    DataTable dtCustomer = dvCustomerFacility.ToTable();
                    for (int i = 0; i < dtCustomer.Rows.Count; i++)
                    {
                        dropCustomer customer = new dropCustomer();
                        customer.cust_id = Utility.ToInt(dtCustomer.Rows[i]["cust_id"]);
                        customer.cust_name = dtCustomer.Rows[i]["cust_name"].ToString();
                        CustomerStatus.Add(customer);
                    }
                }

                List<SelectListItem> Customerselectionlist = new List<SelectListItem>();
                foreach (var item in CustomerStatus)
                {
                    Customerselectionlist.Add(new SelectListItem
                    {
                        Text = item.cust_name,
                        Value = item.cust_id.ToString()
                    });
                }

                response.CustomerDropdownModule = Customerselectionlist;

                if (facility.FacilityId > 0)
                {
                    response.facilitydetail = facility;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.facilitydetail = facility;
                    response.Status = "New";
                    response.ErrorMessage = "Customer not found";
                    response.Statuscode = 0;
                }

            }
            catch (System.Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "Get Facilities/Facilities", error.errormsg);
                HttpContext.Session.SetString("error_log", error.errormsg);
                response.Statuscode = 0;
                response.Status = "Failure";
                return RedirectToAction("Error_Log", "Home");
            }
            return View(response);
        }
        #endregion

        #region Add Edit functionality
        [HttpPost]
        public GetFacilityResponse AddEditFacilities(Facility Facility)
        {
            int userID = 0;
            DateTime Contract_end_dt = new DateTime(2025, 1, 1);
            GetFacilityResponse response = new GetFacilityResponse();
            try
            {
                if (ModelState.IsValid)
                {
                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    if (data != null)
                    {
                        userID = data.CurrentUser.UserID;
                        string phoneNumber = Utility.FormatPhone(Facility.PhoneNumber, false);
                        string faxNumber = Utility.FormatPhone(Facility.Fax, false);
                        int? TechID = Facility.Tech_id != null ? Facility.Tech_id : 0;
                        int? salesrep_id = Facility.Sales_rep_id != null ? Facility.Sales_rep_id : 0;
                        if (Facility.FacilityId == 0)
                        {
                            DBAccess.executeSQL("usp_facility_new", userID, Facility.cust_id, Facility.FacilityName, Facility.FacilityStatus, Facility.Address1, Facility.Address2, Facility.City, Facility.State, Facility.ZipCode, "US", phoneNumber, faxNumber, Facility.Notes, TechID, salesrep_id);
                            RedirectToAction("Index", "Facilities");
                        }
                        else
                        {
                            DBAccess.executeSQL("usp_facility_update", userID, Facility.FacilityId, Facility.cust_id, Facility.FacilityName, Facility.FacilityStatus, Facility.Address1, Facility.Address2, Facility.City, Facility.State, Facility.ZipCode, Facility.Country, phoneNumber, faxNumber, Facility.Notes, TechID, salesrep_id);
                            RedirectToAction("Index", "Facilities");
                        }
                    }
                    response.Status = "Success";
                }
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Add-Edit/Facilities", ex.Message);
                HttpContext.Session.SetString("error_log",ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }

        #endregion

        #region Load Facility Grid

        [HttpPost]
        public JsonResult LoadFacilitesGrid(bool isactive, bool toggleActive)
        {
            LoadState loadState = new LoadState();
            GetManufacturerResponse response = new GetManufacturerResponse();
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();
            GlobalData Custid = GlobalData.GetGlobalSessione(HttpContext.Session, "CustomerSession");
            
            try
            {
                mr = TableDefs.User.GetUserGrid(ds, userID.CurrentUser.UserID, 20);
                if (mr != null)
                {
                    if (mr.Table.Rows.Count > 0)
                    {
                        loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                        loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                        loadState.json_col_ord = mr["json_col_ord"].ToString();
                        response.loadState = loadState;
                        dsResult.Errors = "true";

                    }
                }
                if (Custid == null)
                {
                    DataView dvFacility = TableDefs.Facility.Getfacilities(ds2, isactive);
                    string jsonData2 = JsonConvert.SerializeObject(ds2, Formatting.Indented);
                    var listFacility = JsonConvert.DeserializeObject<lst_tbl_facility>(jsonData2);

                    dsResult.Data = listFacility.tbl_facility.ToList().OrderBy(x => x.facility_name);
                    dsResult.Total = listFacility.tbl_facility.Count;
                }
                else
                {
                    if (Custid.cust_id != null)
                    {

                        DataView dvFacility1 = TableDefs.Facility.GetFacilityByCustomer(ds2, Custid.cust_id, isactive);                     
                        string jsonData2 = JsonConvert.SerializeObject(ds2, Formatting.Indented);
                        var listFacility = JsonConvert.DeserializeObject<lst_tbl_facility>(jsonData2);


                        dsResult.Data = listFacility.tbl_facility.ToList().OrderBy(x => x.facility_name);
                        dsResult.Total = listFacility.tbl_facility.Count;
                    }

                }
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Grid Load/Facilities", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Customer List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region Facility Export List
        [HttpPost]
        [Obsolete]
        public GetFacilityResponse ExportFacilities(List<string> FacilityList)
        {
            DataSet ds = new DataSet();
            GetFacilityResponse response = new GetFacilityResponse();

            try
            {
                if (FacilityList != null)
                {
                    if (FacilityList != null && FacilityList.Count > 0)
                    {
                        //Set File Name and Path
                        string FilePath = Path.Combine(_hostingEnvironment.ContentRootPath, "wwwroot/ExportFile/");
                        string docPath = Path.Combine(FilePath, "Facilities/");
                        if (Directory.Exists(docPath))
                        {
                            DirectoryInfo di = new DirectoryInfo(docPath);
                            FileInfo[] files = di.GetFiles();
                            foreach (FileInfo file in files)
                            {
                                file.Delete();
                            }
                        }

                        string file_name = $"facilities_{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";
                        string pathFileName = docPath + file_name;

                        if (!Directory.Exists(FilePath))
                        {
                            Directory.CreateDirectory(FilePath);
                        }

                        // Path Not exists then create new file path
                        if (!Directory.Exists(docPath))
                        {
                            Directory.CreateDirectory(docPath);
                        }
                        //File name  exists then Delete the  file
                        if (System.IO.File.Exists(pathFileName))
                        {
                            System.IO.File.Delete(docPath);
                        }

                        //convert list to string
                        string facilitylist = String.Join<string>(",", FacilityList);
                        DataTable dtCustomers = TableDefs.Facility.GetFacilitiesForExport(ds, facilitylist).ToTable();

                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dtCustomers);
                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(pathFileName);
                                response.Status = "Success";
                                response.ExcelLink = "/ExportFile/Facilities/" + file_name;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Export/facilities", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
            }
            return response;
        }

        #endregion

        #region Facility Inventory Grid
        //Facility Inventory Grid
        public JsonResult LoadFacilityInventoryGrid(int facility_id, bool isactive)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                DataView dvFacility = TableDefs.Inventory.GetInventoryByFacility(ds, facility_id, isactive);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listInvertory = JsonConvert.DeserializeObject<lst_tbl_FacilityInventory>(jsonData);

                dsResult.Data = listInvertory.tbl_inv.ToList().OrderBy(x => x.serial_no);
                dsResult.Total = listInvertory.tbl_inv.Count;
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "LoadFacilityInv/Facilities", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Inventory List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region  Facilities dropdown for Contracts
        //Facilities dropdown for Contracts
        [HttpPost]
        public JsonResult LoadFacilitiesDropdown(bool isactive, bool toggleActive,int custID)
        {
            List<tbl_facility> facilityList = new List<tbl_facility>();
            List<tbl_facility> facilityListDrop = new List<tbl_facility>();
            LoadState loadState = new LoadState();
            GetFacilityResponse response = new GetFacilityResponse();
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();

            try
            {
                // get the value for sp, API Call
                DataView dvCustomer = TableDefs.Facility.GetFacilityByCustomerForDropDown(ds, custID, true, "All", isactive);
                string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listFacility = JsonConvert.DeserializeObject<lst_tbl_facility>(jsonData2);

                facilityListDrop = listFacility.tbl_facility.Where(x => x.facility_id == 0).ToList();
                listFacility.tbl_facility.RemoveAt(0);
                facilityList = listFacility.tbl_facility.ToList().OrderBy(x => x.facility_name).ToList();
                facilityList = facilityListDrop.Concat(facilityList).ToList();

                dsResult.Data = facilityList;
                dsResult.Total = facilityList.Count;
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Facility Drop/Facilities", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Customer List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region Session Clear
        public void CustFacilitySessionclear()
        {
            GlobalData.SetGlobalSessione(HttpContext.Session, "facility_id", null);
            GlobalData.SetGlobalSessione(HttpContext.Session, "cust_id", null);
            GlobalData.SetGlobalSessione(HttpContext.Session, "CustomerSession", null);


        }
        #endregion

        #region Customer Login Dropdown
        public JsonResult LoadFacilitiesDropdownbycustomerLoign(bool isactive, bool toggleActive)
        {
            List<tbl_facility> facilityList = new List<tbl_facility>();
            List<tbl_facility> facilityListDrop = new List<tbl_facility>();
            GetFacilityResponse response = new GetFacilityResponse();
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();

            try
            {
                GlobalData CustomerDetails = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");

                int cusId = CustomerDetails?.CurrentCust.cust_id != null ? CustomerDetails.CurrentCust.cust_id : 0;

                // get the value for sp, API Call
                DataView dvCustomer = TableDefs.Facility.GetFacilityByCustomerForDropDown(ds, cusId, true, "All", isactive);
                string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listFacility = JsonConvert.DeserializeObject<lst_tbl_facility>(jsonData2);

                facilityListDrop = listFacility.tbl_facility.Where(x => x.facility_id == 0).ToList();
                listFacility.tbl_facility.RemoveAt(0);
                facilityList = listFacility.tbl_facility.ToList().OrderBy(x => x.facility_name).ToList();
                facilityList = facilityListDrop.Concat(facilityList).ToList();

                dsResult.Data = facilityList;
                dsResult.Total = facilityList.Count;
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "CustLoginDrop/Facilities", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Customer List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }

        }
        #endregion

        #region SaveStateData
        [HttpPost]
        public IActionResult SaveStateData(int usr_id, int grid_id, string json_col_ord)
        {
            GetFacilityResponse response = new GetFacilityResponse();
            DBAccess.executeSQL("usp_usr_grid_col_update", usr_id, grid_id, json_col_ord);
            response.Status = "Success";
            return Json(response);
        }
        #endregion

        #region Load the Facilities State
        public IActionResult LoadFacilitiesState(int usr_id, int grid_id)
        {
            LoadState loadState = new LoadState();
            DataSet ds = new DataSet();
            GetFacilityResponse response = new GetFacilityResponse();

            mr = TableDefs.User.GetUserGrid(ds, usr_id, grid_id);
            if (mr != null)
            {
                loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                loadState.json_col_ord = mr["json_col_ord"].ToString();
            }
            if (loadState.usr_id > 0)
            {
                response.loadState = loadState;
                response.Statuscode = 1;
                response.Status = "Success";
            }
            else
            {
                response.loadState = loadState;
                response.Status = "New";
                response.ErrorMessage = "Facilities not found";
                response.Statuscode = 0;
            }
            return Json(response);
        }
        #endregion

        #region Ticket DropDown
        public JsonResult TicketFacilityDropDown(int custID)
        {
            List<tbl_facility> facilityList = new List<tbl_facility>();
            List<tbl_facility> facilityListDrop = new List<tbl_facility>();
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                DataView dvFacility = TableDefs.Facility.GetFacilityByCustomerForDropDown(ds, custID, true, " * Select One *", true);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listfacility = JsonConvert.DeserializeObject<lst_tbl_facility>(jsonData);

                facilityListDrop = listfacility.tbl_facility.Where(x => x.facility_id == 0).ToList();
                listfacility.tbl_facility.RemoveAt(0);
                facilityList = listfacility.tbl_facility.ToList().OrderBy(x => x.facility_name).ToList();
                facilityList = facilityListDrop.Concat(facilityList).ToList();

                dsResult.Data = facilityList;
                dsResult.Total = facilityList.Count;
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "TicketDropDown/Facilities", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Inventory List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion
    }

}
