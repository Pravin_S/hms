﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Data;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using System.Linq;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using ClosedXML.Excel;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace HMSMemri.Controllers
{
    public class VendorsController : IMCPBase
    {
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        protected DataRow mr = null;
        [Obsolete]
        public VendorsController(IOptions<BaseSettings.ServerVar> serverVar, IHostingEnvironment hostingEnvironment) : base(serverVar)
        {
            _serverVar = serverVar;
            _hostingEnvironment = hostingEnvironment;
        }
        #region Index UI
        /// <summary>
        ///   Index page(UI) 
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.customer_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.customer_show_closed;
            return View();

        }
        #endregion

        #region Get Vendor page
        /// <summary>
        /// Get Vendor page
        /// </summary>
        /// <param name="request"> Get Vendor page </param>
        /// <returns></returns>
        public ActionResult AddEditVendor(string Id)
        {
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }
            try
            {
                string IDcheck = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            }
            catch (Exception)
            {

                return View("~/Views/Shared/Error_EditUrl.cshtml");
            }

            GetVendorResponse response = new GetVendorResponse();
            Vendor vendor = new Vendor();
            DataSet ds = new DataSet();
            List<States> States = new List<States>();
            string VendorID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            int ID = Convert.ToInt32(VendorID);
            try
            {
                //State API call
                DataView dvStates = TableDefs.General.GetStatesForDropDown(ds, true, "* Select One *");
                if (dvStates != null)
                {
                    DataTable dtStates = dvStates.ToTable();
                    for (int i = 0; i < dtStates.Rows.Count; i++)
                    {
                        States state = new States();
                        state.abbr = Convert.ToString(dtStates.Rows[i]["abbr"]);
                        state.st = dtStates.Rows[i]["st"].ToString();
                        States.Add(state);
                    }
                }
                //show selected list item in State
                List<SelectListItem> stateselectionlist = new List<SelectListItem>();
                foreach (var stateItem in States)
                {
                    stateselectionlist.Add(new SelectListItem
                    {
                        Text = stateItem.st.ToString(),
                        Value = stateItem.abbr.ToString()
                    });
                }
                response.StateModule = stateselectionlist;
                GlobalData Global = new GlobalData();
                DataRow Getvendor = TableDefs.Vendor.GetVendors(ds, ID);
                if (Getvendor != null)
                {
                    Global.vendor_id = Utility.ToInt(Getvendor["vendor_id"]);
                    GlobalData.SetGlobalSessione(HttpContext.Session, "vendor_id", Global);
                    GlobalData vendor_id = GlobalData.GetGlobalSessione(HttpContext.Session, "vendor_id");

                    vendor.vendor_id = Utility.ToInt(Getvendor["vendor_id"]);
                    vendor.VendorName = Getvendor["vendor_name"].ToString();
                    vendor.Address1 = Getvendor["address1"].ToString();
                    vendor.Address2 = Getvendor["address2"].ToString();
                    vendor.City = Getvendor["city"].ToString();
                    vendor.State = Getvendor["st"].ToString();
                    vendor.Zip = Utility.FormatZip(Getvendor["zip"].ToString(), true);
                    vendor.Country = Getvendor["country"].ToString();
                    vendor.PhoneNumber = Utility.FormatPhone(Getvendor["phone"].ToString(), true);
                    vendor.FaxNumber = Utility.FormatPhone(Getvendor["fax"].ToString(), true);
                    vendor.EmailAddress = Getvendor["email"].ToString();
                    vendor.Website = Getvendor["website"].ToString();
                    vendor.Notes = Getvendor["notes"].ToString();
                    vendor.IsActive = Convert.ToBoolean(Getvendor["is_active"]);
                }
                else
                {
                    vendor.IsActive = true;
                }

                if (vendor.vendor_id > 0)
                {
                    response.Vendordetail = vendor;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.Vendordetail = vendor;
                    response.Status = "New";
                    response.ErrorMessage = "Vendord not found";
                    response.Statuscode = 0;
                }
            }
            catch (System.Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "Get Vendor/Vendor", error.errormsg);
                HttpContext.Session.SetString("error_log", error.errormsg);
                return RedirectToAction("Error_Log", "Home");
                throw;
            }
            return View(response);
        }
        #endregion

        #region LoadVendorsData
        /// <summary>
        /// LoadVendorsData
        /// </summary>
        /// <param name="request"> Load Vendor Grid</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadVendorsGrid(bool isactive, bool toggleActive)
        {
            LoadState loadState = new LoadState();
            GetVendorResponse response = new GetVendorResponse();
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                // Get Save State in Vendor Grid based the user
                mr = TableDefs.User.GetUserGrid(ds, userID.CurrentUser.UserID, 30);
                if (mr != null)
                {
                    if (mr.Table.Rows.Count > 0)
                    {
                        loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                        loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                        loadState.json_col_ord = mr["json_col_ord"].ToString();
                        response.loadState = loadState;
                        dsResult.Errors = "true";

                    }
                }
                DataView dvFacility = TableDefs.Vendor.GetVendorsList(ds, isactive);
                string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listVentory = JsonConvert.DeserializeObject<lst_tbl_vendor>(jsonData2);

                dsResult.Data = listVentory.tbl_vendor.ToList().OrderBy(x => x.vendor_name);
                dsResult.Total = listVentory.tbl_vendor.Count;
            }
            catch (System.Exception ex)
            {

                    ErrorLog error = new ErrorLog();
                    error.errormsg = ex.Message;
                    DBAccess.executeSQL("usp_error_log_add", "Main", " Grid Load/Vendor", error.errormsg);
                    HttpContext.Session.SetString("error_log", error.errormsg);
                    result.success = false;
                    result.message = $"An error occurred retrieving Customer List. {ex.Message}";                
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }           
        }
        #endregion

        #region AddEdit Vendor Functionality
        /// <summary>
        /// AddEdit Vendor Functionality
        /// </summary>
        /// <param name="request"> Save Vendor Details</param>
        /// <returns></returns>
        [HttpPost]
        public GetVendorResponse AddEditVendors(Vendor request)
        {
            int userID = 0;
            GetVendorResponse response = new GetVendorResponse();
            try
            {
                if (ModelState.IsValid)
                {
                    request.PhoneNumber = Utility.FormatPhone(request.PhoneNumber, false);
                    request.FaxNumber = Utility.FormatPhone(request.FaxNumber, false);

                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    if (data != null)
                    {
                        userID = data.CurrentUser.UserID;
                        if (request.vendor_id == 0)
                        {
                            DBAccess.executeSQL("usp_vendor_new", userID, request.VendorName, request.Address1, request.Address2, request.City,
                                request.State, request.Zip, "US", request.PhoneNumber, request.FaxNumber,
                                request.EmailAddress, request.Website, request.Notes, request.IsActive);
                        }
                        else
                        {
                            DBAccess.executeSQL("usp_vendor_update", userID, request.vendor_id, request.VendorName, request.Address1, request.Address2, request.City,
                                request.State, request.Zip, "US", request.PhoneNumber, request.FaxNumber,
                                request.EmailAddress, request.Website, request.Notes, request.IsActive);
                        }
                    }

                    response.Status = "Success";
                }
            }
            catch (System.Exception ex)
            {                           
                DBAccess.executeSQL("usp_error_log_add","Main", "Add-Edit/Vendor", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                string text = HttpContext.Session.GetString("error_log");
                response.Statuscode = 0;
                response.Status = "Failure";
                    

            }
            return response;
        }
        #endregion


        #region LoadVendorContactGrid
        /// <summary>
        /// LoadVendorContactGrid
        /// </summary>
        /// <param name="request"> Load Vendor Contact Grid Details</param>
        /// <returns></returns>
        public JsonResult LoadVendorContactGrid( bool isactive, int vendor_id)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                DataView dvVendor = TableDefs.Vendor.GetVendorContacts(ds, vendor_id, isactive );
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listVendor = JsonConvert.DeserializeObject<lst_tbl_VendorContact>(jsonData);

                dsResult.Data = listVendor.tbl_vendor_contact.ToList().OrderBy(x => x.phone);
                dsResult.Total = listVendor.tbl_vendor_contact.Count;
            }
            catch (Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "LoadGridVendorContact/Vendor", error.errormsg);
                result.success = false;
                result.message = $"An error occurred retrieving Inventory List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region Vendor Export List
        /// <summary>
        /// Vendor Export List
        /// </summary>
        /// <param name="request">Vendor Export</param>
        /// <returns></returns>
        [HttpPost]
        [Obsolete]
        public GetVendorResponse ExporVendors(List<string> vendorList)
        {
            DataSet ds = new DataSet();
            GetVendorResponse response = new GetVendorResponse();
            try
            {
                if (vendorList != null)
                {
                    if (vendorList != null && vendorList.Count > 0)
                    {
                        //Set File Name and Path
                        string FilePath = Path.Combine(_hostingEnvironment.ContentRootPath, "wwwroot/ExportFile/");
                        string docPath = Path.Combine(FilePath, "vendors/");
                        if (Directory.Exists(docPath))
                        {
                            DirectoryInfo di = new DirectoryInfo(docPath);
                            FileInfo[] files = di.GetFiles();
                            foreach (FileInfo file in files)
                            {
                                file.Delete();
                            }
                        }

                        string file_name = $"vendors_{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";
                        string pathFileName = docPath + file_name;

                        if (!Directory.Exists(FilePath))
                        {
                            Directory.CreateDirectory(FilePath);
                        }

                        // Path Not exists then create new file path
                        if (!Directory.Exists(docPath))
                        {
                            Directory.CreateDirectory(docPath);
                        }
                        //File name  exists then Delete the  file
                        if (System.IO.File.Exists(pathFileName))
                        {
                            System.IO.File.Delete(docPath);
                        }

                        //convert list to string
                        string VendorList = String.Join<string>(",", vendorList);                      
                        DataTable dtVendor = TableDefs.Vendor.GetVnedorForExport(ds, VendorList).ToTable();
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dtVendor);
                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(pathFileName);
                                response.Status = "Success";
                                response.ExcelLink = "/ExportFile/vendors/" + file_name;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Export List/Vendor", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
            }
            return response;
        }
        #endregion
    }
}
