﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Collections.Generic;
using Kendo.Mvc.UI;
using System.Data;
using System;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace HMSMemri.Controllers
{
    public class TicketReportsController : IMCPBase
    {
        /// <summary>
        /// Summary for declare Global Variables
        /// </summary>

        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        protected DataRow mr = null;
        protected DataRow ticketReports = null;
        protected DataRow ticketReportTesting = null;
        protected DataRow ticketRepair = null;
        protected DataRow ticketLog = null;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        [Obsolete]
        public TicketReportsController(IOptions<BaseSettings.ServerVar> serverVar, IHostingEnvironment hostingEnvironment) : base(serverVar)
        {
            _serverVar = serverVar;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Summary for Index page(UI),check logged user
        /// </summary>
        /// <returns></returns>

        #region Index UI
        [Obsolete]
        public FileResult TicketReport(string Id)
        {
            DataSet ds = new DataSet();
            AppSettings appSet = new AppSettings();
            GetTicketReportResponse response = new GetTicketReportResponse();
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.contracts_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;
            ViewBag.ShowAll = Global.contracts_show_closed;
            string ID = Id;
            var path = _hostingEnvironment.ContentRootPath + "\\wwwroot\\html\\index.html";
            var MessageDetails = System.IO.File.ReadAllText(path);

            //Declare the File Folder
            string tempFolder = Path.Combine(_hostingEnvironment.ContentRootPath + "\\wwwroot\\TicketReports\\TicketReport");
            string targetPath = Path.Combine(tempFolder + ".pdf");

            string FinaltempFolder = Path.Combine(_hostingEnvironment.ContentRootPath + "\\wwwroot\\TicketReports\\TicketReports");
            string FinaltargetPath = Path.Combine(FinaltempFolder + ".pdf");

            List<string> PDFLIST = new List<string>();
            PDFLIST.Add((Path.Combine(_hostingEnvironment.ContentRootPath + "/wwwroot/")) + "/TicketReports/TicketReport.pdf");

            MessageDetails = MessageDetails.Replace("[[Image]]", @System.Configuration.ConfigurationManager.AppSettings["Image"] + "images\\hms_logo.png");
            //Get Value for AppSettings
            mr = TableDefs.General.GetAppSettings(ds);
            if (mr != null)
            {
                MessageDetails = MessageDetails.Replace("[[Name]]", mr["name"].ToString());
                MessageDetails = MessageDetails.Replace("[[Address1]]", mr["address_1"].ToString());
                MessageDetails = MessageDetails.Replace("[[Address2]]", mr["address_2"].ToString());
                MessageDetails = MessageDetails.Replace("[[City]]", mr["city"].ToString());
                MessageDetails = MessageDetails.Replace("[[State]]", mr["state"].ToString());
                MessageDetails = MessageDetails.Replace("[[ZipCode]]", mr["zip"].ToString());
                MessageDetails = MessageDetails.Replace("[[Phone]]", Utility.FormatPhone(mr["phone"].ToString(), true));
                MessageDetails = MessageDetails.Replace("[[Fax]]", Utility.FormatPhone(mr["fax"].ToString(), true));
                MessageDetails = MessageDetails.Replace("[[Email]]", mr["email"].ToString());
            }
            MessageDetails = MessageDetails.Replace("[[TicketID]]", ID);
            //Get Value for Body of the Ticket Report

            ticketReports = TableDefs.Ticket.GetTicketReport(ds, Utility.ToInt(ID));
            if (ticketReports != null)
            {
                //Facility Information
                MessageDetails = MessageDetails.Replace("[[FacillityName]]", ticketReports["facility_name"].ToString());
                MessageDetails = MessageDetails.Replace("[[FacilityAddress1]]", ticketReports["address1"].ToString());
                MessageDetails = MessageDetails.Replace("[[FacilityAddress2]]", ticketReports["address2"].ToString());
                MessageDetails = MessageDetails.Replace("[[FacilityCity]]", ticketReports["city"].ToString());
                MessageDetails = MessageDetails.Replace("[[FacilityState]]", ticketReports["st"].ToString());
                MessageDetails = MessageDetails.Replace("[[FacilityZip]]", ticketReports["zip"].ToString());
                MessageDetails = MessageDetails.Replace("[[FacilityPhone]]", Utility.FormatPhone(ticketReports["phone"].ToString(), true));

                //Ticket Information
                MessageDetails = MessageDetails.Replace("[[CreatedBy]]", ticketReports["created_by"].ToString());
                MessageDetails = MessageDetails.Replace("[[AssignedTo]]", ticketReports["assigned_to"].ToString());
                MessageDetails = MessageDetails.Replace("[[ClosedBy]]", ticketReports["closed_by"].ToString());
                MessageDetails = MessageDetails.Replace("[[TicketClosed]]", Utility.FormatDateTime(ticketReports["closed_dt"].ToString(), null, "MM/dd/yyyy"));
                MessageDetails = MessageDetails.Replace("[[ServicesDate]]", Utility.FormatDateTime(ticketReports["service_dt"].ToString(), null, "MM/dd/yyyy"));
                MessageDetails = MessageDetails.Replace("[[CheckType]]", ticketReports["ticket_check_tp_desc"].ToString());
                MessageDetails = MessageDetails.Replace("[[Status]]", ticketReports["ticket_stat_desc"].ToString());
                MessageDetails = MessageDetails.Replace("[[Priority]]", ticketReports["ticket_priority_stat_desc"].ToString());
                MessageDetails = MessageDetails.Replace("[[TicketAge]]", ticketReports["ticket_age"].ToString());
                MessageDetails = MessageDetails.Replace("[[TimeWorked]]", ticketReports["time_worked"].ToString());
                MessageDetails = MessageDetails.Replace("[[TicketID]]", ticketReports["ticket_id"].ToString());
                MessageDetails = MessageDetails.Replace("[[TicketDate]]", Utility.FormatDateTime(ticketReports["trk_ent_dt"].ToString(), null, "MM/dd/yyyy"));
                MessageDetails = MessageDetails.Replace("[[ArrivedVia]]", ticketReports["ticket_addmethod_desc"].ToString());

                //Inventory Information
                MessageDetails = MessageDetails.Replace("[[InventoryID]]", ticketReports["inv_id"].ToString());
                MessageDetails = MessageDetails.Replace("[[InventoryClass]]", ticketReports["prod_class_desc"].ToString());
                MessageDetails = MessageDetails.Replace("[[SubClass]]", ticketReports["prod_sub_class_desc"].ToString());
                MessageDetails = MessageDetails.Replace("[[ModelNumber]]", ticketReports["model"].ToString());
                MessageDetails = MessageDetails.Replace("[[SerialNumber]]", ticketReports["serial_no"].ToString());
                MessageDetails = MessageDetails.Replace("[[Control]]", ticketReports["control_num"].ToString());
                MessageDetails = MessageDetails.Replace("[[HospControl]]", ticketReports["hos_control_num"].ToString());
                MessageDetails = MessageDetails.Replace("[[PreviousControl]]", ticketReports["previous_control_num"].ToString());
                MessageDetails = MessageDetails.Replace("[[Coverage]]", ticketReports["vendor_coverage"].ToString());
                MessageDetails = MessageDetails.Replace("[[InventoryStatus]]", ticketReports["inv_stat_desc"].ToString());
                MessageDetails = MessageDetails.Replace("[[Department]]", ticketReports["dept_name"].ToString());
                MessageDetails = MessageDetails.Replace("[[Manufacturer]]", ticketReports["mfg_name"].ToString());
                MessageDetails = MessageDetails.Replace("[[Ownership]]", ticketReports["inv_ownership_tp_desc"].ToString());
                MessageDetails = MessageDetails.Replace("[[WarrantyStatus]]", ticketReports["inv_war_stat_desc"].ToString());
                MessageDetails = MessageDetails.Replace("[[WarrantyExp]]", Utility.FormatDateTime(ticketReports["warranty_exp_dt"].ToString(), null, "MM/dd/yyyy"));
                MessageDetails = MessageDetails.Replace("[[InstallDate]]", Utility.FormatDateTime(ticketReports["install_dt"].ToString(), null, "MM/dd/yyyy"));
                MessageDetails = MessageDetails.Replace("[[LastPMCompleted]]", Utility.FormatDateTime(ticketReports["last_pm_dt"].ToString(), null, "MM/dd/yyyy"));
                MessageDetails = MessageDetails.Replace("[[NextSchedulePM]]", Utility.FormatDateTime(ticketReports["next_pm_dt"].ToString(), null, "MM/dd/yyyy"));
                MessageDetails = MessageDetails.Replace("[[PMInterval]]", ticketReports["inv_pm_interval_tp_desc"].ToString());
                MessageDetails = MessageDetails.Replace("[[RiskFactor]]", ticketReports["risk_num"].ToString());


                //Ticket Log Information
                MessageDetails = MessageDetails.Replace("[[CallerName]]", ticketReports["caller_name"].ToString());
                MessageDetails = MessageDetails.Replace("[[CallerPhone]]", Utility.FormatPhone(ticketReports["caller_phone"].ToString(), true));
                MessageDetails = MessageDetails.Replace("[[PDesc]]", ticketReports["ticket_desc"].ToString());
            }
            else
            {
                //Facility Information
                MessageDetails = MessageDetails.Replace("[[FacillityName]]", "");
                MessageDetails = MessageDetails.Replace("[[FacilityAddress1]]", "");
                MessageDetails = MessageDetails.Replace("[[FacilityAddress2]]", "");
                MessageDetails = MessageDetails.Replace("[[FacilityCity]]", "");
                MessageDetails = MessageDetails.Replace("[[FacilityState]]", "");
                MessageDetails = MessageDetails.Replace("[[FacilityZip]]", "");
                MessageDetails = MessageDetails.Replace("[[FacilityPhone]]", "");

                //Ticket Information
                MessageDetails = MessageDetails.Replace("[[CreatedBy]]", "");
                MessageDetails = MessageDetails.Replace("[[AssignedTo]]", "");
                MessageDetails = MessageDetails.Replace("[[ClosedBy]]", "");
                MessageDetails = MessageDetails.Replace("[[TicketClosed]]", "");
                MessageDetails = MessageDetails.Replace("[[ServicesDate]]", "");
                MessageDetails = MessageDetails.Replace("[[CheckType]]", "");
                MessageDetails = MessageDetails.Replace("[[Status]]", "");
                MessageDetails = MessageDetails.Replace("[[Priority]]", "");
                MessageDetails = MessageDetails.Replace("[[TicketAge]]", "");
                MessageDetails = MessageDetails.Replace("[[TimeWorked]]", "");
                MessageDetails = MessageDetails.Replace("[[TicketDate]]", "");
                MessageDetails = MessageDetails.Replace("[[ArrivedVia]]", "");

                //Inventory Information
                MessageDetails = MessageDetails.Replace("[[InventoryID]]", "");
                MessageDetails = MessageDetails.Replace("[[InventoryClass]]", "");
                MessageDetails = MessageDetails.Replace("[[SubClass]]", "");
                MessageDetails = MessageDetails.Replace("[[ModelNumber]]", "");
                MessageDetails = MessageDetails.Replace("[[SerialNumber]]", "");
                MessageDetails = MessageDetails.Replace("[[Control]]", "");
                MessageDetails = MessageDetails.Replace("[[HospControl]]", "");
                MessageDetails = MessageDetails.Replace("[[PreviousControl]]", "");
                MessageDetails = MessageDetails.Replace("[[Coverage]]", "");
                MessageDetails = MessageDetails.Replace("[[InventoryStatus]]", "");
                MessageDetails = MessageDetails.Replace("[[Department]]", "");
                MessageDetails = MessageDetails.Replace("[[Manufacturer]]", "");
                MessageDetails = MessageDetails.Replace("[[Ownership]]", "");
                MessageDetails = MessageDetails.Replace("[[WarrantyStatus]]", "");
                MessageDetails = MessageDetails.Replace("[[WarrantyExp]]", "");
                MessageDetails = MessageDetails.Replace("[[InstallDate]]", "");
                MessageDetails = MessageDetails.Replace("[[LastPMCompleted]]", "");
                MessageDetails = MessageDetails.Replace("[[NextSchedulePM]]", "");
                MessageDetails = MessageDetails.Replace("[[PMInterval]]", "");
                MessageDetails = MessageDetails.Replace("[[RiskFactor]]", "");


                //Ticket Log Information
                MessageDetails = MessageDetails.Replace("[[CallerName]]", "");
                MessageDetails = MessageDetails.Replace("[[CallerPhone]]", "");
                MessageDetails = MessageDetails.Replace("[[PDesc]]", "");
            }
            //Get Value for Ticket Report Testing
            DataView dvTesting = TableDefs.Ticket.GetTicketReportTestResults(ds, Utility.ToInt(ID));
            string jsonData1 = JsonConvert.SerializeObject(ds, Formatting.Indented);
            var listTicketTest = JsonConvert.DeserializeObject<lst_tbl_test_results>(jsonData1);

            string TestingInfo = null;

            TestingInfo = "<table class='table-bordered'><tr>" +
               "<th> Type </th>" +
               "<th> Parameters </th>" +
               "<th> Values </th>" +
               "<th> Date </th>" +
               "<th> Equipment </th>" +
               "</tr>";

            foreach (var item in listTicketTest.tbl_test_results)
            {
                TestingInfo += "<tr>" +
                    "<td>" + item.test_type + "</td>" +
                    "<td>" + item.test_parm + "</td>" +
                    "<td>" + item.test_val + "</td>" +
                    "<td>" + item.test_dt + "</td>" +
                    "<td>" + item.test_equip + "</td>" +
                    "</tr>";
            }
            TestingInfo += "</table>";
            MessageDetails = MessageDetails.Replace("[[TestingInfoTable]]", TestingInfo);


            //Get Value for Ticket Repair Information

            DataView dvTicket = TableDefs.Ticket.GetTicketReportCharges(ds, Utility.ToInt(ID));
            string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
            var listTicket = JsonConvert.DeserializeObject<lst_tbl_ticket_check_tp>(jsonData);

            string RepairInfo = null;

            RepairInfo = "<table class='table-bordered'><tr>" +
                "<th> Need </th>" +
                "<th> Used </th>" +
                "<th> QTY </th>" +
                "<th> Type </th>" +
                "<th> PO </th >" +
                "<th> Part Number </th>" +
                "<th> Description </th>" +
                "<th> Quoted </th>" +
                "<th> Price </th>" +
                "<th> Total </th>" +
                "</tr>";

            foreach (var item in listTicket.tbl_ticket_check_tp)
            {
                RepairInfo += "<tr>" +
                    "<td><div class='form-group'><input type = 'checkbox' id ='html'> <label for='html'></label> </div></td>" +
                    "<td><div class='form-group'> <input type = 'checkbox' id = 'body' ><label for= 'body'></label> </div> </td>" +
                    "<td>" + item.qty.ToString() + "</td>" +
                    "<td>" + item.charge_tp_desc + "</td>" +
                    "<td>" + item.po + "</td>" +
                    "<td>" + item.part_no + "</td>" +
                    "<td>" + item.charge_desc + "</td>" +
                    "<td>" + item.quoted + "</td>" +
                    "<td>" + item.cost.ToString() + "</td>" +
                    "<td>" + item.total.ToString() + "</td>" +
                    "</tr>";


            }
            RepairInfo += "</table>";
            MessageDetails = MessageDetails.Replace("[[RepairInfoTable]]", RepairInfo);

            //Get Value for Ticket Log Information
            DataView ticketLog = TableDefs.Ticket.GetTicketReportLog(ds, Utility.ToInt(ID));
            string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
            var lst_ticket_log = JsonConvert.DeserializeObject<lst_ticket_log>(jsonData2);
            string LogInfo = null;

            if (ticketLog.Count > 0)
            LogInfo = "<table class='table-bordered'><tr>" +
           "<th> DeviceTest </th>" +
           "<th> Log </th>" +
           "</tr>";

            foreach (var item in lst_ticket_log.tbl_ticket_log)
            {
                LogInfo += "<tr>" +
                    "<td>" + item.log_desc.ToString() + "</td>" +
                    "<td>" + item.log_action + "</td>" +
                    "</tr>";


            }
            LogInfo += "</table>";

            MessageDetails = MessageDetails.Replace("[[LogInfoTable]]", LogInfo);

            //Get Attachment Image/Pdf
            List<tbl_file_attach> ticketStat = new List<tbl_file_attach>();
            GetTicketResponse response1 = new GetTicketResponse();
            DataView dvFile = TableDefs.General.GetFileAttachments(ds, Utility.ToInt(ID), (int)FileAttachType.Ticket);
            if (dvFile != null)
            {
                DataTable dttypes = dvFile.ToTable();
                for (int i = 0; i < dttypes.Rows.Count; i++)
                {
                    tbl_file_attach stat = new tbl_file_attach();
                    stat.file_path = dttypes.Rows[i]["file_path"].ToString();
                    ticketStat.Add(stat);
                }

            }
            bool imagefile = false;
            string AttachmentInfo = null;
            AttachmentInfo = "<div class=ticket-attach><form><p style=page-break-after:always;>&nbsp;&nbsp;</p><div class=text-center><table><tr>" +
                                
                             "</tr>";
            
            List<PdfReader> pdfReaderList = new List<PdfReader>();

            foreach (var item in ticketStat)
            {
                if (ticketStat.Count > 0)
                {
                    string extension = System.IO.Path.GetExtension(item.file_path);
                    if (extension != ".pdf")
                    {
                        var imageitem = @System.Configuration.ConfigurationManager.AppSettings["file_path"] + item.file_path;
                        if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath + "/wwwroot/") + item.file_path))
                        {
                            imagefile = true;
                            var image = string.Format("<tr><td><img src='{0}' &nbsp;/></td><br /></tr>", imageitem);
                            AttachmentInfo += image;
                        }
                    }
                    else if (extension == ".pdf")
                    {
                        var imageitem = Path.Combine(_hostingEnvironment.ContentRootPath + "/wwwroot/") + item.file_path;
                        if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath + "/wwwroot/") + item.file_path))
                        {
                            PdfReader pdfReader = new PdfReader(Path.Combine(imageitem));
                            pdfReaderList.Add(pdfReader);
                            PDFLIST.Add(imageitem);
                        }
                    }                   
                }
            }
            AttachmentInfo += "</table></div></form></div>";

            if (imagefile)
                MessageDetails = MessageDetails.Replace("[[AttachmentInfoTable]]", AttachmentInfo);
            else
                MessageDetails = MessageDetails.Replace("[[AttachmentInfoTable]]", "");


            //Set the Pages 
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter
            {
                Orientation = NReco.PdfGenerator.PageOrientation.Portrait,
                Size = NReco.PdfGenerator.PageSize.A3,
                PageHeight = 290,
                PageWidth = 215,
            };
            //Set the License key in NReco.PdfGenerator.HtmlToPdfConverter .NET CORE
            htmlToPdf.License.SetLicenseKey(

                    "PDF_Generator_Bin_Examples_Pack_253103844294",

                    "d0cVUJS15/wCHGIa+v3s5XTIzFnDsON+/+ze3ivH2g1ejZIisRVqQrWWgxPMiaQenDU9xNJ0xLJTwOWfh19gnee5XtMXumdgzqFgZ9yTiNVJCRu+oy6SScEO2JkNDEQ/H2b+4pjlbeKAQd12Q6QRxmq7B0ps12ystxDNxCJFK3U="

            );
            //Delete Existing File
            if (Directory.Exists(Path.Combine(_hostingEnvironment.ContentRootPath + "\\wwwroot\\TicketReports")))
            {
                DirectoryInfo di = new DirectoryInfo(Path.Combine(_hostingEnvironment.ContentRootPath + "\\wwwroot\\TicketReports"));
                FileInfo[] files = di.GetFiles();
                foreach (FileInfo file in files)
                {
                    file.Delete();
                }
            }

            //Create a folder
            if (!Directory.Exists(Path.Combine(_hostingEnvironment.ContentRootPath + "\\wwwroot\\TicketReports")))
            {
                Directory.CreateDirectory(Path.Combine(_hostingEnvironment.ContentRootPath + "\\wwwroot\\TicketReports"));
            }

            //file.SaveAs(targetPath);
            var pdfBytes = htmlToPdf.GeneratePdf(MessageDetails);
            System.IO.File.WriteAllBytes(targetPath, pdfBytes);

            if (PDFLIST.Count() > 1)
            {
                PdfFile(PDFLIST);
            }
                      
            byte[] fileBytes = PDFLIST.Count() > 1 ? System.IO.File.ReadAllBytes(FinaltargetPath) : System.IO.File.ReadAllBytes(targetPath);
            return File(fileBytes, "application/pdf", "TicketReports.pdf");
        }
        #endregion

        //Uploded Pdf Display
        private void PdfFile(List<string> PDFLIST)
        {
            string tempFolder = Path.Combine(_hostingEnvironment.ContentRootPath + "\\wwwroot\\TicketReports\\TicketReports");
            string path = Path.Combine(tempFolder + ".pdf");
            using (Stream outputPdfStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Document document = new Document();
                document.SetPageSize(PageSize.A3);
                PdfSmartCopy copy = new PdfSmartCopy(document, outputPdfStream);
                document.Open();
                PdfDocument pdfDoc = null;
                PdfReader reader = null;
                for (int i = 0; i < PDFLIST.ToArray().Length; i++)
                {
                    PdfReader.unethicalreading = true;
                    if (PDFLIST[i].LastIndexOf(".pdf") > 0)
                    {
                        string actualPath;

                        actualPath = PDFLIST[i];
                        reader = new PdfReader(Path.Combine(actualPath));
                        int n = reader.NumberOfPages;

                        for (int page = 0; page < n;)
                        {
                            copy.AddPage(copy.GetImportedPage(reader, ++page));
                        }

                        reader.Close();
                    }
                }
                document.Close();
            }
        }
        
    }
}
