﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Collections.Generic;
using Kendo.Mvc.UI;
using System.Data;
using System;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using ClosedXML.Excel;
using System.Net.Http.Headers;

namespace HMSMemri.Controllers
{
    public class ContractsController : IMCPBase
    {
        /// <summary>
        /// Summary for declare Global Variables
        /// </summary>
        
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        protected DataRow mr = null;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        public ContractsController(IOptions<BaseSettings.ServerVar> serverVar, IHostingEnvironment hostingEnvironment) : base(serverVar)
        {
            _serverVar = serverVar;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Summary for Index page(UI),check logged user
        /// </summary>
        /// <returns></returns>

        #region Index UI
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.contracts_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.contracts_show_closed;
            return View();

        }
        #endregion

        #region Grid data load
        [HttpPost]
            public JsonResult LoadContractData(bool isactive, bool toggleActive,int CustID,int FacilityID)
            {
            LoadState loadState = new LoadState();
            GetContractResponse response = new GetContractResponse();
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();

            try
            {
                if (HttpContext.Session.GetString("is_cust_login") == "True")
                {
                        //Set Facility and Cust ID
                        GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                        CustID = CustomerDetail.CurrentCust.cust_id;
                        Global.facility_id = FacilityID;
                        Global.cust_id = CustID;
                        GlobalData.SetGlobalSessione(HttpContext.Session, "facility_id", Global);
                        GlobalData.SetGlobalSessione(HttpContext.Session, "cust_id", Global);

                        DataView dvContracts = TableDefs.Contracts.GetAllContractByCustFacility(ds, CustID, FacilityID, isactive);
                        string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                        var listContract = JsonConvert.DeserializeObject<lst_tbl_contract>(jsonData);
                        dsResult.Data = listContract.tbl_contract.ToList();
                        dsResult.Total = listContract.tbl_contract.Count;
                }
                else { 
                    // Get Save State in Vendor Grid based the user
                    mr = TableDefs.User.GetUserGrid(ds, userID.CurrentUser.UserID, 50);
                    if (mr != null)
                    {
                        if (mr.Table.Rows.Count > 0)
                        {
                            loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                            loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                            loadState.json_col_ord = mr["json_col_ord"].ToString();
                            response.loadState = loadState;
                            dsResult.Errors = "true";

                        }
                    }
                    if (CustID != 0 && FacilityID != 0)
                    {
                        //Set Facility and Cust ID
                        Global.facility_id = FacilityID;
                        Global.cust_id = CustID;
                        GlobalData.SetGlobalSessione(HttpContext.Session, "facility_id", Global);
                        GlobalData.SetGlobalSessione(HttpContext.Session, "cust_id", Global);

                        DataView dvContracts = TableDefs.Contracts.GetAllContractByCustFacility(ds, CustID, FacilityID, isactive);
                        string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                        var listContract = JsonConvert.DeserializeObject<lst_tbl_contract>(jsonData);
                        dsResult.Data = listContract.tbl_contract.ToList();
                        dsResult.Total = listContract.tbl_contract.Count;
                    }
                    else
                    {
                        // get the value for sp, API Call
                        DataView dvContracts = TableDefs.Contracts.GetAllContracts(ds, isactive);
                        string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                        var listContract = JsonConvert.DeserializeObject<lst_tbl_contract>(jsonData);
                        dsResult.Data = listContract.tbl_contract.ToList();
                        dsResult.Total = listContract.tbl_contract.Count;
                    }
                }
            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Grid Load/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Grid Load/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                result.success = false;
                result.message = $"An error occurred retrieving Contracts List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }

        #endregion

        #region Add/Edit for Contracts
        public ActionResult AddEditContract(string Id)
        {
            GetContractRequestDetails response = new GetContractRequestDetails();
            List<ContractType> ContractType = new List<ContractType>();
            List<ContractServiceType> TempContractServiceType = new List<ContractServiceType>();
            List<ContractServiceType> ContractServiceType = new List<ContractServiceType>();
            List<ContractServiceType> ContractServiceType1 = new List<ContractServiceType>();
            List<ContractServiceType> ContractServiceType2 = new List<ContractServiceType>();
            List<ContractServiceType> ContractServiceType3 = new List<ContractServiceType>();
            DataSet ds = new DataSet();
            List<SelectListItem> contractdropdownlist = new List<SelectListItem>();
            List<SelectListItem> servicedropdownlist = new List<SelectListItem>();
            List<InventorVendorList> InventorVendorList = new List<InventorVendorList>();
            string contarctid = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            int ID = Convert.ToInt32(contarctid);
            GlobalData Global = new GlobalData();
            GlobalData facility_id = GlobalData.GetGlobalSessione(HttpContext.Session, "facility_id");
           
            try
            {   //Globally to check the login status
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                GlobalData CustomerID = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (userID?.CurrentUser.UserID == null && CustomerID?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("Index", "Login");
                }

                try
                {
                    string IDcheck = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
                }
                catch (System.Exception)
                {
                    return View("~/Views/Shared/Error_EditUrl.cshtml");
                }

                //Customer Status API call
                DataView dvCustomerStatus = TableDefs.Contracts.GetContractTypeForDropDown(ds, true, "* Select One *");
                if (dvCustomerStatus != null)
                {
                    DataTable dtCustomerStatus = dvCustomerStatus.ToTable();
                    for (int i = 0; i < dtCustomerStatus.Rows.Count; i++)
                    {
                        ContractType status = new ContractType();
                        status.contract_tp_id = Utility.ToInt(dtCustomerStatus.Rows[i]["contract_tp_id"]);
                        status.contract_tp_desc = dtCustomerStatus.Rows[i]["contract_tp_desc"].ToString();
                        ContractType.Add(status);
                    }
                }
                foreach (var x in ContractType)
                {
                    contractdropdownlist.Add(new SelectListItem
                    {
                        Text = x.contract_tp_desc.ToString(),
                        Value = x.contract_tp_id.ToString()
                    });
                }
                response.ContractTypeList = contractdropdownlist;

                // VendorList Drop Down
                DataView dvInventoryProvider = TableDefs.Inventory.GetInventoryProviderForDropDown(ds, true, "* Select One *");
                if (dvInventoryProvider != null)
                {
                    DataTable dtInventoryProvider = dvInventoryProvider.ToTable();
                    for (int i = 0; i < dtInventoryProvider.Rows.Count; i++)
                    {
                        InventorVendorList status = new InventorVendorList();
                        status.vendor_id = Utility.ToInt(dtInventoryProvider.Rows[i]["vendor_id"]);
                        status.Vendor_Name = dtInventoryProvider.Rows[i]["Vendor_Name"].ToString();
                        InventorVendorList.Add(status);
                    }
                }
                List<SelectListItem> IntervalVendorList = new List<SelectListItem>();
                foreach (var item in InventorVendorList)
                {
                    IntervalVendorList.Add(new SelectListItem
                    {
                        Text = item.Vendor_Name.ToString(),
                        Value = item.vendor_id.ToString()
                    });
                }
                response.VendorListModule = IntervalVendorList;
                response.contractrequest = new ContractRequest();

                DataRow GetContractContact = TableDefs.Contracts.GetContracts(ds, ID);

                

                if (GetContractContact != null)
                {
                    response.contractrequest.contract_id = Utility.ToInt(GetContractContact["contract_id"]);
                    response.contractrequest.cust_id = Utility.ToInt(GetContractContact["cust_id"]);
                    response.contractrequest.facility_id = Utility.ToInt(GetContractContact["facility_id"]);
                    response.contractrequest.vendor_id = Utility.ToInt(GetContractContact["vendor_id"]);
                    response.contractrequest.contract_tp_id = Utility.ToInt(GetContractContact["contract_tp_id"]);
                    response.contractrequest.service_tp_id = Utility.ToInt(GetContractContact["service_tp_id"]);
                    response.contractrequest.start_dt= Convert.ToDateTime(GetContractContact["start_dt"]).ToString("MM/dd/yyyy");
                    response.contractrequest.enddate_dt = Convert.ToDateTime(GetContractContact["enddate_dt"]).ToString("MM/dd/yyyy");  
                    response.contractrequest.Notes = GetContractContact["notes"].ToString();
                    response.contractrequest.contract_po = GetContractContact["contract_po"].ToString();
                    response.contractrequest.parts_po = GetContractContact["parts_po"].ToString();
                    response.contractrequest.contract_labor_rate = Utility.ToFloat(GetContractContact["contract_labor_rate"]);
                    response.contractrequest.contract_travel_chg = Utility.ToFloat(GetContractContact["contract_travel_chg"]);
                    response.contractrequest.Isactive = Convert.ToBoolean(GetContractContact["is_active"]);

                    DataRow customername = TableDefs.Customer.GetCustomer(ds, response.contractrequest.cust_id);
                    if(customername != null)
                    {
                        response.contractrequest.cust_name = customername["cust_name"].ToString();
                    }
                    DataRow FacilityName = TableDefs.Facility.GetFacility(ds, response.contractrequest.facility_id);
                    if(FacilityName != null)
                    {
                        response.contractrequest.facility_name = FacilityName["facility_name"].ToString();
                    }

                }
                else
                {
                    response.contractrequest.start_dt = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    response.contractrequest.enddate_dt = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    response.contractrequest.cust_id = facility_id.cust_id;
                    response.contractrequest.facility_id = facility_id.facility_id;
                    response.contractrequest.Isactive = true;
                    if (response.contractrequest.cust_id ==0)
                    {
                        GlobalData cust_id = GlobalData.GetGlobalSessione(HttpContext.Session, "cust_id");
                        DataRow customernames = TableDefs.Customer.GetCustomer(ds, Convert.ToInt32(cust_id.cust_id));
                        if (customernames != null)
                        {
                            response.contractrequest.cust_name = customernames["cust_name"].ToString();
                        }
                    }
                    else
                    {
                        DataRow customername = TableDefs.Customer.GetCustomer(ds, response.contractrequest.cust_id);
                        if (customername != null)
                        {
                            response.contractrequest.cust_name = customername["cust_name"].ToString();
                        }
                    }
                    
                    DataRow FacilityName = TableDefs.Facility.GetFacility(ds, response.contractrequest.facility_id);
                    if(FacilityName != null)
                    {
                        response.contractrequest.facility_name = FacilityName["facility_name"].ToString();
                    }

                    //Service Type Dropdown
                    DataView dvserviceconttype = TableDefs.General.GetServiceTypesForDropDown(ds, true, true, "* Select One *");
                    if (dvserviceconttype != null)
                    {
                        DataTable dtservicetype = dvserviceconttype.ToTable();
                        for (int i = 0; i < dtservicetype.Rows.Count; i++)
                        {
                            ContractServiceType drpdown = new ContractServiceType();
                            drpdown.service_tp_id = Utility.ToInt(dtservicetype.Rows[i]["service_tp_id"]);
                            drpdown.service_tp_desc = dtservicetype.Rows[i]["service_tp_desc"].ToString();
                            ContractServiceType.Add(drpdown);
                        }
                    }

                    List<SelectListItem> IntervalServicetype = new List<SelectListItem>();
                    List<SelectListItem> IntervalServicetype2 = new List<SelectListItem>();

                    DataView servicedrop = TableDefs.Customer.GetCustomerService(ds, response.contractrequest.cust_id);

                    if (servicedrop != null)
                    {
                        DataTable dtServicetypes = servicedrop.ToTable();
                        for (int i = 0; i < dtServicetypes.Rows.Count; i++)
                        {
                            ContractServiceType drpdown = new ContractServiceType();
                            drpdown.service_tp_id = Utility.ToInt(dtServicetypes.Rows[i]["service_tp_id"]);
                            TempContractServiceType.Add(drpdown);
                        }
                    }
                    foreach (var type in TempContractServiceType)
                    {
                        ContractServiceType drpdown = new ContractServiceType();

                        drpdown = ContractServiceType.Where(c => c.service_tp_id == Convert.ToInt32(type.service_tp_id)).FirstOrDefault();
                        ContractServiceType1.Add(drpdown);
                    }
                    if (ContractServiceType1.Count > 1)
                    {
                        ContractServiceType drpdown = new ContractServiceType();
                        drpdown.service_tp_id = 0;
                        drpdown.service_tp_desc = "*Select One*";
                        ContractServiceType2.Add(drpdown);
                        ContractServiceType3.AddRange(ContractServiceType2);
                        ContractServiceType1.AddRange(ContractServiceType3);
                    }
                    foreach (var x in ContractServiceType1.OrderBy(x => x.service_tp_id))
                    {
                        servicedropdownlist.Add(new SelectListItem
                        {
                            Text = x.service_tp_desc.ToString(),
                            Value = x.service_tp_id.ToString()
                        });
                    }
                    response.ServiceTypeModule = servicedropdownlist;

                }


            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Get Contract/Contract",ex.Message);
                    HttpContext.Session.SetString("error_log",ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Get Contract/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                return RedirectToAction("Error_Log", "Home");
                throw;
            }
            return View(response);
        }
        #endregion

        #region Add,Edit the Contract records 
        [HttpPost]
        public GetContractRequestDetails AddEditContracts(ContractRequest request)
        {
            int userID = 0;
            GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            userID = data.CurrentUser.UserID;

            GetContractRequestDetails response = new GetContractRequestDetails();
            try
            {
                if (ModelState.IsValid)
                {
                    if (request.contract_id == 0)
                    {
                        DBAccess.executeSQL("usp_contract_new", userID, request.cust_id, request.vendor_id, request.facility_id, null, null, request.start_dt,
                          request.enddate_dt, request.Notes, request.contract_po, request.parts_po,
                            request.contract_tp_id,request.service_tp_id, request.contract_labor_rate, request.contract_travel_chg, request.Isactive);
                    }
                    else
                    {
                        DBAccess.executeSQL("usp_contract_update", userID,request.contract_id, request.cust_id, request.vendor_id, request.facility_id, null, null,request.start_dt,
                           request.enddate_dt, request.Notes, request.contract_po, request.parts_po,
                            request.contract_tp_id,request.service_tp_id, request.contract_labor_rate, request.contract_travel_chg, request.Isactive);
                    }
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
             
            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Add Edit/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Add-Edit/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region Contract Export List
        [HttpPost]
        [Obsolete]
        public GetContractResponse ExportContracts(List<string> ContractList)
        {
            DataSet ds = new DataSet();
            GetContractResponse response = new GetContractResponse();
            try
            {
                if (ContractList != null)
                {
                    if (ContractList != null && ContractList.Count > 0)
                    {
                        string FilePath = Path.Combine(_hostingEnvironment.ContentRootPath, "wwwroot/ExportFile/");
                        string docPath = Path.Combine(FilePath, "Contract/");
                        if (Directory.Exists(docPath))
                        {
                            DirectoryInfo di = new DirectoryInfo(docPath);
                            FileInfo[] files = di.GetFiles();
                            foreach (FileInfo file in files)
                            {
                                file.Delete();
                            }
                        }
                        string file_name = $"contracts_{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";
                        string pathFileName = docPath + file_name;

                        if (!Directory.Exists(FilePath))
                        {
                            Directory.CreateDirectory(FilePath);
                        }

                        // Path Not exists then create new file path
                        if (!Directory.Exists(docPath))
                        {
                            Directory.CreateDirectory(docPath);
                        }

                        //File name  exists then Delete the  file
                        if (System.IO.File.Exists(pathFileName))
                        {
                            System.IO.File.Delete(docPath);
                        }

                        //convert list to string
                        string contractlist = String.Join<string>(",", ContractList);
                        DataTable dtContracts = TableDefs.Contracts.GetContractsForExport(ds, contractlist).ToTable();

                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dtContracts);
                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(pathFileName);
                                response.Status = "Success";
                                response.ExcelLink = "/ExportFile/Contract/" + file_name;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Export/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                } else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Export/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
            }
            return response;
        
        }
        #endregion

        #region EditContract
        public ActionResult EditContract(string Id)
        {
            GetContractRequestDetails response = new GetContractRequestDetails();
            List<ContractServiceType> TempContractServiceType = new List<ContractServiceType>();
            List<ContractServiceType> ContractServiceType = new List<ContractServiceType>();
            List<ContractServiceType> ContractServiceType1 = new List<ContractServiceType>();
            List<ContractServiceType> ContractServiceType2 = new List<ContractServiceType>();
            List<ContractServiceType> ContractServiceType3 = new List<ContractServiceType>();
            List<ContractType> ContractType = new List<ContractType>();
            DataSet ds = new DataSet();
            List<SelectListItem> contractdropdownlist = new List<SelectListItem>();
            List<SelectListItem> servicedropdownlist = new List<SelectListItem>();
            List<InventorVendorList> InventorVendorList = new List<InventorVendorList>();
            string contarctid = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            int ID = Convert.ToInt32(contarctid);
            GlobalData Global = new GlobalData();
            GlobalData facility_id = GlobalData.GetGlobalSessione(HttpContext.Session, "facility_id");

            try
            {   //Globally to check the login status
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                GlobalData CustomerID = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (userID?.CurrentUser.UserID == null && CustomerID?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("Index", "Login");
                }

                try
                {
                    string IDcheck = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
                }
                catch (System.Exception)
                {
                    return View("~/Views/Shared/Error_EditUrl.cshtml");
                }

                //Customer Status API call
                DataView dvCustomerStatus = TableDefs.Contracts.GetContractTypeForDropDown(ds, true, "* Select One *");
                if (dvCustomerStatus != null)
                {
                    DataTable dtCustomerStatus = dvCustomerStatus.ToTable();
                    for (int i = 0; i < dtCustomerStatus.Rows.Count; i++)
                    {
                        ContractType status = new ContractType();
                        status.contract_tp_id = Utility.ToInt(dtCustomerStatus.Rows[i]["contract_tp_id"]);
                        status.contract_tp_desc = dtCustomerStatus.Rows[i]["contract_tp_desc"].ToString();
                        ContractType.Add(status);
                    }
                }
                foreach (var x in ContractType)
                {
                    contractdropdownlist.Add(new SelectListItem
                    {
                        Text = x.contract_tp_desc.ToString(),
                        Value = x.contract_tp_id.ToString()
                    });
                }
                response.ContractTypeList = contractdropdownlist;

                // VendorList Drop Down
                DataView dvInventoryProvider = TableDefs.Inventory.GetInventoryProviderForDropDown(ds, true, "* Select One *");
                if (dvInventoryProvider != null)
                {
                    DataTable dtInventoryProvider = dvInventoryProvider.ToTable();
                    for (int i = 0; i < dtInventoryProvider.Rows.Count; i++)
                    {
                        InventorVendorList status = new InventorVendorList();
                        status.vendor_id = Utility.ToInt(dtInventoryProvider.Rows[i]["vendor_id"]);
                        status.Vendor_Name = dtInventoryProvider.Rows[i]["Vendor_Name"].ToString();
                        InventorVendorList.Add(status);
                    }
                }
                List<SelectListItem> IntervalVendorList = new List<SelectListItem>();
                foreach (var item in InventorVendorList)
                {
                    IntervalVendorList.Add(new SelectListItem
                    {
                        Text = item.Vendor_Name.ToString(),
                        Value = item.vendor_id.ToString()
                    });
                }
                response.VendorListModule = IntervalVendorList;
                response.contractrequest = new ContractRequest();

                DataRow GetContractContact = TableDefs.Contracts.GetContracts(ds, ID);

                if (GetContractContact != null)
                {
                    response.contractrequest.contract_id = Utility.ToInt(GetContractContact["contract_id"]);
                    response.contractrequest.cust_id = Utility.ToInt(GetContractContact["cust_id"]);
                    response.contractrequest.facility_id = Utility.ToInt(GetContractContact["facility_id"]);
                    response.contractrequest.vendor_id = Utility.ToInt(GetContractContact["vendor_id"]);
                    response.contractrequest.contract_tp_id = Utility.ToInt(GetContractContact["contract_tp_id"]);
                    response.contractrequest.service_tp_id = Utility.ToInt(GetContractContact["service_tp_id"]);
                    response.contractrequest.start_dt = Convert.ToDateTime(GetContractContact["start_dt"]).ToString("MM/dd/yyyy");
                    response.contractrequest.enddate_dt = Convert.ToDateTime(GetContractContact["enddate_dt"]).ToString("MM/dd/yyyy");
                    response.contractrequest.Notes = GetContractContact["notes"].ToString();
                    response.contractrequest.contract_po = GetContractContact["contract_po"].ToString();
                    response.contractrequest.parts_po = GetContractContact["parts_po"].ToString();
                    response.contractrequest.contract_labor_rate = Utility.ToFloat(GetContractContact["contract_labor_rate"]);
                    response.contractrequest.contract_travel_chg = Utility.ToFloat(GetContractContact["contract_travel_chg"]);
                    response.contractrequest.Isactive = Convert.ToBoolean(GetContractContact["is_active"]);

                    DataRow customername = TableDefs.Customer.GetCustomer(ds, response.contractrequest.cust_id);
                    if (customername != null)
                    {
                        response.contractrequest.cust_name = customername["cust_name"].ToString();
                    }
                    DataRow FacilityName = TableDefs.Facility.GetFacility(ds, response.contractrequest.facility_id);
                    if (FacilityName != null)
                    {
                        response.contractrequest.facility_name = FacilityName["facility_name"].ToString();
                    }

                    //Service Type Dropdown
                    DataView dvserviceconttype = TableDefs.General.GetServiceTypesForDropDown(ds, true, true, "* Select One *");
                    if (dvserviceconttype != null)
                    {
                        DataTable dtservicetype = dvserviceconttype.ToTable();
                        for (int i = 0; i < dtservicetype.Rows.Count; i++)
                        {
                            ContractServiceType drpdown = new ContractServiceType();
                            drpdown.service_tp_id = Utility.ToInt(dtservicetype.Rows[i]["service_tp_id"]);
                            drpdown.service_tp_desc = dtservicetype.Rows[i]["service_tp_desc"].ToString();
                            ContractServiceType.Add(drpdown);
                        }
                    }

                    List<SelectListItem> IntervalServicetype = new List<SelectListItem>();
                    List<SelectListItem> IntervalServicetype2 = new List<SelectListItem>();

                    DataView servicedrop = TableDefs.Customer.GetCustomerService(ds, response.contractrequest.cust_id);

                    if (servicedrop != null)
                    {
                        DataTable dtServicetypes = servicedrop.ToTable();
                        for (int i = 0; i < dtServicetypes.Rows.Count; i++)
                        {
                            ContractServiceType drpdown = new ContractServiceType();
                            drpdown.service_tp_id = Utility.ToInt(dtServicetypes.Rows[i]["service_tp_id"]);
                            TempContractServiceType.Add(drpdown);
                        }
                    }
                    foreach (var type in TempContractServiceType)
                    {
                        ContractServiceType drpdown = new ContractServiceType();

                        drpdown = ContractServiceType.Where(c => c.service_tp_id == Convert.ToInt32(type.service_tp_id)).FirstOrDefault();
                        ContractServiceType1.Add(drpdown);
                    }
                    if (ContractServiceType1.Count > 1)
                    {
                        ContractServiceType drpdown = new ContractServiceType();
                        drpdown.service_tp_id = 0;
                        drpdown.service_tp_desc = "*Select One*";
                        ContractServiceType2.Add(drpdown);
                        ContractServiceType3.AddRange(ContractServiceType2);
                        ContractServiceType1.AddRange(ContractServiceType3);
                    }
                    foreach (var x in ContractServiceType1.OrderBy(x => x.service_tp_id))
                    {
                        servicedropdownlist.Add(new SelectListItem
                        {
                            Text = x.service_tp_desc.ToString(),
                            Value = x.service_tp_id.ToString()
                        });
                    }
                    response.ServiceTypeModule = servicedropdownlist;
                }
                else
                {
                    response.contractrequest.start_dt = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    response.contractrequest.enddate_dt = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    response.contractrequest.cust_id = facility_id.cust_id;
                    response.contractrequest.facility_id = facility_id.facility_id;
                    response.contractrequest.Isactive = true;
                    if (response.contractrequest.cust_id == 0)
                    {
                        GlobalData cust_id = GlobalData.GetGlobalSessione(HttpContext.Session, "cust_id");
                        DataRow customernames = TableDefs.Customer.GetCustomer(ds, Convert.ToInt32(cust_id.cust_id));
                        if (customernames != null)
                        {
                            response.contractrequest.cust_name = customernames["cust_name"].ToString();
                        }
                    }
                    else
                    {
                        DataRow customername = TableDefs.Customer.GetCustomer(ds, response.contractrequest.cust_id);
                        if (customername != null)
                        {
                            response.contractrequest.cust_name = customername["cust_name"].ToString();
                        }
                    }

                    DataRow FacilityName = TableDefs.Facility.GetFacility(ds, response.contractrequest.facility_id);
                    if (FacilityName != null)
                    {
                        response.contractrequest.facility_name = FacilityName["facility_name"].ToString();
                    }

                }
            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Get Contract/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Get Contract/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                return RedirectToAction("Error_Log", "Home");
                throw;
            }
            return View(response);
        }

        public JsonResult LoadTicketFileAttachment(int contract_id)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                
                DataView dvFile = TableDefs.General.GetFileAttachments(ds, contract_id, (int)FileAttachType.Contract);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listFile = JsonConvert.DeserializeObject<lst_tbl_file_attach>(jsonData);
               
                dsResult.Data = listFile.tbl_file_attach.ToList().OrderBy(x => x.file_attach_id);
                dsResult.Total = listFile.tbl_file_attach.Count;

                GlobalData.SetGlobalSessione(HttpContext.Session, "file_attach_id", Global);
                
            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "LoadFileAttachment/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "LoadFileAttachment/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                result.success = false;
                result.message = $"An error occurred retrieving Inventory List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }

        [HttpPost]
        private string GetImagePath(IFormFile image)
        {
            string VirtualPath = $@"/attachments/{DateTime.Now:yyyy}/{DateTime.Now:MM}/{DateTime.Now:dd}/";

            string photo1Name = string.Empty;
            IFormFile Photo1 = image;


            var extension = "";
            var ImagePath = "";
            if (string.Equals(image.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) || string.Equals(image.ContentType, "application/pdf", StringComparison.OrdinalIgnoreCase)
                || string.Equals(image.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) || string.Equals(image.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) || string.Equals(image.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                var millsecond = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                var FileName = Path.GetFileName(image.FileName);

                var Filename = ContentDispositionHeaderValue.Parse(image.ContentDisposition).FileName.Trim('"');
                //  Filename = hasSpecialChar(Filename);

                extension = Path.GetExtension(Filename);
                Filename = Path.GetFileNameWithoutExtension(Filename)+ extension;
                var filePath = Filename;
                ImagePath = VirtualPath + filePath;

                if (!Directory.Exists(_hostingEnvironment.WebRootPath + VirtualPath))
                {
                    Directory.CreateDirectory(_hostingEnvironment.WebRootPath + VirtualPath);
                }

                // normal code
                if (System.IO.File.Exists(_hostingEnvironment.WebRootPath + ImagePath))
                    System.IO.File.Delete(_hostingEnvironment.WebRootPath + ImagePath);
                using (FileStream fs = System.IO.File.Create(_hostingEnvironment.WebRootPath + ImagePath))
                {
                    Photo1.CopyTo(fs);
                    fs.Flush();
                }
            }
            return ImagePath;
        }

        [HttpPost]
        public GetContractResponse uploadContractFileAttachments(tbl_file_attach request)
        {
            GetContractResponse response = new GetContractResponse();
            DataSet ds = new DataSet();
            int userID = 0;
            try
            {
                GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

                double fileSizeKB = request.Image_path.Length / 1024;

                var Logo = GetImagePath(request.Image_path);
                var Files = request.Image_path;
                if (Files != null)
                {
                    userID = data.CurrentUser.UserID;
                    var FileName = Path.GetFileName(Logo);
                    DBAccess.executeSQL("usp_file_attach_new", userID, (int)FileAttachType.Contract, request.contract_id, FileName, fileSizeKB > 0 ? (fileSizeKB + " " + "KB") : (request.Image_path.Length + " " + "bytes"), Logo);

                }
                response.Status = "Success";

            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "UploadAttachment/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "UploadAttachment/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }

        #endregion

        #region LoadInvContract
        public JsonResult LoadInvContract(int contract_id)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {

                DataView dvFile = TableDefs.Contracts.GetContractInventory(ds, contract_id);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listInvContract = JsonConvert.DeserializeObject<lst_tbl_contract_inv>(jsonData);

                dsResult.Data = listInvContract.tbl_contract.Where(x => x.inv_id != null).ToList().OrderBy(x => x.contract_inv_id);
                dsResult.Total = listInvContract.tbl_contract.Where(x => x.inv_id != null).ToList().Count;
              
            }
            catch (Exception ex)
            {
                result.success = false;
                result.message = $"An error occurred retrieving Inventory Contract List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region Attachment
        public GetContractResponse AttachmentDelete(int file_attach_id)
        {
            GetContractResponse response = new GetContractResponse();
            DataSet ds = new DataSet();

            try
            {
                DBAccess.executeSQL("usp_file_attach_remove", file_attach_id);
                response.Status = "Success";

            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "AttachmentDelete/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "AttachmentDelete/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        
    }
}
