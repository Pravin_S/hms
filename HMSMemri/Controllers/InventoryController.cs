﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Collections.Generic;
using Kendo.Mvc.UI;
using System.Data;
using System;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.IO;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;

namespace HMSMemri.Controllers
{
    public class InventoryController : IMCPBase
    {
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        protected DataRow mr = null;

        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        [Obsolete]
        public InventoryController(IOptions<BaseSettings.ServerVar> serverVar , IHostingEnvironment hostingEnvironment) : base(serverVar)
        {
            _serverVar = serverVar;
            _hostingEnvironment = hostingEnvironment;
        }

        #region Index UI
        public IActionResult Index()
        {
            //Globally to check the login status
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }

            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.customer_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.customer_show_closed;
            return View();

        }
        #endregion

        #region Add Edit Inventory page
        public IActionResult AddEditInventory(string Id)
        {
            GetInventoiesResponse response = new GetInventoiesResponse();
            Inventory inventory = new Inventory();
            List<InventoryOwnerShipDrop> InventoryOwnerShipDrop = new List<InventoryOwnerShipDrop>();
            List<InventoryIntervalpm> InventoryIntervalpm = new List<InventoryIntervalpm>();
            List<InventoryAlarm> InventoryAlarm = new List<InventoryAlarm>();
            List<InventoryConcern> InventoryConcern = new List<InventoryConcern>();
            List<InventoryItemCoverage> InventoryItemCoverage = new List<InventoryItemCoverage>();
            List<InventorVendorList> InventorVendorList = new List<InventorVendorList>();
            List<InventoryBatteryInterval> InventoryBatteryInterval = new List<InventoryBatteryInterval>();
            List<InventoryManufactureList> InventoryManufactureList = new List<InventoryManufactureList>();
            List<InventoryWarrentyStatus> InventoryWarrentyStatus = new List<InventoryWarrentyStatus>();
            List<InventoryStatus> InventoryStatus = new List<InventoryStatus>();
            List<InventoryManagedEntity> InventoryManagedEntity = new List<InventoryManagedEntity>();
            List<InventoryRootClass> InventoryRootClass = new List<InventoryRootClass>();
            List<InventoryServiceCover> InventoryServiceCover = new List<InventoryServiceCover>();
            List<InventoryPoductSubRootClass> InventorySubRootClass = new List<InventoryPoductSubRootClass>();
            List<InventoryServiceType> TempInventoryServiceType = new List<InventoryServiceType>();
            List<InventoryServiceType> InventoryServiceType = new List<InventoryServiceType>();
            List<InventoryServiceType> InventoryServiceType1 = new List<InventoryServiceType>();
            List<InventoryServiceType> InventoryServiceType2 = new List<InventoryServiceType>();
            List<InventoryServiceType> InventoryServiceType3 = new List<InventoryServiceType>();
            List<InventoryDepartment> InventoryDepartment = new List<InventoryDepartment>();
            //Globally to check the login status
            GlobalData Global = new GlobalData();
            if (HttpContext.Session.GetString("is_cust_login") != "True")
            {
                GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (userID?.CurrentUser.UserID == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                if (CustomerDetail?.CurrentCust.CustContactId == null)
                {
                    return RedirectToAction("CustomerLogin", "Login");
                }

            }

            GlobalData cust_id = GlobalData.GetGlobalSessione(HttpContext.Session, "cust_id");
            GlobalData facility_id = GlobalData.GetGlobalSessione(HttpContext.Session, "facility_id");

            
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.customer_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;
            try
            {
                DataSet ds = new DataSet();

                // OwnerShip DropDown

                DataView dvInventoryOwnership = TableDefs.Inventory.GetInventoryOwnerShipForDropDown(ds, true, "* Select One *");
                if (dvInventoryOwnership != null)
                {
                    DataTable dtInventoryOwnership = dvInventoryOwnership.ToTable();
                    for (int i = 0; i < dtInventoryOwnership.Rows.Count; i++)
                    {
                        InventoryOwnerShipDrop status = new InventoryOwnerShipDrop();
                        status.inv_ownership_tp_id = Utility.ToInt(dtInventoryOwnership.Rows[i]["inv_ownership_tp_id"]);
                        status.inv_ownership_tp_desc = dtInventoryOwnership.Rows[i]["inv_ownership_tp_desc"].ToString();
                        InventoryOwnerShipDrop.Add(status);
                    }
                }

                List<SelectListItem> OwnerShipModule = new List<SelectListItem>();
                foreach (var item in InventoryOwnerShipDrop)
                {
                    OwnerShipModule.Add(new SelectListItem
                    {
                        Text = item.inv_ownership_tp_desc.ToString(),
                        Value = item.inv_ownership_tp_id.ToString()
                    });
                }
                response.OwnerShipModule = OwnerShipModule;

                //Interval Pm  DropDown

                DataView dvInventoryIntervalPm = TableDefs.Inventory.GetInventoryIntervalPMForDropDown(ds, true, "* Select One *");
                if (dvInventoryIntervalPm != null)
                {
                    DataTable dtInventoryIntervalPm = dvInventoryIntervalPm.ToTable();
                    for (int i = 0; i < dtInventoryIntervalPm.Rows.Count; i++)
                    {
                        InventoryIntervalpm status = new InventoryIntervalpm();
                        status.inv_pm_interval_tp_id = Utility.ToInt(dtInventoryIntervalPm.Rows[i]["inv_pm_interval_tp_id"]);
                        status.inv_pm_interval_tp_desc = dtInventoryIntervalPm.Rows[i]["inv_pm_interval_tp_desc"].ToString();
                        InventoryIntervalpm.Add(status);
                    }
                }

                List<SelectListItem> IntervalPmModule = new List<SelectListItem>();
                foreach (var item in InventoryIntervalpm)
                {
                    IntervalPmModule.Add(new SelectListItem
                    {
                        Text = item.inv_pm_interval_tp_desc.ToString(),
                        Value = item.inv_pm_interval_tp_id.ToString()
                    });
                }
                response.IntervalPmModule = IntervalPmModule;

                // Alarm DropDown
                DataView dvInventoryAlarm = TableDefs.Inventory.GetInventoryAlarmForDropDown(ds, true, "* Select One *");
                if (dvInventoryAlarm != null)
                {
                    DataTable dtInventoryAlarm = dvInventoryAlarm.ToTable();
                    for (int i = 0; i < dtInventoryAlarm.Rows.Count; i++)
                    {
                        InventoryAlarm status = new InventoryAlarm();
                        status.inv_alarm_tp_id = Utility.ToInt(dtInventoryAlarm.Rows[i]["inv_alarm_tp_id"]);
                        status.inv_alarm_tp_desc = dtInventoryAlarm.Rows[i]["inv_alarm_tp_desc"].ToString();
                        InventoryAlarm.Add(status);
                    }
                }

                List<SelectListItem> IntervalAlarm = new List<SelectListItem>();
                foreach (var item in InventoryAlarm)
                {
                    IntervalAlarm.Add(new SelectListItem
                    {
                        Text = item.inv_alarm_tp_desc.ToString(),
                        Value = item.inv_alarm_tp_id.ToString()
                    });
                }
                response.AlarmModule = IntervalAlarm;
                // Item Coverage DropDown
                DataView dvInventoryItemCoverage = TableDefs.Inventory.GetInventoryItemCoverageForDropDown(ds, true, "* Select One *");
                if (dvInventoryItemCoverage != null)
                {
                    DataTable dtInventoryItemCoverage = dvInventoryItemCoverage.ToTable();
                    for (int i = 0; i < dtInventoryItemCoverage.Rows.Count; i++)
                    {
                        InventoryItemCoverage status = new InventoryItemCoverage();
                        status.inv_cover_tp_id = Utility.ToInt(dtInventoryItemCoverage.Rows[i]["inv_cover_tp_id"]);
                        status.inv_cover_tp_desc = dtInventoryItemCoverage.Rows[i]["inv_cover_tp_desc"].ToString();
                        InventoryItemCoverage.Add(status);
                    }
                }
                List<SelectListItem> IntervalItemCoverage = new List<SelectListItem>();
                foreach (var item in InventoryItemCoverage)
                {
                    IntervalItemCoverage.Add(new SelectListItem
                    {
                        Text = item.inv_cover_tp_desc.ToString(),
                        Value = item.inv_cover_tp_id.ToString()
                    });
                }
                response.ItemCoverageModule = IntervalItemCoverage;
                // VendorList Drop Down
                DataView dvInventoryProvider = TableDefs.Inventory.GetInventoryProviderForDropDown(ds, true, "* Select One *");
                if (dvInventoryProvider != null)
                {
                    DataTable dtInventoryProvider = dvInventoryProvider.ToTable();
                    for (int i = 0; i < dtInventoryProvider.Rows.Count; i++)
                    {
                        InventorVendorList status = new InventorVendorList();
                        status.vendor_id = Utility.ToInt(dtInventoryProvider.Rows[i]["vendor_id"]);
                        status.Vendor_Name = dtInventoryProvider.Rows[i]["Vendor_Name"].ToString();
                        InventorVendorList.Add(status);
                    }
                }
                List<SelectListItem> IntervalVendorList = new List<SelectListItem>();
                foreach (var item in InventorVendorList)
                {
                    IntervalVendorList.Add(new SelectListItem
                    {
                        Text = item.Vendor_Name.ToString(),
                        Value = item.vendor_id.ToString()
                    });
                }

                response.VendorListModule = IntervalVendorList;
                //Battery Changed DropDown

                DataView dvInventoryBatteryInterval = TableDefs.Inventory.GetInventoryBatteryIntervalForDropDown(ds, true, "* Select One *");
                if (dvInventoryBatteryInterval != null)
                {
                    DataTable dtInventoryBatteryInterval = dvInventoryBatteryInterval.ToTable();
                    for (int i = 0; i < dtInventoryBatteryInterval.Rows.Count; i++)
                    {
                        InventoryBatteryInterval status = new InventoryBatteryInterval();
                        status.inv_bat_change_tp_id = Utility.ToInt(dtInventoryBatteryInterval.Rows[i]["inv_bat_change_tp_id"]);
                        status.inv_bat_change_tp_desc = dtInventoryBatteryInterval.Rows[i]["inv_bat_change_tp_desc"].ToString();
                        InventoryBatteryInterval.Add(status);
                    }
                }
                List<SelectListItem> IntervalBatteryInterval = new List<SelectListItem>();
                foreach (var item in InventoryBatteryInterval)
                {
                    IntervalBatteryInterval.Add(new SelectListItem
                    {
                        Text = item.inv_bat_change_tp_desc.ToString(),
                        Value = item.inv_bat_change_tp_id.ToString()
                    });
                }

                response.BatteryChangeModule = IntervalBatteryInterval;
                //Manufacturer List DropDown
                DataView dvInventoryManufacturerList = TableDefs.Inventory.GetInventoryManufactureForDropDown(ds, true, "* Select One *");
                if (dvInventoryManufacturerList != null)
                {
                    DataTable dtInventoryManufacturerList = dvInventoryManufacturerList.ToTable();
                    for (int i = 0; i < dtInventoryManufacturerList.Rows.Count; i++)
                    {
                        InventoryManufactureList status = new InventoryManufactureList();
                        status.mfg_id = Utility.ToInt(dtInventoryManufacturerList.Rows[i]["mfg_id"]);
                        status.mfg_name = dtInventoryManufacturerList.Rows[i]["mfg_name"].ToString();
                        InventoryManufactureList.Add(status);
                    }
                }
                List<SelectListItem> IntervalManufactureList = new List<SelectListItem>();
                foreach (var item in InventoryManufactureList)
                {
                    IntervalManufactureList.Add(new SelectListItem
                    {
                        Text = item.mfg_name.ToString(),
                        Value = item.mfg_id.ToString()
                    });
                }
                response.ManufactureModule = IntervalManufactureList;
                // Warrenty Status DropDown
                DataView dvInventoryWarrentyStatus = TableDefs.Inventory.GetWarrantyStatussForDropDown(ds, false, true, "* Select One *");
                if (dvInventoryWarrentyStatus != null)
                {
                    DataTable dtInventoryWarrentyStatus = dvInventoryWarrentyStatus.ToTable();
                    for (int i = 0; i < dtInventoryWarrentyStatus.Rows.Count; i++)
                    {
                        InventoryWarrentyStatus status = new InventoryWarrentyStatus();
                        status.inv_war_stat_id = Utility.ToInt(dtInventoryWarrentyStatus.Rows[i]["inv_war_stat_id"]);
                        status.inv_war_stat_desc = dtInventoryWarrentyStatus.Rows[i]["inv_war_stat_desc"].ToString();
                        InventoryWarrentyStatus.Add(status);
                    }
                }
                List<SelectListItem> IntervalWarrentyStatus = new List<SelectListItem>();
                foreach (var item in InventoryWarrentyStatus)
                {
                    IntervalWarrentyStatus.Add(new SelectListItem
                    {
                        Text = item.inv_war_stat_desc.ToString(),
                        Value = item.inv_war_stat_id.ToString()
                    });
                }
                response.WarrentyStatusModule = IntervalWarrentyStatus;
                // Status DropDown
                DataView dvInventoryStatus = TableDefs.Inventory.GetInventoryStatusForDropDown(ds, true, "* Select One *");
                if (dvInventoryStatus != null)
                {
                    DataTable dtInventoryStatus = dvInventoryStatus.ToTable();
                    for (int i = 0; i < dtInventoryStatus.Rows.Count; i++)
                    {
                        InventoryStatus status = new InventoryStatus();
                        status.inv_stat_id = Utility.ToInt(dtInventoryStatus.Rows[i]["inv_stat_id"]);
                        status.inv_stat_desc = dtInventoryStatus.Rows[i]["inv_stat_desc"].ToString();
                        InventoryStatus.Add(status);
                    }
                }
                List<SelectListItem> IntervalStatus = new List<SelectListItem>();
                foreach (var item in InventoryStatus)
                {
                    IntervalStatus.Add(new SelectListItem
                    {
                        Text = item.inv_stat_desc.ToString(),
                        Value = item.inv_stat_id.ToString()
                    });
                }
                response.StatusModule = IntervalStatus;
                //Managed Entity DropDown
                DataView dvInventoryManaged = TableDefs.General.GetManagedEntityTypesForDropDown(ds, true, true, "* Select One *");
                if (dvInventoryManaged != null)
                {
                    DataTable dtInventoryManaged = dvInventoryManaged.ToTable();
                    for (int i = 0; i < dtInventoryManaged.Rows.Count; i++)
                    {
                        InventoryManagedEntity status = new InventoryManagedEntity();
                        status.managed_entity_tp_id = Utility.ToInt(dtInventoryManaged.Rows[i]["managed_entity_tp_id"]);
                        status.managed_entity_tp_desc = dtInventoryManaged.Rows[i]["managed_entity_tp_desc"].ToString();
                        InventoryManagedEntity.Add(status);
                    }
                }
                List<SelectListItem> IntervalManaged = new List<SelectListItem>();
                foreach (var item in InventoryManagedEntity)
                {
                    IntervalManaged.Add(new SelectListItem
                    {
                        Text = item.managed_entity_tp_desc.ToString(),
                        Value = item.managed_entity_tp_id.ToString()
                    });
                }
                response.ManagedEntityModule = IntervalManaged;
                // Root Class DropDown
                DataView dvInventoryRootClass = TableDefs.Inventory.GetProductClassForDropDown(ds, true, true, "* Select One *");
                if (dvInventoryRootClass != null)
                {
                    DataTable dtInventoryRootClass = dvInventoryRootClass.ToTable();
                    for (int i = 0; i < dtInventoryRootClass.Rows.Count; i++)
                    {
                        InventoryRootClass status = new InventoryRootClass();
                        status.prod_class_id = Utility.ToInt(dtInventoryRootClass.Rows[i]["prod_class_id"]);
                        status.prod_class_desc = dtInventoryRootClass.Rows[i]["prod_class_desc"].ToString();
                        InventoryRootClass.Add(status);
                    }
                }
                List<SelectListItem> IntervalRootClass = new List<SelectListItem>();
                foreach (var item in InventoryRootClass)
                {
                    IntervalRootClass.Add(new SelectListItem
                    {
                        Text = item.prod_class_desc.ToString(),
                        Value = item.prod_class_id.ToString()
                    });
                }
                response.RootClassModule = IntervalRootClass;

                // Service Coverage DropDown
                DataView dvInventoryService = TableDefs.Inventory.GetInventoryServiceCoverageForDropDown(ds, true, "* Select One *", true);
                if (dvInventoryService != null)
                {
                    DataTable dtInventoryService = dvInventoryService.ToTable();
                    for (int i = 0; i < dtInventoryService.Rows.Count; i++)
                    {
                        InventoryServiceCover status = new InventoryServiceCover();
                        status.inv_service_cover_tp_id = Utility.ToInt(dtInventoryService.Rows[i]["inv_service_cover_tp_id"]);
                        status.inv_service_cover_tp_desc = dtInventoryService.Rows[i]["inv_service_cover_tp_desc"].ToString();
                        InventoryServiceCover.Add(status);
                    }
                }
                List<SelectListItem> IntervalService = new List<SelectListItem>();
                foreach (var item in InventoryServiceCover)
                {
                    IntervalService.Add(new SelectListItem
                    {
                        Text = item.inv_service_cover_tp_desc.ToString(),
                        Value = item.inv_service_cover_tp_id.ToString()
                    });
                }
                response.ServiceCoverModule = IntervalService;
                // Department DropDown
                
                DataView dvInventoryDepartment= TableDefs.Inventory.GetInventoryDepartmentForDropDown(ds, true, "* Select One *",true);
                if (dvInventoryDepartment != null)
                {
                    DataTable dtInventoryStatus = dvInventoryDepartment.ToTable();
                    for (int i = 0; i < dtInventoryStatus.Rows.Count; i++)
                    {
                        InventoryDepartment status = new InventoryDepartment();
                        status.dept_id = Utility.ToInt(dtInventoryStatus.Rows[i]["dept_id"]);
                        status.dept_name = dtInventoryStatus.Rows[i]["dept_name"].ToString();
                        InventoryDepartment.Add(status);
                    }
                }
                List<SelectListItem> IntervalDepartment = new List<SelectListItem>();
                foreach (var item in InventoryDepartment)
                {
                    IntervalDepartment.Add(new SelectListItem
                    {
                        Text = item.dept_name.ToString(),
                        Value = item.dept_id.ToString()
                    });
                }
                response.DepartmentModule = IntervalDepartment;




                string invID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
                int ID = Convert.ToInt32(invID);

                DataRow GetInventory = TableDefs.Inventory.GetInventory(ds, ID);
                if (GetInventory != null)

                {                
                    inventory.inv_id = Utility.ToInt(GetInventory["inv_id"]);
                    inventory.facility_id = Utility.ToInt(GetInventory["facility_id"]);                  
                    inventory.ManufactureList = Utility.ToInt(GetInventory["mfg_id"]);
                    inventory.HospitalControl = GetInventory["hos_control_num"].ToString();
                    inventory.Control = GetInventory["control_num"].ToString();
                    inventory.Department = Utility.ToInt(GetInventory["dept_id"]);
                    inventory.risk_num = Utility.ToInt(GetInventory["risk_num"]);
                    inventory.Ownership = Utility.ToInt(GetInventory["ownership_tp_id"]);
                    inventory.InstalledDate = string.IsNullOrEmpty(GetInventory["install_dt"].ToString()) ? null : Convert.ToDateTime(GetInventory["install_dt"]).ToString("MM/dd/yyyy");
                    inventory.WarrentyExpires = string.IsNullOrEmpty(GetInventory["warranty_exp_dt"].ToString()) ? null : Convert.ToDateTime(GetInventory["warranty_exp_dt"]).ToString("MM/dd/yyyy");
                   
                    inventory.WarrentyStatus = Utility.ToInt(GetInventory["warranty_stat_id"]);
                    inventory.Status = Utility.ToInt(GetInventory["inv_stat_id"]);
                    inventory.LastPmCompleted = string.IsNullOrEmpty(GetInventory["last_activity_dt"].ToString()) ? null : Convert.ToDateTime(GetInventory["last_activity_dt"]).ToString("MM/dd/yyyy");
                    inventory.NextSchedulePm = string.IsNullOrEmpty(GetInventory["next_activity_dt"].ToString()) ? null : Convert.ToDateTime(GetInventory["next_activity_dt"]).ToString("MM/dd/yyyy");                    
                    inventory.ItemCoverage = Utility.ToInt(GetInventory["coverage_tp_id"]);
                    inventory.PmInterval = Utility.ToInt(GetInventory["pm_interval_tp_id"]);
                    inventory.ServiceType = Utility.ToInt(GetInventory["service_tp_id"]);

                    inventory.EquipmentSerial = GetInventory["serial_no"].ToString();
                    inventory.vendor_id = Utility.ToInt(GetInventory["vendor_id"]);
                    inventory.vendor_coverage = Utility.ToInt(GetInventory["vendor_coverage"]);
                    inventory.SystemId = GetInventory["system_id"].ToString();
                    inventory.PreviousControl = GetInventory["previous_control_num"].ToString();
                    inventory.Room = GetInventory["room"].ToString();              

                    inventory.InventoryNotes = GetInventory["notes"].ToString();
                    inventory.Provider1 = Utility.ToInt(GetInventory["vendor_id_1"]);
                    inventory.SrvCoverage1 = Utility.ToInt(GetInventory["coverage_1"]);
                    inventory.Provider2 = Utility.ToInt(GetInventory["vendor_id_2"]);
                    inventory.SrvCoverage2 = Utility.ToInt(GetInventory["coverage_2"]);

                    inventory.ManagedEntity = Utility.ToInt(GetInventory["managed_entity_tp_id"]);
                    inventory.Alarm = Utility.ToInt(GetInventory["alarm_tp_id"]);
                    inventory.HasBattery = Convert.ToBoolean(GetInventory["is_battery_replaced"]);
                    inventory.IsActive = Convert.ToBoolean(GetInventory["is_active"]);
                    inventory.RootClass = Utility.ToInt(GetInventory["root_class_id"]);
                    inventory.SubClass = Utility.ToInt(GetInventory["sub_class_id"]);
                    inventory.SelectedModelDescription= GetInventory["model"].ToString();

                    inventory.BatteryLastChanged = string.IsNullOrEmpty(GetInventory["battery_last_changed_dt"].ToString()) ? null : Convert.ToDateTime(GetInventory["battery_last_changed_dt"]).ToString("MM/dd/yyyy");
                    inventory.BatteryChangeInterval = Utility.ToInt(GetInventory["battery_interval"]);                  
                    inventory.IsActive = Convert.ToBoolean(GetInventory["is_active"]);
                    DataRow FacilityName = TableDefs.Facility.GetFacility(ds, inventory.facility_id);
                    if (FacilityName != null)
                    {
                        inventory.FacilityName = FacilityName["facility_name"].ToString();
                        inventory.cust_id = Utility.ToInt(FacilityName["cust_id"]);
                    }
                    DataRow customername = TableDefs.Customer.GetCustomer(ds, inventory.cust_id);
                    if (customername != null)
                    {
                        inventory.CustomerName = customername["cust_name"].ToString();
                    }
                    //Edit bind Sub Class ID based the Root Class ID
                    int RootClassID = inventory.RootClass;
                    if (RootClassID != 0)
                    {
                        // Sub Root Class DropDown
                        DataView dvInventorySubRootClass = TableDefs.Inventory.GetProductSubClassByRootClass(ds, RootClassID, true, true, "* Select One *");
                        if (dvInventorySubRootClass != null)
                        {
                            DataTable dtInventorySubRootClass = dvInventorySubRootClass.ToTable();
                            for (int i = 0; i < dtInventorySubRootClass.Rows.Count; i++)
                            {
                                InventoryPoductSubRootClass status = new InventoryPoductSubRootClass();
                                status.prod_sub_class_id = Utility.ToInt(dtInventorySubRootClass.Rows[i]["prod_sub_class_id"]);
                                status.prod_sub_class_desc = dtInventorySubRootClass.Rows[i]["prod_sub_class_desc"].ToString();
                                InventorySubRootClass.Add(status);
                            }
                        }
                        List<SelectListItem> IntervalSubRootClass = new List<SelectListItem>();
                        foreach (var item in InventorySubRootClass)
                        {
                            IntervalSubRootClass.Add(new SelectListItem
                            {
                                Text = item.prod_sub_class_desc.ToString(),
                                Value = item.prod_sub_class_id.ToString()
                            });
                        }
                        response.SubRootClassModule = IntervalSubRootClass;

                    }
                    else
                    {
                        List<SelectListItem> IntervalSubRootClass = new List<SelectListItem>();
                        
                            IntervalSubRootClass.Add(new SelectListItem
                            {
                                Text = "* Select One *",
                                Value = "0"
                            });
                        
                        response.SubRootClassModule = IntervalSubRootClass;
                    }

                    // Service type dropdown
                    DataView dvInventorytypes = TableDefs.General.GetServiceTypesForDropDown(ds, true, true, "* Select One *");
                    if (dvInventorytypes != null)
                    {
                        DataTable dtInventorytypes = dvInventorytypes.ToTable();
                        for (int i = 0; i < dtInventorytypes.Rows.Count; i++)
                        {
                            InventoryServiceType service = new InventoryServiceType();
                            service.service_tp_id = Utility.ToInt(dtInventorytypes.Rows[i]["service_tp_id"]);
                            service.service_tp_desc = dtInventorytypes.Rows[i]["service_tp_desc"].ToString();
                            InventoryServiceType.Add(service);
                        }
                    }
                    List<SelectListItem> IntervalServicetype = new List<SelectListItem>();
                    List<SelectListItem> IntervalServicetype2 = new List<SelectListItem>();

                    DataView servicedrop = TableDefs.Customer.GetCustomerService(ds, inventory.cust_id);

                    if (servicedrop != null)
                    {
                        DataTable dtServicetypes = servicedrop.ToTable();
                        for (int i = 0; i < dtServicetypes.Rows.Count; i++)
                        {
                            InventoryServiceType service = new InventoryServiceType();
                            service.service_tp_id = Utility.ToInt(dtServicetypes.Rows[i]["service_tp_id"]);
                            TempInventoryServiceType.Add(service);
                        }
                    }
                    foreach (var type in TempInventoryServiceType)
                    {
                        InventoryServiceType service = new InventoryServiceType();

                        service = InventoryServiceType.Where(c => c.service_tp_id == Convert.ToInt32(type.service_tp_id)).FirstOrDefault();
                        InventoryServiceType1.Add(service);
                    }
                    if (InventoryServiceType1.Count > 1)
                    {
                        InventoryServiceType services = new InventoryServiceType();
                        services.service_tp_id = 0;
                        services.service_tp_desc = "*Select One*";
                        InventoryServiceType2.Add(services);
                        InventoryServiceType3.AddRange(InventoryServiceType2);
                        InventoryServiceType1.AddRange(InventoryServiceType3);
                    }
                   
                    foreach (var item in InventoryServiceType1.OrderBy(x => x.service_tp_id))
                    {
                        IntervalServicetype.Add(new SelectListItem
                        {
                            Text = item.service_tp_desc.ToString(),
                            Value = item.service_tp_id.ToString()
                        });
                    }
                    response.ServiceTypeModule = IntervalServicetype;
                }
            
                else
                {
                    inventory.facility_id = facility_id.facility_id;
                    inventory.cust_id = facility_id.cust_id;
                    inventory.BatteryLastChanged = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    inventory.NextSchedulePm = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    inventory.WarrentyExpires = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    inventory.InstalledDate = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    inventory.LastPmCompleted = DateTime.UtcNow.ToString("MM/dd/yyyy");
                    inventory.IsActive = true;
                    DataRow FacilityName = TableDefs.Facility.GetFacility(ds, inventory.facility_id);
                    if (FacilityName != null)
                    {
                        inventory.FacilityName = FacilityName["facility_name"].ToString();
                    }
                    DataRow customername = TableDefs.Customer.GetCustomer(ds, inventory.cust_id);
                    if (customername != null)
                    
                    {
                        inventory.CustomerName = customername["cust_name"].ToString();
                    }

                    DataView dvInventorySubRootClass = TableDefs.Inventory.GetProductSubClassByRootClass(ds, inventory.cust_id, true, true, "* Select One *");
                    if (dvInventorySubRootClass != null)
                    {
                        DataTable dtInventorySubRootClass = dvInventorySubRootClass.ToTable();
                        for (int i = 0; i < dtInventorySubRootClass.Rows.Count; i++)
                        {
                            InventoryPoductSubRootClass status = new InventoryPoductSubRootClass();
                            status.prod_sub_class_id = Utility.ToInt(dtInventorySubRootClass.Rows[i]["prod_sub_class_id"]);
                            status.prod_sub_class_desc = dtInventorySubRootClass.Rows[i]["prod_sub_class_desc"].ToString();
                            InventorySubRootClass.Add(status);
                        }
                    }
                    List<SelectListItem> IntervalSubRootClass = new List<SelectListItem>();
                    foreach (var item in InventorySubRootClass)
                    {
                        IntervalSubRootClass.Add(new SelectListItem
                        {
                            Text = item.prod_sub_class_desc.ToString(),
                            Value = item.prod_sub_class_id.ToString()
                        });
                    }
                    response.SubRootClassModule = IntervalSubRootClass;
                    // Service type dropdown

                    DataView dvInventorytypes = TableDefs.General.GetServiceTypesForDropDown(ds, true, true, "* Select One *");
                    if (dvInventorytypes != null)
                    {
                        DataTable dtInventorytypes = dvInventorytypes.ToTable();
                        for (int i = 0; i < dtInventorytypes.Rows.Count; i++)
                        {
                            InventoryServiceType service = new InventoryServiceType();
                            service.service_tp_id = Utility.ToInt(dtInventorytypes.Rows[i]["service_tp_id"]);
                            service.service_tp_desc = dtInventorytypes.Rows[i]["service_tp_desc"].ToString();
                            InventoryServiceType.Add(service);
                        }
                    }
                    List<SelectListItem> IntervalServicetype = new List<SelectListItem>();
                    List<SelectListItem> IntervalServicetype2 = new List<SelectListItem>();
                    DataView servicedrop = TableDefs.Customer.GetCustomerService(ds, inventory.cust_id);

                    if (servicedrop != null)
                    {
                        DataTable dtServicetypes = servicedrop.ToTable();
                        for (int i = 0; i < dtServicetypes.Rows.Count; i++)
                        {
                            InventoryServiceType service = new InventoryServiceType();
                            service.service_tp_id = Utility.ToInt(dtServicetypes.Rows[i]["service_tp_id"]);
                            TempInventoryServiceType.Add(service);
                        }
                    }
                    foreach (var type in TempInventoryServiceType)
                    {
                        InventoryServiceType service = new InventoryServiceType();

                        service = InventoryServiceType.Where(c => c.service_tp_id == Convert.ToInt32(type.service_tp_id)).FirstOrDefault();
                        InventoryServiceType1.Add(service);
                    }
                    if (InventoryServiceType1.Count >1)
                    {
                        InventoryServiceType services = new InventoryServiceType();
                        services.service_tp_id = 0;
                        services.service_tp_desc = "*Select One*";
                        InventoryServiceType2.Add(services);
                        InventoryServiceType3.AddRange(InventoryServiceType2);
                        InventoryServiceType1.AddRange(InventoryServiceType3);
                    }
                    foreach (var item in InventoryServiceType1.OrderBy(x => x.service_tp_id))
                    {
                        IntervalServicetype.Add(new SelectListItem
                        {
                            Text = item.service_tp_desc.ToString(),
                            Value = item.service_tp_id.ToString()
                        });
                    }

                    response.ServiceTypeModule = IntervalServicetype;
                }

                if (inventory.inv_id > 0)
                {
                    response.Inventorydetail = inventory;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.Inventorydetail = inventory;
                    response.Status = "Failure";
                    response.ErrorMessage = " Inventory not found";
                    response.Statuscode = 0;
                }
            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Get Inventory/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Get Inventory/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
                return RedirectToAction("Error_Log", "Home");
            }
            ViewBag.ShowAll = Global.customer_show_closed;
            return View(response);
        }
        #endregion

        #region Grid data load
        [HttpPost]
        public JsonResult LoadInventoryGrid(bool isactive, bool toggleActive, int CustID, int FacilityID)
        {
            LoadState loadState = new LoadState();
            GetInventoryResponse response = new GetInventoryResponse();
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            
            try
            {
                if (HttpContext.Session.GetString("is_cust_login") == "True")
                {
                    
                    GlobalData CustomerDetail = GlobalData.GetGlobalSessione(HttpContext.Session, "CustData");
                    CustID = CustomerDetail.CurrentCust.cust_id;
                    Global.facility_id = FacilityID;
                    Global.cust_id = CustID;
                    GlobalData.SetGlobalSessione(HttpContext.Session, "facility_id", Global);
                    GlobalData.SetGlobalSessione(HttpContext.Session, "cust_id", Global);

                    DataView dvFacility = TableDefs.Inventory.GetInventoryByCustomerFacility(ds, CustID, FacilityID, isactive);
                    string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                    var listFacility = JsonConvert.DeserializeObject<lst_tbl_inv>(jsonData2);

                    dsResult.Data = listFacility.tbl_Inv.ToList().OrderBy(x => x.inv_id);
                    dsResult.Total = listFacility.tbl_Inv.Count;
                }
                else
                {
                    mr = TableDefs.User.GetUserGrid(ds, userID.CurrentUser.UserID, 60);
                    if (mr != null)
                    {
                        if (mr.Table.Rows.Count > 0)
                        {
                            loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                            loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                            loadState.json_col_ord = mr["json_col_ord"].ToString();
                            response.loadState = loadState;
                            dsResult.Errors = "true";

                        }
                    }
                    if (CustID != 0 && FacilityID != 0)
                    {
                        //Set Facility and Cust ID
                        Global.facility_id = FacilityID;
                        Global.cust_id = CustID;
                        GlobalData.SetGlobalSessione(HttpContext.Session, "facility_id", Global);
                        GlobalData.SetGlobalSessione(HttpContext.Session, "cust_id", Global);

                        DataView dvFacility = TableDefs.Inventory.GetInventoryByCustomerFacility(ds, CustID, FacilityID, isactive);
                        string jsonData2 = JsonConvert.SerializeObject(ds, Formatting.Indented);
                        var listFacility = JsonConvert.DeserializeObject<lst_tbl_inv>(jsonData2);

                        dsResult.Data = listFacility.tbl_Inv.ToList().OrderBy(x => x.inv_id);
                        dsResult.Total = listFacility.tbl_Inv.Count;
                    }
                }
               
            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Grid Load/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Grid Load/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                result.success = false;
                result.message = $"An error occurred retrieving Inventory List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }

        #endregion

        #region Add Edit Functionality

        [HttpPost]
        public GetInventoiesResponse AddEditInventories(Inventory request)
        {
            int userID = 0;
            GetInventoiesResponse response = new GetInventoiesResponse();
            try
            {
                if (ModelState.IsValid)
                {
                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    if (data != null)
                    {
                        userID = data.CurrentUser.UserID;
                        if (request.inv_id == 0)
                        {
                            DBAccess.executeSQL("usp_inv_new", userID, request.facility_id, request.ManufactureList, request.RootClass, request.SubClass,
                                request.SelectedModelDescription, request.ServiceType, request.HospitalControl, request.Control,
                              request.Department, 0, request.Ownership,request.InstalledDate, request.WarrentyExpires, request.Status,
                       request.LastPmCompleted,request.NextSchedulePm, request.ItemCoverage, request.PmInterval, request.EquipmentSerial,
                                request.WarrentyStatus, 0, 0, request.SystemId, request.PreviousControl, request.Room, DateTime.UtcNow.ToString("MM/dd/yyyy"), request.InventoryNotes,
                                 request.Provider1, request.SrvCoverage1, request.Provider2, request.SrvCoverage2, request.ManagedEntity,
                                request.Alarm, request.HasBattery,request.BatteryLastChanged, request.BatteryChangeInterval, DateTime.UtcNow.ToString("MM/dd/yyyy"),
                                request.IsActive);
                        }
                        else
                        {
                            DBAccess.executeSQL("usp_inv_update", userID, request.inv_id, request.facility_id, request.ManufactureList, request.RootClass, request.SubClass,
                                request.SelectedModelDescription, request.ServiceType, request.HospitalControl, request.Control,
                              request.Department, 0, request.Ownership,request.InstalledDate,request.WarrentyExpires, request.Status,
                         request.LastPmCompleted,request.NextSchedulePm, request.ItemCoverage, request.PmInterval, request.EquipmentSerial,
                                request.WarrentyStatus, 0, 0, request.SystemId, request.PreviousControl, request.Room, DateTime.UtcNow.ToString("MM/dd/yyyy"), request.InventoryNotes, 0,
                                 request.Provider1, request.SrvCoverage1, request.Provider2, request.SrvCoverage2, request.ManagedEntity,
                                request.Alarm, request.HasBattery, request.BatteryLastChanged, request.BatteryChangeInterval, DateTime.UtcNow.ToString("MM/dd/yyyy"),
                                request.IsActive);

                        }
                        response.Status = "Success";
                    }
                }
  
            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Add-Edit/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Add-Edit/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region Inventory Manufacture Model Grid Load
        [HttpPost]
        public JsonResult LoadManufactureModelGrid(int mfg_model_id)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();

            try
            {
                // get the value for sp, API Call
                DataRow dvInventory = TableDefs.Inventory.GetManufacturer(ds, mfg_model_id);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listMModel = JsonConvert.DeserializeObject<lst_tbl_mmodel>(jsonData);

                dsResult.Data = listMModel.tbl_mfg_model.OrderBy(x => x.mfg_model_id).ToList();
                dsResult.Total = listMModel.tbl_mfg_model.Count;
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Manufacture Model/Inventory", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Inventory List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region Get  SubClass DropDown
        public JsonResult SubClassDropDown(int id)
        {
            GetInventoiesResponse response = new GetInventoiesResponse();
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                DataView dvsubClass = TableDefs.Inventory.GetProductSubClassByRootClass(ds, id,false,false,"All");
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listsubClass = JsonConvert.DeserializeObject<lst_tbl_Subclass>(jsonData);

                dsResult.Data = listsubClass.tbl_prod_sub_class.ToList();
                dsResult.Total = listsubClass.tbl_prod_sub_class.Count;
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Sub-Class DropDown/Inventory", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Inventory List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region Inventory Export
        [HttpPost]
        [Obsolete]
        public GetInventoiesResponse ExportInventory(List<string> InventoryList)
        {
            DataSet ds = new DataSet();
            GetInventoiesResponse response = new GetInventoiesResponse();

            try
            {
                if (InventoryList != null)
                {
                    if (InventoryList != null && InventoryList.Count > 0)
                    {
                        //Set File Name and Path
                        string FilePath = Path.Combine(_hostingEnvironment.ContentRootPath, "wwwroot/ExportFile/");
                        string docPath = Path.Combine(FilePath, "inventory/");
                        if (Directory.Exists(docPath))
                        {
                            DirectoryInfo di = new DirectoryInfo(docPath);
                            FileInfo[] files = di.GetFiles();
                            foreach (FileInfo file in files)
                            {
                                file.Delete();
                            }
                        }

                        string file_name = $"inventory_{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";
                        string pathFileName = docPath + file_name;

                        if (!Directory.Exists(FilePath))
                        {
                            Directory.CreateDirectory(FilePath);
                        }

                        // Path Not exists then create new file path
                        if (!Directory.Exists(docPath))
                        {
                            Directory.CreateDirectory(docPath);
                        }
                        //File name  exists then Delete the  file
                        if (System.IO.File.Exists(pathFileName))
                        {
                            System.IO.File.Delete(docPath);
                        }

                        //convert list to string
                        string inventoryList = String.Join<string>(",", InventoryList);
                        DataTable dtInventory = TableDefs.Inventory.GetInventoryForExport(ds, inventoryList).ToTable();

                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dtInventory);
                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(pathFileName);
                                response.Status = "Success";
                                response.ExcelLink = "/ExportFile/inventory/" + file_name;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "Export/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "Export/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
            }
            return response;
        }
        #endregion

        #region Add Edit Functionality

        [HttpPost]
        public GetInventoryResponse AddModels(mfg_model request)
        {
            int userID = 0;
            GetInventoryResponse response = new GetInventoryResponse();
            try
            {
                GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                if (data != null)
                 {
                    userID = data.CurrentUser.UserID;
                    if (request.mfg_model_id == 0)
                    {
                        DBAccess.executeSQL("usp_mfg_model_new", userID, request.prod_sub_class_id, request.mfg_id, request.model_name,
                      "" , request.is_active);
                    }
                    else
                    {
                        DBAccess.executeSQL("usp_mfg_model_update", userID,request.mfg_model_id, request.prod_sub_class_id, request.mfg_id, request.model_name,
                          request.model_desc, request.is_active);

                    }
                    response.Status = "Success";
                }
            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region SaveStateData
        [HttpPost]
        public IActionResult SaveStateData(int usr_id, int grid_id, string json_col_ord)
        {
            GetInventoryResponse response = new GetInventoryResponse();
            DBAccess.executeSQL("usp_usr_grid_col_update", usr_id, grid_id, json_col_ord);
            response.Status = "Success";
            return Json(response);
        }
        #endregion

        #region Load the Facilities State
        public IActionResult LoadInventoryState(int usr_id, int grid_id)
        {
            LoadState loadState = new LoadState();
            DataSet ds = new DataSet();
            GetInventoryResponse response = new GetInventoryResponse();

            mr = TableDefs.User.GetUserGrid(ds, usr_id, grid_id);
            if (mr != null)
            {
                loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                loadState.json_col_ord = mr["json_col_ord"].ToString();
            }
            if (loadState.usr_id > 0)
            {
                response.loadState = loadState;
                response.Statuscode = 1;
                response.Status = "Success";
            }
            else
            {
                response.loadState = loadState;
                response.Status = "New";
                response.ErrorMessage = "Inventory not found";
                response.Statuscode = 0;
            }
            return Json(response);
        }
        #endregion

        #region Inventory Ticket Details
        [HttpPost]
        public JsonResult GetInventoryTicket(int InventoryId)
        {
            result.success = true;
            DataSet ds = new DataSet();
            Global = new GlobalData();
            DataRow GetInventory = TableDefs.Inventory.GetInventory(ds, InventoryId);
            if(GetInventory != null)
            {
                Global.inv_id = Utility.ToInt(GetInventory["inv_id"]);
                Global.facility_id = Utility.ToInt(GetInventory["facility_id"]);
                DataRow FacilityName = TableDefs.Facility.GetFacility(ds, Utility.ToInt(GetInventory["facility_id"]));
                if (FacilityName != null)
                {
                    Global.cust_id = Utility.ToInt(FacilityName["cust_id"]);
                }
                GlobalData.SetGlobalSessione(HttpContext.Session, "Ticket_inv", Global);
            }
            else
            {
                result.success = false;
            }
           
            return Json(result);
        }
        #endregion

        #region Delete Inventory
        public GetInventoiesResponse InventoryDelete(int inv_id)
        {
            GetInventoiesResponse response = new GetInventoiesResponse();
            int userID = 0;
            GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            userID = data.CurrentUser.UserID;
            try
            {
                DBAccess.executeSQL("usp_inv_delete", userID, inv_id);
                response.Status = "Success";

            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region Move Inventory
        public GetInventoiesResponse InventoryMove(Inventory request)
        {
            GetInventoiesResponse response = new GetInventoiesResponse();
            int userID = 0;
            GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            userID = data.CurrentUser.UserID;
            try
            {
                DBAccess.executeSQL("usp_inv_move", userID,request.inv_id,request.facility_id);
                response.Status = "Success";

            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion


        #region
        public JsonResult LoadInvFileAttachment(int Inv_id)
        {
            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            try
            {
                DataView dvFile = TableDefs.General.GetFileAttachments(ds, Inv_id, (int)FileAttachType.Inventory);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listFile = JsonConvert.DeserializeObject<lst_tbl_file_attach>(jsonData);

                dsResult.Data = listFile.tbl_file_attach.ToList().OrderBy(x => x.file_attach_id);
                dsResult.Total = listFile.tbl_file_attach.Count;

                GlobalData.SetGlobalSessione(HttpContext.Session, "file_attach_id", Global);

            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "LoadFileAttachment/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "LoadFileAttachment/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                result.success = false;
                result.message = $"An error occurred retrieving Inventory List. {ex.Message}";
            }
            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        [HttpPost]
        private string GetImagePath(IFormFile image)
        {
            string VirtualPath = $@"/attachments/{DateTime.Now:yyyy}/{DateTime.Now:MM}/{DateTime.Now:dd}/";

            string photo1Name = string.Empty;
            IFormFile Photo1 = image;


            var extension = "";
            var ImagePath = "";
            if (string.Equals(image.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) || string.Equals(image.ContentType, "application/pdf", StringComparison.OrdinalIgnoreCase)
                || string.Equals(image.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) || string.Equals(image.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) || string.Equals(image.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                var millsecond = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                var FileName = Path.GetFileName(image.FileName);

                var Filename = ContentDispositionHeaderValue.Parse(image.ContentDisposition).FileName.Trim('"');
                //  Filename = hasSpecialChar(Filename);

                extension = Path.GetExtension(Filename);
                Filename = Path.GetFileNameWithoutExtension(Filename) + extension;
                var filePath = Filename;
                ImagePath = VirtualPath + filePath;

                if (!Directory.Exists(_hostingEnvironment.WebRootPath + VirtualPath))
                {
                    Directory.CreateDirectory(_hostingEnvironment.WebRootPath + VirtualPath);
                }

                // normal code
                if (System.IO.File.Exists(_hostingEnvironment.WebRootPath + ImagePath))
                    System.IO.File.Delete(_hostingEnvironment.WebRootPath + ImagePath);
                using (FileStream fs = System.IO.File.Create(_hostingEnvironment.WebRootPath + ImagePath))
                {
                    Photo1.CopyTo(fs);
                    fs.Flush();
                }
            }
            return ImagePath;
        }

        [HttpPost]
        public GetInventoiesResponse uploadInvFileAttachments(tbl_file_attach request)
        {
            GetInventoiesResponse response = new GetInventoiesResponse();
            DataSet ds = new DataSet();
            int userID = 0;
            try
            {
                GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

                double fileSizeKB = request.Image_path.Length / 1024;

                var Logo = GetImagePath(request.Image_path);
                var Files = request.Image_path;
                if (Files != null)
                {
                    userID = data.CurrentUser.UserID;
                    var FileName = Path.GetFileName(Logo);
                    DBAccess.executeSQL("usp_file_attach_new", userID, (int)FileAttachType.Inventory, request.inv_id, FileName, fileSizeKB > 0 ? (fileSizeKB + " " + "KB") : (request.Image_path.Length + " " + "bytes"), Logo);

                }
                response.Status = "Success";

            }
            catch (Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "UploadAttachment/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "UploadAttachment/Contract", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        public GetInventoiesResponse AttachmentDelete(int file_attach_id)
        {
            GetInventoiesResponse response = new GetInventoiesResponse();
            DataSet ds = new DataSet();

            try
            {
                DBAccess.executeSQL("usp_file_attach_remove", file_attach_id);
                response.Status = "Success";

            }
            catch (System.Exception ex)
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    DBAccess.executeSQL("usp_error_log_add", "Main", "AttachmentDelete/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                else
                {
                    DBAccess.executeSQL("usp_error_log_add", "Customer", "AttachmentDelete/Inventory", ex.Message);
                    HttpContext.Session.SetString("error_log", ex.Message);
                }
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion


        #region Clone Inventory
        [HttpGet]
        //DropDown
        public JsonResult DepartmentDropdown()
        {
            try
            {
                DataSet ds = new DataSet();
                List<InventoryDepartment> deptTypeDropdown = new List<InventoryDepartment>();
                DataView dvDept = TableDefs.Inventory.GetInventoryDepartmentForDropDown(ds, true, "* Select One *", true);
                if (dvDept != null)
                {
                    DataTable dtdept = dvDept.ToTable();
                    for (int i = 0; i < dtdept.Rows.Count; i++)
                    {
                        InventoryDepartment status = new InventoryDepartment();
                        status.dept_id = Utility.ToInt(dtdept.Rows[i]["dept_id"]);
                        status.dept_name = dtdept.Rows[i]["dept_name"].ToString();
                        deptTypeDropdown.Add(status);
                    }
                }
                var data = deptTypeDropdown.ToList();
                //Returning Json Data  
                return Json(new { data = data });
            }
            catch (Exception)
            {

                throw;
            }

            return Json(new { data = 0 });
        }

        [HttpPost]
        public GetInventoiesResponse PostDept(tbl_inv request)
        {
            int userID = 0;
            GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            userID = data.CurrentUser.UserID;
            GetInventoiesResponse response = new GetInventoiesResponse();
            try
            {
                if (request.inv_id != 0)

                {
                    DBAccess.executeSQL("usp_inv_clone", userID, request.inv_id, request.hos_control_num, request.control_num, request.serial_no, request.dept_id);
                }
                response.Status = "Success";
            }
            catch (System.Exception ex)
            {
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;

        }
        #endregion

    }
}
