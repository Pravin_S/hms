﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using Microsoft.AspNetCore.Http;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace HMSMemri.Controllers
{
    public class LoginController : IMCPBase
    {
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        private IConfiguration _iConfiguration;
        public LoginController(IConfiguration iConfiguration, IOptions<BaseSettings.ServerVar> serverVar) : base(serverVar)
        {
            _iConfiguration = iConfiguration;
            _serverVar = serverVar;
        }

        public IActionResult Index()
        {
            try
            {
                return View();
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Index/Login", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                return RedirectToAction("Error_Log", "Home");
                throw;
            }
        }

        #region Login

        /// <summary>
        /// SignIn for User.
        /// </summary>
        /// <param name="request"> UserName,Password</param>
        /// <returns>UserDeatils</returns>
        [HttpPost]
        public LoginResponse SignIn(LoginModel request)
        {
            LoginResponse response = new LoginResponse();
            response.Statuscode = 0;
            Global = new GlobalData();
            try
            {
                UserClass user = new UserClass();
                Global.CurrentUser = Security.Login(request.UserName, request.Password, request.ErrorMessage);
                GlobalData.SetGlobalSessione(HttpContext.Session, "UserData", Global);
                //Set Session in User name,Password
                if (Global.CurrentUser.IsActive)
                {
                    HttpContext.Session.SetString("userName", Global.CurrentUser.UserName );
                    HttpContext.Session.SetString("is_cust_login", Global.CurrentUser.is_cust_login.ToString());
                    user.is_cust_login = false;
                    response.userdetail = Global.CurrentUser;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.Status = "Failure";
                    response.ErrorMessage = Global.CurrentUser.ErrorMsg;
                    response.Statuscode = 0;
                }

            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "SignIn/Login", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";

            }
            return response;
        }

        #endregion

        #region Forget Password
        /// <summary>
        /// Forget Password
        /// </summary>
        /// <param name="request">userEmail</param>
        /// <returns>send temporary password to the user for login</returns>
        [HttpPost]
        public LoginResponse ForgetPassword(LoginModel request)
        {
            LoginResponse response = new LoginResponse();
            response.Statuscode = 0;
            try
            {
                var UserDetails = Security.GetUserByEmail(request.Email);
                if (UserDetails.UserID != 0)
                {
                    string new_pswd = Utility.GenerateTemporaryPswd();

                    DBAccess.executeSQL("usp_usr_pswd_update", UserDetails.UserID, Utility.Encrypt(new_pswd), true /*reset_pswd*/);

                    //send user email with new/temporary password
                    StringBuilder sbBody = new StringBuilder();
                    sbBody.AppendFormat("We received a request for password change for user {0} at HMS Health.<br>", request.Email);

                    sbBody.AppendFormat("New Password: {0}", new_pswd);
                    sbBody.Append("<br>");
                    sbBody.Append("Regards,<br>");

                    sbBody.Append("HMS Health");

                    Messaging.SendHTMLEmail(_iConfiguration, request.Email, "HMS Health Password Reset", sbBody.ToString());
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.Status = "Failure";
                    response.ErrorMessage = "User not found";
                    response.Statuscode = 0;
                }              
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "ForgetPassword/Login", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }

        #endregion

        #region Change Password
        /// <summary>
        /// Change Password
        /// </summary>
        /// <param name="request">UserId ,New password </param>
        /// <returns></returns>
        [HttpPost]
        public LoginResponse ChangePassword(ChangePwdRequest request)
        {
            LoginResponse response = new LoginResponse();
            response.Statuscode = 0;
            Global = new GlobalData();
            try
            {
                if (HttpContext.Session.GetString("is_cust_login") != "True")
                {
                    Global.CurrentUser = Security.ChangePasword(request.UserId, request.Password);
                    if (Global.CurrentUser.IsActive)
                    {
                        response.userdetail = Global.CurrentUser;
                        response.Statuscode = 1;
                        response.Status = "Success";
                    }
                    else
                    {
                        response.Status = "Invalid login.  Access logged.";
                        response.Statuscode = 0;
                    }
                }
                else
                {
                    Global.CurrentCust = Security.ChangeCustPasword(request.UserId, request.Password);
                    if (Global.CurrentCust.IsActive)
                    {
                        response.custdetail = Global.CurrentCust;
                        response.Statuscode = 1;
                        response.Status = "Success";
                    }
                    else
                    {
                        response.Status = "Invalid login.  Access logged.";
                        response.Statuscode = 0;
                    }
                }
              
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Change Password/Login", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region
        [HttpPost]
        public void Logout()
        {
            GlobalData.SetGlobalSessione(HttpContext.Session, "UserData", Global);
            GlobalData.SetGlobalSessione(HttpContext.Session, "CustData", Global);                      
        }
        #endregion



        #region Customer Login Control
        public IActionResult CustomerLogin()
        {
            try
            {
                return View();
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Customer", "Index/CustomerLogin", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                return RedirectToAction("Error_Log", "Home");
                throw;
            }
        }

        #endregion

        #region Customer Forget Password
        [HttpPost]
        public LoginResponse CustForgetPassword(CustLoginModel request)
        {
            LoginResponse response = new LoginResponse();
            response.Statuscode = 0;
            try
            {
                var UserDetails = Security.GetCustByEmail(request.email);
                if (UserDetails.CustContactId != 0)
                {
                    string new_pswd = Utility.GenerateTemporaryPswd();

                    DBAccess.executeSQL("usp_cust_contact_pswd_update", UserDetails.CustContactId, Utility.Encrypt(new_pswd), true /*reset_pswd*/);

                    //send user email with new/temporary password
                    StringBuilder sbBody = new StringBuilder();
                    sbBody.AppendFormat("We received a request for password change for user {0} at HMS Health.<br>", request.email);

                    sbBody.AppendFormat("New Password: {0}", new_pswd);
                    sbBody.Append("<br>");
                    sbBody.Append("Regards,<br>");

                    sbBody.Append("HMS Health");

                    Messaging.SendHTMLEmail(_iConfiguration, request.email, "HMS Health Password Reset", sbBody.ToString());
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.Status = "Failure";
                    response.ErrorMessage = "Customer not found";
                    response.Statuscode = 0;
                }
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Customer", "CustOmerForget/CustomerLogin", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }

        #endregion

        #region Customer Sigin
        [HttpPost]
        public LoginResponse CustSignIn(CustLoginModel request)
        {
            LoginResponse response = new LoginResponse();
            response.Statuscode = 0;
            Global = new GlobalData();
            DataSet ds = new DataSet();
            try
            {
                CustomerClass user = new CustomerClass();
                Global.CurrentCust = Security.CustLogin(request.CustEmail, request.Password, request.ErrorMessage);
                GlobalData.SetGlobalSessione(HttpContext.Session, "CustData", Global);
                
                //Set Session in User name,Password
                if (Global.CurrentCust.IsActive)
                {
                    HttpContext.Session.SetString("userName", Global.CurrentCust.CustContactName);
                    HttpContext.Session.SetString("is_cust_login", Global.CurrentCust.is_cust_login.ToString());
                    DataRow GetCustomer = TableDefs.Customer.GetCustomer(ds, Global.CurrentCust.cust_id);
                    if(GetCustomer != null)
                    {
                        Global.CurrentCust.CustomerName = GetCustomer["cust_name"].ToString();
                    }
                    HttpContext.Session.SetString("CustomerName", Global.CurrentCust.CustomerName);
                    response.custdetail = Global.CurrentCust;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.Status = "Failure";
                    response.ErrorMessage = Global.CurrentCust.ErrorMsg;
                    response.Statuscode = 0;
                }

            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Customer", "CustomerSignin/Login", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";

            }
            return response;
        }

        #endregion
      
    }
}
