﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Data;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using System.Linq;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace HMSMemri.Controllers
{
    public class VendorContactController : IMCPBase
    {
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        public VendorContactController(IOptions<BaseSettings.ServerVar> serverVar) : base(serverVar)
        {
            _serverVar = serverVar;
        }
        #region Index Page

        /// <summary>
        ///   Index page(UI) 
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.customer_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.customer_show_closed;
            return View();

        }
        #endregion

        #region Get Vendor Contact 
        /// <summary>
        /// Get Vendor Contact page
        /// </summary>
        /// <param name="request"> Get Vendor Contact page </param>
        /// <returns></returns>
        public ActionResult AddEditVendorContact(string Id)
            {
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }

            try
            {
                string IDcheck = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            }
            catch (Exception)
            {
                return View("~/Views/Shared/Error_EditUrl.cshtml");
            }
            GetVendorContactResponse response = new GetVendorContactResponse();
            VendorContact VendorContact = new VendorContact();
            DataSet ds = new DataSet();
            List<VendorContactDrop> VendorContactDrop = new List<VendorContactDrop>();
            string VendorContactID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            int ID = Convert.ToInt32(VendorContactID);
            GlobalData Global = new GlobalData();
            GlobalData vendor_id = GlobalData.GetGlobalSessione(HttpContext.Session, "vendor_id");
            try
            {
                //contact type Drop Down
                DataView dvContact = TableDefs.General.GetContactTypesForDropDown(ds, true, true, "* Select One *");
                if (dvContact != null)
                {
                    DataTable dtContact = dvContact.ToTable();
                    for (int i = 0; i < dtContact.Rows.Count; i++)
                    {
                        VendorContactDrop status = new VendorContactDrop();
                        status.contact_tp_id = Utility.ToInt(dtContact.Rows[i]["contact_tp_id"]);
                        status.contact_tp_desc = dtContact.Rows[i]["contact_tp_desc"].ToString();
                        VendorContactDrop.Add(status);
                    }
                }
                List<SelectListItem> ContactModule = new List<SelectListItem>();
                foreach (var item in VendorContactDrop)
                {
                    ContactModule.Add(new SelectListItem
                    {
                        Text = item.contact_tp_desc.ToString(),
                        Value = item.contact_tp_id.ToString()
                    });
                }

                response.ContactModule = ContactModule;
                DataRow GetVendorContact = TableDefs.VendorContact.GetVendorContacts(ds, ID);
                if (GetVendorContact != null)
                {
                    VendorContact.vendor_Contact_id = Utility.ToInt(GetVendorContact["vendor_Contact_id"]);
                    VendorContact.vendor_id = Utility.ToInt(GetVendorContact["vendor_id"]);
                    VendorContact.FirstName = GetVendorContact["first_name"].ToString();
                    VendorContact.LastName = GetVendorContact["last_name"].ToString();
                    VendorContact.Title = GetVendorContact["title"].ToString();
                    VendorContact.PhoneNumber = Utility.FormatPhone(GetVendorContact["phone"].ToString(), true);
                    VendorContact.CellPhone = Utility.FormatPhone(GetVendorContact["cell"].ToString(), true);
                    VendorContact.FaxNumber = Utility.FormatPhone(GetVendorContact["fax"].ToString(), true);
                    VendorContact.EmailAddress = GetVendorContact["email"].ToString();
                    VendorContact.AltNumber1 = GetVendorContact["alternate_num1"].ToString();
                    VendorContact.AltNumber2 = GetVendorContact["alternate_num2"].ToString();
                    VendorContact.Notes = GetVendorContact["notes"].ToString();
                    VendorContact.ContactType = Utility.ToInt(GetVendorContact["contact_tp_id"]);
                    VendorContact.IsActive = Convert.ToBoolean(GetVendorContact["is_active"]);
                }
                else
                {
                    VendorContact.vendor_id = vendor_id.vendor_id;
                    VendorContact.IsActive = true;
                }

                if (VendorContact.vendor_Contact_id > 0)
                {
                    response.VendorContactDetail = VendorContact;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.VendorContactDetail = VendorContact;
                    response.Status = "New";
                    response.ErrorMessage = "VendorContactd not found";
                    response.Statuscode = 0;
                }
            }
            catch (System.Exception ex)
            {
                ErrorLog error = new ErrorLog();
                error.errormsg = ex.Message;
                DBAccess.executeSQL("usp_error_log_add", "Main", "Get VendorContact/Vendor Contact", error.errormsg);
                HttpContext.Session.SetString("error_log", error.errormsg);
                return RedirectToAction("Error_Log", "Home");
                throw;
            }
            return View(response);
        }
        #endregion

        #region Add Edit VendorContact
        /// <summary>
        /// Add Edit Vendor Contact page
        /// </summary>
        /// <param name="request"> Save Vendor Contact Details </param>
        /// <returns></returns>
        
        [HttpPost]
        public GetVendorContactResponse AddEditVendorContacts(VendorContact request)
        {
            int userID = 0;
            GetVendorContactResponse response = new GetVendorContactResponse();

            try
            {
                if (ModelState.IsValid)
                {
                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    if (data != null)
                    {
                        userID = data.CurrentUser.UserID;
                        request.PhoneNumber = Utility.FormatPhone(request.PhoneNumber, false);
                        request.CellPhone = Utility.FormatPhone(request.CellPhone, false);
                        request.FaxNumber = Utility.FormatPhone(request.FaxNumber, false);
                        request.AltNumber1 = Utility.FormatPhone(request.AltNumber1, false);
                        request.AltNumber2 = Utility.FormatPhone(request.AltNumber2, false);

                        if (request.vendor_Contact_id == 0)
                        {
                            DBAccess.executeSQL("usp_vendor_contact_new", userID, request.vendor_id, request.FirstName,
                               request.LastName, request.Title, request.PhoneNumber, request.CellPhone,
                               request.EmailAddress, request.FaxNumber, request.AltNumber1, request.AltNumber2,request.ContactType,
                               request.Notes, request.IsActive);
                        }
                        else
                        {
                            DBAccess.executeSQL("usp_vendor_contact_update", userID, request.vendor_Contact_id, request.vendor_id, request.FirstName,
                                request.LastName, request.Title, request.PhoneNumber, request.CellPhone,
                                request.EmailAddress, request.FaxNumber, request.AltNumber1, request.AltNumber2,request.ContactType,
                                request.Notes, request.IsActive);
                        }
                        response.Status = "Success";
                    }
                }
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Add-Edit/Vendor Contact",ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion
    }
}