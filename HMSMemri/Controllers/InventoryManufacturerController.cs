﻿using HMSMemri.BaseSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using hmsBO;
using HMSMemri.Models;
using System.Data;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System.Linq;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;

namespace HMSMemri.Controllers
{
    public class InventoryManufacturerController : IMCPBase
    {
        /// <summary>
        /// Summary for Declare global variables
        /// </summary>
        protected AjaxResult result = new AjaxResult();
        protected DataSourceResult dsResult = new DataSourceResult();
        private readonly IOptions<BaseSettings.ServerVar> _serverVar;
        protected DataRow mr = null;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;

        [Obsolete]
        public InventoryManufacturerController(IOptions<BaseSettings.ServerVar> serverVar, IHostingEnvironment hostingEnvironment) : base(serverVar)
        {
            _serverVar = serverVar;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Index UI
        /// </summary>
        /// <returns></returns>
        
        #region Index UI
        public IActionResult Index()
        {
            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }
            Global = new GlobalData();
            Global.LastPage = Global.CurPage;
            Global.CurPage = WebPages.customer_aspx;
            PageRight = Rights.Warehouse_Receiving;
            PopUp = false;

            ViewBag.ShowAll = Global.customer_show_closed;
            return View();
        }
        #endregion

        #region Grid data load
        [HttpPost]
        public JsonResult LoadManufacturerData(bool isactive, bool toggleActive)
        {
            LoadState loadState = new LoadState();
            GetManufacturerResponse response = new GetManufacturerResponse();
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");

            Global = new GlobalData();
            result.success = true;
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();

            try
            {
                mr = TableDefs.User.GetUserGrid(ds, userID.CurrentUser.UserID, 40);
                if (mr != null)
                {
                    if (mr.Table.Rows.Count > 0)
                    {
                        loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                        loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                        loadState.json_col_ord = mr["json_col_ord"].ToString();
                        response.loadState = loadState;
                        dsResult.Errors = "true";

                    }
                }


                //the value for sp, API Call

                DataView dvManufacturer = TableDefs.Inventory.GetManufacturers(ds, isactive);
                string jsonData = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var listManufacturer = JsonConvert.DeserializeObject<lst_tbl_mfg>(jsonData);

                dsResult.Data = listManufacturer.tbl_mfg.ToList().OrderBy(x => x.mfg_name);
                dsResult.Total = listManufacturer.tbl_mfg.Count;
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Grid Load/Manufacturer", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                result.success = false;
                result.message = $"An error occurred retrieving Manufacturer List. {ex.Message}";
            }

            if (result.success)
            {
                return Json(dsResult);
            }
            else
            {
                return Json(result);
            }
        }
        #endregion

        #region Displaying the edited Manufacturer records
        public ActionResult AddEditInv_Manufacturer(string Id)
        {
            //Declaring the variables
            GetManufacturerResponse response = new GetManufacturerResponse();
            Manufacturer Manufacturer = new Manufacturer();
            List<InvStates> States = new List<InvStates>();
            response.Statuscode = 0;

            //Globally to check the login status
            GlobalData userID = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
            if (userID?.CurrentUser.UserID == null)
            {
                return RedirectToAction("Index", "Login");
            }

            try
            {
                string IDcheck = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";
            }
            catch (Exception)
            {

                return View("~/Views/Shared/Error_EditUrl.cshtml");
            }
            string ID = Id != null ? Encoding.GetEncoding(28591).GetString(Convert.FromBase64String(Id)) : "0";

            try
            {
                DataSet ds = new DataSet();
                //Manufacturer grid API call
                mr = TableDefs.Inventory.GetManufacturer(ds, Convert.ToInt32(ID));
                if (mr != null)
                {
                    Manufacturer.ManufacturerId = Utility.ToInt(mr["mfg_id"]);
                    Manufacturer.ManufacturerName = mr["mfg_name"].ToString();
                    Manufacturer.Address1 = mr["address1"].ToString();
                    Manufacturer.Address2 = mr["address2"].ToString();
                    Manufacturer.City = mr["city"].ToString();
                    Manufacturer.State = mr["st"].ToString();
                    Manufacturer.ZipCode = mr["zip"].ToString();
                    Manufacturer.Country = mr["country"].ToString();
                    Manufacturer.PhoneNumber = Utility.FormatPhone(mr["phone"].ToString(), true);
                    Manufacturer.Fax = Utility.FormatPhone(mr["fax"].ToString(), true);
                    Manufacturer.Email = mr["email"].ToString();
                    Manufacturer.Website = mr["website"].ToString();
                    Manufacturer.Notes = mr["notes"].ToString();
                    Manufacturer.IsActive = Utility.ToBool(mr["is_active"], false);

                }
                else
                {
                    Manufacturer.IsActive = true;
                }

                //State API call
                DataView dvStates = TableDefs.General.GetStatesForDropDown(ds, true, "* Select One *");
                if (dvStates != null)
                {
                    DataTable dtStates = dvStates.ToTable();
                    for (int i = 0; i < dtStates.Rows.Count; i++)
                    {
                        InvStates state = new InvStates();
                        state.invAbbr = dtStates.Rows[i]["abbr"].ToString();
                        state.InvSt = dtStates.Rows[i]["st"].ToString();
                        States.Add(state);
                    }
                }
                //show selected list item in Manufacturer Status
                List<SelectListItem> Manufacturerselectionlist = new List<SelectListItem>();

                //show selected list item in State
                List<SelectListItem> stateselectionlist = new List<SelectListItem>();
                foreach (var stateItem in States)
                {
                    stateselectionlist.Add(new SelectListItem
                    {
                        Text = stateItem.InvSt.ToString(),
                        Value = stateItem.invAbbr.ToString()
                    });
                }
                response.StateModule = stateselectionlist;

                if (Manufacturer.ManufacturerId > 0)
                {
                    response.Manufacturerdetail = Manufacturer;
                    response.Statuscode = 1;
                    response.Status = "Success";
                }
                else
                {
                    response.Manufacturerdetail = Manufacturer;
                    response.Status = "New";
                    response.ErrorMessage = "Manufacturer not found";
                    response.Statuscode = 0;
                }
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Get Manufacturer/Manufacturer", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
                return RedirectToAction("Error_Log", "Home");
            }
            return View(response);
        }
        #endregion

        #region Add,Edit the Manufacturer records 
        [HttpPost]
        public GetManufacturerResponse AddEditInv_Manufacturers(Manufacturer Manufacturer)
        {
            GetManufacturerResponse response = new GetManufacturerResponse();
            int userID = 0;
            try
            {
                if (ModelState.IsValid)
                {
                    GlobalData data = GlobalData.GetGlobalSessione(HttpContext.Session, "UserData");
                    userID = data.CurrentUser.UserID;
                    string phoneNumber = Utility.FormatPhone(Manufacturer.PhoneNumber, false);
                    string faxNumber = Utility.FormatPhone(Manufacturer.Fax, false);

                    if (Manufacturer.ManufacturerId == 0)
                    {
                        DBAccess.executeSQL("usp_mfg_new", userID, Manufacturer.ManufacturerName, Manufacturer.Address1, Manufacturer.Address2, Manufacturer.City, Manufacturer.State, Manufacturer.ZipCode, "US", phoneNumber, faxNumber, Manufacturer.Email,  Manufacturer.Website, Manufacturer.Notes, Manufacturer.IsActive);
                    }
                    else
                    {
                        DBAccess.executeSQL("usp_mfg_update", userID, Manufacturer.ManufacturerId, Manufacturer.ManufacturerName, Manufacturer.Address1, Manufacturer.Address2, Manufacturer.City, Manufacturer.State, Manufacturer.ZipCode, "US", phoneNumber, faxNumber, Manufacturer.Email, Manufacturer.Website, Manufacturer.Notes, Manufacturer.IsActive);
                    }
                    response.Status = "Success";
                }               
            }
            catch (System.Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Add-Edit/Manufacturer", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
                response.Statuscode = 0;
                response.Status = "Failure";
            }
            return response;
        }
        #endregion

        #region Manufacturer Export List
        [HttpPost]
        [Obsolete]
        public GetManufacturerResponse ExporManufacturers(List<string> manufacturerList)
        {
            DataSet ds = new DataSet();
            GetManufacturerResponse response = new GetManufacturerResponse();

            try
            {
                if (manufacturerList != null)
                {
                    if (manufacturerList != null && manufacturerList.Count > 0)
                    {
                        //Set File Name and Path
                        string FilePath = Path.Combine(_hostingEnvironment.ContentRootPath, "wwwroot/ExportFile/");
                        string docPath = Path.Combine(FilePath, "manufacturers/");
                        if (Directory.Exists(docPath))
                        {
                            DirectoryInfo di = new DirectoryInfo(docPath);
                            FileInfo[] files = di.GetFiles();
                            foreach (FileInfo file in files)
                            {
                                file.Delete();
                            }
                        }
                        string file_name = $"manufacturers_{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";
                        string pathFileName = docPath + file_name;

                        if (!Directory.Exists(FilePath))
                        {
                            Directory.CreateDirectory(FilePath);
                        }

                        // Path Not exists then create new file path
                        if (!Directory.Exists(docPath))
                        {
                            Directory.CreateDirectory(docPath);
                        }
                        //File name  exists then Delete the  file
                        if (System.IO.File.Exists(pathFileName))
                        {
                            System.IO.File.Delete(docPath);
                        }

                        //convert list to string
                        string ManufacturerList = String.Join<string>(",", manufacturerList); 
                        DataTable dtManufacture = TableDefs.Inventory.GetManufacturersForExport(ds, ManufacturerList).ToTable();
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dtManufacture);
                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(pathFileName);
                                response.Status = "Success";
                                response.ExcelLink = "/ExportFile/manufacturers/" + file_name;
                            }
                        }
}
                }
            }
            catch (Exception ex)
            {
                DBAccess.executeSQL("usp_error_log_add", "Main", "Export/Manufacturer", ex.Message);
                HttpContext.Session.SetString("error_log", ex.Message);
            }
            return response;
        }
        #endregion

        #region SaveStateData
        [HttpPost]
        public IActionResult SaveStateData(int usr_id,int grid_id, string json_col_ord)
        {
            GetManufacturerResponse response = new GetManufacturerResponse();
            DBAccess.executeSQL("usp_usr_grid_col_update", usr_id, grid_id, json_col_ord);
            response.Status = "Success";
            return Json(response);
        }
        #endregion

        #region Load the Inventory Manufacturer State
        public IActionResult LoadManufacturerState(int usr_id, int grid_id)
        {
            LoadState loadState = new LoadState();
            DataSet ds = new DataSet();
            GetManufacturerResponse response = new GetManufacturerResponse();

            mr = TableDefs.User.GetUserGrid(ds, usr_id, grid_id);
            if (mr != null)
            {
                loadState.usr_id = Utility.ToInt(mr["usr_id"]);
                loadState.grid_id = Utility.ToInt(mr["grid_id"]);
                loadState.json_col_ord = mr["json_col_ord"].ToString();
            }
            if (loadState.usr_id > 0)
            {
                response.loadState = loadState;
                response.Statuscode = 1;
                response.Status = "Success";
            }
            else
            {
                response.loadState = loadState;
                response.Status = "New";
                response.ErrorMessage = "Manufacturer not found";
                response.Statuscode = 0;
            }
            return Json(response);
        }
        #endregion
    }
}

