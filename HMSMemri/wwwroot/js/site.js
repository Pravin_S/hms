﻿// Login Page functionality Start //

//User Validation in login
function Uservalidation() {
	var UserName = $('#UserName').val();
	if (UserName == null || UserName == "" || UserName == undefined) {
		document.getElementById("userid").hidden = false;
		document.getElementById("UserName").classList.add("inputError");
		document.getElementById("userlable").classList.add("text-danger");
	} else {
		document.getElementById("userid").hidden = true;
		document.getElementById("UserName").classList.remove("inputError");
		document.getElementById("userlable").classList.remove("text-danger");
	}
}

//Password Validation in login
function Pwdvalidation() {
	var Password = $('#Password').val();
	if (Password == null || Password == "" || Password == undefined) {
		document.getElementById("pwdid").hidden = false;
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("pwdlable").classList.add("text-danger");
	} else {
		document.getElementById("pwdid").hidden = true;
		document.getElementById("Password").classList.remove("inputError");
		document.getElementById("pwdlable").classList.remove("text-danger");
	}
}

//Forget password check email validation
function checkEmail() {
	var email = document.getElementById('Email');
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!filter.test(email.value)) {
		document.getElementById("emailable").classList.add("text-danger");
		document.getElementById("Email").classList.add("inputError");
		email.focus;
		return false;
	} else {
		document.getElementById("emailable").classList.remove("text-danger");
		document.getElementById("Email").classList.remove("inputError");

		return true;
	}
}
function CustEmailCheck() {
	var email = document.getElementById('CustEmail');
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!filter.test(email.value)) {
		document.getElementById("CustEmail").classList.add("inputError");
		document.getElementById("CustNamelable").classList.add("text-danger");
		document.getElementById("custidlable").hidden = false;
		email.focus;
		return false;
	} else {
		document.getElementById("CustEmail").classList.remove("inputError");
		document.getElementById("CustNamelable").classList.remove("text-danger");
		document.getElementById("custidlable").hidden = true;
		return true;
	}
}

function validation() {
	var UserName = $('#UserName').val();
	var Password = $('#Password').val();
	if ((UserName == null || UserName == "" || UserName == undefined) && (Password == null || Password == "" || Password == undefined)) {
		document.getElementById("userid").hidden = false;
		document.getElementById("pwdid").hidden = false;
		document.getElementById("UserName").classList.add("inputError");
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("userlable").classList.add("text-danger");
		document.getElementById("pwdlable").classList.add("text-danger");
		return false;
	} else if (UserName == null || UserName == "" || UserName == undefined) {
		document.getElementById("userid").hidden = false;
		document.getElementById("pwdid").hidden = true;
		document.getElementById("UserName").classList.add("inputError");
		document.getElementById("Password").classList.remove("inputError");
		document.getElementById("userlable").classList.add("text-danger");
		document.getElementById("pwdlable").classList.remove("text-danger");
		return false;
	} else if (Password == null || Password == "" || Password == undefined) {
		document.getElementById("userid").hidden = true;
		document.getElementById("pwdid").hidden = false;
		document.getElementById("UserName").classList.remove("inputError");
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("userlable").classList.remove("text-danger");
		document.getElementById("pwdlable").classList.add("text-danger");
		return false;
	} else if (UserName !== "" && Password !== "") {
		return true;
	}
}

//Confirm password validation
function Confirmvalidation() {
	var NewPassword = $('#NewPassword').val();
	const re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()+=-\?;,./{}|\":<>\[\]\\\' ~_]).{8,}/
	if (NewPassword.length < 8) {
		document.getElementById("Pwderrortext1").hidden = false;
		document.getElementById("Pwderrortext2").hidden = true;
		return false;
	} else
		if (!re.test(NewPassword)) {
			document.getElementById("Pwderrortext2").hidden = false;
			document.getElementById("Pwderrortext1").hidden = true;
			return false;
		} else {
			document.getElementById("Pwderrortext1").hidden = true;
			document.getElementById("Pwderrortext2").hidden = true;
			return true;
		}
}

// change password validation
function changePwdvalidation() {
	var NewPassword = $('#NewPassword').val();
	var ConfirmPassword = $('#ConfirmPassword').val();
	if (NewPassword != ConfirmPassword) {
		document.getElementById("confirmerrortext").hidden = false;
		return false;
	} else {
		document.getElementById("confirmerrortext").hidden = true;
		return true;
	}
}

// Login API call
function Signin() {
	sessionStorage.clear();
	localStorage.clear();
	var UserName = $('#UserName').val();
	var Password = $('#Password').val();
	if (validation()) {
		$.loadingBlockShow();
		document.getElementById("userid").hidden = true;
		document.getElementById("pwdid").hidden = true;
		document.getElementById("pwdlable").classList.remove("text-danger");
		document.getElementById("userlable").classList.remove("text-danger");
		$.ajax({
			type: 'POST',
			url: '/Login/SignIn',
			data: {
				'UserName': UserName,
				'Password': Password
			},
			success: function (result) {
				$.loadingBlockHide();
				if (result.status == "Success") {
					localStorage.setItem("UserId", result.userdetail.userID);
					localStorage.setItem("UserName", result.userdetail.userName);
					localStorage.setItem("IsLoginId", result.userdetail.userID);
					localStorage.setItem("Is_cust_login", false);
					if (result.userdetail.isResetPwd == true) {
						$("#ChangepasswordModal").modal();
						$('#NewPassword').val(null);
						$('#ConfirmPassword').val(null);
						document.getElementById("Pwderrortext1").hidden = true;
						document.getElementById("Pwderrortext2").hidden = true;
						document.getElementById("confirmerrortext").hidden = true;
					} else {
						window.location.href = "Customer/Index";
					}
				} else {
					$.loadingBlockHide();
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}
}

//Reset Password API Call
function Resetpwd() {
	var Email = $('#Email').val();
	if (checkEmail()) {
		$.ajax({
			type: 'POST',
			url: '/Login/ForgetPassword',
			data: {
				'Email': Email
			},
			success: function (result) {
				if (result.status == "Success") {
					$.iaoAlert({
						msg: "Email send successfully",
						type: "success",
						mode: "dark",
					})
					$("#ForgotpasswordModal .close").click();
					$('#Email').empty();

				} else {
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}
}

// Change Password API Call
function Changepwd() {
	var NewPassword = $('#NewPassword').val();
	if (Confirmvalidation() && changePwdvalidation()) {
		var UserId = localStorage.getItem("IsLoginId");
		$.ajax({
			type: 'POST',
			url: '/Login/ChangePassword',
			data: {
				'UserId': UserId,
				'Password': NewPassword
			},
			success: function (result) {
				if (result.status == "Success") {
					$("#ChangepasswordModal .close").click();
					$('#NewPassword').val(null);
					$('#ConfirmPassword').val(null);
					$('#Password').val(null);
					$.iaoAlert({
						msg: "Success",
						type: "success",
						mode: "dark",
					})
				} else {
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}
}
// Login Page functionality End //

//Change page pop Start//
function Changepop() {
	$("#ChangepasswordModal").modal();
	$('#NewPassword').val(null);
	$('#ConfirmPassword').val(null);
	document.getElementById("Pwderrortext1").hidden = true;
	document.getElementById("Pwderrortext2").hidden = true;
	document.getElementById("confirmerrortext").hidden = true;
}
//Change page pop Start//

// Customer Page functionality Start //

function EditCustomer() {
	window.location.href = "Customer/EditCustomer";
	localStorage.setItem("CustomerId", 0);
}

//Customer Update(save)  API Call
function SubmitForm(e) {
	var serviceType = document.getElementsByClassName("multiselect-selected-text")[0].innerHTML;
	var serviceTypeList = [];
	var serviceTypesMultiple = '';
	serviceTypeList = serviceType.split(',');
	for (var i = 0; i < serviceTypeList.length; i++) {
		serviceTypesMultiple += serviceTypeList[i];
	}

	var request = {
		CustomerId: document.getElementById("CustomerId").value,
		CustomerName: document.getElementById("IsCustomerName").value,
		Address1: document.getElementById("IsAddress1").value,
		Address2: document.getElementById("IsAddress2").value,
		city: document.getElementById("IsCity").value,
		ZipCode: document.getElementById("IsZipCode").value,
		State: document.getElementById("IsState").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		Fax: document.getElementById("IsFax").value,
		VisitAllocation: document.getElementById("IsVisitAllocation").value,
		DefaultLabor: document.getElementById("IsDefaultLabor").value,
		CustomerStatus: document.getElementById("IsCustomerStatus").value,
		Notes: document.getElementById("IsNotes").value,
		service_typeIds: document.getElementById("Subjects_dropdown").value,
		ServiceTypesList: serviceType,
	}

	$.ajax({
		type: 'POST',
		url: '/Customer/EditCustomers',
		data: {
			'customer': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Customer/Index";
			} else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

		},
	});
}
// Customer Page functionality End //

// Facility Page functionality Start //
//SAve function facility page
function SubmitFormFacility(event) {

	var request = {
		cust_id: document.getElementById("IsCustomer").value,
		FacilityId: document.getElementById("FacilityId").value,
		FacilityName: document.getElementById("IsFacilityName").value,
		Address1: document.getElementById("IsAddress1").value,
		Address2: document.getElementById("IsAddress2").value,
		city: document.getElementById("IsCity").value,
		ZipCode: document.getElementById("IsZipCode").value,
		State: document.getElementById("IsState").value,
		Country: document.getElementById("IsCountry").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		Fax: document.getElementById("IsFax").value,
		FacilityStatus: document.getElementById("IsFacilityStatus").value,
		tech_id: document.getElementById("IsAssignedTech").value,
		sales_rep_id: document.getElementById("IsAssignedSales").value,
		Notes: document.getElementById("IsNotes").value
		//  IsActive: document.getElementById("IsActiveId").value
	}
	$.ajax({
		type: 'POST',
		url: '/Facilities/AddEditFacilities',
		data: {
			'Facility': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Facilities/Index";
			} else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

		},
	});
	// Facility Page functionality End //
}

// Manufacturer Page functionality Start //
function SubmitFormManufacturer(event) {
	var request = {
		ManufacturerId: document.getElementById("ManufacturerId").value,
		ManufacturerName: document.getElementById("IsManufacturerName").value,
		Address1: document.getElementById("IsAddress1").value,
		Address2: document.getElementById("IsAddress2").value,
		city: document.getElementById("IsCity").value,
		ZipCode: document.getElementById("IsZipCode").value,
		State: document.getElementById("IsState").value,
		Notes: document.getElementById("IsNotes").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		Fax: document.getElementById("IsFax").value,
		Url: document.getElementById("IsUrl").value,
		Email: document.getElementById("IsEmail").value,
		IsActive: document.getElementById("IsActive").checked
	}
	$.ajax({
		type: 'POST',
		url: '/InventoryManufacturer/AddEditInv_Manufacturers',
		data: {
			'Manufacturer': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/InventoryManufacturer/Index";
			} else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}

		},
		error: function (xhr, ajaxOptions, thrownError) {

		},
	});
}
// Manufacturer Page functionality End //

// Vendor page Start//
function SaveVendorForm(event) {
	let postUrl = "/Vendors/AddEditVendors";
	let errorurl = "/Home/Error_Log";

	var request = {
		vendor_id: document.getElementById("VendorId").value,
		VendorName: document.getElementById("IsVendorName").value,
		Address1: document.getElementById("IsAddress1").value,
		Address2: document.getElementById("IsAddress2").value,
		City: document.getElementById("IsCity").value,
		State: document.getElementById("IsState").value,
		Zip: document.getElementById("IsZip").value,
		IsActive: document.getElementById("IsActive").checked,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		FaxNumber: document.getElementById("IsFaxNumber").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		EmailAddress: document.getElementById("IsEmail").value,
		Website: document.getElementById("IsWebsite").value,
		Notes: document.getElementById("IsNotes").value,
		IsActive: document.getElementById("IsActive").checked
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Vendors/Index";
			}
			else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}

		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			return false;
		}
	});
}
// Vendor page End//

// Vendor Contact page Start//
function SaveVendorContactForm() {
	let postUrl = "/VendorContact/AddEditVendorContacts";

	var request = {
		vendor_contact_id: document.getElementById("VendorContactId").value,
		vendor_id: document.getElementById("VendorId").value,
		FirstName: document.getElementById("IsFirstName").value,
		LastName: document.getElementById("IsLastName").value,
		Title: document.getElementById("IsTitle").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		CellPhone: document.getElementById("IsCellPhone").value,
		FaxNumber: document.getElementById("IsFaxNumber").value,
		EmailAddress: document.getElementById("IsEmailAddress").value,
		AltNumber1: document.getElementById("IsAltNumber1").value,
		AltNumber2: document.getElementById("IsAltNumber2").value,
		Notes: document.getElementById("IsNotes").value,
		ContactType: document.getElementById("IsContactType").value,
		IsActive: document.getElementById("IsActive").checked
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Vendors/Index";
			}
			else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}
// Vendor Contact page End//

// Bio Med Ticket page Start//
function SubmitTicketsForm() {
	let postUrl = "/Tickets/AddEditTicket";

	var request = {
		ticket_id: document.getElementById("ticket_id").value,
		CustomerID: document.getElementById("IsCustomer").value,
		FacilityID: document.getElementById("IsFacility").value,
		CallerName: document.getElementById("IsCallerName").value,
		CallerPhone: document.getElementById("IsCallerPhone").value,
		TicketAddMethodID: document.getElementById("IsTicketAddMethodID").value,
		Summary: document.getElementById("IsSummary").value,
		AssignedUserID: document.getElementById("IsAssignedUserID").value,
		GroupID: document.getElementById("IsGroupID").value,
		//PriorityID: document.getElementById("IsPriorityID").value,
		PriorityID: document.querySelector('input[name="PriorityID"]:checked').value,
		//StartDate: document.getElementById("IsStartDate").value,
		DeadLineDate: document.getElementById("IsDeadLineDate").value + " "
			+ document.getElementById("IsDeadLineTime").value,
		TicketDescription: document.getElementById("IsTicketDescription").value,
		TicketStatID: document.getElementById("IsTicketStatID").value,
		CaseNumber: document.getElementById("IsCaseNumber").value,
		TicketTypeID: document.getElementById("IsTicketTypeID").value,
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				if (request.ticket_id == 0 || request.ticket_id == undefined) {
					sessionStorage.setItem("IslastTicketView", true);
					sessionStorage.setItem("IslastTicket", result.ticketdetail.ticket_id);
					$.iaoAlert({
						msg: "Saved successfully",
						type: "success",
						mode: "dark",
					})
					window.location.href = "/Tickets/EditTickets";
				} else {
					$.iaoAlert({
						msg: "Saved successfully",
						type: "success",
						mode: "dark",
					})
					window.location.href = "/Tickets/Index";
				}
			}
			else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}
//Edit Ticket Page
function SubmitTicketsForms() {
	let postUrl = "/Tickets/AddEditTickets";

	var request = {
		ticket_id: document.getElementById("ticket_id").value,
		CustomerID: document.getElementById("IsCustomer").value,
		FacilityID: document.getElementById("IsFacility").value,
		CallerName: document.getElementById("IsCallerName").value,
		CallerPhone: document.getElementById("IsCallerPhone").value,
		TicketAddMethodID: document.getElementById("IsTicketAddMethodID").value,
		Summary: document.getElementById("IsSummary").value,
		AssignedUserID: document.getElementById("IsAssignedUserID").value,
		GroupID: document.getElementById("IsGroupID").value,
		//PriorityID: document.getElementById("IsPriorityID").value,
		PriorityID: document.querySelector('input[name="PriorityID"]:checked').value,
		//StartDate: document.getElementById("IsStartDate").value,
		DeadLineDate: document.getElementById("IsDeadLineDate").value + " "
			+ document.getElementById("IsDeadLineTime").value,
		TicketDescription: document.getElementById("IsTicketDescription").value,
		TicketStatID: document.getElementById("IsTicketStatID").value,
		CaseNumber: document.getElementById("IsCaseNumber").value,
		TicketTypeID: document.getElementById("IsTicketTypeID").value,
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Tickets/Index";
			}
			else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}


// Inventory Page Start //
function SaveInventoryForm() {
	let postUrl = "/Inventory/AddEditInventories";

	var request = {
		inv_id: document.getElementById("inv_id").value,
		facility_id: document.getElementById("FacilityId").value,
		ManufactureList: document.getElementById("IsManufactureList").value,
		EquipmentSerial: document.getElementById("IsEquipmentSerial").value,
		HospitalControl: document.getElementById("IsHospitalControl").value,
		Control: document.getElementById("IsControl").value,
		PreviousControl: document.getElementById("IsPreviousControl").value,
		SystemId: document.getElementById("IsSystemId").value,
		Department: document.getElementById("IsDepartment").value,
		Room: document.getElementById("IsRoom").value,
		InventoryNotes: document.getElementById("IsInventoryNotes").value,

		OwnerShip: document.getElementById("IsOwnerShip").value,
		ManagedEntity: document.getElementById("IsManagedEntity").value,
		InstalledDate: document.getElementById("IsInstalledDate").value,
		WarrentyStatus: document.getElementById("IsWarrentyStatus").value,
		WarrentyExpires: document.getElementById("IsWarrentyExpires").value,
		LastPmCompleted: document.getElementById("IsLastPmCompleted").value,
		NextSchedulePm: document.getElementById("IsNextSchedulePm").value,
		PmInterval: document.getElementById("IsPmInterval").value,
		Alarm: document.getElementById("IsAlarm").value,
		ItemCoverage: document.getElementById("IsItemCoverage").value,
		Status: document.getElementById("IsStatus").value,
		ServiceType: document.getElementById("IsServiceType").value,

		Provider1: document.getElementById("IsProvider1").value,
		SrvCoverage1: document.getElementById("IsSrvCoverage1").value,
		Provider2: document.getElementById("IsProvider2").value,
		SrvCoverage2: document.getElementById("IsSrvCoverage2").value,

		HasBattery: document.getElementById("IsHasBattery").checked,
		BatteryLastChanged: document.getElementById("IsBatteryLastChanged").value,
		BatteryChangeInterval: document.getElementById("IsBatteryChangeInterval").value,
		IsActive: document.getElementById("IsActive").checked,
		RootClass: document.getElementById("IsRootClass").value,
		SubClass: document.getElementById("IsSubRootClass").value,
		SelectedModelDescription: document.getElementById("IsModelDescription").value
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Inventory/Index";
			}
			else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}

		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}

	});
}
// Inventory Page End //

//  contract page Start//
function SaveContractForm() {
	let postUrl = "/Contracts/AddEditContracts";
	var request = {
		contract_id: document.getElementById("contract_id").value,
		cust_id: document.getElementById("cust_id").value,
		facility_id: document.getElementById("facility_id").value,
		contract_tp_id: document.getElementById("Iscontract_tp_id").value,
		service_tp_id: document.getElementById("IsServiceType").value,
		Notes: document.getElementById("IsNotes").value,
		contract_po: document.getElementById("Iscontract_po").value,
		parts_po: document.getElementById("Isparts_po").value,
		contract_labor_rate: document.getElementById("Iscontract_labor_rate").value,
		contract_travel_chg: document.getElementById("Iscontract_travel_chg").value,
		vendor_id: document.getElementById("Isvendor_id").value,
		start_dt: document.getElementById("IsStartTermDate").value,
		enddate_dt: document.getElementById("IsEndTermDate").value,
		Isactive: document.getElementById("Isactive").checked
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Contracts/Index";
			} else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}

		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}
// contract page End//

// Customer Contact Page Start //
function SaveCustomerContactForm() {

	const checkpwd = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()+=-\?;,./{}|\":<>\[\]\\\' ~_]).{8,}/;
	if (document.getElementById("IsClientPassword").value != document.getElementById("OldIsClientPassword").value) {
		if (document.getElementById("IsClientPassword").value.length < 8) {
			document.getElementById("Pwderrortext1").hidden = false;
			document.getElementById("Pwderrortext2").hidden = true;
			return false;
		} else
			if (!checkpwd.test(document.getElementById("IsClientPassword").value)) {
				document.getElementById("Pwderrortext2").hidden = false;
				document.getElementById("Pwderrortext1").hidden = true;
				return false;
			}
	}
	let postUrl = "/CustomerContact/AddEditCustomerContacts";
	var request = {
		cust_contact_id: document.getElementById("ContactId").value,
		cust_id: document.getElementById("CustId").value,
		facility_id: document.getElementById("FacilityId").value,
		FirstName: document.getElementById("IsFirstName").value,
		LastName: document.getElementById("IsLastName").value,
		Title: document.getElementById("IsTitle").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		CellPhone: document.getElementById("IsCellPhone").value,
		FaxNumber: document.getElementById("IsFaxNumber").value,
		EmailAddress: document.getElementById("IsEmailAddress").value,
		AltNumber1: document.getElementById("IsAltNumber1").value,
		AltNumber2: document.getElementById("IsAltNumber2").value,
		Notes: document.getElementById("IsNotes").value,
		ContactType: document.getElementById("IsContactType").value,
		ClientLogin: document.getElementById("IsClientLogin").value,
		ClientPassword: document.getElementById("IsClientPassword").value,
		IsActive: document.getElementById("IsActive").checked
	}

	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/CustomerContact";
			} else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}
// Customer Contact Page End //


//Customer Login Start//
function CustLoginValidation() {
	var UserName = $('#CustEmail').val();
	var Password = $('#Password').val();
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!filter.test(UserName) && (Password == null || Password == "" || Password == undefined)) {
		document.getElementById("custidlable").hidden = false;
		document.getElementById("pwdid").hidden = false;
		document.getElementById("CustEmail").classList.add("inputError");
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("CustNamelable").classList.add("text-danger");
		document.getElementById("pwdlable").classList.add("text-danger");
		return false;
	} else if (!filter.test(UserName) || UserName == null || UserName == "" || UserName == undefined) {
		document.getElementById("custidlable").hidden = false;
		document.getElementById("pwdid").hidden = true;
		document.getElementById("CustEmail").classList.add("inputError");
		document.getElementById("Password").classList.remove("inputError");
		document.getElementById("CustNamelable").classList.add("text-danger");
		document.getElementById("pwdlable").classList.remove("text-danger");
		return false;
	} else if (Password == null || Password == "" || Password == undefined) {
		document.getElementById("custidlable").hidden = true;
		document.getElementById("pwdid").hidden = false;
		document.getElementById("CustEmail").classList.remove("inputError");
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("CustNamelable").classList.remove("text-danger");
		document.getElementById("pwdlable").classList.add("text-danger");
		return false;
	} else if (UserName !== "" && Password !== "") {
		return true;
	}
}
function CustSignin() {
	sessionStorage.clear();
	localStorage.clear();
	var CustEmail = $('#CustEmail').val();
	var Password = $('#Password').val();
	if (CustLoginValidation()) {
		$.loadingBlockShow();
		document.getElementById("custidlable").hidden = true;
		document.getElementById("pwdid").hidden = true;
		document.getElementById("pwdlable").classList.remove("text-danger");
		document.getElementById("CustNamelable").classList.remove("text-danger");
		$.ajax({
			type: 'POST',
			url: '/Login/CustSignIn',
			data: {
				'CustEmail': CustEmail,
				'Password': Password
			},
			success: function (result) {
				$.loadingBlockHide();
				if (result.status == "Success") {
					localStorage.setItem("IsLoginId", result.custdetail.custContactId);
					localStorage.setItem("Is_cust_login", true);
					localStorage.setItem("CustContactId", result.custdetail.custContactId);
					if (result.custdetail.isResetCustPwd == true) {
						$("#ChangepasswordModal").modal();
						$('#NewPassword').val(null);
						$('#ConfirmPassword').val(null);
						document.getElementById("Pwderrortext1").hidden = true;
						document.getElementById("Pwderrortext2").hidden = true;
						document.getElementById("confirmerrortext").hidden = true;
					} else {
						window.location.href = "/Inventory/Index";
					}
				} else {
					$.loadingBlockHide();
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}
}


function ResetCustomerpwd() {
	var Email = $('#Email').val();
	if (checkEmail()) {
		$.ajax({
			type: 'POST',
			url: '/Login/CustForgetPassword',
			data: {
				'Email': Email
			},
			success: function (result) {
				if (result.status == "Success") {
					$.iaoAlert({
						msg: "Email send successfully",
						type: "success",
						mode: "dark",
					})
					$("#ForgotpasswordModal .close").click();
					$('#CustEmail').empty();

				} else {
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}
}

//Customer Login End //


//EditRepair
function SaveRepairdata() {
	var ticket_id = localStorage.getItem("IsTicketId");
	var formData = new FormData();
	var typeID = document.getElementById("myList").value;
	formData.append("ticket_id", ticket_id);
	formData.append("ticket_charge_id", document.getElementById("tktchrgId").value);
	formData.append("charge_tp_id", document.getElementById("myList").value);
	formData.append("qty", document.getElementById("quantities").value);
	formData.append("cost", document.getElementById("costs").value);
	formData.append("charge", document.getElementById("charges").value);
	formData.append("part_no", document.getElementById("parts1").value);
	formData.append("po", document.getElementById("po1").value);
	formData.append("charge_desc", document.getElementById("descriptiontext").value);
	formData.append("quoted", document.getElementById("quoteds").value);
	formData.append("charge_tp_desc", document.getElementById("myList").value);
	formData.append("total", document.getElementById("totalcharges").value);
	if (repairTypeValidation(typeID)) {
		$.loadingBlockShow();
		document.getElementById("typeid").hidden = true;
		document.getElementById("typelable").classList.remove("text-danger");
		$.ajax({
			url: "/Tickets/PostRepair",
			type: "POST",
			contentType: false,
			data: formData,
			dataType: "json",
			cache: false,
			processData: false,
			success: function (d) {
				$.loadingBlockHide();
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				setTimeout(function () {
					window.location.reload(gridRepairloadData);
				}, 1500);
				$("#RepairModal .close").click();
				gridRepairloadData();
			},
			error: function () {
				alert("Please contact admin");
				//window.location.href = "/Tickets/Repairs";
				$.loadingBlockHide();
				return false;
			}
		});
	}
}

//Delete Repair
function RepairDelete(arg) {
	$.loadingBlockShow();
	$.ajax({
		type: 'POST',
		url: "/Tickets/RepairDelete",
		data: {
			ticket_charge_id: arg.dataset.id
		},
		success: function (result) {

			$.loadingBlockHide();
			$.iaoAlert({
				msg: "Delete successfully",
				type: "success",
				mode: "dark",
			})
			setTimeout(function () {
				window.location.reload(gridRepairloadData);
			}, 1500);
			$("#RepairModal .close").click();
			gridRepairloadData();
		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			alert("Please contact admin");
			return false;
		}
	});
}

function TicketAttachmentDelete(arg) {
	$.loadingBlockShow();
	$.ajax({
		type: 'POST',
		url: "/Tickets/TicketAttachmentDelete",
		data: {
			file_attach_id: arg.dataset.id
		},
		success: function (result) {
			$.loadingBlockHide();
			$.iaoAlert({
				msg: "File delete successfully",
				type: "success",
				mode: "dark",
			})
			setTimeout(function () {
				window.location.reload(LoadFileAttachment);
			}, 1200);
			LoadFileAttachment();
		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			alert("Please contact admin");
			return false;
		}
	});
}



function SaveAttachment() {
	$.loadingBlockHide();
	var formData = new FormData();
	formData.append("Image_path", $('#IsFileExtension')[0].files[0]);
	formData.append("ticket_id", sessionStorage.getItem("ticket_id"));
	if ($('#IsFileExtension')[0].files[0] != null && $('#IsFileExtension')[0].files[0] != undefined) {
		$.ajax({
			url: "/Tickets/uploadFileAttachments",
			type: "POST",
			contentType: false,
			data: formData,
			dataType: "json",
			cache: false,
			processData: false,
			success: function (d) {
				$.loadingBlockHide();
				$.iaoAlert({
					msg: "File upload successfully",
					type: "success",
					mode: "dark",
				})
				setTimeout(function () {
					window.location.reload(LoadFileAttachment);
				}, 1500);
				LoadFileAttachment();
			},
			error: function () {
				alert("Please contact admin");
				$.loadingBlockHide();
				return false;
			}
		});
	} else {
		$.iaoAlert({
			msg: "Kindly upload Image or Pdf file!",
			type: "error",
			mode: "dark",
		})
	}

}


function checkFileAttachment() {
	var fi = document.getElementById('IsFileExtension');

	if (fi.files.length > 0) {
		for (var i = 0; i <= fi.files.length - 1; i++) {
			var fileName, fileExtension, fileSize, fileType, dateModified;

			fileName = fi.files.item(i).name;
			fileExtension = fileName.replace(/^.*\./, '');
			fileSize = fi.files.item(i).size;

			if (fileExtension == 'png' || fileExtension == 'jpg' || fileExtension == 'jpeg' || fileExtension == 'pdf') {
				$("#btnfileUpload").attr('disabled', false);

			}
			else {
				$.iaoAlert({
					msg: "Kindly upload Image or Pdf file!",
					type: "error",
					mode: "dark",
				})
				$("#btnfileUpload").attr('disabled', true);
			}

		}
	}
}



function repairTypeValidation(typeID) {
	var mylist = document.getElementById('myList');
	var filter = /^[A-Z]*$/;
	if (myList.value > 0) {
		document.getElementById("myList").classList.remove("inputError");
		document.getElementById("typelable").classList.remove("text-danger");
		document.getElementById("typeid").hidden = true;
		return true;
	} else {

		document.getElementById("myList").classList.add("inputError");
		document.getElementById("typeid").hidden = false;
		document.getElementById('typeid').innerHTML = "Must Select One";

		mylist.focus;
		return false;

	}
}


//Notify Dropdown Validation
function NotifyUserTypeValidation(notifyID) {
	var mynotifyList1 = document.getElementById('mynotifyList1');
	var filter = /^[A-Z]*$/;
	if (mynotifyList1.value > 0) {
		document.getElementById("mynotifyList1").classList.remove("inputError");
		document.getElementById("notifylbid").classList.remove("text-danger");
		document.getElementById("notid").hidden = true;
		return true;
	} else {

		document.getElementById("mynotifyList1").classList.add("inputError");
		document.getElementById("notid").hidden = false;
		document.getElementById('notid').innerHTML = "User is required";
		mynotifyList1.focus;
		return false;

	}
}

//Notify Add
function AddNotifyPage() {
	var ticket_id = localStorage.getItem("IsTicketId");
	var formData = new FormData();
	var notifyID = document.getElementById("mynotifyList1").value;
	formData.append("ticket_id", ticket_id);
	formData.append("notify_id", document.getElementById("mynotifyList1").value);
	if (NotifyUserTypeValidation(notifyID)) {
		$.loadingBlockShow();
		document.getElementById("notid").hidden = true;
		document.getElementById("notifylbid").classList.remove("text-danger");

		$.ajax({
			url: "/Tickets/AddNotify",
			type: "POST",
			contentType: false,
			data: formData,
			dataType: "json",
			cache: false,
			processData: false,
			success: function (result) {
				if (result.status == "Success") {
					$.iaoAlert({
						msg: "Saved successfully",
						type: "success",
						mode: "dark",
					})
					$('#mynotifyList1').val(0).trigger('change');
					gridNotifyloadData();
				} else {
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
					gridNotifyloadData();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

				return false;
			}
		});
	}

}



//Delete Notify
function NotifyDelete(arg) {
	var ticket_id = localStorage.getItem("IsTicketId");
	$.loadingBlockShow();
	$.ajax({
		type: 'POST',
		url: "/Tickets/DeleteNotify",
		data: {
			ticket_id: ticket_id,
			notify_email: arg.dataset.id
		},
		success: function (result) {
			$.loadingBlockHide();
			$.iaoAlert({
				msg: "Delete successfully",
				type: "success",
				mode: "dark",
			})
			gridNotifyloadData();
		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			alert("Please contact admin");
			return false;
		}
	});
}


//EditLog
function SaveLogdata() {
	var ticket_id = localStorage.getItem("IsTicketId");
	var formData = new FormData();
	formData.append("ticket_id", ticket_id);
	formData.append("ticket_log_id", document.getElementById("tktlogid").value);
	formData.append("log_hours", document.getElementById("hours").value);
	formData.append("log_desc", document.getElementById("desc").value);
	$.ajax({
		url: "/Tickets/Editlog",
		type: "POST",
		contentType: false,
		data: formData,
		dataType: "json",
		cache: false,
		processData: false,
		success: function (d) {
			$.loadingBlockHide();
			$.iaoAlert({
				msg: "Saved successfully",
				type: "success",
				mode: "dark",
			})
			setTimeout(function () {
				window.location.reload(gridLogsloadData);
			}, 1500);
			$("#LogModal .close").click();
			gridLogsloadData();

		},
		error: function () {
			alert("Please contact admin");
			//window.location.href = "/Tickets/Repairs";
			$.loadingBlockHide();
			return false;
		}
	});

}


//DeleteLog
function DeleteLog(arg) {
	$.loadingBlockShow();
	$.ajax({
		type: 'POST',
		url: "/Tickets/logDelete",
		data: {
			ticket_log_id: arg.dataset.id
		},
		success: function (result) {

			$.loadingBlockHide();
			$.iaoAlert({
				msg: "Delete successfully",
				type: "success",
				mode: "dark",
			})
			setTimeout(function () {
				window.location.reload(gridLogsloadData);
			}, 1500);
			gridLogsloadData();
		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			alert("Please contact admin");
			return false;
		}
	});
}


function openNav() {
	$(".ticket-section").toggleClass('move');
}
$(document).ready(function () {
	if ($('.ticket-section').length == 1) {
		$("html").addClass('fullheight');
	}
});


//Ticket Action Page Start

function ActionValidation() {

	var LogEntry = document.getElementById('IsLogEntry').checked;
	var YankTicket = document.getElementById('IsYankTicket').checked;
	var ticket = document.getElementById('IsAssinTicket').checked;
	var Status = document.getElementById('IsChangeStatus').checked;

	var summary = document.getElementById('IsActionSummary').value;
	var user = document.getElementById('IsAssignedUserID1').value;
	var group = document.getElementById('IsGroupID1').value;

	if (LogEntry == true || YankTicket == true) {
		if (summary == "" || summary == null || summary == '' || summary == undefined) {
			$("#checkSummary").text(" Summary is Required");
			$("#checkUser").text("");
			return false;
		} else {
			$("#checkSummary").text("");
			$("#checkUser").text("");
			return true;
		}
	}
	else if (ticket == true) {
		if (user == "-- Not Selected --" && group == "-- Not Selected --") {
			$("#checkUser").text(" Must select a User or Group!");
			$("#checkSummary").text("");
			return false;
		}
		else {
			$("#checkUser").text("");
			$("#checkSummary").text("");
			return true;
		}
	}
	else if (Status == true) {
		if (summary == "" || summary == null || summary == '' || summary == undefined) {
			$("#checkSummary").text(" Summary is Required");
			$("#checkUser").text("");
			return false;
		} else {
			$("#checkUser").text("");
			$("#checkSummary").text("");
			return true;
		}

	}
}


function GroupChange() {
	var group = $('#IsGroupID1').val();
	var user = $('#IsAssignedUserID1').val();
	if (user != "-- Not Selected --" && group != "-- Not Selected --") {
		$('#IsAssignedUserID1').val("-- Not Selected --").trigger('change');
	}
}

function UserChange() {
	var group = $('#IsGroupID1').val();
	var user = $('#IsAssignedUserID1').val();

	if (user != "-- Not Selected --" && group != "-- Not Selected --") {
		$('#IsGroupID1').val("-- Not Selected --").trigger('change');
	}
}



// Action Edit page
function SubmitActionForms() {
	let postUrl = "/Tickets/AddEditAction";
	if (ActionValidation()) {

		var LogEntry = document.getElementById('IsLogEntry').checked;
		var YankTicket = document.getElementById('IsYankTicket').checked;
		var Ticket = document.getElementById('IsAssinTicket').checked;
		var Status = document.getElementById('IsChangeStatus').checked;


		var request = {
			LogEntry: LogEntry,
			YankTicket: YankTicket,
			Ticket: Ticket,
			Status: Status,

			ticket_id: document.getElementById("ticket_id").value,
			AssignedUserID: document.getElementById("IsAssignedUserID").value,
			GroupID: document.getElementById("IsGroupID").value,

			log_entry: document.getElementById("IsActionSummary").value,
			log_tp_id: document.getElementById("IsLogType").value,
			time_worked: document.getElementById("IsTimeWorked").value,

			AssignedUserID1: document.getElementById("IsAssignedUserID1").value,
			GroupID1: document.getElementById("IsGroupID1").value,
			ServiceDate: document.getElementById("IsServiceDate").value,
			TicketStatID: document.getElementById("IsTicketStatID").value,


		}
		$.ajax({
			cache: false,
			type: "POST",
			url: postUrl,
			data: {
				'request': request
			},

			success: function (result) {
				if (result.status == "Success") {
					$.iaoAlert({
						msg: "Saved successfully",
						type: "success",
						mode: "dark",
					})
					sessionStorage.setItem("IsTicketActive", true);
					window.location.href = "/Tickets/Logs";
				}

				else {
					if (result.status == "Failure") {
						window.location.href = "/Home/Error_log";
					}
				}
			},

			error: function (xhr, ajaxOptions, thrownError) {

				return false;
			}
		});
	}
}


//Ticket Action Page End

function SaveContractAttachment() {
	$.loadingBlockHide();
	var formData = new FormData();
	formData.append("Image_path", $('#IsFileExtension')[0].files[0]);
	formData.append("contract_id", document.getElementById('contract_id').value);
	if ($('#IsFileExtension')[0].files[0] != null && $('#IsFileExtension')[0].files[0] != undefined) {
		$.ajax({
			url: "/Contracts/uploadContractFileAttachments",
			type: "POST",
			contentType: false,
			data: formData,
			dataType: "json",
			cache: false,
			processData: false,
			success: function (d) {
				$.loadingBlockHide();
				$.iaoAlert({
					msg: "File upload successfully",
					type: "success",
					mode: "dark",
				})
				$('#IsFileExtension').val(null);
				LoadTicketFileAttachment();
			},
			error: function () {
				alert("Please contact admin");
				$.loadingBlockHide();
				return false;
			}
		});
	} else {
		$.iaoAlert({
			msg: "Kindly upload Image or Pdf file!",
			type: "error",
			mode: "dark",
		})
	}

}

function InvAttachmentDelete(arg) {
	$.loadingBlockShow();
	$.ajax({
		type: 'POST',
		url: "/Contracts/AttachmentDelete",
		data: {
			file_attach_id: arg.dataset.id
		},
		success: function (result) {
			$.loadingBlockHide();
			$.iaoAlert({
				msg: "File delete successfully",
				type: "success",
				mode: "dark",
			})
			LoadTicketFileAttachment();
		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			alert("Please contact admin");
			return false;
		}
	});
}

//Inventory Delete
function DeleteInv(arg) {
	var inv_id = document.getElementById("inv_id").value;
	$.loadingBlockShow();
	$.ajax({
		type: 'POST',
		url: "/Inventory/InventoryDelete",
		data: {
			inv_id: inv_id
		},
		success: function (result) {
			$.loadingBlockHide();
			$.iaoAlert({
				msg: "Delete successfully",
				type: "success",
				mode: "dark",
			})
			$("#InvDeletePopModal .close").click();
			window.location.href = "/Inventory/Index";
		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			alert("Please contact admin");
			return false;
		}
	});
}

//Inventory Move
function MoveInv(arg) {
	var inv_id = document.getElementById("inv_id").value;
	var facility_id = document.getElementById("myListfacility").value;
	var customer_id = document.getElementById("myListCustomer").value;
	$.loadingBlockShow();
	$.ajax({
		type: 'POST',
		url: "/Inventory/InventoryMove",
		data: {
			inv_id: inv_id,
			facility_id: facility_id,
			customer_id: customer_id
		},
		success: function (result) {
			$.loadingBlockHide();
			$.iaoAlert({
				msg: "Move successfully",
				type: "success",
				mode: "dark",
			})
			$("#InvMovePopModal .close").click();
			window.location.href = "/Inventory/AddEditInventory/" + btoa(inv_id);
		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			alert("Please contact admin");
			return false;
		}
	});
}


function SaveInvAttachment() {
	$.loadingBlockHide();
	var formData = new FormData();
	formData.append("Image_path", $('#IsFileExtension')[0].files[0]);
	formData.append("inv_id", document.getElementById('inv_id').value);
	if ($('#IsFileExtension')[0].files[0] != null && $('#IsFileExtension')[0].files[0] != undefined) {
		$.ajax({
			url: "/Inventory/uploadInvFileAttachments",
			type: "POST",
			contentType: false,
			data: formData,
			dataType: "json",
			cache: false,
			processData: false,
			success: function (d) {
				$.loadingBlockHide();
				$.iaoAlert({
					msg: "File upload successfully",
					type: "success",
					mode: "dark",
				})
				$('#IsFileExtension').val(null);
				LoadInvFileAttachment();
			},
			error: function () {
				alert("Please contact admin");
				$.loadingBlockHide();
				return false;
			}
		});
	} else {
		$.iaoAlert({
			msg: "Kindly upload Image or Pdf file!",
			type: "error",
			mode: "dark",
		})
	}

}

function AttachmentDelete(arg) {
	$.loadingBlockShow();
	$.ajax({
		type: 'POST',
		url: "/Inventory/AttachmentDelete",
		data: {
			file_attach_id: arg.dataset.id
		},
		success: function (result) {
			$.loadingBlockHide();
			$.iaoAlert({
				msg: "File delete successfully",
				type: "success",
				mode: "dark",
			})
			LoadInvFileAttachment();
		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			alert("Please contact admin");
			return false;
		}
	});
}


function CloneInventoryForm() {
	let postUrl = "/Inventory/AddEditInventories";

	var request = {
		inv_id: document.getElementById("inv_id").value,
		facility_id: document.getElementById("FacilityId").value,
		ManufactureList: document.getElementById("IsManufactureList").value,
		EquipmentSerial: document.getElementById("IsEquipmentSerial").value,
		HospitalControl: document.getElementById("IsHospitalControl").value,
		Control: document.getElementById("IsControl").value,
		PreviousControl: document.getElementById("IsPreviousControl").value,
		SystemId: document.getElementById("IsSystemId").value,
		Department: document.getElementById("IsDepartment").value,
		Room: document.getElementById("IsRoom").value,
		InventoryNotes: document.getElementById("IsInventoryNotes").value,

		OwnerShip: document.getElementById("IsOwnerShip").value,
		ManagedEntity: document.getElementById("IsManagedEntity").value,
		InstalledDate: document.getElementById("IsInstalledDate").value,
		WarrentyStatus: document.getElementById("IsWarrentyStatus").value,
		WarrentyExpires: document.getElementById("IsWarrentyExpires").value,
		LastPmCompleted: document.getElementById("IsLastPmCompleted").value,
		NextSchedulePm: document.getElementById("IsNextSchedulePm").value,
		PmInterval: document.getElementById("IsPmInterval").value,
		Alarm: document.getElementById("IsAlarm").value,
		ItemCoverage: document.getElementById("IsItemCoverage").value,
		Status: document.getElementById("IsStatus").value,
		ServiceType: document.getElementById("IsServiceType").value,

		Provider1: document.getElementById("IsProvider1").value,
		SrvCoverage1: document.getElementById("IsSrvCoverage1").value,
		Provider2: document.getElementById("IsProvider2").value,
		SrvCoverage2: document.getElementById("IsSrvCoverage2").value,

		HasBattery: document.getElementById("IsHasBattery").checked,
		BatteryLastChanged: document.getElementById("IsBatteryLastChanged").value,
		BatteryChangeInterval: document.getElementById("IsBatteryChangeInterval").value,
		IsActive: document.getElementById("IsActive").checked,
		RootClass: document.getElementById("IsRootClass").value,
		SubClass: document.getElementById("IsSubRootClass").value,
		SelectedModelDescription: document.getElementById("IsModelDescription").value
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Inventory/Index";
			}
			else {
				if (result.status == "Failure") {
					window.location.href = "/Home/Error_log";
				}
			}

		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}

	});
}

function DeptValidation(typeID) {
	var deptList = document.getElementById('deptList');
	var filter = /^[A-Z]*$/;
	if (deptList.value > 0) {
		document.getElementById("deptList").classList.remove("inputError");
		document.getElementById("deptlable").classList.remove("text-danger");
		document.getElementById("deptid").hidden = true;
		return true;
	} else {

		document.getElementById("deptList").classList.add("inputError");
		document.getElementById("deptid").hidden = false;
		document.getElementById('deptid').innerHTML = "Must Select One";

		deptList.focus;
		return false;

	}
}

function SaveCloneInvdata() {
	var inv_id = document.getElementById("inv_id").value;
	var formData = new FormData();
	var typeID = document.getElementById("deptList").value;
	formData.append("inv_id", inv_id);
	formData.append("hos_control_num", document.getElementById("IsHospital1").value);
	formData.append("control_num", document.getElementById("IsControl1").value);
	formData.append("serial_no", document.getElementById("IsSerial1").value);
	formData.append("dept_id", document.getElementById("deptList").value);

	if (DeptValidation(typeID)) {
		$.loadingBlockShow();
		document.getElementById("deptid").hidden = true;
		document.getElementById("deptlable").classList.remove("text-danger");
		$.ajax({
			url: "/Inventory/PostDept",
			type: "POST",
			contentType: false,
			data: formData,
			dataType: "json",
			cache: false,
			processData: false,
			success: function (d) {
				$.loadingBlockHide();
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Inventory/Index";
				$("#CloneModal .close").click();
			},
			error: function () {
				alert("Please contact admin");
				$.loadingBlockHide();
				return false;
			}
		});
	}
}

function TestResultsDelete(arg) {
	var usr_id = localStorage.getItem("UserId");
	$.loadingBlockShow();
	$.ajax({
		type: 'POST',
		url: "/Tickets/TicketTestResultsDelete",
		data: {
			usr_id: usr_id,
			test_results_id: arg.dataset.id
		},
		success: function (result) {
			$.loadingBlockHide();
			$.iaoAlert({
				msg: " Test result delete successfully",
				type: "success",
				mode: "dark",
			})
			$("#TestResultDeletePopModal .close").click();
			LoadTestResults();
		},
		errorurl: function (xhr, ajaxOptions, thrownError) {
			return false;
		}
	});
}

function SaveTestResultdata(arg) {
	var ticket_id = localStorage.getItem("IsTicketId");
	console.log("fdfs", ticket_id);
	var formData = new FormData();
	var typeID = document.getElementById("testList").value;
	var resID = document.getElementById("result1").value;
	formData.append("ticket_id", ticket_id);
	formData.append("test_id", document.getElementById("testList").value);
	formData.append("param1", document.getElementById("result1").value);
	formData.append("param2", document.getElementById("result2").value);
	formData.append("param3", document.getElementById("result3").value);
	formData.append("test_dt", document.getElementById("IsDatePicker").value);
	formData.append("test_equip", document.getElementById("IsTest").value);

	if (TestResultValidation()) {
		$.loadingBlockShow();
		document.getElementById("testid").hidden = true;
		document.getElementById("testlable").classList.remove("text-danger");

		$.ajax({
			url: "/Tickets/SaveTestResult",
			type: "POST",
			contentType: false,
			data: formData,
			dataType: "json",
			cache: false,
			processData: false,
			success: function (result) {
				if (result.status == "Success") {
					$.loadingBlockHide();
					$.iaoAlert({
						msg: "Saved successfully",
						type: "success",
						mode: "dark",
					})
					window.location.href = "/Tickets/Testing";
					$("#ResultModal .close").click();
				} else {
					if (result.status == "Failure") {
						window.location.href = "/Home/Error_log";
					}
				}
			},
			error: function () {
				return false;
			}
		});
	}
}

function TestValidation(typeID) {
	var deptList = document.getElementById('testList');

	if (deptList.value == 0) {
		document.getElementById("testList").classList.add("inputError");
		document.getElementById("testid").hidden = false;
		document.getElementById('testid').innerHTML = "Must Select One";
		deptList.focus;
		return false;
	}
	else {
		document.getElementById("testList").classList.remove("inputError");
		document.getElementById("testlable").classList.remove("text-danger");
		document.getElementById("testid").hidden = true;
		return true;
	}
}

function resultValidation() {
	var result = document.getElementById('result1').value;
	if (result == null || result == "" || result == undefined) {
		document.getElementById('resid').innerHTML = "First value can not be blank.";
		document.getElementById("resid").hidden = false;
		result.focus;
		return false;
	} else {
		document.getElementById("resid").hidden = true;
		return true;
	}
}

function TestResultValidation() {
	var deptList = document.getElementById('testList');
	var result = document.getElementById('result1').value;

	if ((deptList.value == 0) && (result == null || result == "" || result == undefined)) {
		document.getElementById("testList").classList.add("inputError");
		document.getElementById("testid").hidden = false;
		document.getElementById('testid').innerHTML = "Must Select One";
		document.getElementById('resid').innerHTML = "First value can not be blank.";
		document.getElementById("resid").hidden = false;
		return false;
	}
	else if (deptList.value == 0) {
		document.getElementById("testList").classList.add("inputError");
		document.getElementById("testid").hidden = false;
		document.getElementById('testid').innerHTML = "Must Select One";
		document.getElementById("resid").hidden = true;
		return false;
	}
	else if (result == null || result == "" || result == undefined) {
		document.getElementById('resid').innerHTML = "First value can not be blank.";
		document.getElementById("testid").hidden = true;
		document.getElementById("resid").hidden = false;
		return false;
	}
	else if (deptList.value > 0 && result !== "") {
		document.getElementById("testList").classList.remove("inputError");
		document.getElementById("testlable").classList.remove("text-danger");
		document.getElementById("testid").hidden = true;
		document.getElementById("resid").hidden = true;
		return true;
	}


}