using hmsBO;
using HMSMemri.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;

namespace HMSMemri.BaseSettings
{
    public class GlobalData
    {
        public WebPages LastPage = WebPages.NO_PAGE;
        public WebPages CurPage = WebPages.NO_PAGE;

        public CustomerClass CurrentCust=new CustomerClass();
        public UserClass CurrentUser = new UserClass();

        public bool is_cust_login { get; set; }
        

        public int cust_id { get; set; } 
        public int cust_Contact_id { get; set; }
        public int vendor_id { get; set; }
        public int mfg_model_id { get; set; }

        public int inv_id { get; set; }
        public int prod_sub_class_id { get; set; }
        public int facility_id { get; set; }
        public GetCustomerResponse CurrentCustomer = new GetCustomerResponse();
        public GetVendorResponse CurrentVendor = new GetVendorResponse();
        public GetInventoiesResponse CurrentFacility = new GetInventoiesResponse();
        public bool customer_show_closed = false;
        public bool facilities_show_closed = false;
        public bool inventory_show_closed = false;
        public bool tickets_show_closed = false;
        public bool manufacturer_show_closed = false;
        public bool contracts_show_closed = false;
        public static void SetGlobalSessione(Microsoft.AspNetCore.Http.ISession Session, string SessionGlobal, GlobalData Global)
        {
            Session.SetString(SessionGlobal, JsonConvert.SerializeObject(Global));
        }

        public static GlobalData GetGlobalSessione(Microsoft.AspNetCore.Http.ISession Session, string SessionGlobal)
        {
            if (Session.GetString(SessionGlobal) != null)
            {
                return JsonConvert.DeserializeObject<GlobalData>(Session.GetString(SessionGlobal));
            }
            return null;
            
        }


        /// <summary>
        /// get full name
        /// </summary>
        /// <param name="user">user</param>
        /// <returns></returns>
        public static object GetFullName(HttpContext httpContext)
        {
            GlobalData data = GlobalData.GetGlobalSessione(httpContext.Session, "UserData");
            if (data == null)
                return "";
            else
                return data.CurrentUser.UserName;

        }
    }
}
