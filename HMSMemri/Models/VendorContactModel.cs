﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{

    /// <summary>
    /// Get Vendor Contact Responses like status,sucess and error messge
    /// </summary>
    /// 
    public class GetVendorContactResponse
    {
        public VendorContact VendorContactDetail { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public List<SelectListItem> ContactModule { get; set; }
    }

    public class VendorContact
    {
        public int vendor_Contact_id { get; set; }
        public int vendor_id { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First Name is required")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Phone Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Cell Phone")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid cell phone")]
        public string CellPhone { get; set; }

        [Display(Name = "Fax Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid fax number")]
        public string FaxNumber { get; set; }

        [Display(Name = "Email Address")]
        [RegularExpression(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$", ErrorMessage = "Not a valid Email Address")]
        public string EmailAddress { get; set; }

        [Display(Name = "Alt Number1")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid alt number1")]
        public string AltNumber1 { get; set; }

        [Display(Name = "Alt Number2")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid alt number2")]
        public string AltNumber2 { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }
        public bool IsActive { get; set; }
        public int ContactType { get;set; }
    }

    public class tbl_vendorcontact
    {
        public int vendor_contact_id { get; set; }
        public int vendor_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string title { get; set; }
        public string phone { get; set; }
        public string cell { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string alternate_num1 { get; set; }
        public string alternate_num2 { get; set; }
        public string notes { get; set; }
        public bool is_active { get; set; }
    }
    public class lst_tbl_vendorcontact
    {
        public List<tbl_vendorcontact> tbl_vendorcontact { get; set; }
    }
    public class VendorContactDrop
    {
        public int contact_tp_id { get; set; }
        public string contact_tp_desc { get; set; }     
    }
}
