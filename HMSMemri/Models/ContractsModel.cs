using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{
    /// <summary>
    /// Get Contracts Responses like status,sucess and error messge
    /// </summary>
    public class GetContractResponse
    {
        public Contracts Contractdetail { get; set; }
        public tbl_file_attach FileDetail { get; set; }
        public tbl_contract tbl_ContractDetail { get;set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public string ExcelLink { get; set; }
        public List<SelectListItem> CustomerStatusModule { get; set; }
        public LoadState loadState { get; set; }
    }

    public class CustomerList
    {
        public int cust_id { get; set; }
        public string cust_name { get; set; }
    }
    //Get Contracts detail values in Contracts Edit form
    public class Contracts
    {
        public int ContractId { get; set; }

        public int CustomerID { get; set; }
        public int FacilityID { get; set; }
        public int VendorID { get; set; }
        public string ContractRef { get; set; }
        public string ContractInRef { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Notes { get; set; }
        public string ContractPO { get; set; }
        public string PartsPO { get; set; }
        public int ContractTypeID { get; set; }
        public int ContractLaborRate { get; set; }
        public bool IsActive { get; set; }
        public string CustomerName { get; set; }
        public string FacilityName { get; set; }
        public string VendorName { get; set; }
    }
   
    // Set or get databse column name
    public class tbl_contract
    {
        public int contract_id { get; set; }
        public int cust_id { get; set; }
        public  int vendor_id { get; set; }
        public int facility_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; } 
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public string contract_ref { get; set; }
        public string contract_in_ref { get; set; }
        public DateTime? start_dt { get; set; }
        public DateTime? enddate_dt { get; set; }
        public string notes { get; set; }
        public string contract_po { get; set; }
        public string parts_po { get; set; }
        public int contract_tp_id { get; set; }
        public Decimal contract_labor_rate { get; set; }
        public bool is_active { get; set; }
        public string cust_name { get; set; }
        public string vendor_name { get; set; }
        public string facility_name { get; set; }

        public int trk_usr_id { get; set; }
        public int contract_inv_id { get; set; } 
        public int inv_id { get; set; }
        public string mfg_name { get; set; }
        public string prod_class_desc { get; set; }
        public string prod_sub_class_desc { get; set; }
        public string model { get; set; }
        public string serial_no { get; set; }
        public string control_num { get; set; }

    }

    public class lst_tbl_contract
    {
        public List<tbl_contract> tbl_contract { get; set; }
    }
    public class tbl_contract_inv
    {
        int? trk_usr_id { get; set; }
        public int? contract_inv_id { get; set; }
        public int? inv_id { get; set; }
        public string mfg_name { get; set; }
        public string prod_class_desc { get; set; }
        public string prod_sub_class_desc { get; set; }
        public string model { get; set; }
        public string serial_no { get; set; }
        public string control_num { get; set; }
    }
    public class lst_tbl_contract_inv
    {
        public List<tbl_contract_inv> tbl_contract { get; set; }
    }
}
