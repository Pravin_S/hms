using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMSMemri.Models
{
    /// <summary>
    /// Get Customer Responses like status,sucess and error messge
    /// </summary>
    public class GetCustomerResponse
    {
        public Customer customerdetail { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public string ExcelLink { get; set; }
        public List<SelectListItem> CustomStatusModule { get; set; }
        public List<SelectListItem> StateModule { get; set; }
        public List<SelectListItem> serviceTypeModule { get; set; }

        public LoadState loadState { get; set; }
        
    }
    //Load state	
    public class LoadState
    {
        public int usr_id { get; set; }
        public int grid_id { get; set; }
        public string json_col_ord { get; set; }
    }

    //Get Customer detail values in customer Edit form
    public class Customer
    {
        public int CustomerId { get; set; }

        [Required(ErrorMessage = "Customer Name is required")]
        public string CustomerName { get; set; }

        public string CustomerCode { get; set; }

        [Required(ErrorMessage = "Address 1 is required")]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Required(ErrorMessage = "Zipcode is required")]
        public string ZipCode { get; set; }
      
        [RegularExpression(@"^.{2,}$", ErrorMessage = "State is required")]
        public string State { get; set; }
        public string Country { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNumber { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid fax number")]
        public string Fax { get; set; }
        public int VisitAllocation { get; set; }
        public double? DefaultLabor { get; set; }

        [RegularExpression(@"^.{2,}$", ErrorMessage = "Customer Status is required")]
        public int CustomerStatus { get; set; }

        public string Notes { get; set; }

        public bool IsActive { get; set; }
        public int FacilityCount { get; set; }
        public int ContactCount { get; set; }
        public int TicketCount { get; set; }
        public List<tbl_cust_service> cust_service_type { get; set; }

        [Required(ErrorMessage = "Service is required")]
        [NotMapped]
        public int[] service_typeIds { get; set; }
        public string ServiceTypesList { get; set; }
        public string ErrorMsg { get; set; }
    }
   
    // Set or get databse column name
    public class tbl_cust
    {
        public int cust_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; } 
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public string cust_name { get; set; }
        public string cust_code { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string st { get; set; }
        public string country { get; set; }
        public string zip_code { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public int cust_stat_id { get; set; }
        public string cust_stat_desc { get; set; }
        public string notes { get; set; }
        public float? labor_rate { get; set; }
        public bool is_active { get; set; }
        public bool is_notes { get; set; }
        public List<tbl_facility> lst_tbl_facility { get; set; }
    }

    public class lst_tbl_cust
    {
        public List<tbl_cust> tbl_cust { get; set; }
    }
    //get or set customer status
    public class CustomerStatus
    {
        public int cust_stat_id { get; set; }
        public string cust_stat_desc { get; set; }
        public bool is_active { get; set; }
    }

    //get or set state values
    public class States
    {
        public string abbr { get; set; }
        public string st { get; set; }
    }

    public class CustomerSearch
    {
        public List<GridList> gridList { get; set; }
    }

    public class GridList
    {
        public int cust_id { get; set; }
    }
    public class ServiceType
    {
        public int service_tp_id { get; set; }
        public string service_tp_desc { get; set; }
    }

    public class tbl_cust_service
    {
        public int service_tp_id { get; set; }
    }

}
