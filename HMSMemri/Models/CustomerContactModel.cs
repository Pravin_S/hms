﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{
    public class GetCustomerContactResponse
    {
        public CustomerContact CustomerContactDetail { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public List<SelectListItem> ContactModule { get; set; }
    }

    public class CustomerContact
    {
        public int cust_contact_id { get; set; }
        public int facility_contact_id { get; set; }
        public int cust_id { get; set; }
        public int facility_id { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }   
        
        public string Title { get; set; }
        
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone Number")]
        public string PhoneNumber { get; set; }

        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Cell Phone")]
        public string CellPhone { get; set; }

        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Fax Number")]
        public string FaxNumber { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$", ErrorMessage = "Not a valid Email Address")]
        [Required(ErrorMessage = "Email is required")]
        public string EmailAddress { get; set; }

        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid alt number1")]
        public string AltNumber1 { get; set; }

        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid alt number2")]
        public string AltNumber2 { get; set; }

        public string Notes { get; set; }
        public bool IsActive { get; set; }
        [RegularExpression(@"^[1-9][0-9]*$", ErrorMessage = "Contact Type is required")]
        public int ContactType { get; set; }
        public bool ContactLocation { get; set; }
        public string ClientLogin { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string ClientPassword { get; set; }
        public string LoginEnabled { get; set; }
       
    }
    // Drop Down Contact Type
    public class CustomerContactDrop
    {
        public int contact_tp_id { get; set; }
        public string contact_tp_desc { get; set; }
        public bool is_active { get; set; }
    }

    public class tbl_cust_contact
    {
        public int cust_contact_id { get; set; }
        public int cust_id { get; set; }
        public int facility_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string title { get; set; }
        public string phone { get; set; }
        public string cell { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string alternate_num1 { get; set; }
        public string alternate_num2 { get; set; }
        public string notes { get; set; }
        public string pswd { get; set; }
        public bool is_active { get; set; }
        public int contact_tp_id { get; set; }
        public string contact_tp_desc { get; set; }

    }
    public class lst_tbl_cust_contact
    {
        public List<tbl_cust_contact> tbl_cust_contact { get; set; }
    }
}