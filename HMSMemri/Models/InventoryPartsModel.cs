using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{
    /// <summary>
    /// Get Contracts Responses like status,sucess and error messge
    /// </summary>
    public class GetInventoryPartsResponse
    {
        public InventoryParts InventoryParts { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public string ExcelLink { get; set; }
    }


    //Get Inventory Parts detail values in Contracts Edit form
    public class InventoryParts
    {
        public int TicketChargeID { get; set; }
        public int TicketID { get; set; }
        public string TicketDescription { get; set; }
        public string TicketTypeDescription { get; set; }
        public DateTime ServiceDate { get; set; }
        public string PartNo { get; set; }
        public string ChargeDescription { get; set; }
        public string Charge { get; set; }
    }
   
    // Set or get databse column name
    public class tbl_ticket_check_tp
    {
        public int ticket_charge_id { get; set; }
        public int ticket_id { get; set; }
        public int charge_tp_id { get; set; }
        public float qty { get; set; }
        public float cost { get; set; }
        public float charge { get; set; }
        public string part_no { get; set; }
        public string charge_desc { get; set; }
        public float quoted { get; set; }
        public string po { get; set; }
        public string charge_tp_desc { get; set; }
        public float total { get; set; }
        public string ticket_desc { get; set; }
        public string ticket_tp_desc { get; set; }
        public DateTime? service_dt { get; set; }
    }

    public class lst_tbl_ticket_check_tp
    {
        public List<tbl_ticket_check_tp> tbl_ticket_check_tp { get; set; }
    }

}
