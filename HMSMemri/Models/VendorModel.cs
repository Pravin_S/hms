using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{
    /// <summary>
    /// Get Vendor Responses like status,sucess and error messge
    /// </summary>
    public class GetVendorResponse
    {
        public Vendor Vendordetail { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ExcelLink { get; set; }
        public string ErrorMessage { get; set; }
        public List<SelectListItem> StateModule { get; set; }
        public LoadState loadState { get; set; }
    }
    public class Vendor
    {
        public int vendor_id { get; set; }

        [Display(Name = "Vendor Name")]
        [Required(ErrorMessage = "Vendor Name is required")]
        public string VendorName { get; set; }

        [Display(Name = "Address 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        public string Address2 { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "State")]
        public string State { get; set; }

        [Display(Name = "Zip")]
        public string Zip { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "Phone Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Fax Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid fax number")]
        public string FaxNumber { get; set; }

        [Display(Name = "Email Address")]
        [RegularExpression(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$", ErrorMessage = "Not a valid Email Address")]
        public string EmailAddress { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "IsActive Vendor")]
        public bool IsActive { get; set; }
    }    
    // Set or get databse column name
    public class tbl_vendor
    {
        public int vendor_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public string vendor_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string st { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        public string notes { get; set; }
        public bool is_active { get; set; }

    }

    public class lst_tbl_vendor
    {
        public List<tbl_vendor> tbl_vendor { get; set; }
    }
    // VendorContact Grid
    public class tbl_vendor_contact
    {
        public int vendor_contact_id { get; set; } 
        public int vendor_id { get; set; }
        public string phone { get; set; }
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public bool is_active { get; set; }
    }
    public class lst_tbl_VendorContact
    {
        public List<tbl_vendor_contact> tbl_vendor_contact { get; set; }
    }
}
