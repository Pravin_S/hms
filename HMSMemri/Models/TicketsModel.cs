﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{
    public class GetTicketResponse
    {
        public Tickets Ticketdetail { get; set; }
        public EditTickets EditTicketsdetail { get; set; }
        public  InventoryTicket InventoryTicketDetails { get; set; }
        public tbl_file_attach FileDetail { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public string FileUploadExtension { get; set; }
        public List<SelectListItem> TicketAddMethodModule { get; set; }
        public List<SelectListItem> TicketTypeModule { get; set; }
        public List<SelectListItem> PriorityModule { get; set; }
        public List<SelectListItem> UserNameDropDownModule { get; set; }
        public List<SelectListItem> GroupNameDropDownModule { get; set; }
        public List<SelectListItem> TicketStatusModule { get; set; }
        public List<SelectListItem> CustomerDropdownModule { get; set; }
        public List<SelectListItem> FacilityDropdownModule { get; set; }
        public List<SelectListItem> tbl_ticket_check_tp { get; set; }
        public tbl_ticket_check_tp TicketCheckDetail { get; set; }
        public tbl_ticket_notify NotifyDetail { get; set; }
        public tbl_ticket_log LogDetail { get; set; }
        public ActionTickets ActionTicketDetail { get; set; }
        public List<SelectListItem> LogTypeDropdownModule { get; set; }
    }

    public class TicketStatus
    {
        public int ticket_stat_id { get; set; }
        public string ticket_stat_desc { get; set; }
    }
    public class TicketAddMethod
    {
        public int ticket_addmethod_id { get; set; }
        public string ticket_addmethod_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class TicketType
    {
        public int ticket_tp_id { get; set; }
        public string ticket_tp_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class Priority
    {
        public int ticket_priority_stat_id { get; set; }
        public string ticket_priority_stat_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class UserNameDropDown
    {
        public int usr_id { get; set; }
        public string usr_name { get; set; }
        public bool is_active { get; set; }
    }

    public class GroupNameDropDown
    {
        public int group_id { get; set; }
        public string group_name { get; set; }
        public bool is_active { get; set; }
    }

    public class ChargeTypeDropDown
    {
        public int charge_tp_id { get; set; }
        public string charge_tp_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class Tickets
    {
        public int ticket_id { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Customer Name is required")]
        public int CustomerID { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Facility Name is required")]
        public int FacilityID { get; set; }
        [RegularExpression(@"^[1-9][0-9]*$", ErrorMessage = "User is required")]
        public int AssignedUserID { get; set; }
        public int ClosedUserID { get; set; }
        public int GroupID { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Ticket Status is required")]
        public int TicketStatID { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Ticket Arrived Via is required")]
        public int TicketAddMethodID { get; set; }
        public int ChkTypeUD { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "TIcket Type is required")]
        public int TicketTypeID { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Priorty is required")]
        public int PriorityID { get; set; }
        public string StartDate { get; set; }
        public string DeadLineDate { get; set; }
        public string DeadLineDate1 { get; set; }
        [Required(ErrorMessage = "Ticket Description is required")]
        public String TicketDescription { get; set; }
        public int TicketHealthID { get; set; }
        public DateTime CloseDate { get; set; }
        public DateTime ServiceDate { get; set; }
        public string TicketStatDescription { get; set; }
        public bool IsActive { get; set; }
        public Boolean IsChargesRequired { get; set; }
        public string Summary { get; set; }

        [Required(ErrorMessage = "Caller Name is required")]
        public string CallerName { get; set; }
        [Required(ErrorMessage = "Caller Phone is required")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Caller Phone ")]
        public string CallerPhone { get; set; }
        public string CaseNumber { get; set; }
        public int User { get; set; }
        public string GroupName { get; set; }
        public string cust_name { get; set; }
        public string facility_name { get; set; }
        public int inv_id { get; set; }
         
    }
    public class tbl_ticket
    {
        public int ticket_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public string case_num { get; set; }
        public int? cust_id { get; set; }
        public int? facility_id { get; set; }
        public int? assigned_to_usr_id { get; set; }
        public int? closed_by_usr_id { get; set; }
        public int? group_id { get; set; }
        public int? ticket_stat_id { get; set; }
        public int? ticket_addmethod_id { get; set; }
        public string summary { get; set; }
        public int? check_tp_id { get; set; }
        public int? ticket_tp_id { get; set; }
        public int? priority_id { get; set; }
        public DateTime? start_dt { get; set; }
        public DateTime? deadline_dt { get; set; }

        public string ticket_desc { get; set; }
        public int? ticket_health_id { get; set; }

        public DateTime? closed_dt { get; set; }
        public DateTime? service_dt { get; set; }
        public Boolean? is_charges_required { get; set; }
        public string caller_name { get; set; }

        public string caller_phone { get; set; }
        public bool? is_active { get; set; }
        public string ticket_stat_desc { get; set; }
        public string ticket_priority_stat_desc { get; set; }
        public string ticket_tp_desc { get; set; }
        public string group_name { get; set; }
    }

    public class lst_tbl_tickets
    {
        public List<tbl_ticket> tbl_ticket { get; set; }
    }
    //fcaility DropDown
    public class dropFacilty
    {
        public int facility_id { get; set; }
        public string facility_name { get; set; }
    }
    // File Attachment Model
    public class tbl_file_attach
    {
        public int file_attach_id { get; set; }
        public int usr_id { get; set; }
        public int file_attach_tp_id { get; set; }
        public int record_id { get; set; }
        public string file_name { get; set; }
        public string file_size { get; set; }
        [Required(ErrorMessage = "Please select file.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf)$", ErrorMessage = "Only Image files allowed.")]
        public string file_path { get; set; }
        public string file_Save { get; set; }
        public int ticket_id { get; set; }
        public int contract_id { get; set; }
        public int inv_id { get; set; }
        public IFormFile Image_path { get; set; }
    }
    public class lst_tbl_file_attach
    {
        public List<tbl_file_attach> tbl_file_attach { get; set; }
    }

    public class tbl_ticket_notify
    {
        public int notify_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public int ticket_id { get; set; }
        public string notify_email { get; set; }
    }
    public class lst_tbl_ticket_notify
    {
        public List<tbl_ticket_notify> tbl_ticket_notify { get; set; }   
    }

    public class EditTickets
    {
        public int ticket_id { get; set; }
        public int CustomerID { get; set; }
        public int FacilityID { get; set; }
        public int AssignedUserID { get; set; }
        public int GroupID { get; set; }
        public int TicketStatID { get; set; }
        public int TicketAddMethodID { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Ticket Type is required")]
        public int TicketTypeID { get; set; }
        public int PriorityID { get; set; }
        public string StartDate { get; set; }
        public string DeadLineDate { get; set; }
        public string DeadLineDate1 { get; set; }
        [Required(ErrorMessage = "Problem Description is required")]
        public String TicketDescription { get; set; }
        public int TicketHealthID { get; set; }
        public DateTime CloseDate { get; set; }
        public DateTime ServiceDate { get; set; }
        public string TicketStatDescription { get; set; }
        public bool IsActive { get; set; }
        public Boolean IsChargesRequired { get; set; }
        public string Summary { get; set; }

        [Required(ErrorMessage = "Caller Name is required")]
        public string CallerName { get; set; }
        [Required(ErrorMessage = "Caller Phone is required")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Caller Phone ")]
        public string CallerPhone { get; set; }
        public string CaseNumber { get; set; }
    }

    // Inventory Ticket Details
    public class InventoryTicket
    {
        public int ticket_id { get; set; }       
        public string Status { get; set; }
        public int ticketType { get; set; }
        public string ticket_tp_desc { get; set; }
        //public int ticket_tp_id { get; set; }
        public string priority { get; set; }
        public string HospitalNumber { get; set; }
        public string control { get; set; }
        public string PreviouscontrolNumber { get; set; }
        public string serial { get; set; }
        public string Model { get; set; }
        public string Rootclass { get; set; }
        public string SubClass { get; set; }
        public string SystemId { get; set; }
        public int inv_id { get; set; }
        public string Department { get; set; }      
        //public int PmInterval { get; set; }
        public string PmInterval { get; set; }
        public string LastPmCompleted { get; set; }
        public string NextScheduledPm { get; set; }
        public string servicecovreage { get; set; }
        public string Ownership { get; set; }
        //public string ownership_tp_id { get; set; }
        public string InstallDate { get; set; }
        public string WarrentyExpires { get; set; }
        public string WarrentyStatus { get; set; }
        public string CallerName { get; set; }
        public string CallerPhone { get; set; }
        public string ClosedDate { get; set; }
        public string CreateDate { get; set; }
        public string StartDate { get; set; }
        public string ServiceDate { get; set; }
        public string CreatedBy { get; set; }
        public string Manufacturer { get; set; }
        public string ArrivedVia { get; set; }
        public string AssignTo { get; set; }
        public string ClosedBy { get; set; }
        public string Riskfactor { get; set; }
        public string concern { get; set; }
        public string cust_name { get; set; }
        public string facility_name { get; set; }
        public string InvStatus { get; set; }
        public string contract_po { get; set; }
        public string parts_po { get; set; }
    }

    public class tbl_ticket_log
    {
        public int ticket_log_id { get; set; }
        public int ticket_id { get; set; }
        public string usr_login { get; set; }
        public DateTime? trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public string log_action { get; set; }
        public float log_hours { get; set; }
        public string log_desc { get; set; }
       
    }
    public class lst_ticket_log
    {
        public List<tbl_ticket_log> tbl_ticket_log { get; set; }
    }

    public class tbl_ticket_audit
    {
        public int ticket_audit_id { get; set; }
        public int ticket_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public string audit_detatils { get; set; }
        public string usr_name { get; set; }
    }
    public class lst_tbl_ticket_audit
    {
        public List<tbl_ticket_audit> tbl_ticket_audit { get; set; }
    }
    // Action Page
    public class ActionTickets
    {
        public int ticket_id { get; set; }
        public int CustomerID { get; set; }
        public int FacilityID { get; set; }
        public int AssignedUserID { get; set; }
        public int GroupID { get; set; }
        public int TicketStatID { get; set; }
        public int TicketStatID1 { get; set; }
        public string ServiceDate { get; set; }
        public string log_tp_id { get; set; }
        public string log_entry { get; set; }
        public int Compentency { get; set; }
        public string AssignedUserID1 { get; set; }
        public string GroupID1 { get; set; }
        public bool AssignTicket { get; set; }
        public bool YankTicket { get; set; }
        public bool LogEntry { get; set; }
        public bool Ticket { get; set; }
        public bool Status { get; set; }
        public bool add_log_entry { get; set; }
        public int ticket_log_tp_id { get; set; }
        public float time_worked { get; set; }
    }
    // Log Type DropDown
    public class dropLogType
    {
        public int ticket_log_tp_id { get; set; }
        public string ticket_log_tp_desc { get; set; }
    }

    public class tbl_test_results
    {
        public int test_results_id { get; set; }
        public string test_type { get; set; }
        public string test_parm { get; set; }
        public string test_dt { get; set; }
        public string test_val { get; set; }
        public string test_equip { get; set; }
        public int ticket_id { get; set; }
        public int test_id { get; set; }
        public string param1 { get; set; }
        public string param2 { get; set; }
        public string param3 { get; set; }
    }

    public class lst_tbl_test_results
    {
        public List<tbl_test_results> tbl_test_results { get; set; }
    }
    public class tbl_test
    {
        public int test_id { get; set; }  
        public string test_type { get; set; }
        public int test_tp_id { get; set; }
        public string display_header { get; set; }
    }
    
}