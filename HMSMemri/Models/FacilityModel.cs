using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{
    /// <summary>
    /// Summary for Facility Response
    /// </summary>
    public class GetFacilityResponse
    {
        public Facility facilitydetail { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public string ExcelLink { get; set; }
        public List<SelectListItem> FacilityStatusModule { get; set; }
        public List<SelectListItem> StateModule { get; set; }
        public List<SelectListItem> CustomerDropdownModule { get; set; }
        public LoadState loadState { get; set; }
    }

    public class lst_tbl_facility
    {
        public List<tbl_facility> tbl_facility { get; set; }
    }
    //Database variable get,set
    public class tbl_facility
    {
        public int facility_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public int cust_id { get; set; }
        public string facility_code { get; set; }
        public string facility_name { get; set; }
        public int facility_stat_id { get; set; }
        public string facility_stat_desc { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string st { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string notes { get; set; }
        public int? tech_id { get; set; }
        public int? sales_rep_id { get; set; }
        public bool is_active { get; set; }
        public int FacilityCount { get; set; }
        public int ContactCount { get; set; }
        public int TicketCount { get; set; }
    }

    /// <summary>
    /// Summary for Facility Page get,set
    /// </summary>
    public class Facility
    {
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Customer Name is required")]
        public int cust_id { get; set; }
        public int FacilityId { get; set; }
        [Required(ErrorMessage = "Facility Name is required")]
        public string FacilityName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNumber { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Fax number")]
        public string Fax { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Facility Status is required")]
        public int FacilityStatus { get; set; }
        public string Notes { get; set; }
        public bool IsActive { get; set; }
        public string ErrorMsg { get; set; }
        public int? Tech_id { get; set; }
        public int? Sales_rep_id { get; set; }
        public string Facility_stat_desc { get; set; }
        public int ContactCount { get; set; }
        public int TicketCount { get; set; }
        public int Inventory_cnt { get; set; }
        public int OpenTicketCount { get; set; }
        public int ContractCount { get; set; }
    }
    //Facility Status get,set 
    public class dropFacilityStatus
    {
        public int facility_stat_id { get; set; }
        public string facility_status_desc { get; set; }
        public bool is_active { get; set; }
    }

    //Customer Dropdown get,set
    public class dropCustomer
    {
        public int cust_id { get; set; }
        public string cust_name { get; set; }
    }
}
