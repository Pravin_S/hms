using System;

namespace HMSMemri.Models
{
    public class ServerVar
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
    public class ErrorLog
    {
        public int errorid { get; set; }
        public string site { get; set; }
        public  string Loaction { get; set; }
        public string errormsg { get; set; }
    }
}
