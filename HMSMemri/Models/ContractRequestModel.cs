using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{
    /// <summary>
    /// Get Contract Responses 
    /// </summary>
    public class GetContractRequestDetails
    {
        public ContractRequest contractrequest { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public List<SelectListItem> VendorListModule { get; set; }
        public List<SelectListItem> ContractTypeList { get; set; }
        public List<SelectListItem> ServiceTypeModule { get; set; }
    }

    //Get Contract detail values in contract Edit form
    public class ContractRequest
    {
        public int contract_id { get; set; }
        public int cust_id { get; set; }
        public int facility_id { get; set; }

        [Display(Name = "Contract Type")]
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Contract Type is required")]
        public int contract_tp_id { get; set; }
        [Required, Range(1, int.MaxValue, ErrorMessage = "Service Type is required")]
        public int service_tp_id { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "Contract PO#")]
        public string contract_po { get; set; }

        [Display(Name = "Parts PO#")]
        public string parts_po { get; set; }

        [Display(Name = "Contract Labor#")]
        public float contract_labor_rate { get; set; }

        [Display(Name = "Travel Charges#")]
        public float contract_travel_chg { get; set; }

        [Display(Name = "Vendor")]
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Vendor is required")]
        public int vendor_id { get; set; }
        
        [Display(Name = "Start Term Date")]
        [Required(ErrorMessage = "Start Term Date is required")]
        [RegularExpression(@"^\(?([0-9]{2})\)?[-/ ]?([0-9]{2})[-/ ]?([0-9]{4})$", ErrorMessage = "Not a valid Start Term Date")]
        public string start_dt { get; set; }
        [Required(ErrorMessage = "End Term Date is required")]
        [Display(Name = "End Term Date")]
        [RegularExpression(@"^\(?([0-9]{2})\)?[-/ ]?([0-9]{2})[-/ ]?([0-9]{4})$", ErrorMessage = "Not a valid End Term Date")]
        public string enddate_dt { get; set; }
        public bool Isactive { get; set; }
        public string cust_name { get; set; }
        public string facility_name { get; set; }
    }
    public class ContractType
    {
        public int contract_tp_id { get; set; }
        public string contract_tp_desc { get; set; }
    }

    public class ContractServiceType
    {
        public int service_tp_id { get; set; }
        public string service_tp_desc { get; set; }
    }
}
