﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{

    public class GetInventoryResponse
    {
        public MfgInventory inventorydetail { get; set; }
        public mfg_model mfg_models { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public LoadState loadState { get; set; }
    }

    //Get Inventory detail values in Inventory Edit form
    public class MfgInventory
    {
        public int InventoryId { get; set; }

        [Required(ErrorMessage = "Inventory Name is required")]
        public string InventoryName { get; set; }

        [Required(ErrorMessage = "Address 1 is required")]
        public string Address1 { get; set; }

        [Required(ErrorMessage = "Address 2 is required")]
        public string Address2 { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Required(ErrorMessage = "Zipcode is required")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "State is required")]
        public string State { get; set; }

        public string Country { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Fax is required")]
        public string Fax { get; set; }

        [Required(ErrorMessage = "Default Email is required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Website is required")]
        public string Website { get; set; }

        [Required(ErrorMessage = "Customer Status is required")]

        public string Notes { get; set; }

        public bool IsActive { get; set; }
    }



    // Set or get databse column name
    public class tbl_inventory
    {
        public int mfg_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public string mfg_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string st { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        public string notes { get; set; }
        public string facility_name { get; set; }
    }

    public class lst_tbl_inventory
    {
        public List<tbl_inv> tbl_inv { get; set; }
    }

    public class lst_tbl_mmodel
    {
        public List<tbl_mfg_model> tbl_mfg_model { get; set; }
    }
    //Inventory page start
    public class GetInventoiesResponse
    {
        public Inventory Inventorydetail { get; set; }
        public tbl_file_attach FileDetail { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public string ExcelLink { get; set; }
        public List<SelectListItem> OwnerShipModule { get; set; }
        public List<SelectListItem> IntervalPmModule { get; set; }
        public List<SelectListItem> AlarmModule { get; set; }
        public List<SelectListItem> ItemCoverageModule { get; set; }
        public List<SelectListItem> VendorListModule { get; set; }
        public List<SelectListItem> BatteryChangeModule { get; set; }
        public List<SelectListItem> ManufactureModule { get; set; }
        public List<SelectListItem> WarrentyStatusModule { get; set; }
        public List<SelectListItem> StatusModule { get; set; }
        public List<SelectListItem> ManagedEntityModule { get; set; }
        public List<SelectListItem> RootClassModule { get; set; }
        public List<SelectListItem> ServiceCoverModule { get; set; }

        public List<SelectListItem> SubRootClassModule { get; set; }
        public List<SelectListItem> ServiceTypeModule { get; set; }
        public List<SelectListItem> DepartmentModule { get; set; }
    }

    public class Inventory
    {
        public int inv_id { get; set; }
        public int facility_id { get; set; }
        public int cust_id { get; set; }
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Display(Name = "Facility Name")]
        public string FacilityName { get; set; }
        [Display(Name = " Manufacture List")]
        public int ManufactureList { get; set; }
        [Display(Name = "Root Class")]
        public int RootClass { get; set; }
        public int SubClass { get; set; }
        [Display(Name = "Model")]
        public string Model { get; set; }
        [Display(Name = "Selected Model Description")]
        public string SelectedModelDescription { get; set; }
        [Display(Name = "Equipment Serial")]
        public string EquipmentSerial { get; set; }
        [Display(Name = "Hospital Control")]
        public string HospitalControl { get; set; }
        [Display(Name = "Control")]
        public string Control { get; set; }
        [Display(Name = "Previous Control")]
        public string PreviousControl { get; set; }
        [Display(Name = "System Id")]
        public string SystemId { get; set; }
        [Display(Name = "Department ")]
        [RegularExpression(@"^[1-9][0-9]*$", ErrorMessage = "Department is required")]
        public int Department { get; set; }
        [Display(Name = "Room")]
        public string Room { get; set; }
        [Display(Name = "Inventory Notes")]
        public string InventoryNotes { get; set; }
        [Display(Name = "Ownership")]
        public int Ownership { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Managed Entity is required")]
        public int ManagedEntity { get; set; }
        [Display(Name = "Format")]
        public string Format { get; set; }
        [Display(Name = "Install Date")]
        [RegularExpression(@"^\(?([0-9]{2})\)?[-/ ]?([0-9]{2})[-/ ]?([0-9]{4})$", ErrorMessage = "Not a valid Install Date")]
        public string InstalledDate { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Warrenty Status is required")]
        public int WarrentyStatus { get; set; }
        [Display(Name = "Warrenty Expires")]
        [RegularExpression(@"^\(?([0-9]{2})\)?[-/ ]?([0-9]{2})[-/ ]?([0-9]{4})$", ErrorMessage = "Not a valid Warrenty Expires Date")]
        public string WarrentyExpires { get; set; }
        [RegularExpression(@"^\(?([0-9]{2})\)?[-/ ]?([0-9]{2})[-/ ]?([0-9]{4})$", ErrorMessage = "Not a valid Last Pm Completed Date")]
        public string LastPmCompleted { get; set; }
        [Display(Name = "Next Scheduled Pm")]
        [RegularExpression(@"^\(?([0-9]{2})\)?[-/ ]?([0-9]{2})[-/ ]?([0-9]{4})$", ErrorMessage = "Not a valid Next Scheduled Pm Date")]
        public string NextSchedulePm { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Pm Interval is required")]
        public int PmInterval { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Alarm is required")]
        public int Alarm { get; set; }
        public int ItemCoverage { get; set; }
        [RegularExpression(@"^.{2,}$", ErrorMessage = "Status  is required")]
        public int Status { get; set; }
        public int Provider1 { get; set; }
        public int SrvCoverage1 { get; set; }
        public int Provider2 { get; set; }
        public int SrvCoverage2 { get; set; }
        public bool HasBattery { get; set; }
        [RegularExpression(@"^\(?([0-9]{2})\)?[-/ ]?([0-9]{2})[-/ ]?([0-9]{4})$", ErrorMessage = "Not a valid Battery Last Changed Date")]
        public string BatteryLastChanged { get; set; }
        public int BatteryChangeInterval { get; set; }
        public string ManufacturerAtributes { get; set; }
        public int vendor_id { get; set; }
        public int vendor_coverage { get; set; }
        public int risk_num { get; set; }
        public DateTime status_change_dt { get; set; }
        public DateTime battery_next_changed_dt { get; set; }
        public bool IsActive { get; set; }
        public string AddModel { get; set; }
        [Required, Range(1, int.MaxValue, ErrorMessage = "Service Type is required")]
        public int ServiceType { get; set; }
    }

    // Drop Down inventory Page
    public class InventoryOwnerShipDrop
    {
        public int inv_ownership_tp_id { get; set; }
        public string inv_ownership_tp_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class InventoryIntervalpm
    {
        public int inv_pm_interval_tp_id { get; set; }
        public string inv_pm_interval_tp_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class InventoryAlarm
    {
        public int inv_alarm_tp_id { get; set; }
        public string inv_alarm_tp_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class InventoryConcern
    {
        public int inv_concern_tp_id { get; set; }
        public string inv_concern_tp_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class InventoryItemCoverage
    {
        public int inv_cover_tp_id { get; set; }
        public string inv_cover_tp_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class InventorVendorList
    {
        public int vendor_id { get; set; }
        public string Vendor_Name { get; set; }
        public bool is_active { get; set; }
    }
    public class InventoryBatteryInterval
    {
        public int inv_bat_change_tp_id { get; set; }
        public string inv_bat_change_tp_desc { get; set; }
        public bool is_active { get; set; }
    }
    public class InventoryManufactureList
    {
        public int mfg_id { get; set; }
        public string mfg_name { get; set; }
        public bool is_active { get; set; }

    }
    //Warrenty Status DropDown
    public class InventoryWarrentyStatus
    {
        public int inv_war_stat_id { get; set; }
        public string inv_war_stat_desc { get; set; }
    }
    // Status DropDown
    public class InventoryStatus
    {
        public int inv_stat_id { get; set; }
        public string inv_stat_desc { get; set; }
    }
    // Managed Entity DropDown
    public class InventoryManagedEntity
    {
        public int managed_entity_tp_id { get; set; }
        public string managed_entity_tp_desc { get; set; }
    }
    // Root Class DropDown
    public class InventoryRootClass
    {
        public int prod_class_id { get; set; }
        public string prod_class_desc { get; set; }
    }

    public class InventoryPoductSubRootClass
    {
        public int prod_sub_class_id { get; set; }
        public string prod_sub_class_desc { get; set; }
    }
    // Service Coverage DropDown
    public class InventoryServiceCover
    {
        public int inv_service_cover_tp_id { get; set; }
        public string inv_service_cover_tp_desc { get; set; }
    }
    // Service Type DropDown 
    public class InventoryServiceType
    {
        public int service_tp_id { get; set; }
        public string service_tp_desc { get; set; }
    }
    // department DropDown
    public class InventoryDepartment
    {
        public int dept_id { get; set; }
        public string dept_name { get; set; }
    }
    public class tbl_mfg_model
    {
        public int mfg_model_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public int prod_sub_class_id { get; set; }
        public int mfg_id { get; set; }
        public string model_name { get; set; }
        public string model_desc { get; set; }
        public bool is_active { get; set; }


    }

    public class mfg_model
    {
        public int mfg_model_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public int prod_sub_class_id { get; set; }
        public int mfg_id { get; set; }
        public string model_name { get; set; }
        public string model_desc { get; set; }
        public bool is_active { get; set; }


    }
    public class Tbl_inventory
    {
        public int inv_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public int facility_id { get; set; }
        public int mfg_model_id { get; set; }
        public string hos_control_num { get; set; }
        public string control_num { get; set; }
        public string serial_no { get; set; }
        public bool is_active { get; set; }
        public int dept_id { get; set; }
        public int risk_num { get; set; }
        public int ownership_tp_id { get; set; }
        public DateTime install_dt { get; set; }
        public DateTime warrent_exp_dt { get; set; }
        public int inv_stat_id { get; set; }
        public DateTime last_activity_dt { get; set; }
        public DateTime next_activity_dt { get; set; }
        public int coverage_tp_id { get; set; }
        public int pm_interval_tp_id { get; set; }
        public int warranty_stat_id { get; set; }
        public int vendor_id { get; set; }
        public int vendor_coverage { get; set; }
        public int system_id { get; set; }
        public string previous_control_num { get; set; }
        public string room { get; set; }
        public string notes { get; set; }
        public string concerns { get; set; }
        public int vendor_id_1 { get; set; }
        public string coverage_1 { get; set; }
        public int vendor_id_2 { get; set; }
        public string coverage_2 { get; set; }
        public string managed_entity { get; set; }
        public int alarm_tp_id { get; set; }
        public bool battery_replacement { get; set; }
        public DateTime battery_last_changed_dt { get; set; }
        public int battery_interval { get; set; }
        public DateTime battery_next_changed_dt { get; set; }
        public string inv_stat_desc { get; set; }
        public string facility_name { get; set; }
        public int root_class_id { get; set; }
        public int sub_class_id { get; set; }
        public string model { get; set; }
        public string cust_name { get; set; }
        public int service_tp_id { get; set; }
    }

    public class tbl_prod_sub_class
    {
        public int prod_sub_class_id { get; set; }
        public string prod_sub_class_desc { get; set; }
    }

    public class lst_tbl_Subclass
    {
        public List<tbl_prod_sub_class> tbl_prod_sub_class { get; set; }
    }
    public class lst_tbl_inv
    {
        public List<tbl_Inv> tbl_Inv { get; set; }
    }
    public class tbl_Inv
    {
        public int inv_id { get; set; }
        public string facility_name { get; set; }
        public string hos_control_num { get; set; }
        public string control_num { get; set; }
        public string serial_no { get; set; }
        public string inv_stat_desc { get; set; }
        public string cust_name { get; set; }
        public string model { get; set; }

    }
}


