using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{

    public class lst_tbl_FacilityInventory
    {
        public List<tbl_inv> tbl_inv { get; set; }
    }
    //Database variable get,set
    public class tbl_inv
    {
        public int inv_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public int facility_id { get; set; }
        public string facility_name { get; set; }
        public int mfg_model_id { get; set; }
        public string hos_control_num { get; set; }
        public string control_num { get; set; }
        public string serial_no { get; set; }
        public bool is_active { get; set; }
        public string inv_stat_desc { get; set; }
        public string cust_name { get; set; }
        public string model { get; set; }
        public int dept_id { get; set; }
    }   
}