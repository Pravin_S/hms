﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{
    public class GetManufacturerResponse
    {
        public Manufacturer Manufacturerdetail { get; set; }
        public int Statuscode { get; set; }
        public string Status { get; set; }
        public string ExcelLink { get; set; }
        public string ErrorMessage { get; set; }
        public List<SelectListItem> StateModule { get; set; }
        public LoadState loadState { get; set; }
    }

    //Get Manufacturer detail values in Manufacturer Edit form
    public class Manufacturer
    {
        public int ManufacturerId { get; set; }

        [Required(ErrorMessage = "Manufacturer Name is required")]
        public string ManufacturerName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNumber { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid fax number")]
        public string Fax { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$", ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }
        public string Website { get; set; }
        public string Notes { get; set; }
        public string SupportLogin { get; set; }
        public string SupportPassword { get; set; }
        public bool IsActive { get; set; }
    }
    // Set or get databse column name
    public class tbl_mfg
    {
        public int mfg_id { get; set; }
        public DateTime trk_ent_dt { get; set; }
        public int trk_ent_usr_id { get; set; }
        public DateTime trk_upd_dt { get; set; }
        public int trk_upd_usr_id { get; set; }
        public string mfg_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string st { get; set; }
        public string zip { get; set; }
        public string country { get; set; }     
        public string phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        public string notes { get; set; }
        public bool is_active { get; set; }
    }

    public class lst_tbl_mfg
    {
        public List<tbl_mfg> tbl_mfg { get; set; }
    }
    //get or set state values
    public class InvStates
    {
        public string invAbbr { get; set; }
        public string InvSt { get; set; }
    }
}
