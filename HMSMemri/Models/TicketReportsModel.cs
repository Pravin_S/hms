using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMSMemri.Models
{
    /// <summary>
    /// Get Customer Responses like status,sucess and error messge
    /// </summary>
    public class GetTicketReportResponse
    {
        public string MessageDetails { get; set; }  
        public AppSettings appsettings { get; set; }
        
    }
    public class AppSettings
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
    }
    public class TicketReports
    {
        public string facility_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string st { get; set; }
        public string phone { get; set; }
    }
}