using hmsBO;
using System;

namespace HMSMemri.Models
{
    public class AjaxResult
    {
        public bool success { get; set; }
        public int id { get; set; }
        public string message { get; set; }
        public string additionalMsg { get; set; }
    }

}
