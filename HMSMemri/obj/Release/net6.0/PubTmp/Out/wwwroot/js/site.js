﻿// Login Page functionality Start //

//User Validation in login
function Uservalidation() {
	var UserName = $('#UserName').val();
	if (UserName == null || UserName == "" || UserName == undefined) {
		document.getElementById("userid").hidden = false;
		document.getElementById("UserName").classList.add("inputError");
		document.getElementById("userlable").classList.add("text-danger");
	} else {
		document.getElementById("userid").hidden = true;
		document.getElementById("UserName").classList.remove("inputError");
		document.getElementById("userlable").classList.remove("text-danger");
	}
}

//Password Validation in login
function Pwdvalidation() {
	var Password = $('#Password').val();
	if (Password == null || Password == "" || Password == undefined) {
		document.getElementById("pwdid").hidden = false;
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("pwdlable").classList.add("text-danger");
	} else {
		document.getElementById("pwdid").hidden = true;
		document.getElementById("Password").classList.remove("inputError");
		document.getElementById("pwdlable").classList.remove("text-danger");
	}
}

//Forget password check email validation
function checkEmail() {
	var email = document.getElementById('Email');
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!filter.test(email.value)) {
		document.getElementById("emailable").classList.add("text-danger");
		document.getElementById("Email").classList.add("inputError");
		email.focus;
		return false;
	} else {
		document.getElementById("emailable").classList.remove("text-danger");
		document.getElementById("Email").classList.remove("inputError");
		
		return true;
	}
}
function CustEmailCheck() {
	var email = document.getElementById('CustEmail');
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!filter.test(email.value)) {
		document.getElementById("CustEmail").classList.add("inputError");
		document.getElementById("CustNamelable").classList.add("text-danger");
		document.getElementById("custidlable").hidden = false;
		email.focus;
		return false;
	} else {
		document.getElementById("CustEmail").classList.remove("inputError");
		document.getElementById("CustNamelable").classList.remove("text-danger");
		document.getElementById("custidlable").hidden = true;
		return true;
	}
}

function validation() {
	var UserName = $('#UserName').val();
	var Password = $('#Password').val();
	if ((UserName == null || UserName == "" || UserName == undefined) && (Password == null || Password == "" || Password == undefined)) {
		document.getElementById("userid").hidden = false;
		document.getElementById("pwdid").hidden = false;
		document.getElementById("UserName").classList.add("inputError");
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("userlable").classList.add("text-danger");
		document.getElementById("pwdlable").classList.add("text-danger");
		return false;
	} else if (UserName == null || UserName == "" || UserName == undefined) {
		document.getElementById("userid").hidden = false;
		document.getElementById("pwdid").hidden = true;
		document.getElementById("UserName").classList.add("inputError");
		document.getElementById("Password").classList.remove("inputError");
		document.getElementById("userlable").classList.add("text-danger");
		document.getElementById("pwdlable").classList.remove("text-danger");
		return false;
	} else if (Password == null || Password == "" || Password == undefined) {
		document.getElementById("userid").hidden = true;
		document.getElementById("pwdid").hidden = false;
		document.getElementById("UserName").classList.remove("inputError");
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("userlable").classList.remove("text-danger");
		document.getElementById("pwdlable").classList.add("text-danger");
		return false;
	} else if (UserName !== "" && Password !== "") {
		return true;
	}
}

//Confirm password validation
function Confirmvalidation() {
	var NewPassword = $('#NewPassword').val();
	const re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()+=-\?;,./{}|\":<>\[\]\\\' ~_]).{8,}/
	if (NewPassword.length < 8) {
		document.getElementById("Pwderrortext1").hidden = false;
		document.getElementById("Pwderrortext2").hidden = true;
		return false;
	} else
		if (!re.test(NewPassword)) {
			document.getElementById("Pwderrortext2").hidden = false;
			document.getElementById("Pwderrortext1").hidden = true;
			return false;
		} else {
			document.getElementById("Pwderrortext1").hidden = true;
			document.getElementById("Pwderrortext2").hidden = true;
			return true;
		}
}

// change password validation
function changePwdvalidation() {
	var NewPassword = $('#NewPassword').val();
	var ConfirmPassword = $('#ConfirmPassword').val();
	if (NewPassword != ConfirmPassword) {
		document.getElementById("confirmerrortext").hidden = false;
		return false;
	} else {
		document.getElementById("confirmerrortext").hidden = true;
		return true;
	}
}

// Login API call
function Signin() {
	var UserName = $('#UserName').val();
	var Password = $('#Password').val();
	if (validation()) {
		$.loadingBlockShow();
		document.getElementById("userid").hidden = true;
		document.getElementById("pwdid").hidden = true;
		document.getElementById("pwdlable").classList.remove("text-danger");
		document.getElementById("userlable").classList.remove("text-danger");
		$.ajax({
			type: 'POST',
			url: '/Login/SignIn',
			data: {
				'UserName': UserName,
				'Password': Password
			},
			success: function (result) {
				$.loadingBlockHide();
				if (result.status == "Success") {
					localStorage.setItem("UserId", result.userdetail.userID);
					localStorage.setItem("UserName", result.userdetail.userName);
					localStorage.setItem("IsLoginId", result.userdetail.userID);
					localStorage.setItem("Is_cust_login", false);
					if (result.userdetail.isResetPwd == true) {
						$("#ChangepasswordModal").modal();
						$('#NewPassword').val(null);
						$('#ConfirmPassword').val(null);
						document.getElementById("Pwderrortext1").hidden = true;
						document.getElementById("Pwderrortext2").hidden = true;
						document.getElementById("confirmerrortext").hidden = true;
					} else {
						window.location.href = "Customer/Index";
					}
				} else {
					$.loadingBlockHide();
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}
}

//Reset Password API Call
function Resetpwd() {
	var Email = $('#Email').val();
	if (checkEmail()) {
		$.ajax({
			type: 'POST',
			url: '/Login/ForgetPassword',
			data: {
				'Email': Email
			},
			success: function (result) {
				if (result.status == "Success") {
					$.iaoAlert({
						msg: "Email send successfully",
						type: "success",
						mode: "dark",
					})
					$("#ForgotpasswordModal .close").click();
					$('#Email').empty();

				} else {
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}
}

// Change Password API Call
function Changepwd() {
	var NewPassword = $('#NewPassword').val();
	if (Confirmvalidation() && changePwdvalidation()) {
		var UserId = localStorage.getItem("IsLoginId");
		$.ajax({
			type: 'POST',
			url: '/Login/ChangePassword',
			data: {
				'UserId': UserId,
				'Password': NewPassword
			},
			success: function (result) {
				if (result.status == "Success") {
					$("#ChangepasswordModal .close").click();
					$('#NewPassword').val(null);
					$('#ConfirmPassword').val(null);
					$('#Password').val(null);
					$.iaoAlert({
						msg: "Success",
						type: "success",
						mode: "dark",
					})
				} else {
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}	
}
// Login Page functionality End //

//Change page pop Start//
function Changepop() {
	$("#ChangepasswordModal").modal();
	$('#NewPassword').val(null);
	$('#ConfirmPassword').val(null);
	document.getElementById("Pwderrortext1").hidden = true;
	document.getElementById("Pwderrortext2").hidden = true;
	document.getElementById("confirmerrortext").hidden = true;
}
//Change page pop Start//

// Customer Page functionality Start //

function EditCustomer() {
	window.location.href = "Customer/EditCustomer";
	localStorage.setItem("CustomerId", 0);
}

//Customer Update(save)  API Call
function SubmitForm(e) {
	var serviceType = document.getElementsByClassName("multiselect-selected-text")[0].innerHTML;
	var serviceTypeList = [];
	var serviceTypesMultiple = '';
	serviceTypeList = serviceType.split(',');
	for (var i = 0; i < serviceTypeList.length; i++) {
		serviceTypesMultiple += serviceTypeList[i];
	}

	var request = {
		CustomerId: document.getElementById("CustomerId").value,
		CustomerName: document.getElementById("IsCustomerName").value,
		Address1: document.getElementById("IsAddress1").value,
		Address2: document.getElementById("IsAddress2").value,
		city: document.getElementById("IsCity").value,
		ZipCode: document.getElementById("IsZipCode").value,
		State: document.getElementById("IsState").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		Fax: document.getElementById("IsFax").value,
		VisitAllocation: document.getElementById("IsVisitAllocation").value,
		DefaultLabor: document.getElementById("IsDefaultLabor").value,
		CustomerStatus: document.getElementById("IsCustomerStatus").value,
		Notes: document.getElementById("IsNotes").value,
		service_typeIds: document.getElementById("Subjects_dropdown").value,
		ServiceTypesList: serviceType,
	}

	$.ajax({
		type: 'POST',
		url: '/Customer/EditCustomers',
		data: {
			'customer': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Customer/Index";
			} else {
				$.iaoAlert({
					msg: result.errorMessage,
					type: "error",
					mode: "dark",
				})
			}

		},
		error: function (xhr, ajaxOptions, thrownError) {

		},
	});
}
// Customer Page functionality End //

// Facility Page functionality Start //
//SAve function facility page
function SubmitFormFacility(event) {

	var request = {
		cust_id: document.getElementById("IsCustomer").value,
		FacilityId: document.getElementById("FacilityId").value,
		FacilityName: document.getElementById("IsFacilityName").value,
		Address1: document.getElementById("IsAddress1").value,
		Address2: document.getElementById("IsAddress2").value,
		city: document.getElementById("IsCity").value,
		ZipCode: document.getElementById("IsZipCode").value,
		State: document.getElementById("IsState").value,
		Country: document.getElementById("IsCountry").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		Fax: document.getElementById("IsFax").value,
		FacilityStatus: document.getElementById("IsFacilityStatus").value,
		tech_id: document.getElementById("IsAssignedTech").value,
		sales_rep_id: document.getElementById("IsAssignedSales").value,
		Notes: document.getElementById("IsNotes").value
		//  IsActive: document.getElementById("IsActiveId").value
	}
	$.ajax({
		type: 'POST',
		url: '/Facilities/AddEditFacilities',
		data: {
			'Facility': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Facilities/Index";
			} else {
				$.iaoAlert({
					msg: result.errorMessage,
					type: "error",
					mode: "dark",
				})
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

		},
	});
	// Facility Page functionality End //
}

// Manufacturer Page functionality Start //
function SubmitFormManufacturer(event) {
	var request = {
		ManufacturerId: document.getElementById("ManufacturerId").value,
		ManufacturerName: document.getElementById("IsManufacturerName").value,
		Address1: document.getElementById("IsAddress1").value,
		Address2: document.getElementById("IsAddress2").value,
		city: document.getElementById("IsCity").value,
		ZipCode: document.getElementById("IsZipCode").value,
		State: document.getElementById("IsState").value,
		Notes: document.getElementById("IsNotes").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		Fax: document.getElementById("IsFax").value,
		Url: document.getElementById("IsUrl").value,
		Email: document.getElementById("IsEmail").value,
		IsActive: document.getElementById("IsActive").checked
	}
	$.ajax({
		type: 'POST',
		url: '/InventoryManufacturer/AddEditInv_Manufacturers',
		data: {
			'Manufacturer': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/InventoryManufacturer/Index";
			} else {
				$.iaoAlert({
					msg: result.errorMessage,
					type: "error",
					mode: "dark",
				})
			}

		},
		error: function (xhr, ajaxOptions, thrownError) {

		},
	});
}
// Manufacturer Page functionality End //

// Vendor page Start//
function SaveVendorForm(event) {
	let postUrl = "/Vendors/AddEditVendors";

	var request = {
		vendor_id: document.getElementById("VendorId").value,
		VendorName: document.getElementById("IsVendorName").value,
		Address1: document.getElementById("IsAddress1").value,
		Address2: document.getElementById("IsAddress2").value,
		City: document.getElementById("IsCity").value,
		State: document.getElementById("IsState").value,
		Zip: document.getElementById("IsZip").value,
		IsActive: document.getElementById("IsActive").checked,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		FaxNumber: document.getElementById("IsFaxNumber").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		EmailAddress: document.getElementById("IsEmail").value,
		Website: document.getElementById("IsWebsite").value,
		Notes: document.getElementById("IsNotes").value,
		IsActive: document.getElementById("IsActive").checked
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Vendors/Index";
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}
// Vendor page End//

// Vendor Contact page Start//
function SaveVendorContactForm() {
	let postUrl = "/VendorContact/AddEditVendorContacts";

	var request = {
		vendor_contact_id: document.getElementById("VendorContactId").value,
		vendor_id: document.getElementById("VendorId").value,
		FirstName: document.getElementById("IsFirstName").value,
		LastName: document.getElementById("IsLastName").value,
		Title: document.getElementById("IsTitle").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		CellPhone: document.getElementById("IsCellPhone").value,
		FaxNumber: document.getElementById("IsFaxNumber").value,
		EmailAddress: document.getElementById("IsEmailAddress").value,
		AltNumber1: document.getElementById("IsAltNumber1").value,
		AltNumber2: document.getElementById("IsAltNumber2").value,
		Notes: document.getElementById("IsNotes").value,
		ContactType: document.getElementById("IsContactType").value,
		IsActive: document.getElementById("IsActive").checked
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Vendors/Index";
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}
// Vendor Contact page End//

// Bio Med Ticket page Start//
function SubmitTicketsForm() {
	let postUrl = "/Tickets/AddEditTicket";

	var request = {
		ticket_id: document.getElementById("ticket_id").value,
		CallerName: document.getElementById("IsCallerName").value,
		CallerPhone: document.getElementById("IsCallerPhone").value,
		TicketAddMethodID: document.getElementById("IsTicketAddMethodID").value,
		Summary: document.getElementById("IsSummary").value,
		AssignedUserID: document.getElementById("IsAssignedUserID").value,
		GroupID: document.getElementById("IsGroupID").value,
		PriorityID: document.getElementById("IsPriorityID").value,
		StartDate: document.getElementById("IsStartDate").value,
		DeadLineDate: document.getElementById("IsDeadLineDate").value,
		TicketDescription: document.getElementById("IsTicketDescription").value,
		TicketStatID: document.getElementById("IsTicketStatID").value,
		CaseNumber: document.getElementById("IsCaseNumber").value,
		TicketTypeID: document.getElementById("IsTicketTypeID").value,
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Tickets/Index";
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}

// Inventory Page Start //
function SaveInventoryForm() {
	let postUrl = "/Inventory/AddEditInventories";

	var request = {
		inv_id: document.getElementById("inv_id").value,
		facility_id: document.getElementById("FacilityId").value,
		ManufactureList: document.getElementById("IsManufactureList").value,
		EquipmentSerial: document.getElementById("IsEquipmentSerial").value,
		HospitalControl: document.getElementById("IsHospitalControl").value,
		Control: document.getElementById("IsControl").value,
		PreviousControl: document.getElementById("IsPreviousControl").value,
		SystemId: document.getElementById("IsSystemId").value,
		Room: document.getElementById("IsRoom").value,
		InventoryNotes: document.getElementById("IsInventoryNotes").value,

		OwnerShip: document.getElementById("IsOwnerShip").value,
		ManagedEntity: document.getElementById("IsManagedEntity").value,
		InstalledDate: document.getElementById("IsInstalledDate").value,
		WarrentyStatus: document.getElementById("IsWarrentyStatus").value,
		WarrentyExpires: document.getElementById("IsWarrentyExpires").value,
		LastPmCompleted: document.getElementById("IsLastPmCompleted").value,
		NextSchedulePm: document.getElementById("IsNextSchedulePm").value,
		PmInterval: document.getElementById("IsPmInterval").value,
		Alarm: document.getElementById("IsAlarm").value,
		ItemCoverage: document.getElementById("IsItemCoverage").value,
		Status: document.getElementById("IsStatus").value,
		ServiceType: document.getElementById("IsServiceType").value,

		Provider1: document.getElementById("IsProvider1").value,
		SrvCoverage1: document.getElementById("IsSrvCoverage1").value,
		Provider2: document.getElementById("IsProvider2").value,
		SrvCoverage2: document.getElementById("IsSrvCoverage2").value,

		HasBattery: document.getElementById("IsHasBattery").checked,
		BatteryLastChanged: document.getElementById("IsBatteryLastChanged").value,
		BatteryChangeInterval: document.getElementById("IsBatteryChangeInterval").value,
		IsActive: document.getElementById("IsActive").checked,
		RootClass: document.getElementById("IsRootClass").value,
		SubClass: document.getElementById("IsSubRootClass").value,
		SelectedModelDescription: document.getElementById("IsModelDescription").value
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Inventory/Index";
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}

	});
}
// Inventory Page End //

//  contract page Start//
function SaveContractForm() {
	let postUrl = "/Contracts/AddEditContracts";
	var request = {
		contract_id: document.getElementById("contract_id").value,
		cust_id: document.getElementById("cust_id").value,
		facility_id: document.getElementById("facility_id").value,
		contract_tp_id: document.getElementById("Iscontract_tp_id").value,
		Notes: document.getElementById("IsNotes").value,
		contract_po: document.getElementById("Iscontract_po").value,
		parts_po: document.getElementById("Isparts_po").value,
		contract_labor_rate: document.getElementById("Iscontract_labor_rate").value,
		contract_travel_chg: document.getElementById("Iscontract_travel_chg").value,
		vendor_id: document.getElementById("Isvendor_id").value,
		start_dt: document.getElementById("IsStartTermDate").value,
		enddate_dt: document.getElementById("IsEndTermDate").value,
		Isactive: document.getElementById("Isactive").checked
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/Contracts/Index";

			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}
// contract page End//

// Customer Contact Page Start //
function SaveCustomerContactForm() {
	let postUrl = "/CustomerContact/AddEditCustomerContacts";
	var request = {
		cust_contact_id: document.getElementById("ContactId").value,
		cust_id: document.getElementById("CustId").value,
		facility_id: document.getElementById("FacilityId").value,
		FirstName: document.getElementById("IsFirstName").value,
		LastName: document.getElementById("IsLastName").value,
		Title: document.getElementById("IsTitle").value,
		PhoneNumber: document.getElementById("IsPhoneNumber").value,
		CellPhone: document.getElementById("IsCellPhone").value,
		FaxNumber: document.getElementById("IsFaxNumber").value,
		EmailAddress: document.getElementById("IsEmailAddress").value,
		AltNumber1: document.getElementById("IsAltNumber1").value,
		AltNumber2: document.getElementById("IsAltNumber2").value,
		Notes: document.getElementById("IsNotes").value,
		ContactType: document.getElementById("IsContactType").value,
		ClientLogin: document.getElementById("IsClientLogin").value,
		ClientPassword: document.getElementById("IsClientPassword").value,
		IsActive: document.getElementById("IsActive").checked
	}
	$.ajax({
		cache: false,
		type: "POST",
		url: postUrl,
		data: {
			'request': request
		},
		success: function (result) {
			if (result.status == "Success") {
				$.iaoAlert({
					msg: "Saved successfully",
					type: "success",
					mode: "dark",
				})
				window.location.href = "/CustomerContact";
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {

			return false;
		}
	});
}
// Customer Contact Page End //


//Customer Login Start//
function CustLoginValidation() {
	var UserName = $('#CustEmail').val();
	var Password = $('#Password').val();
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!filter.test(UserName) && (Password == null || Password == "" || Password == undefined)) {
		document.getElementById("custidlable").hidden = false;
		document.getElementById("pwdid").hidden = false;
		document.getElementById("CustEmail").classList.add("inputError");
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("CustNamelable").classList.add("text-danger");
		document.getElementById("pwdlable").classList.add("text-danger");
		return false;
	} else if (!filter.test(UserName) || UserName == null || UserName == "" || UserName == undefined) {
		document.getElementById("custidlable").hidden = false;
		document.getElementById("pwdid").hidden = true;
		document.getElementById("CustEmail").classList.add("inputError");
		document.getElementById("Password").classList.remove("inputError");
		document.getElementById("CustNamelable").classList.add("text-danger");
		document.getElementById("pwdlable").classList.remove("text-danger");
		return false;
	} else if (Password == null || Password == "" || Password == undefined) {
		document.getElementById("custidlable").hidden = true;
		document.getElementById("pwdid").hidden = false;
		document.getElementById("CustEmail").classList.remove("inputError");
		document.getElementById("Password").classList.add("inputError");
		document.getElementById("CustNamelable").classList.remove("text-danger");
		document.getElementById("pwdlable").classList.add("text-danger");
		return false;
	} else if (UserName !== "" && Password !== "") {
		return true;
	}
}
function CustSignin() {
	var CustEmail = $('#CustEmail').val();
	var Password = $('#Password').val();
	if (CustLoginValidation()) {
		$.loadingBlockShow();
		document.getElementById("custidlable").hidden = true;
		document.getElementById("pwdid").hidden = true;
		document.getElementById("pwdlable").classList.remove("text-danger");
		document.getElementById("CustNamelable").classList.remove("text-danger");
		$.ajax({
			type: 'POST',
			url: '/Login/CustSignIn',
			data: {
				'CustEmail': CustEmail,
				'Password': Password
			},
			success: function (result) {
				$.loadingBlockHide();
				if (result.status == "Success") {
					localStorage.setItem("IsLoginId", result.custdetail.custContactId);
					localStorage.setItem("Is_cust_login", true);
					localStorage.setItem("CustContactId", result.custdetail.custContactId);
					if (result.custdetail.isResetCustPwd == true) {
						$("#ChangepasswordModal").modal();
						$('#NewPassword').val(null);
						$('#ConfirmPassword').val(null);
						document.getElementById("Pwderrortext1").hidden = true;
						document.getElementById("Pwderrortext2").hidden = true;
						document.getElementById("confirmerrortext").hidden = true;
					} else {
						window.location.href = "/Inventory/Index";
					}
				} else {
					$.loadingBlockHide();
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}
}


function ResetCustomerpwd() {
	var Email = $('#Email').val();
	if (checkEmail()) {
		$.ajax({
			type: 'POST',
			url: '/Login/CustForgetPassword',
			data: {
				'Email': Email
			},
			success: function (result) {
				if (result.status == "Success") {
					$.iaoAlert({
						msg: "Email send successfully",
						type: "success",
						mode: "dark",
					})
					$("#ForgotpasswordModal .close").click();
					$('#CustEmail').empty();

				} else {
					$.iaoAlert({
						msg: result.errorMessage,
						type: "error",
						mode: "dark",
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			},
		});
	}
}

//Customer Login End //