﻿using System;
using System.Data;
using System.DirectoryServices.Protocols;
using System.Net;

namespace hmsBO
{
	public enum Authorization { LoginRequired = -1, NotAuthorized = 0, Authorized = 1 }

	public enum Rights
	{
		None = 0,                              //no right needed
		Application_Administrator = 1,         //full access, ignores all other security rights
		Application_Access = 2,
		Warehouse_Receiving = 40300,  //Project web appliation access
	}

	public enum LogType { Invalid = -1, Info = 0, Error = 1, Success = 2 }
	/// <summary>
	/// Summary description for security.
	/// </summary>
	public static class Security
	{
		public static string LOGIN_REQUIRED = "javascript:alert('Please log in to use this feature.');";
		public static string NOT_AUTHORIZED = "javascript:alert('You are not authorized for this area.');";

		public static UserClass Login(string userLogin, string userPswd, string ErrorMsg)
		{
			UserClass user = new UserClass();
			DataSet ds = new DataSet();
			ErrorMsg = String.Empty;
			string LogMsg = String.Empty;


			string IP = "";
			string remote_host = "";
			string user_agent = "";


			if (userLogin == null || userPswd == null)
			{
				ErrorMsg = "Invalid login.  Access logged.";
			}
			else
			{
				userLogin = userLogin.Replace("\"", "").Replace("'", "").Replace("-", "").Trim();
				userPswd = userPswd.Replace("\"", "").Replace("'", "");

				if (userLogin.Length == 0 || userPswd.Length == 0)
				{
					ErrorMsg = "Invalid login.  Access logged.";
					user.ErrorMsg = ErrorMsg;
				}
				else
				{
					try
					{
						DBAccess.fillDS(ds, TableDefs.User.tbl_usr, "usp_usr_login", userLogin);
						if (ds.Tables.Contains(TableDefs.User.tbl_usr.TableName) && ds.Tables[TableDefs.User.tbl_usr.TableName].Rows.Count > 0)
						{
							DataRow drUser = ds.Tables[TableDefs.User.tbl_usr.TableName].Rows[0];
							string dbPswd = drUser["usr_pswd"].ToString();
							string encryptPswd = Utility.Encrypt(userPswd);
							if (dbPswd == encryptPswd)
							{
								user.SetData((int)drUser["usr_id"], (string)drUser["usr_name"], Utility.ToBool(drUser["is_active"], false), Utility.ToBool(drUser["reset_pswd"], false));
								if (!Utility.ToBool(drUser["is_active"], false))
								{
									ErrorMsg = "Invalid login.  Access logged.";
									LogMsg = "Inactive user";
								}
							}
							else
							{
								ErrorMsg = "Invalid login.  Access logged.";
								LogMsg = "Incorrect password";
							}
						}
						else
						{
							ErrorMsg = "Invalid login.  Access logged.";
							LogMsg = "User not found";
						}
					}
					catch (Exception ex)
					{
						ErrorMsg = ex.Message + "  Access logged.";
						LogMsg = ErrorMsg;
					}
					user.ErrorMsg = LogMsg;
				}				
			}
			if (ErrorMsg == String.Empty)
			{
				//log success
				LogEntry(remote_host, user_agent, IP, userLogin, user.UserID, LogType.Success, "login", "");
			}
			else
			{
				//log failure
				LogEntry(remote_host, user_agent, IP, userLogin, user.UserID, LogType.Invalid, "login", LogMsg);
			}
			return user;
		}

		#region
		public static CustomerClass CustLogin(string custLogin, string custPswd, string ErrorMsg)
		{
			CustomerClass user = new CustomerClass();
			DataSet ds = new DataSet();
			ErrorMsg = String.Empty;
			string LogMsg = String.Empty;


            string IP = "";
            string remote_host = "";
            string user_agent = "";


            if (custLogin == null || custPswd == null)
			{
				ErrorMsg = "Invalid login.  Access logged.";
			}
			else
			{
				custLogin = custLogin.Replace("\"", "").Replace("'", "").Replace("-", "").Trim();
				custPswd = custPswd.Replace("\"", "").Replace("'", "");

				if (custLogin.Length == 0 || custPswd.Length == 0)
				{
					ErrorMsg = "Invalid login.  Access logged.";
					user.ErrorMsg = ErrorMsg;
				}
				else
				{
					try
					{
						//DBAccess.fillDS(ds, TableDefs.Customer.tbl_cust_contact, "usp_cust_contact_login", custLogin);
						DataRow GetCustLogin = TableDefs.Customer.GetCustomerContactLogin(ds, custLogin);
						if (ds.Tables.Contains(TableDefs.Customer.tbl_cust_contact.TableName) && ds.Tables[TableDefs.Customer.tbl_cust_contact.TableName].Rows.Count > 0)
						{
							DataRow drUser = ds.Tables[TableDefs.Customer.tbl_cust_contact.TableName].Rows[0];
						
							string dbPswd = drUser["pswd"].ToString();
							string encryptPswd = Utility.Encrypt(custPswd);
							if (dbPswd == encryptPswd)
							{
								user.SetData((int)drUser["cust_contact_id"] , (string)drUser["cust_contact_name"], (int)drUser["cust_id"], Utility.ToBool(drUser["is_active"], false), Utility.ToBool(drUser["reset_pswd"],false));
								if (!Utility.ToBool(drUser["is_active"], false))
								{
									ErrorMsg = "Invalid login.  Access logged.";
									LogMsg = "Inactive user";
								}
							}
							else
							{
								ErrorMsg = "Invalid login.  Access logged.";
								LogMsg = "Incorrect password";
							}
						}
						else
						{
							ErrorMsg = "Invalid login.  Access logged.";
							LogMsg = "User not found";
						}
					}
					catch (Exception ex)
					{
						ErrorMsg = ex.Message + "  Access logged.";
						LogMsg = ErrorMsg;
					}
					user.ErrorMsg = LogMsg;
				}
			}
            if (ErrorMsg == String.Empty)
            {
				//log success
				CustLogEntry(remote_host, user_agent, IP, custLogin, user.CustContactId, LogType.Success, "login", "");
            }
            else
            {
				//log failure
				CustLogEntry(remote_host, user_agent, IP, custLogin, user.CustContactId, LogType.Invalid, "login", LogMsg);
            }
            return user;
		}
		#endregion

		public static void LogEntry(string remote_host, string user_agent, string IP, string login, UserClass User, LogType logType, string action, string notes)
		{
			LogEntry(remote_host, user_agent, IP, login, User.UserID, logType, action, notes);
		}

		public static void LogEntry(string remote_host, string user_agent, string IP, string login, int usr_id,
			LogType logType, string action, string notes)
		{
			DBAccess.executeSQL("usp_usr_trk_log_entry  ", remote_host, user_agent, IP, login, usr_id, (int)logType, action, notes);
		}


		#region Customer log entry

		public static void CustLogEntry(string remote_host, string user_agent, string IP, string login, CustomerClass Cust, LogType logType, string action, string notes)
		{
			CustLogEntry(remote_host, user_agent, IP, login, Cust.CustContactId, logType, action, notes);
		} 

		public static void CustLogEntry(string remote_host, string user_agent, string IP, string login, int cust_contact_id,
			LogType logType, string action, string notes)
		{
			DBAccess.executeSQL("usp_usr_trk_cust_contact_log_entry", remote_host, user_agent, IP, login, cust_contact_id, (int)logType, action, notes);
		}


		#endregion



		public static UserClass GetUserByEmail(string email)
		{
			UserClass user = new UserClass();
			DataSet ds = new DataSet();
			string LogMsg = String.Empty;
			try
			{
				DBAccess.fillDS(ds, TableDefs.User.tbl_usr, "usp_usr_get_by_email", email);
				if (DBAccess.GetOnlyRow(ds, TableDefs.User.tbl_usr) != null)
				{
					DataRow drUser = DBAccess.GetOnlyRow(ds, TableDefs.User.tbl_usr);
					user.SetData((int)drUser["usr_id"], (string)drUser["usr_name"], Utility.ToBool(drUser["is_active"], false), Utility.ToBool(drUser["reset_pswd"], false));
				}
				else
				{
					LogMsg = "User not found";
				}
			}
			catch (Exception)
			{

				throw;
			}
			return user;

		}

		public static UserClass ChangePasword(string UserId, string userPswd)
		{
			UserClass user = new UserClass();
			DataSet ds = new DataSet();
			string LogMsg = String.Empty;
			try
			{
				DBAccess.executeSQL("usp_usr_pswd_update", UserId, Utility.Encrypt(userPswd), false/*reset_pswd*/);

				DBAccess.fillDS(ds, TableDefs.User.tbl_usr, "usp_usr_get", UserId);
				if (DBAccess.GetOnlyRow(ds, TableDefs.User.tbl_usr) != null)
				{
					DataRow drUser = DBAccess.GetOnlyRow(ds, TableDefs.User.tbl_usr);
					user.SetData((int)drUser["usr_id"], (string)drUser["usr_name"], Utility.ToBool(drUser["is_active"], false), Utility.ToBool(drUser["reset_pswd"], false));
				}
				else
				{
					LogMsg = "User not found";
				}
			}
			catch (Exception)
			{

				throw;
			}
			return user;

		}
		#region Customer Email
		public static CustomerClass GetCustByEmail(string email)
		{
			CustomerClass user = new CustomerClass();
			DataSet ds = new DataSet();
			string LogMsg = String.Empty;
			try
			{
				DataRow GetCustEmail = TableDefs.Customer.GetCustomerContactByEmail(ds, email);
				//DBAccess.fillDS(ds, TableDefs.Customer.tbl_usr, "usp_usr_get_by_email", email);
				if (DBAccess.GetOnlyRow(ds, TableDefs.Customer.tbl_cust_contact) != null)
				{
					DataRow drUser = DBAccess.GetOnlyRow(ds, TableDefs.Customer.tbl_cust_contact);
					user.SetData((int)drUser["cust_contact_id"],"" ,(int)drUser["cust_id"], Utility.ToBool(drUser["is_active"], false), Utility.ToBool(drUser["reset_pswd"], false));
				}
				else
				{
					LogMsg = "Customer not found";
				}
			}
			catch (Exception)
			{

				throw;
			}
			return user;

		}
		#endregion

		#region Customer password update
		public static CustomerClass ChangeCustPasword(string CustContactId, string CustPswd )
		{
			CustomerClass user = new CustomerClass();
			DataSet ds = new DataSet();
			string LogMsg = String.Empty;
			try
			{
				DBAccess.executeSQL("usp_cust_contact_pswd_update", CustContactId, Utility.Encrypt(CustPswd), false);

				DBAccess.fillDS(ds, TableDefs.Customer.tbl_cust_contact, "usp_cust_contact_get", CustContactId);
				if (DBAccess.GetOnlyRow(ds, TableDefs.Customer.tbl_cust_contact) != null)
				{
					DataRow drUser = DBAccess.GetOnlyRow(ds, TableDefs.Customer.tbl_cust_contact);
					user.SetData((int)drUser["cust_contact_id"], "", (int)drUser["cust_id"], Utility.ToBool(drUser["is_active"], false), Utility.ToBool(drUser["reset_pswd"], false));
				}
				else
				{
					LogMsg = "User not found";
				}
			}
			catch (Exception)
			{

				throw;
			}
			return user;

		}
		#endregion
	}

    public class UserClass
	{
		public int UserID { get; set; }
		public string UserName { get; set; }
		public bool IsActive { get; set; }
		public bool Authorized { get; set; }
		public bool IsResetPwd { get; set; }
		public string ErrorMsg { get; set; }
		public bool is_cust_login { get; set; }
		public UserClass()
		{
		}

		public void SetData(int userID, string userName, bool isActive, bool IsResetPwd)
		{
			UserID = userID;
			UserName = userName;
			IsActive = isActive;
			this.IsResetPwd = IsResetPwd;
			is_cust_login = false;

		}

		/// <summary>
		/// Used to log someone out
		/// </summary>
		public void Reset()
		{
			UserID = 0;
			UserName = "";
			IsActive = false;
			Authorized = false;
			IsResetPwd = false;
		}

		public bool IsAuthorized(Rights CheckRight)
		{
			if (CheckRight == Rights.None)
			{
				return true;
			}
			return (bool)DBAccess.executeScalar("usp_usr_chk_right", UserID, (int)CheckRight);
		}
	}

	

	#region

	public class CustomerClass
	{
		public int cust_id { get; set; }
		public int CustContactId { get; set; }
		public string CustContactName { get; set; }
		public string CustomerName { get; set; }
		public bool IsActive { get; set; }
		public bool Authorized { get; set; }
		public bool IsResetCustPwd { get; set; }
		public string ErrorMsg { get; set; }
		public bool is_cust_login { get; set; }
		public CustomerClass()
		{
		}

		public void SetData(int custContactId, string custContactName,int CustomerId, bool isActive, bool IsResetPwd)
		{
			CustContactId = custContactId;
			CustContactName = custContactName;
			cust_id = CustomerId;
			IsActive = isActive;
			this.IsResetCustPwd = IsResetPwd;
			is_cust_login = true;
		}

	}

	#endregion

}
