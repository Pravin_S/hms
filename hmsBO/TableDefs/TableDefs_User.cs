﻿using System.Data;

namespace hmsBO
{

	public static partial class TableDefs
	{
		public static class User
		{
			public enum Tables
			{
				tbl_group,          //group
				tbl_usr,            //user
				tbl_usr_grid        //Grid column order
			}

			public static TableClass tbl_group = new TableClass("tbl_group", "goup_id");
			public static TableClass tbl_usr = new TableClass("tbl_usr", "usr_id");
			public static TableClass tbl_usr_grid = new TableClass("tbl_usr", "usr_id,grid_id");

			//BioMed Group Dropdown
			public static DataView GetGroupsForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_group, "usp_group_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_group.TableName]);
				dv.Sort = "group_id";
				return dv;
			}
			public static DataView GetGroups(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_group, "usp_group_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_group.TableName]);
				dv.Sort = "group_name";
				return dv;
			}

			public static DataRow GetGroup(DataSet ds, int group_id)
			{
				DBAccess.fillDS(ds, tbl_group, "usp_group_get", group_id);
				return DBAccess.GetOnlyRow(ds, tbl_group);
			}
			//BioMed Users dropdown
			public static DataView GetUsersForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_usr, "usp_usr_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_usr.TableName]);
				dv.Sort = "usr_name";
				return dv;
			}
			public static DataView GetUsers(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_usr, "usp_usr_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_usr.TableName]);
				dv.Sort = "usr_name";
				return dv;
			}
			public static DataRow GetUser(DataSet ds, int usr_id)
			{
				DBAccess.fillDS(ds, tbl_usr, "usp_usr_get", usr_id);
				return DBAccess.GetOnlyRow(ds, tbl_usr);
			}

			public static DataRow GetUserGrid(DataSet ds, int usr_id, int grid_id)
			{
				DBAccess.fillDS(ds, tbl_usr_grid, "usp_usr_grid_col_get", usr_id, grid_id);
				return DBAccess.GetOnlyRow(ds, tbl_usr_grid);
			}



		}
	}
}