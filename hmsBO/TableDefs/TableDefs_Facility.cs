﻿using System.Data;

namespace hmsBO
{

	public static partial class TableDefs
	{
		public static class Facility
		{
			/// <summary>
			/// Summary for Facility Table,Facility Status
			/// </summary>

			#region Enum tables for Facility Table,facility status
			public enum Tables
			{
				tbl_facility,           //facility
				tbl_facility_stat       //facility status
			}

			public static TableClass tbl_facility = new TableClass("tbl_facility", "facility_id");
			public static TableClass tbl_facility_stat = new TableClass("tbl_facility_stat", "facility_stat_id");
			#endregion

			#region get all facilities
			public static DataView Getfacilities(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_facility, "usp_facility_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_facility.TableName]);
				dv.Sort = "facility_name";
				return dv;
			}
			#endregion

			// contract Facilities Dropdown
			public static DataView GetFacilityByCustomerForDropDown(DataSet ds, int cust_id, bool addAll, string allText, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_facility, "usp_facility_by_customer_dropdown_get_all", cust_id, addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_facility.TableName]);
				dv.Sort = "facility_name";
				return dv;
			}

			// FacilitiesExport
			public static DataView GetFacilitiesForExport(DataSet ds, string customerIds)
			{
				DBAccess.fillDS(ds, tbl_facility, "usp_facility_export", customerIds);
				DataView dv = new DataView(ds.Tables[tbl_facility.TableName]);
				dv.Sort = "facility_name";
				return dv;
			}

			//Edit facility
			public static DataRow GetFacility(DataSet ds, int facility_id)
			{
				DBAccess.fillDS(ds, tbl_facility, "usp_facility_get", facility_id);
				return DBAccess.GetOnlyRow(ds, tbl_facility);
			}

			//Facility status dropdown
			public static DataView GetFacilityStatusForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_facility_stat, "usp_facility_stat_dropdown_get_all", addAll, allText);
				DataView dv = new DataView(ds.Tables[tbl_facility_stat.TableName]);
				dv.Sort = "facility_stat_desc";
				return dv;
			}
			public static DataView GetFacilityByCustomer(DataSet ds, int cust_id, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_facility, "usp_facility_get_by_cust_id", cust_id, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_facility.TableName]);
				dv.Sort = "facility_name";
				return dv;
			}
		}
	}
}