﻿using System.Data;

namespace hmsBO
{

	public static partial class TableDefs
	{
		public static class Customer
		{
			public enum Tables
			{
				tbl_cust,           //customer
				tbl_cust_contact,   //customer contact
				tbl_cust_service,   //customer service 
				tbl_cust_stat       //customer status
			}
			public static TableClass tbl_cust = new TableClass("tbl_cust", "cust_id");
			public static TableClass tbl_cust_contact = new TableClass("tbl_cust_contact", "cust_contact_id");
			public static TableClass tbl_cust_service = new TableClass("tbl_cust_service", "cust_id", "cust_service_tp_id");
			public static TableClass tbl_cust_stat = new TableClass("tbl_cust_stat", "cust_stat_id");

			public static DataRow GetCustomer(DataSet ds, int cust_id)
			{
				DBAccess.fillDS(ds, tbl_cust, "usp_cust_get", cust_id);
				return DBAccess.GetOnlyRow(ds, tbl_cust);
			}
            public static DataView GetCustomersForDropDowns(DataSet ds)
            {
                return GetCustomersForDropDown(ds, true, "* All Customers *", true);
            }
            public static DataView GetCustomersForDropDown(DataSet ds, bool addAll, string allText, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_cust, "usp_cust_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_cust.TableName]);
				dv.Sort = "cust_name";
				return dv;
			}
			public static DataView GetCustomers(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_cust, "usp_cust_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_cust.TableName]);
				dv.Sort = "cust_name";
				return dv;
			}
			public static DataView GetCustomersForExport(DataSet ds, string customerIds)
			{
				DBAccess.fillDS(ds, tbl_cust, "usp_cust_export", customerIds);
				DataView dv = new DataView(ds.Tables[tbl_cust.TableName]);
				dv.Sort = "cust_name";
				return dv;
			}
			public static DataView GetCustomerContacts(DataSet ds, int cust_id, bool isactive)
			{
				DBAccess.fillDS(ds, tbl_cust_contact, "usp_cust_contact_get_all", cust_id, isactive);
				DataView dv = new DataView(ds.Tables[tbl_cust_contact.TableName]);
				dv.Sort = "first_name";
				return dv;
			}
			public static DataRow GetCustomerContact(DataSet ds, int cust_contact_id)
			{
				DBAccess.fillDS(ds, tbl_cust_contact, "usp_cust_contact_get", cust_contact_id);
				return DBAccess.GetOnlyRow(ds, tbl_cust_contact);
			}

			public static DataView GetCustomerStatusForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_cust_stat, "usp_cust_stat_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_cust_stat.TableName]);
				dv.Sort = "cust_stat_desc";
				return dv;
			}

			public static DataView GetCustomerService(DataSet ds, int cust_id)
			{
				DBAccess.fillDS(ds, tbl_cust_service, "usp_cust_service_get_all", cust_id);
				DataView dv = new DataView(ds.Tables[tbl_cust_service.TableName]);
				return dv;
			}
			public static DataRow GetCustomerContactByEmail(DataSet ds, string email)
			{
				DBAccess.fillDS(ds, tbl_cust_contact, "usp_cust_contact_get_by_email", email);
				return DBAccess.GetOnlyRow(ds, tbl_cust_contact);
			}
			public static DataRow GetCustomerContactLogin(DataSet ds, string email)
			{
				DBAccess.fillDS(ds, tbl_cust_contact, "usp_cust_contact_login", email);
				return DBAccess.GetOnlyRow(ds, tbl_cust_contact);
			}

		}
	}
}