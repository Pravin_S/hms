﻿using System.Data;

namespace hmsBO
{
	public static partial class TableDefs
	{
		public static class Vendor
		{
			public enum Tables
			{
				tbl_vendor,       //tbl_vendor
				tbl_vendor_contact
			}

			public static TableClass tbl_vendor = new TableClass("tbl_vendor", "vendor_id");
			public static TableClass tbl_vendor_contact = new TableClass("tbl_vendor_contact", "vendor_contact_id");

			//Get  Inactive or Active Vendor List
			public static DataView GetVendorsList(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_vendor, "usp_vendor_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_vendor.TableName]);
				dv.Sort = "vendor_name";
				return dv;
			}

			//Get  Facilities
			public static DataRow GetVendors(DataSet ds, int vendor_id)
			{
				DBAccess.fillDS(ds, tbl_vendor, "usp_vendor_get", vendor_id);
				return DBAccess.GetOnlyRow(ds, tbl_vendor);
			}

			public static DataView GetVendorContacts(DataSet ds, int vendor_id, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_vendor_contact, "usp_vendor_contact_get_all", vendor_id, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_vendor_contact.TableName]);
				dv.Sort = "first_name";
				return dv;
			}
			public static DataRow GetVendorContact(DataSet ds, int vendor_contact_id)
			{
				DBAccess.fillDS(ds, tbl_vendor_contact, "usp_vendor_contact_get", vendor_contact_id);
				return DBAccess.GetOnlyRow(ds, tbl_vendor_contact);
			}
			// Vendor Export
			public static DataView GetVnedorForExport(DataSet ds, string vendorIds)
			{
				DBAccess.fillDS(ds, tbl_vendor, "usp_vendor_export", vendorIds);
				DataView dv = new DataView(ds.Tables[tbl_vendor.TableName]);
				dv.Sort = "vendor_name";
				return dv;
			}

		}
	}
}