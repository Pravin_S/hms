﻿using System.Data;

namespace hmsBO
{

	public static partial class TableDefs
	{
		public static class Inventory
		{
			public enum Tables
			{
				tbl_inv_alarm_tp,       //invnetory alarm type
				tbl_inv,                //inventory
				tbl_inv_pm_interval_tp, //inventory pm interval type
				tbl_inv_war_stat,       //inventory warranty status
				tbl_mfg,               //manufacturer
				tbl_inv_ownership_tp,   //Inventory OwnerShip DropDown
				tbl_inv_concern_tp,   //Concern Cocncern DropDown
				tbl_inv_cover_tp,   // Item Coverage DropDown
				tbl_vendor,           // Vendor DropDown
				tbl_inv_bat_change_tp,   // BatteryChange Interval dropdown
				tbl_prod_class,         //product class
				tbl_prod_sub_class,      //product sub class
				tbl_inv_stat,        // Status DropDown
				tbl_inv_service_cover_tp,  // Service Coverage DropDown
				tbl_dept					// Department DropDown
			}

			public static TableClass tbl_inv_alarm_tp = new TableClass("tbl_inv_alarm_tp", "inv_alarm_tp_id");
			public static TableClass tbl_inv = new TableClass("tbl_inv", "inv_id");
			public static TableClass tbl_inv_pm_interval_tp = new TableClass("tbl_inv_pm_interval_tp", "inv_pm_interval_tp_id");
			public static TableClass tbl_inv_war_stat = new TableClass("tbl_inv_war_stat", "inv_war_stat_id");
			public static TableClass tbl_mfg = new TableClass("tbl_mfg", "mfg_id"); public static TableClass tbl_inv_ownership_tp = new TableClass("tbl_inv_ownership_tp", "inv_ownership_tp_id");
			public static TableClass tbl_inv_concern_tp = new TableClass("tbl_inv_concern_tp", "inv_concern_tp_id");
			public static TableClass tbl_inv_cover_tp = new TableClass("tbl_inv_cover_tp", "inv_cover_tp_id");
			public static TableClass tbl_vendor = new TableClass("tbl_vendor", "vendor_id");
			public static TableClass tbl_inv_bat_change_tp = new TableClass("tbl_inv_bat_change_tp", "inv_bat_change_tp_id");
			public static TableClass tbl_prod_class = new TableClass("tbl_prod_class", "prod_class_id");
			public static TableClass tbl_prod_sub_class = new TableClass("tbl_prod_sub_class", "prod_sub_class_id");
			public static TableClass tbl_inv_stat = new TableClass("tbl_inv_stat", "inv_stat_id");
			public static TableClass tbl_inv_service_cover_tp = new TableClass("tbl_inv_service_cover_tp", "inv_service_cover_tp_id");
			public static TableClass tbl_dept = new TableClass("tbl_dept", "dept_id");
			public static DataView GetAlarmTypesForDropDown(DataSet ds)
			{
				return GetAlarmTypesForDropDown(ds, false, true, "* All AlarmTypes *");
			}
			public static DataView GetAlarmTypesForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_alarm_tp, "usp_inv_alarm_tp_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_inv_alarm_tp.TableName]);
				dv.Sort = "inv_alarm_tp_desc";
				return dv;
			}

			public static DataView GetInventorysForDropDown(DataSet ds)
			{
				return GetInventorysForDropDown(ds, false, true, "* All Inventorys *");
			}
			public static DataView GetInventorysForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv, "usp_inv_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_inv.TableName]);
				dv.Sort = "serial_no";
				return dv;
			}
			public static DataView GetInventoryAll(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_inv, "usp_inv_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_inv.TableName]);
				dv.Sort = "serial_no";
				return dv;
			}
			public static DataView GetInventoryByFacility(DataSet ds, int facility_id, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_inv, "usp_inv_get_by_facility", facility_id, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_inv.TableName]);
				dv.Sort = "serial_no";
				return dv;
			}
			public static DataView GetInventoryByCustomerFacility(DataSet ds, int cust_id, int facility_id, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_inv, "usp_inv_get_by_cust_by_facility", cust_id, facility_id, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_inv.TableName]);
				dv.Sort = "serial_no";
				return dv;
			}
			public static DataRow GetInventory(DataSet ds, int inv_id)
			{
				DBAccess.fillDS(ds, tbl_inv, "usp_inv_get", inv_id);
				return DBAccess.GetOnlyRow(ds, tbl_inv);
			}

			public static DataView GetManufacturersForDropDown(DataSet ds)
			{
				return GetManufacturersForDropDown(ds, false, true, "* All Manufacturers *");
			}
			public static DataView GetManufacturersForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_mfg, "usp_mfg_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_mfg.TableName]);
				dv.Sort = "mfg_name";
				return dv;
			}
			public static DataView GetManufacturers(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_mfg, "usp_mfg_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_mfg.TableName]);
				dv.Sort = "mfg_name";
				return dv;
			}
			public static DataRow GetManufacturer(DataSet ds, int mfg_id)
			{
				DBAccess.fillDS(ds, tbl_mfg, "usp_mfg_get", mfg_id);
				return DBAccess.GetOnlyRow(ds, tbl_mfg);
			}
			public static DataView GetPMInternalsForDropDown(DataSet ds)
			{
				return GetPMInternalsForDropDown(ds, false, true, "* All PMInternals *");
			}
			public static DataView GetPMInternalsForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_pm_interval_tp, "usp_inv_pm_interval_tp_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_inv_pm_interval_tp.TableName]);
				dv.Sort = "inv_pm_interval_tp_desc";
				return dv;
			}

			public static DataView GetWarrantyStatussForDropDown(DataSet ds)
			{
				return GetWarrantyStatussForDropDown(ds, false, true, "* All WarrantyStatuss *");
			}
			public static DataView GetWarrantyStatussForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_war_stat, "usp_inv_war_stat_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_inv_war_stat.TableName]);
				dv.Sort = "inv_war_stat_desc";
				return dv;
			}
			//Inventory Ownership DropDown

			public static DataView GetInventoryOwnerShipForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_ownership_tp, "usp_inv_ownership_tp_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_inv_ownership_tp.TableName]);
				dv.Sort = "inv_ownership_tp_desc";
				return dv;
			}
			// inventory Interval PM List DropDown
			public static DataView GetInventoryIntervalPMForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_pm_interval_tp, "usp_inv_pm_interval_tp_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_inv_pm_interval_tp.TableName]);
				dv.Sort = "inv_pm_interval_tp_desc";
				return dv;
			}
			// Inventory Alarm DropDown
			public static DataView GetInventoryAlarmForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_alarm_tp, "usp_inv_alarm_tp_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_inv_alarm_tp.TableName]);
				dv.Sort = "inv_alarm_tp_desc";
				return dv;
			}
			// Concerns Drop down
			public static DataView GetInventoryConcernForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_concern_tp, "usp_inv_concern_tp_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_inv_concern_tp.TableName]);
				dv.Sort = "inv_concern_tp_desc ";
				return dv;
			}
			// item Coverage DropDown
			public static DataView GetInventoryItemCoverageForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_cover_tp, "usp_inv_cover_tp_dropdown_get_all ", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_inv_cover_tp.TableName]);
				dv.Sort = "inv_cover_tp_desc ";
				return dv;
			}
			// Vendor DropDown
			public static DataView GetInventoryProviderForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_vendor, "usp_vendor_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_vendor.TableName]);
				dv.Sort = "vendor_name ";
				return dv;
			}
			// Battery Interval Drop Down 
			public static DataView GetInventoryBatteryIntervalForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_bat_change_tp, "usp_inv_bat_change_tp_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_inv_bat_change_tp.TableName]);
				dv.Sort = "inv_bat_change_tp_desc";
				return dv;
			}
			// Manufacture List DropDown
			public static DataView GetInventoryManufactureForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_mfg, "usp_mfg_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_mfg.TableName]);
				dv.Sort = "mfg_name";
				return dv;
			}
			public static DataRow GetProductClass(DataSet ds, int prod_clas_id)
			{
				DBAccess.fillDS(ds, tbl_prod_class, "usp_prod_class_get", prod_clas_id);
				return DBAccess.GetOnlyRow(ds, tbl_prod_class);
			}
			public static DataView GetProductClassForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_prod_class, "usp_prod_class_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_prod_class.TableName]);
				dv.Sort = "prod_class_desc";
				return dv;
			}
			public static DataRow GetProductSubClass(DataSet ds, int prod_sub_class_id)
			{
				DBAccess.fillDS(ds, tbl_prod_sub_class, "usp_prod_sub_class_get", prod_sub_class_id);
				return DBAccess.GetOnlyRow(ds, tbl_prod_sub_class);
			}
			public static DataView GetProductSubClassByRootClass(DataSet ds, int root_class_id, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_prod_sub_class, "usp_prod_sub_class_dropdown_get_all", root_class_id, addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_prod_sub_class.TableName]);
				dv.Sort = "prod_sub_class_desc";
				return dv;
			}

			public static DataView GetInventoryStatusForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_inv_stat, "usp_inv_stat_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_inv_stat.TableName]);
				dv.Sort = "inv_stat_id";
				return dv;
			}
			//Service Coverage DropDown
			public static DataView GetInventoryServiceCoverageForDropDown(DataSet ds, bool addAll, string allText, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_inv_service_cover_tp, "usp_inv_service_cover_tp_dropdown_get_all", 0, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_inv_service_cover_tp.TableName]);
				dv.Sort = "inv_service_cover_tp_id";
				return dv;
			}
			// ManuFacturer Export
			public static DataView GetManufacturersForExport(DataSet ds, string manufacturerIds)
			{
				DBAccess.fillDS(ds, tbl_mfg, "usp_mfg_export", manufacturerIds);
				DataView dv = new DataView(ds.Tables[tbl_mfg.TableName]);
				dv.Sort = "mfg_name";
				return dv;
			}
			// Inventory Export
			public static DataView GetInventoryForExport(DataSet ds, string inventoryIds)
			{
				DBAccess.fillDS(ds, tbl_inv, "usp_inv_export", inventoryIds);
				DataView dv = new DataView(ds.Tables[tbl_inv.TableName]);
				dv.Sort = "serial_no";
				return dv;
			}
			// Department Drop Down
			public static DataView GetInventoryDepartmentForDropDown(DataSet ds, bool addAll, string allText, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_dept, "usp_dept_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_dept.TableName]);
				dv.Sort = "dept_id";
				return dv;
			}

		}
	}
}