﻿using System.Data;

namespace hmsBO
{

    public static partial class TableDefs
    {
        /// <summary>
        /// Summary for Contract Table,Contract Type
        /// </summary>
        /// 
             #region Summary for Contract Table,Contract Type
        public static class Contracts
        {
            public enum Tables
            {
                tbl_contract,           //contracts
                tbl_contract_tp
            }
            public static TableClass tbl_contract = new TableClass("tbl_contract", "contract_id");
            public static TableClass tbl_contract_tp = new TableClass("tbl_contract_tp_id", "Id");
            #endregion

            //Edit the Contracts Page
            public static DataRow GetContracts(DataSet ds, int contract_id)
            {
                DBAccess.fillDS(ds, tbl_contract, "usp_contract_get", contract_id);
                return DBAccess.GetOnlyRow(ds, tbl_contract);
            }

            //Get the All Contract Page
            public static DataView GetAllContracts(DataSet ds, bool includeInactives)
            {
                DBAccess.fillDS(ds, tbl_contract, "usp_contract_get_all", includeInactives);
                DataView dv = new DataView(ds.Tables[tbl_contract.TableName]);
                dv.Sort = "contract_id";
                return dv;
            }

            //Get All Contracts by Customer, Facility
            public static DataView GetAllContractByCustFacility(DataSet ds, int CustID, int FacilityID, bool includeInactives)
            {
                DBAccess.fillDS(ds, tbl_contract, "usp_contract_by_cust_facility_get_all", CustID, FacilityID, includeInactives);
                DataView dv = new DataView(ds.Tables[tbl_contract.TableName]);
                dv.Sort = "contract_id";
                return dv;
            }

            // Get Contracts Type Dropdown
            public static DataView GetContractTypeForDropDown(DataSet ds, bool addAll, string allText)
            {
                DBAccess.fillDS(ds, tbl_contract_tp, "usp_contract_tp_dropdown_get_all", addAll, allText);
                DataView dv = new DataView(ds.Tables[tbl_contract_tp.TableName]);
                dv.Sort = "contract_tp_desc";
                return dv;
            }
            
            public static DataView GetContractsForExport(DataSet ds, string facility_id)
            {
                try
                {
                    DBAccess.fillDS(ds, tbl_contract, "usp_contract_export", facility_id);
                    DataView dv = new DataView(ds.Tables[tbl_contract.TableName]);
                    dv.Sort = "facility_name";
                    return dv;
                }
                catch (System.Exception w)
                {

                    throw;
                }
               
            }
            public static DataView GetContractInventory(DataSet ds, int contract_id)
            {
                DBAccess.fillDS(ds, tbl_contract, "usp_contract_inv_get_all", contract_id);
                DataView dv = new DataView(ds.Tables[tbl_contract.TableName]);
                dv.Sort = "contract_inv_id";
                return dv;
            }

        }
    }
}