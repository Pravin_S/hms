﻿using System.Data;

namespace hmsBO
{
	public static partial class TableDefs
	{
		public static class VendorContact
		{
			public enum Tables
			{
				tbl_vendorcontact       //tbl_vendorcontact
			}

			public static TableClass tbl_vendorcontact = new TableClass("tbl_vendorcontact", "vendor_contact_id");

			//Get  Inactive or Active Vendor List
			public static DataView GetVendorContactsList(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_vendorcontact, "usp_vendor_contact_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_vendorcontact.TableName]);
				dv.Sort = "first_name";
				return dv;
			}

			//Get  Facilities
			public static DataRow GetVendorContacts(DataSet ds, int vendor_contact_id)
			{
				DBAccess.fillDS(ds, tbl_vendorcontact, "usp_vendor_contact_get", vendor_contact_id);
				return DBAccess.GetOnlyRow(ds, tbl_vendorcontact);
			}
		}
	}
}