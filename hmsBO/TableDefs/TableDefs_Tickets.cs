﻿using System.Data;
using System.Data.Common;

namespace hmsBO
{
	public static partial class TableDefs
	{
		public static class Ticket
		{
			public enum Tables
			{
				tbl_test,
				tbl_test_results,
				tbl_ticket,
				tbl_ticket_addmethods,
				tbl_ticket_audit,
				tbl_ticket_charge,
				tbl_ticket_check_tp,
				tbl_ticket_log,
				tbl_ticket_priority,
				tbl_ticket_notify,

				tbl_ticket_tp,
				tbl_ticket_stat,
				tbl_ticket_log_tp
			}

			public static TableClass tbl_test = new TableClass("tbl_test", "test_id");
			public static TableClass tbl_test_results = new TableClass("tbl_test_results", "test_results_id");
			public static TableClass tbl_ticket = new TableClass("tbl_ticket", "ticket_id");
			public static TableClass tbl_ticket_addmethods = new TableClass("tbl_tickets_addmethods", "ticket_addmethod_id");
			public static TableClass tbl_ticket_audit = new TableClass("tbl_ticket_audit", "ticket_audit_id");
			public static TableClass tbl_ticket_charge = new TableClass("tbl_ticket_check_tp", "ticket_check_tp_id");
			public static TableClass tbl_ticket_charge_tp = new TableClass("tbl_ticket_charge_tp", "charge_tp_id");
			public static TableClass tbl_ticket_check_tp = new TableClass("tbl_ticket_check_tp", "ticket_check_tp_id");
			public static TableClass tbl_ticket_log = new TableClass("tbl_ticket_log", "ticket_log_id");
			public static TableClass tbl_ticket_priority = new TableClass("tbl_ticket_priority", "ticket_priority_stat_id");
			public static TableClass tbl_ticket_notify = new TableClass("tbl_ticket_notify", "notify_id");
			public static TableClass tbl_ticket_tp = new TableClass("tbl_ticket_tp", "ticket_tp_id");
			public static TableClass tbl_ticket_stat = new TableClass("tbl_ticket_stat", "ticket_stat_id");
			public static TableClass tbl_ticket_log_tp = new TableClass("tbl_ticket_log_tp", "ticket_log_tp_id");


			public static DataView GetTicketAudit(DataSet ds, int ticket_id)
			{
				DBAccess.fillDS(ds, tbl_ticket_audit, "usp_ticket_audit_by_ticket", ticket_id);
				DataView dv = new DataView(ds.Tables[tbl_ticket_audit.TableName]);
				dv.Sort = "ticket_audit_id";
				return dv;
			}

			public static DataView GetTicketChargeTypesForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_ticket_charge_tp, "usp_ticket_charge_tp_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_ticket_charge_tp.TableName]);
				dv.Sort = "charge_tp_desc";
				return dv;
			}

			public static DataView GetTickets(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_ticket, "usp_ticket_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_ticket.TableName]);
				dv.Sort = "ticket_id";
				return dv;
			}

			public static DataView GetTicketsByCustomer(DataSet ds, int cust_id, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_ticket, "usp_ticket_get_by_cust", cust_id, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_ticket.TableName]);
				dv.Sort = "ticket_id";
				return dv;
			}

			public static DataView GetTicketsByCustomerByFacility(DataSet ds, int cust_id, int facility_id, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_ticket, "usp_ticket_get_by_cust_by_facility", cust_id, facility_id, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_ticket.TableName]);
				dv.Sort = "ticket_id";
				return dv;
			}
			public static DataView GetTicketsByFacility(DataSet ds, int facility_id, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_ticket, "usp_ticket_get_by_facility", facility_id, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_ticket.TableName]);
				dv.Sort = "ticket_id";
				return dv;
			}

			public static DataView GetTicketsByUser(DataSet ds, int facility_id, int usr_id, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_ticket, "usp_ticket_get_by_usr", facility_id, usr_id, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_ticket.TableName]);
				dv.Sort = "ticket_id";
				return dv;
			}

			public static DataRow GetTicketLog(DataSet ds, int ticket_log_id)
			{
				DBAccess.fillDS(ds, tbl_ticket_log, "usp_ticket_log_get", ticket_log_id);
				return DBAccess.GetOnlyRow(ds, tbl_ticket_log);
			}

			public static DataView GetTicketNotify(DataSet ds, int ticket_id)
			{
				DBAccess.fillDS(ds, tbl_ticket_notify, "usp_ticket_notify_get_all", ticket_id);
				DataView dv = new DataView(ds.Tables[tbl_ticket_notify.TableName]);
				dv.Sort = "notify_email";
				return dv;
			}

			public static DataRow GetTicketReport(DataSet ds, int ticket_id)
			{
				DBAccess.fillDS(ds, tbl_ticket, "usp_ticket_rpt", ticket_id);
				return DBAccess.GetOnlyRow(ds, tbl_ticket);
			}

			public static DataView GetTicketReportCharges(DataSet ds, int ticket_id)
			{
				DBAccess.fillDS(ds, tbl_ticket_charge, "usp_ticket_charge_by_ticket", ticket_id);
				DataView dv = new DataView(ds.Tables[tbl_ticket_charge.TableName]);
				dv.Sort = "ticket_charge_id";
				return dv;
			}

			public static DataView GetTicketReportLog(DataSet ds, int ticket_id)
			{
				DBAccess.fillDS(ds, tbl_ticket_log, "usp_ticket_log_by_ticket", ticket_id);
				DataView dv = new DataView(ds.Tables[tbl_ticket_log.TableName]);
				dv.Sort = "ticket_log_id";
				return dv;
			}

			public static DataView GetTicketReportTestResults(DataSet ds, int ticket_id)
			{
				DBAccess.fillDS(ds, tbl_test_results, "usp_ticket_test_results_by_ticket", ticket_id);
				DataView dv = new DataView(ds.Tables[tbl_test_results.TableName]);
				dv.Sort = "test_results_id";
				return dv;
			}

			public static DataView GetTicketReportTestResultsHistory(DataSet ds, int ticket_id)
			{
				DBAccess.fillDS(ds, tbl_test_results, "usp_ticket_test_results_history", ticket_id);
				DataView dv = new DataView(ds.Tables[tbl_test_results.TableName]);
				dv.Sort = "test_results_id desc";
				return dv;
			}

			public static DataRow GetTicketsByID(DataSet ds, int ticket_id)
			{
				DBAccess.fillDS(ds, tbl_ticket, "usp_ticket_get", ticket_id);
				return DBAccess.GetOnlyRow(ds, tbl_ticket);
			}

			public static DataView GetTicketAddMethod(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_ticket_addmethods, "usp_ticket_addmethod_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_ticket_addmethods.TableName]);
				dv.Sort = "ticket_addmethod_desc";
				return dv;
			}
			public static DataView GetTicketChargesByInventoryID(DataSet ds, int inv_id, string beginDate)
			{
				DBAccess.fillDS(ds, tbl_ticket_charge, "usp_ticket_charge_by_inv_id", inv_id, beginDate);
				DataView dv = new DataView(ds.Tables[tbl_ticket_charge.TableName]);
				dv.Sort = "service_dt desc";
				return dv;
			}
			public static DataView GetTicketType(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_ticket_tp, "usp_ticket_tp_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_ticket_tp.TableName]);
				dv.Sort = "ticket_tp_desc";
				return dv;
			}
			public static DataView GetPriority(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_ticket_priority, "usp_ticket_priority_stat_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_ticket_priority.TableName]);
				dv.Sort = "ticket_priority_stat_desc";
				return dv;
			}
			public static DataView GetTicketStatus(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_ticket_stat, "usp_ticket_stat_dropdown_get_all", addAll, allText, 1);
				DataView dv = new DataView(ds.Tables[tbl_ticket_stat.TableName]);
				dv.Sort = "ticket_stat_desc";
				return dv;
			}
			public static DataView GetLogType(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_ticket_log_tp, "usp_ticket_log_tp_dropdown_get_all", addAll, allText);
				DataView dv = new DataView(ds.Tables[tbl_ticket_log_tp.TableName]);
				dv.Sort = "ticket_log_tp_desc";
				return dv;
			}
			public static DataView GetTicketChargesExport(DataSet ds, string ticketchargeids)
			{
				DBAccess.fillDS(ds, tbl_ticket_charge, "usp_ticket_charge_export", ticketchargeids);
				DataView dv = new DataView(ds.Tables[tbl_ticket_charge.TableName]);
				dv.Sort = "ticket_charge_id";
				return dv;
			}

			public static DataView GetTicketTestTypesForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_test, "usp_ticket_test_tp_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_test.TableName]);
				dv.Sort = "test_type";
				return dv;
			}
			public static DataRow GetTest(DataSet ds, int test_id)
			{
				DBAccess.fillDS(ds, tbl_test, "usp_ticket_test_get", test_id);
				return DBAccess.GetOnlyRow(ds, tbl_test);
			}
		}
	}

}