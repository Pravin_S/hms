﻿using System.Data;

namespace hmsBO
{

	public static partial class TableDefs
	{

		public static class General
		{
			public enum Tables
			{
				tbl_app_settings,       //application settings
				tbl_contact_tp,         //Contact Type
				tbl_file_attach,        //File attachments
				tbl_managed_entity_tp,  //Managed entities
				tbl_service_tp,         //customer service types
				tbl_state               //States
			}

			public static TableClass tbl_app_settings = new TableClass("tbl_app_settings", "id");
			public static TableClass tbl_contact_tp = new TableClass("tbl_contact_tp", "contact_tp_id");
			public static TableClass tbl_file_attach = new TableClass("tbl_file_attach", "file_attach_id");
			public static TableClass tbl_managed_entity_tp = new TableClass("tbl_managed_entity_tp", "managed_entity_tp_id");
			public static TableClass tbl_service_tp = new TableClass("tbl_service_tp", "service_tp_id");
			public static TableClass tbl_state = new TableClass("tbl_state", "abbr");

			public static DataRow GetAppSettings(DataSet ds)
			{
				DBAccess.fillDS(ds, tbl_app_settings, "usp_app_settings_get");
				return DBAccess.GetOnlyRow(ds, tbl_app_settings);
			}
			public static DataView GetContactTypesForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_contact_tp, "usp_contact_tp_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_contact_tp.TableName]);
				dv.Sort = "contact_tp_desc";
				return dv;
			}
			public static DataView GetFileAttachments(DataSet ds, int record_id, int file_attach_tp_id)
			{
				DBAccess.fillDS(ds, tbl_file_attach, "usp_file_attach_get_all", record_id, file_attach_tp_id);
				DataView dv = new DataView(ds.Tables[tbl_file_attach.TableName]);
				dv.Sort = "file_name";
				return dv;
			}
			public static DataView GetManagedEntityTypesForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_managed_entity_tp, "usp_managed_entity_tp_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_managed_entity_tp.TableName]);
				dv.Sort = "managed_entity_tp_desc";
				return dv;
			}
			public static DataView GetStatesForDropDown(DataSet ds, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_state, "usp_state_dropdown_get_all", addAll, allText);
				DataView dv = new DataView(ds.Tables[tbl_state.TableName]);
				dv.Sort = "st";
				return dv;
			}
			public static DataView GetServiceTypes(DataSet ds, bool includeInactives)
			{
				DBAccess.fillDS(ds, tbl_service_tp, "usp_service_tp_get_all", includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_service_tp.TableName]);
				dv.Sort = "service_tp_desc";
				return dv;
			}
			public static DataView GetServiceTypesForDropDown(DataSet ds, bool includeInactives, bool addAll, string allText)
			{
				DBAccess.fillDS(ds, tbl_service_tp, "usp_service_tp_dropdown_get_all", addAll, allText, includeInactives);
				DataView dv = new DataView(ds.Tables[tbl_service_tp.TableName]);
				dv.Sort = "service_tp_desc";
				return dv;
			}
		}
	}
}