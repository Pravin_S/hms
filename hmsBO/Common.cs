﻿using System;
using System.Data;
using System.DirectoryServices.Protocols;
using System.Net;

public enum WebPages
{
    //these page name must match exactly to tbl_menu asp_page_name column
    NO_PAGE,
    customer_aspx,
   facilities_aspx,
   inventory_aspx,
   tickets_aspx,
   manufacturer_aspx,
   contracts_aspx
}
public enum Grids
{
    Customer = 10,
    Facility = 20
}
public enum FileAttachType
{
    Contract = 10,
    Inventory = 20,
    Ticket = 30
}